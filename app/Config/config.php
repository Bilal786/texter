<?php
/* 
Here we are define some configs variable for site uses.

***************** SITE SETTING *********************/
//$siteFolder               	= dirname(dirname(dirname($_SERVER['SCRIPT_NAME'])));  //SETTING FOR DEFAULT SERVER
$siteFolder               	= "";
$config['App.siteFolder'] = $siteFolder;

$config['App.SiteUrl'] 		= 'https://' . $_SERVER['HTTP_HOST'] . $siteFolder;
$site_url =  "https://" . $_SERVER['HTTP_HOST'] ."/". $siteFolder;

if(isset($_SERVER['HTTPS']))
{
    if ($_SERVER["HTTPS"] == "on") 
    {
		$config['App.SiteUrl'] 		= 'https://' . $_SERVER['HTTP_HOST'] . $siteFolder;
		$site_url =  "https://" . $_SERVER['HTTP_HOST'] . $siteFolder;
    }
}

define('SITE_URL', $site_url."/app/webroot");

define('SITE_URL_ABS', $site_url."");
//define('SITE_URL', $site_url);  //SETTING FOR DEFAULT S
$config['App.AdminMail']  		= "mapchatteam@gmail.com";
$config['App.SITENAME']  		= "txtter.com";
$config['App.AdminCCMail']  	= "rakeshgfh@mailinator.com";
//$config['App.AdminCCMail']  	= "admin@txtter.com";
$config['App.Session.Time']  	= 1000000000;

/*FOR PAGING*/
$config['App.PageLimit']  	= 20;
$config['App.AdminPageLimit'] = 20;

/*FOR GENERAL STATUS*/
$config['App.Status.inactive'] = 0;
$config['App.Status.active'] = 1;
$config['App.Status.delete'] = 2;

$config['App.MaxFileSize'] = 1048576;
$config['App.Admin.role'] =1;
$config['App.SubAdmin.role'] =3;
$config['App.User.role'] =2;

define('MOBILE_MEDIA', 'uploads/media');
$config['Status']     = array(1=>'Active', 0=>'Inactive');

$config['Privacy.Status']          	= array(1=>'Public',2=>'Private');
$config['User.Login.Search']          	= array('last_week'=>'Last week','last_6_month'=>' Last 6 Month','last_month'=>'Last month','last_year'=>'Last year');

$config['App.PerPage']     	= array(10=>10, 15=>15, 20=>20, 30=>30, 50=>50);
										
/***************** CONFIG VARIABLES FOR USER **************/
$config['App.Role.Admin']      	= 1;
$config['App.Role.User']     	= 2;
$config['App.Default.Userid']     	= 143;

$config['App.Sex']         	= array('0'=>'','1'=>'Male','2'=>'Female');
$config['App.FrontSex']         	= array(''=>'Gender','1'=>'Male','2'=>'Female');
$config['App.Card.Type'] = array('visa'=>'Visa','mastercard'=>'MasterCard','discover'=>'Discover','amex'=>'American Express');

// Api parameter for flight api

$config['App.Flight.App.Id']      	= '4ce9e1b1';
$config['App.Flight.App.Key']     	= '63c972162b903141c006b3c8f43afacc';


// For block user
$config['App.Block.User'] = 1;
$config['App.Unblock.User'] = 0;
// End

$config['App.Host'] = $_SERVER['HTTP_HOST'];
/* $config['App.SMS_API_KEY'] = '6cd24060';
$config['App.SMS_API_SECRET'] = "eaa97b10";
$config['App.SMS_FROM'] = "MyCompany20"; */

define('CURRENCY_IMAGE', "uploads/currency_image"); 

define('UPLOAD_FOLDER', "uploads");

define('PROFILE_PICS', "uploads/user");
define('PROFILE_EXTRA_PICS', "uploads/user_images");
define('PROFILE_COVER_PICS', "uploads/user_cover");

define('USER_DIR', "uploads/user"); 
define('USER_THUMB1_DIR', "uploads/user/thumb"); 
define('USER_TINY_DIR', "uploads/user/thumb"); 
define('USER_THUMB_DIR', "uploads/user/thumb");
define('USER_LARGE_DIR', "uploads/user/large"); 
define('USER_ORIGINAL_DIR', "uploads/user/original");

define('USER_COVER_DIR', "uploads/user_cover"); 
define('USER_COVER_THUMB1_DIR', "uploads/user_cover/thumb"); 
define('USER_COVER_TINY_DIR', "uploads/user_cover/thumb"); 
define('USER_COVER_THUMB_DIR', "uploads/user_cover/thumb");
define('USER_COVER_LARGE_DIR', "uploads/user_cover/large"); 
define('USER_COVER_ORIGINAL_DIR', "uploads/user_cover/original");

define('USER_DATING_PIC_DIR', "uploads/dating_pic");

define('Interest_Category_DIR', "uploads/interest"); 

define('DEFAULT_USER_IMG', "no_image.jpeg"); 
define('DEFAULT_USER_IMAGE', "/".USER_DIR."/no_image.png");
 
define('USER_TINY_WIDTH', '50');
define('USER_TINY_HEIGHT', '50');
 
define('USER_THUMB_WIDTH', '100');
define('USER_THUMB_HEIGHT', '100');

define('USER_THUMB_WIDTH1', '150');
define('USER_THUMB_HEIGHT1', '150');

define('USER_LARGE_WIDTH', '250');
define('USER_LARGE_HEIGHT', '250');

/* define('XMPP_HOSTNAME', '115.112.57.155');
define('XMPP_PORT', '5222');
define('XMPP_USERNAME', 'admin');
define('XMPP_PASSWORD', 'Business#12');
define('XMPP_SERVER', '115.112.57.155');
define('GROUP_CHAT_SERVICE', 'conference');
 */
 
$config['App.open_fire_host'] = "chat.txtter.com";
$config['App.OfSecretKey'] = "3AdxWfwj";

define('XMPP_HOSTNAME', 'chat.txtter.com');
define('XMPP_PORT', '5222');
define('XMPP_USERNAME', 'admin');
define('XMPP_PASSWORD', 'admin');
define('XMPP_SERVER', 'chat.txtter.com');
define('GROUP_CHAT_SERVICE', 'conference');
