<?php
/**
 * This is core configuration file.
 *
 * Use it to configure core behaviour of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * In this file you set up your database connection details.
 *
 * @package       cake.config
 */
/**
 * Database configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * datasource => The name of a supported datasource; valid options are as follows:
 *		Database/Mysql 		- MySQL 4 & 5,
 *		Database/Sqlite		- SQLite (PHP5 only),
 *		Database/Postgres	- PostgreSQL 7 and higher,
 *		Database/Sqlserver	- Microsoft SQL Server 2005 and higher
 *
 * You can add custom database datasources (or override existing datasources) by adding the
 * appropriate file to app/Model/Datasource/Database.  Datasources should be named 'MyDatasource.php',
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use 'port' => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database.  This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres specifies which schema you would like to use the tables in. Postgres defaults to 'public'.
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 */

/*
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'mysql-server-1.cwa69xewditk.ap-southeast-1.rds.amazonaws.com',
		'login' => 'developers',
		'password' => 'P@ssw0rd2o15#year',
		'database' => 'db_txtter',
		'prefix' => '',
		'encoding' => 'utf8', 

		

		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'mysqltxtterserver.c8ezemdemcoy.us-west-2.rds.amazonaws.com',
		'login' => 'db-txtter-octal',
		'password' => '7cKv[JfJvcKH#as1',
		'database' => 'db_txtter_octal',
		'prefix' => '',
		'encoding' => 'utf8',
	

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'mysql-server-1.cwa69xewditk.ap-southeast-1.rds.amazonaws.com',
		'login' => 'developers',
		'password' => 'P@ssw0rd2o15#year',
		'database' => 'db_txtter',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $chat_db = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'mysql-server-1.cwa69xewditk.ap-southeast-1.rds.amazonaws.com',
		'login' => 'developers',
		'password' => 'P@ssw0rd2o15#year',
		'database' => 'db_txtter_openfire',
		'prefix' => '',
		'encoding' => 'utf8',
	);*/


	/*
	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'dev-applications-rds.co0vummzoqwg.us-east-1.rds.amazonaws.com',
		'login' => 'dev_txtter',
		'password' => 'gGjD2C7KdCsuEWQ8',
		'database' => 'dev_txtter',
		'prefix' => '',
		'encoding' => 'utf8',
	); 

	public $chat_db = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'dev-applications-rds.co0vummzoqwg.us-east-1.rds.amazonaws.com',
		'login' => 'dev_txtter',
		'password' => 'gGjD2C7KdCsuEWQ8',
		'database' => 'dev_txtter_openfire',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	*/


class DATABASE_CONFIG {


	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'mysqlmapbuzzserver.cppm6fle9ghe.us-west-2.rds.amazonaws.com',
		'login' => 'root',
		'password' => '7cKv[JfJvcKH#as1',
		'database' => 'db_txtter_20170103',
		'prefix' => '',
		'encoding' => 'utf8mb4'
	); 

	public $chat_db = array(

		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'mysqlmapbuzzserver.cppm6fle9ghe.us-west-2.rds.amazonaws.com',
		'login' => 'root',
		'password' => '7cKv[JfJvcKH#as1',
		'database' => 'openfire_20170103',
		'prefix' => '',
		'encoding' => 'utf8mb4'
	);
 
}
