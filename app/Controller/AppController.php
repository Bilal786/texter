<?php
/**
 * Application controller
 *
 * This file is the base controller of all other controllers
 *
 * PHP version 5
 *
 * @category Controllers
 * @version  1.0
 */
App::uses('CakeEmail', 'Network/Email');

class AppController extends Controller {
    /**
     * Components
     *
     * @var array
     * @access public
     */
	 
    var $preResData   = array();
	

    var $components = array(
        'Auth',
        'Session',
        'Security',
        'Upload',
        'Cookie',
        'RequestHandler',
        'Email',
    );    
	
	/**
     * Helpers
     *
     * @var array
     * @access public
     */
    var $helpers = array(
        'Html',
        'Form',
        'Session',
        'Text',
        'Js' => array('Jquery'),
        'Layout',
        'Time',
        'ExPaginator',
        'Admin',
        'General'
    );

    /**
     * Models
     *
     * @var array
     * @access public
     */
    var $uses = array();



    /**
     * beforeFilter
     *
     * @return void
     */
    public function beforeFilter() {
	
        /* Define array for element */
        $this->Security->blackHoleCallback = '__securityError';
        $this->disableCache();
        $this->loadModel('Setting');
		self::get_all_settings();

        if (isset($this->request->params['admin'])){
            $this->layout = 'admin'; 			
            $this->Auth->userModel = 'User';
            $this->Auth->authenticate = array('Form' => array('scope' => array('OR'=>array('User.role_id' => array(Configure::read('App.Admin.role'), Configure::read('App.SubAdmin.role')), 'User.is_admin'=>1),'AND'=>array('User.status' => Configure::read('App.Status.active')))));
			
            $this->Auth->loginError = "Login failed. Invalid username or password";

            $this->Auth->loginAction = array('admin' => true, 'controller' => 'admins', 'action' => 'login');
            $this->Auth->loginRedirect = array('admin' => true, 'controller' => 'admins', 'action' => 'dashboard');
            $this->Auth->authError = 'You must login to view this information.';
            $this->Auth->autoRedirect = true;
            $this->Auth->allow('admin_login');
			$admin_data = self::get_admin_user_data();
			$this->set(compact('admin_data'));
        }else{
			
        }
        if ($this->RequestHandler->isAjax()){
            $this->layout = 'ajax';
            $this->autoRender = false;
            $this->Security->validatePost = false;
            Configure::write('debug', 2);
        }
		
		if (!empty($this->request->data))
		{
            array_walk_recursive($this->request->data,create_function('&$value, &$key', '$value = trim($value);') );
			return true;
			
        }
		
		
    }
	
	
	public function get_all_settings()
	{
		$this->loadModel('Setting');
		$settings=$this->Setting->find('all',array('fields'=>array('Setting.value')));
		Configure::write('Site.title',$settings[0]['Setting']['value']);
		Configure::write('App.SiteName',$settings[0]['Setting']['value']);
		Configure::write('App.AdminMail',$settings[1]['Setting']['value']);
		Configure::write('App.SMS_API_KEY',$settings[3]['Setting']['value']);
		Configure::write('App.SMS_API_SECRET',$settings[4]['Setting']['value']);
		Configure::write('App.SMS_FROM',$settings[5]['Setting']['value']);
	}
		
		
    /**
     * isAuthorized
     *
     * @return void
     */

    function isAuthorized() {
        if (isset($this->params['admin'])){
            if ($this->Auth->user()) {
                    $this->Auth->allow('admin_login', 'admin_logout', 'admin_dashboard', 'admin_edit', 'admin_play', 'admin_play_video');
            }
        } else {
            return true;
        }
    }


    /**
     * blackHoleCallback for SecurityComponent
     *
     * @return void
     */
    public function __securityError() {
        
    }
	function __copy_directory( $source, $destination )
	{
		if ( is_dir( $source ) )
		{
			@mkdir( $destination );
			$directory = dir( $source );
			while ( FALSE !== ( $readdirectory = $directory->read() ) )
			{
				if ( $readdirectory == '.' || $readdirectory == '..' )
				{
					continue;
				}
				$PathDir = $source . '/' . $readdirectory;
				if ( is_dir( $PathDir ) )
				{
					$this->__copy_directory( $PathDir, $destination . '/' . $readdirectory );
					continue;
				}
				copy( $PathDir, $destination . '/' . $readdirectory );
			}
			$directory->close();
		}
		else
		{
			copy( $source, $destination );
		}
	}
	
    public function beforeRender(){
        //$this->_configureErrorLayout();
		$this->response->disableCache();
        if ($this->Auth->user()) {
            if ($this->Auth->user('role_id') == 2) {
                $this->loadModel('User');
                $this->User->id = $this->Auth->user('id');
                $timestamp = time();
                $this->User->saveField('last_activity', $timestamp);
                //$this->displaySqlDump();die;
            }
        }
        if ($this->name == 'CakeError') {
            //$this->layout = 'error';
        }
		
    }

    /**
     * sendMail
     *
     * @return	void
     * @access	private
     */
	
	public function sendMaile($to, $subject, $message, $from, $template_id = null ){
        if(($_SERVER['HTTP_HOST'] == '192.168.1.71')||($_SERVER['HTTP_HOST'] == 'localhost')){			
			$this->EmailService->to = $to;
			$this->EmailService->from = $from;
			$this->EmailService->subject = $subject;
			$this->EmailService->sendAs = 'HTML';
			$this->EmailService->delivery = 'aws_ses';
			die;
      
        return false;
    }
	}

	 
	 
     public function sendMail($to, $subject, $message, $from, $template_id = null ){
        $email = new CakeEmail('aws');
		$cc = Configure::read("App.AdminCCMail");		
		//$cc = "keka@mailinator.com";
        $email->template('default', 'default')
                ->emailFormat('html')
                ->from($from)
                ->to($to)
				->cc($cc)
                ->subject($subject);
        if ($email->send($message))
            return true;
        return false;
    }

    public function rrmdir($dir) {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file))
                $this->rrmdir($file);
            else
                unlink($file);
        }

        rmdir($dir);
    }

    public function displaySqlDump(){
		if (!class_exists('ConnectionManager') || Configure::read('debug') < 2) {
			return false;
		}
		$noLogs = !isset($logs);
		if ($noLogs):
			$sources = ConnectionManager::sourceList();
			$logs = array();
			foreach ($sources as $source):
				$db =& ConnectionManager::getDataSource($source);
				echo "<pre>";
				$log = $db->getLog(false, false);
				debug($log);
				die;
				if (!$db->isInterfaceSupported('getLog')):
					continue;
				endif;
				$logs[$source] = $db->getLog();
			endforeach;
		endif;

		if ($noLogs || isset($_forced_from_dbo_)):
			foreach ($logs as $source => $logInfo):
				$text = $logInfo['count'] > 1 ? 'queries' : 'query';
				printf(
					'<table class="cake-sql-log" id="cakeSqlLog_%s" summary="Cake SQL Log" cellspacing="0" border = "0">',
					preg_replace('/[^A-Za-z0-9_]/', '_', uniqid(time(), true))
				);
				printf('<caption>(%s) %s %s took %s ms</caption>', $source, $logInfo['count'], $text, $logInfo['time']);
			?>
			<thead>
				<tr><th>Nr</th><th>Query</th><th>Error</th><th>Affected</th><th>Num. rows</th><th>Took (ms)</th></tr>
			</thead>
			<tbody>
			<?php
				foreach ($logInfo['log'] as $k => $i) :
					echo "<tr><td>" . ($k + 1) . "</td><td>" . h($i['query']) . "</td><td>{$i['error']}</td><td style = \"text-align: right\">{$i['affected']}</td><td style = \"text-align: right\">{$i['numRows']}</td><td style = \"text-align: right\">{$i['took']}</td></tr>\n";
				endforeach;
			?>
			</tbody></table>
			<?php 
			endforeach;
		else:
			echo '<p>Encountered unexpected $logs cannot generate SQL log</p>';
		endif;	
	}

	
	
	function get_admin_user_data()
	{
		$user_id = $this->Session->read('Auth.User.id');
		$this->loadModel('User');
		$admin_data = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id,'OR'=>array('User.role_id'=>array(Configure::read('App.Admin.role'),Configure::read('App.SubAdmin.role'))))));
		
		return $admin_data;
	}
	
	function getURLContent($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_URL, $url);
		$contents = curl_exec($ch);
		curl_close($ch);
		return $contents;
	}
}
