<?php

/**
 * Messages Controller
 *
 * PHP version 5.4
 *
 */
class MessagesController extends AppController{

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Messages';
    public $components = array(
        'General', 'Upload'
		//, 'Twitter'
    );
	
    public $helpers = array('General','Autosearch','Js');
    public $uses = array('Message');

    /*
     * beforeFilter
     * @return void
     */

    function beforeRender() {
        $model = Inflector::singularize($this->name);
        foreach ($this->{$model}->hasAndBelongsToMany as $k => $v) {
            if (isset($this->{$model}->validationErrors[$k])) {
                $this->{$model}->{$k}->validationErrors[$k] = $this->{$model}->validationErrors[$k];
            }
        }
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('Message');
		$this->Auth->allow('start_access', 'reset_password', 'thankx', 'reset_password_change','login', 'register', 'activate', 'success', 'fbconnect', 'forgot_password','get_password', 'password_changed', 'linked_connect', 'save_linkedin_data', 'tw_connect', 'tw_response', 'glogin', 'save_google_info','social_login', 'tlogin', 'save_cover_photo', 'getTwitterData', 'fb_data', 'fb_logout', 'social_join_mail');
    }

    /*
     * List all messages in admin panel
     */

    public function admin_index($defaultTab = 'All'){
		$number_of_record = Configure::read('App.AdminPageLimit');
        $filters = array();
		
		$this->Message->bindModel(array(
					'belongsTo'=>array(
						'Sender'=>array(
							'className'=>'User',
							'foreignKey'=>'sender_id',
							'fields'=>array('id', 'username', 'phone_number'),
							'dependent'=>true
						),
						'Receiver'=>array(
							'className'=>'User',
							'foreignKey'=>'receiver_id',
							'fields'=>array('id', 'username', 'phone_number'),
							'dependent'=>true
						)
					)
				
				),false);
		
        $filters_without_status = array();
        if (!isset($this->request->params['named']['page'])) {
            $this->Session->delete('AdminSearch');
            $this->Session->delete('Url');
        }
        if ($defaultTab != 'All'){
            $filters[] = array('Message.status' => array_search($defaultTab, Configure::read('Status')));
        }

        if (!empty($this->request->data)){
		
            $this->Session->delete('AdminSearch');
            $this->Session->delete('Url');

            App::uses('Sanitize', 'Utility');
			if (!empty($this->request->data['Number']['number_of_record'])) {
				$number_of_record = Sanitize::escape($this->request->data['Number']['number_of_record']);
				$this->Session->write('number_of_record', $number_of_record);
			}
			
            if (!empty($this->request->data['Message']['message'])) {
                $message = Sanitize::escape($this->request->data['Message']['message']);
                $this->Session->write('AdminSearch.message', $message);
            }
			
            if (!empty($this->request->data['Message']['phone_number'])){
                $phone_number = Sanitize::escape($this->request->data['Message']['phone_number']);
                $this->Session->write('AdminSearch.phone_number', $phone_number);
            }
			
            if (!empty($this->request->data['Message']['username'])) {
                $username = Sanitize::escape($this->request->data['Message']['username']);
                $this->Session->write('AdminSearch.username', $username);
            }
			
			if (isset($this->request->data['Message']['status']) && $this->request->data['Message']['status'] != ''){
                $status = Sanitize::escape($this->request->data['Message']['status']);
                $this->Session->write('AdminSearch.status', $status);
                $defaultTab = Configure::read('Status.' . $status);
            }
        }

		if ($this->Session->check('number_of_record')) {
				$number_of_record = $this->Session->read('number_of_record');
				$this->request->data['Number']['number_of_record'] = $number_of_record;
		}
        $search_flag = 0;
        $search_status = '';
        if ($this->Session->check('AdminSearch')) {
            $keywords = $this->Session->read('AdminSearch');

            foreach ($keywords as $key => $values) {
                if ($key == 'status') {
                    $search_status = $values;
                    $filters[] = array('Message.' . $key => $values);
                }
                if ($key == 'message') {
                    $filters[] = array('Message.' . $key. ' LIKE' => "%" . $values . "%");
                }
                if ($key == 'phone_number') {
                    $filters[] = array('OR'=>array('Sender.phone_number LIKE' => "%" . $values . "%", 'Receiver.phone_number LIKE' => "%" . $values . "%"));
                }
                if ($key == 'username') {
                    $filters[] = array('OR'=>array('Sender.username LIKE' => "%" . $values . "%", 'Receiver.username LIKE' => "%" . $values . "%"));
                }
            }
            $search_flag = 1;
        }
        $this->set(compact('search_flag', 'defaultTab'));

        $this->paginate = array(
            'Message' => array(
                'limit' => $number_of_record,
                'order' => array('Message.id' => 'DESC'),
                'group' => array('Message.conversation_id'),
                'conditions' => $filters
			)
        );

        $data = $this->paginate('Message');
        $this->set(compact('data'));
        $this->set('title_for_layout', __('Messages', true));
				
        if ((isset($this->request->data['Message']['result_as'])) && ($this->request->data['Message']['result_as'] == 'file')) {		
		$message_data = $this->Message->find('all', array(		
                'order' => array('Message.id' => 'DESC'),
                'conditions' => $filters
		));
		
        $delimiter = "\t";
        $filename = "Txtter-Chat-" . date('d-M-Y-h-i-s') . ".xls";
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");

        header("Pragma: no-cache");
        header("Expires: 0");
        print "sender username \t sender phone \t  Reciever username \t Reciever phone \t Message \t Created";
        print "\r\n";
		
		foreach($message_data as $messageKey=>$messageData){
			print  $messageData['Sender']['username']. "\t" . $messageData['Sender']['phone_number']. "\t" .$messageData['Receiver']['username']. "\t" . $messageData['Receiver']['phone_number'] . "\t" . $messageData['Message']['message'] . "\t " .$messageData['Message']['created'];
            print "\r\n";
		}
		die;
        }

        if (isset($this->request->params['named']['page']))
            $this->Session->write('Url.page', $this->request->params['named']['page']);
        if (isset($this->request->params['named']['sort']))
            $this->Session->write('Url.sort', $this->request->params['named']['sort']);
        if (isset($this->request->params['named']['direction']))
            $this->Session->write('Url.direction', $this->request->params['named']['direction']);
        $this->Session->write('Url.type', '');
        $this->Session->write('Url.defaultTab', $defaultTab);

        if ($this->request->is('ajax')) {
            $this->render('ajax/admin_index');
        } else {
            $active = 0;
            $inactive = 0;
            if ($search_status == '' || $search_status == Configure::read('App.Status.active')) {
                $temp = $filters_without_status;
                $temp[] = array('Message.status' => 1);
                $active = $this->Message->find('count', array('conditions' => $temp));
            }
            if ($search_status == '' || $search_status == Configure::read('App.Status.inactive')){
                $temp = $filters_without_status;
                $temp[] = array('Message.status' => 0);
                $inactive = $this->Message->find('count', array('conditions' => $temp));
            }

            $tabs = array('All' => $active + $inactive, 'Active' => $active, 'Inactive' => $inactive);
            $this->set(compact('tabs'));
        }
    }

    /*
     * List all messages in admin panel
     */

    public function admin_conversations($conversation_id = null, $defaultTab = 'All'){
		$number_of_record = Configure::read('App.AdminPageLimit');
        $filters = array("Message.conversation_id"=>$conversation_id);
		
		$this->Message->bindModel(array(
					'belongsTo'=>array(
						'Sender'=>array(
							'className'=>'User',
							'foreignKey'=>'sender_id',
							'fields'=>array('id', 'username', 'phone_number'),
							'dependent'=>true
						),
						'Receiver'=>array(
							'className'=>'User',
							'foreignKey'=>'receiver_id',
							'fields'=>array('id', 'username', 'phone_number'),
							'dependent'=>true
						)
					)
				
				),false);
		
        $filters_without_status = array();
        if (!isset($this->request->params['named']['page'])) {
            $this->Session->delete('AdminSearch');
            $this->Session->delete('Url');
        }
        if ($defaultTab != 'All'){
            $filters[] = array('Message.status' => array_search($defaultTab, Configure::read('Status')));
        }

        if (!empty($this->request->data)){
		
            $this->Session->delete('AdminSearch');
            $this->Session->delete('Url');

            App::uses('Sanitize', 'Utility');
			if (!empty($this->request->data['Number']['number_of_record'])) {
				$number_of_record = Sanitize::escape($this->request->data['Number']['number_of_record']);
				$this->Session->write('number_of_record', $number_of_record);
			}
        }

		if ($this->Session->check('number_of_record')) {
				$number_of_record = $this->Session->read('number_of_record');
				$this->request->data['Number']['number_of_record'] = $number_of_record;
		}
        $search_flag = 0;
        $search_status = '';
		$this->set(compact('search_flag', 'defaultTab'));

        $this->paginate = array(
            'Message' => array(
                'limit' => $number_of_record,
                'order' => array('Message.id' => 'ASC'),
                'conditions' => $filters
			)
        );

        $data = $this->paginate('Message');
		$sender = $data[0]['Sender']['username'];
		$receiver = $data[0]['Receiver']['username'];
        $this->set(compact('data'));
        $this->set('title_for_layout', __("Messages between $sender and $receiver", true));


        if (isset($this->request->params['named']['page']))
            $this->Session->write('Url.page', $this->request->params['named']['page']);
        if (isset($this->request->params['named']['sort']))
            $this->Session->write('Url.sort', $this->request->params['named']['sort']);
        if (isset($this->request->params['named']['direction']))
            $this->Session->write('Url.direction', $this->request->params['named']['direction']);
        $this->Session->write('Url.type', '');
        $this->Session->write('Url.defaultTab', $defaultTab);

        if ($this->request->is('ajax')) {
            $this->render('ajax/admin_conversations');
        } else {
            $active = 0;
            $inactive = 0;
            if ($search_status == '' || $search_status == Configure::read('App.Status.active')) {
                $temp = $filters_without_status;
                $temp[] = array('Message.status' => 1);
                $active = $this->Message->find('count', array('conditions' => $temp));
            }
            if ($search_status == '' || $search_status == Configure::read('App.Status.inactive')){
                $temp = $filters_without_status;
                $temp[] = array('Message.status' => 0);
                $inactive = $this->Message->find('count', array('conditions' => $temp));
            }

            $tabs = array('All' => $active + $inactive, 'Active' => $active, 'Inactive' => $inactive);
            $this->set(compact('tabs'));
        }
    }

    public function admin_view($id = null) {
        $this->Message->id = $id;
        if (!$this->Message->exists()) {
            throw new NotFoundException(__('Invalid message'));
        }

        $this->Message->recursive = 3;
        $data = $this->Message->read(null, $id);
        $this->set('message', $data);
    }

    /**
     * delete existing message
     */
    public function admin_delete($id = null){
        $message_id = $this->Message->id = $id;

        if (!$this->Message->exists()){
            throw new NotFoundException(__('Invalid message'));
        }
        if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
            $blackHoleCallback = $this->Security->blackHoleCallback;
            $this->$blackHoleCallback();
        }
		$message_data = $this->Message->find('first',array('conditions'=>array('Message.id'=>$id)));
		//die;
        if ($this->Message->deleteAll(array('Message.id'=>$id))) {
		$this->Session->setFlash(__('Message deleted successfully'), 'admin_flash_success');
            $this->redirect($this->referer());
        }
        $this->Session->setFlash(__('Message was not deleted', 'admin_flash_error'));
        $this->redirect($this->referer());
    }

    /**
     * delete existing message
     */
    public function admin_delete_conversation($conversation_id = null){
        if(!isset($conversation_id)){
			$this->Session->setFlash(__('Invalid message', 'admin_flash_error'));
			$this->redirect($this->referer());			
        }
		
        if ($this->Message->deleteAll(array('Message.conversation_id'=>$conversation_id))) {
		$this->Session->setFlash(__('Conversation deleted successfully'), 'admin_flash_success');
            $this->redirect($this->referer());
        }
        $this->Session->setFlash(__('Message was not deleted', 'admin_flash_error'));
        $this->redirect($this->referer());
    }

    public function admin_process() {

        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
            $blackHoleCallback = $this->Security->blackHoleCallback;
            $this->$blackHoleCallback();
        }

        if (!empty($this->request->data)) {
            App::uses('Sanitize', 'Utility');
            $action = Sanitize::escape($this->request->data['Message']['pageAction']);

            $ids = $this->request->data['Message']['id'];

            if (count($this->request->data) == 0 || $this->request->data['Message'] == null) {
                $this->Session->setFlash('No items selected.', 'admin_flash_error');
                $this->redirect($this->referer());
            }

            if ($action == "delete") {
				$this->Message->deleteAll(array('Message.id' => $ids)); 
                $this->Session->setFlash('Messages have been deleted successfully', 'admin_flash_success');
                $this->redirect($this->referer());
            }

            if ($action == "activate") {
                $this->Message->updateAll(array('Message.status' => Configure::read('App.Status.active')), array('Message.id' => $ids));
                $this->Session->setFlash('Messages have been activated successfully', 'admin_flash_success');
                $this->redirect($this->referer());
            }

            if ($action == "deactivate") {
                $this->Message->updateAll(array('Message.status' => Configure::read('App.Status.inactive')), array('Message.id' => $ids));
                $this->Session->setFlash('Messages have been deactivated successfully', 'admin_flash_success');
                $this->redirect($this->referer());
            }
        } else {
            $this->redirect(array('controller' => 'admins', 'action' => 'index'));
        }
    }
}

