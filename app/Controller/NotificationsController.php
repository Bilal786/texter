<?php

/**
 * Notifications Controller
 *
 * PHP version 5.4
 *
 */
class NotificationsController extends AppController
{

  /**
   * Controller name
   *
   * @var string
   * @access public
   */
  var $name = 'Notifications';

  public function beforeFilter()
  {
    parent::beforeFilter();
    $this->Auth->allow('login');
    $this->loadModel("AppSetting");
    $this->loadModel("AdminNotificationLog");
    $this->loadModel('User');
  }

  public function admin_index()
  {

    if ($this->request->is('post')) {

      foreach ($this->request->data as $k => $v) {
        if ($k == "Notification") {
          $msg = trim($v['message']);
        }
      }

      $type = isset($this->request->data['Notification']['user_type']) ? $this->request->data['Notification']['user_type'] : 0;

      if (isset($msg) && $msg != "") {

        $push_setting = $this->AppSetting->find('first', array('conditions' => array('AppSetting.id = 2')));
        if (!empty($push_setting)) {
          $status = isset($push_setting['AppSetting']['status']) ? $push_setting['AppSetting']['status'] : 0;
          if ($status == '1') {
            $push_setting['AppSetting']['status'] = 0;
            if ($this->AppSetting->save($push_setting)) {
              //track button click
              $button_track = array(
                'user_id' => $this->Auth->user('id'),
                'token' => 'no_token',
                'device_type' => '5',
                'message' => 'Button pressed',
                'date' => date("Y-m-d H:i:s", strtotime('now'))
              );
              $this->AdminNotificationLog->save($button_track);

              $msg = base64_encode($msg);
              $proc_command = "wget https://appservices.txtter.com/broadcast.php?t=$msg -q -O - -b";
              $proc = popen($proc_command, "r");
              pclose($proc);

              // $users = $this->User->find('list', array(
              //   'fields' => array('User.device_id'),
              //   'conditions' => array('User.device_id !=' => '', 'User.device_id !=' => 'Null'),
              //   'recursive' => 0
              // ));              

              // $push_setting['AppSetting']['status'] = 1;
              // $this->AppSetting->save($push_setting);
              
              // ini_set('max_execution_time', 0);

              // foreach ($users as $k => $v) {
              //   $token = trim($v);
              //   if (empty($token)) {
              //     continue;
              //   }

                
              //   if (strlen($token) >= 150) {
              //     $this->send_notification_to_users($token, $msg);
              //   } else if (strlen($token) >= 30 && strlen($token) < 100) {
              //     //$this->send_notification_for_iphone($token, $msg);
              //   } else {
              //   }
              // }

              $this->Session->setFlash(__('Notification has been sent successfully.', true), 'admin_flash_success');
            } else {
              $this->Session->setFlash(__('Notification sending failed please check with server admin.', true), 'admin_flash_error');
            }
          } else {
            $this->Session->setFlash(__('Notification loop already running, please try to send after sometime.', true), 'admin_flash_error');
          }
        } else {
          $this->Session->setFlash(__('Something went wrong please check with server admin.', true), 'admin_flash_error');
        }
      } else {
        $this->Session->setFlash(__('Message is required. Please, correct errors.', true), 'admin_flash_error');
      }
    }
  }

  function getLastQuery()
  {
    $dbo = ConnectionManager::getDataSource('default');
    $logs = $dbo->getLog();
    $lastLog = end($logs['log']);
    return $lastLog['query'];
  }

  function send_notification_for_iphone($deviceToken = null, $message = null, $type = 99, $objectId = 1, $iindObjectId = 1, $badge = 1)
  {

    if (strlen($deviceToken) < 40) {
      return false;
    }

    $message = array('aps' => array('alert' => $message, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'badge' => $badge));

    $passphrase = "qwer";
    //$passphrase = "123";
    $base_path = getcwd();

    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/CertificatesProdPush.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    stream_context_set_option($ctx, 'ssl', 'cafile', $base_path . '/entrust_2048_ca.cer');

    $fp = stream_socket_client(
      'ssl://gateway.push.apple.com:2195',
      $err,
      $errstr,
      60,
      STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT,
      $ctx
    );

    if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

    //echo 'Connected to APNS' . PHP_EOL;


    $message['aps']['sound'] = 'default';
    $message['aps']['content_available'] = 1;
    $body = json_encode($message);

    $message = $body;

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($message)) . $message;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));
    // Close the connection to the server
    print_r($result);
    //echo "<br>";
    fclose($fp);

    //echo "close";
  }

  function send_notification_to_users($deviceToken = null, $title = null, $type = 99, $objectId = 1, $iindObjectId = 1, $badge = 1)
  {
    $message = array('aps' => array('alert' => $title, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'badge' => $badge));
    $sound = 'default';

    $url = "https://fcm.googleapis.com/fcm/send";
    $serverKey = 'AAAA1hU4Sr0:APA91bGMiE06_3FPCqhYAOs8XPozornWTbTihv1uItLLY3Ip6d6tpxWRwYJsyd4UlhfPrk-apOXe35tMc9M3eu101szvz2Ip7eElFjYEit1LjoGV59Vgr5k7lFFA_5c1pYdARPvnve8v';

    $notification = array('title' => '', 'text' => $title, 'body' => $title, 'badge' => $badge, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'sound' => $sound);
    $arrayToSend = array('to' => $deviceToken, 'notification' => $notification, 'priority' => 'high');
    $json = json_encode($arrayToSend);

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=' . $serverKey;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //Send the request
    $response = @curl_exec($ch);
    //    if ($response === FALSE) {
    //      die('FCM Send Error: ' . curl_error($ch));
    //    }
    curl_close($ch);
    return $response;
  }
}
