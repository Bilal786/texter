<?php
/**
 * Application controller
 *
 * This file is the base controller of all other controllers
 *
 * PHP version 5
 *
 * @category Controllers
 * @version  1.0
 */

class PasswordresetController extends Controller {
    /**
     * Components
     *
     * @var array
     * @access public
     */

    var $preResData   = array();


    var $components = array(
        'Auth',
        'Session',
        'Security',
        'Upload',
        'Cookie',
        'RequestHandler',
        'Email',
    );

	/**
     * Helpers
     *
     * @var array
     * @access public
     */
    var $helpers = array(
        'Html',
        'Form',
        'Session',
        'Text',
        'Js' => array('Jquery'),
        'Layout',
        'Time',
        'ExPaginator',
        'Admin',
        'General'
    );

    /**
     * Models
     *
     * @var array
     * @access public
     */
    var $uses = array();



    /**
     * beforeFilter
     *
     * @return void
     */
    public function beforeFilter(){
        parent::beforeFilter();
        $this->layout = '';
        $this->autoRender = false;
        $this->Auth->allow('*');
    }

    public function index(){
        //$this->set('token', $_GET["token"]);

        $this->render('/passwordreset/reset');
    }

    public function success(){
        $this->render('/passwordreset/reset');
    }
}
