<?php

/**
 * RecentImages Controller
 *
 * PHP version 5.4
 *
 */
class RecentImagesController extends AppController {

  /**
   * Controller name
   *
   * @var string
   * @access public
   */
  public $name = 'RecentImages';
  public $components = array(
    'General', 'Upload'
  );
  public $helpers = array('General', 'Autosearch', 'Js');
  public $uses = array('User');

  /*
   * beforeFilter
   * @return void
   */

  function beforeRender() {
    $model = Inflector::singularize($this->name);
    foreach ($this->{$model}->hasAndBelongsToMany as $k => $v) {
      if (isset($this->{$model}->validationErrors[$k])) {
        $this->{$model}->{$k}->validationErrors[$k] = $this->{$model}->validationErrors[$k];
      }
    }
  }

  public function beforeFilter() {
    parent::beforeFilter();
    $this->loadModel('User');
    $this->loadModel('UserImage');
    //$this->Auth->allow('social_join_mail');
  }

  /*
   * List all recent users in admin panel
   */

  public function admin_index($defaultTab = 'All', $role = 'Admin') {
    $number_of_record = Configure::read('App.AdminPageLimit');

    if (!empty($this->request->data)) {

      App::uses('Sanitize', 'Utility');
      if (!empty($this->request->data['Number']['number_of_record'])) {
        $number_of_record = Sanitize::escape($this->request->data['Number']['number_of_record']);
        $this->Session->write('number_of_record', $number_of_record);
      }
      if ($this->Session->check('number_of_record')) {
        $number_of_record = $this->Session->read('number_of_record');
        $this->request->data['Number']['number_of_record'] = $number_of_record;
      }
    }

    $this->set(compact('defaultTab'));

    $this->UserImage->bindModel(array(
      'belongsTo' => array(
        'UserDetails' => array(
          'className' => 'User',
          'foreignKey' => 'user_id',
          'fields' => array('id', 'username', 'email', 'first_name', 'last_name')
        )
      )
        ), false);

    //$filters[] = array('OR' => array("User.role_id" => array(Configure::read('App.Admin.role'), Configure::read('App.SubAdmin.role'))));
    $filters = array('NOT' => array('UserImage.image' => null));
    $this->paginate = array(
      'UserImage' => array(
        'limit' => $number_of_record,
        'order' => array('UserImage.created' => 'DESC'),
        'conditions' => $filters
    ));

    $data = $this->paginate('UserImage');

    $this->set(compact('data', 'role'));
    $this->set('title_for_layout', __('Recent Images', true));


    if (isset($this->request->params['named']['page'])) {
      $this->Session->write('Url.page', $this->request->params['named']['page']);
    }
    $this->Session->write('Url.type', $role);
    $this->Session->write('Url.defaultTab', $defaultTab);

    if ($this->request->is('ajax')) {
      $this->render('ajax/admin_index');
    } else {
      //$temp = $filters_without_status;
      $temp = array('UserImage.status' => 1);
      $active = $this->UserImage->find('count', array('conditions' => $temp));

      $tabs = array('All' => $active);
      $this->set(compact('tabs'));
    }
  }

  public function admin_delete($id) {
    $data = $this->UserImage->find('first', array('conditions' => array('UserImage.id' => $id)));
    $userImageArr = array();
    if (!empty($data['UserImage'])) {
      foreach ($data as $key => $userImageVal) {        
        if($userImageVal['image_type'] == '1' || empty($userImageVal['image_type'])) {
          $userImageArr[] = SITE_URL . "/uploads/user_images/large/" . $userImageVal['image'];
          $userImageArr[] = SITE_URL . "/uploads/user_images/original/" . $userImageVal['image'];
          $userImageArr[] = SITE_URL . "/uploads/user_images/thumb/" . $userImageVal['image'];
          
          $userImageArr[] = SITE_URL . "/uploads/user/large/" . $userImageVal['image'];
          $userImageArr[] = SITE_URL . "/uploads/user/original/" . $userImageVal['image'];
          $userImageArr[] = SITE_URL . "/uploads/user/thumb/" . $userImageVal['image'];
        }
        if($userImageVal['image_type'] == '2') {
          $userImageArr[] = SITE_URL . "/uploads/user_cover/large/" . $userImageVal['image'];
          $userImageArr[] = SITE_URL . "/uploads/user_cover/original/" . $userImageVal['image'];
          $userImageArr[] = SITE_URL . "/uploads/user_cover/thumb/" . $userImageVal['image'];
        }
        if($userImageVal['image_type'] == '6') {
          $userImageArr[] = SITE_URL . "/uploads/dating_pic/" . $userImageVal['image'];
        }
      }
    }

    if($this->UserImage->delete($id)){
      if (!empty($userImageArr)) {
        $usrImgArr = array();
        foreach ($userImageArr as $imageKey => $imageVal) {
          @unlink($imageVal);
        }
      }
      $this->Session->setFlash(__('Image deleted successfully'), 'admin_flash_success');
      $this->redirect(Router::url( $this->referer(), true ));
    }
    $this->Session->setFlash(__('Image was not deleted', 'admin_flash_error'));
    $this->redirect(Router::url( $this->referer(), true ));
  }

}
