<?php

/**
 * Categories Controller
 *
 * PHP version 5.4
 *
 */
class SubscriptionPlansController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    var $name = 'SubscriptionPlans';
    public $helpers = array('General');

    /*
     * beforeFilter
     * @return void
     */

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('SubscriptionPlan');
    }

    /*
     * List all admin users in admin panel
     */

    public function admin_index($defaultTab = 'All') {
        
        if (!isset($this->request->params['named']['page'])) {
            $this->Session->delete('AdminSearch');
            $this->Session->delete('Url');
        }
        $filters = $filters_without_status = array();
         App::uses('Sanitize', 'Utility');
         $number_of_record = Configure::read('App.AdminPageLimit');
        if (!empty($this->request->data['Number']['number_of_record'])) {
            $number_of_record = Sanitize::escape($this->request->data['Number']['number_of_record']);
            $this->Session->write('number_of_record', $number_of_record);
        }
        if ($this->Session->check('number_of_record')) {
            $number_of_record = $this->Session->read('number_of_record');
            $this->request->data['Number']['number_of_record'] = $number_of_record;
        }
        $search_flag = 0;
        
        $this->set(compact('search_flag', 'defaultTab'));
        $this->paginate = array(
            'SubscriptionPlan' => array(
                'limit' => $number_of_record,
                'order' => array('SubscriptionPlan.id' => 'Asc'),
                'conditions' => $filters
            )
        );
        $data = $this->paginate('SubscriptionPlan');
        $this->set(compact('data'));
        $this->set('title_for_layout', __('Subscription Plans Management', true));
        if (isset($this->request->params['named']['page']))
            $this->Session->write('Url.page', $this->request->params['named']['page']);
        if (isset($this->request->params['named']['sort']))
            $this->Session->write('Url.sort', $this->request->params['named']['sort']);
        if (isset($this->request->params['named']['direction']))
            $this->Session->write('Url.direction', $this->request->params['named']['direction']);
        $this->Session->write('Url.defaultTab', $defaultTab);
        if ($this->request->is('ajax')) {
            $this->render('ajax/admin_index');
        } else {
            $tabs = array('All' => 'All');
            $this->set(compact('tabs'));
        }
    }

    /*
     * Add Music in admin panel
     */

    public function admin_add() {
         $this->set('title_for_layout', __('Add new Credit Plan', true));
       
        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                if (!isset($this->request->params['data']['_Token']['key']) || ($this->request->params['data']['_Token']['key'] != $this->request->params['_Token']['key'])) {
                    $blackHoleCallback = $this->Security->blackHoleCallback;
                    $this->$blackHoleCallback();
                }
                //validate  data
                $this->SubscriptionPlan->set($this->request->data);
                $this->SubscriptionPlan->setValidation('admin');
                if ($this->SubscriptionPlan->validates()) {
					if(empty($this->request->data['SubscriptionPlan']['credits'])){
						$this->request->data['SubscriptionPlan']['credits'] = 0;
				   }
				   
                   if(empty($this->request->data['SubscriptionPlan']['free_credits'])){
						$this->request->data['SubscriptionPlan']['free_credits'] = 0;
				   }
					$this->request->data['SubscriptionPlan']['total_credits'] = $this->request->data['SubscriptionPlan']['credits']+$this->request->data['SubscriptionPlan']['free_credits'];
					
                    if ($this->SubscriptionPlan->save($this->request->data)) {
                        $this->Session->setFlash(__('SubscriptionPlans has been added successfully'), 'admin_flash_success');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('The SubscriptionPlans file could not be added. Please, try again.'), 'admin_flash_error');
                    }
                } else {
                    $this->Session->setFlash('The SubscriptionPlan file could not be added.  Please, correct errors.', 'admin_flash_error');
                }
            }
        }
    }

    /**
     * edit existing Music file
     */
    public function admin_edit($id = null) {
        $this->set('title_for_layout', __('Edit Credit Plan', true));
        $this->SubscriptionPlan->id = $id;
        
        if (!$this->SubscriptionPlan->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data)) {
                if (!isset($this->request->params['data']['_Token']['key']) || ($this->request->params['data']['_Token']['key'] != $this->request->params['_Token']['key'])) {
                    $blackHoleCallback = $this->Security->blackHoleCallback;
                    $this->$blackHoleCallback();
                }
                //validate user data
                $this->SubscriptionPlan->set($this->request->data);
                $this->SubscriptionPlan->setValidation('admin');
                
                
                if ($this->SubscriptionPlan->validates()) {
				
                   if(empty($this->request->data['SubscriptionPlan']['credits'])){
						$this->request->data['SubscriptionPlan']['credits'] = 0;
				   }
				   
                   if(empty($this->request->data['SubscriptionPlan']['free_credits'])){
						$this->request->data['SubscriptionPlan']['free_credits'] = 0;
				   }
				   
					$this->request->data['SubscriptionPlan']['total_credits'] = $this->request->data['SubscriptionPlan']['credits']+$this->request->data['SubscriptionPlan']['free_credits'];	
                  
                    if ($this->SubscriptionPlan->save($this->request->data)) {
                        $this->Session->setFlash(__('The Subscription Plan has been updated successfully', true), 'admin_flash_success');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('The Subscription Plan could not be saved. Please, try again.', true), 'admin_flash_error');
                    }
                } else {
                          $this->Session->setFlash(__('The Subscription Plan could not be saved. Please, correct errors.', true), 'admin_flash_error');
                }
            }
        } else {
            $this->request->data = $this->SubscriptionPlan->read(null, $id);
        }
    }

    /**
     * delete existing music file
     */
    public function admin_delete($id = null) {
        $this->SubscriptionPlan->id = $id;
        if (!$this->SubscriptionPlan->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
            $blackHoleCallback = $this->Security->blackHoleCallback;
            $this->$blackHoleCallback();
        }
        $this->request->data = $this->SubscriptionPlan->read(null, $id);

        if ($this->SubscriptionPlan->deleteAll(array('SubscriptionPlan.id' => $id), true)) {
            
            $this->Session->setFlash(__('Subscription Plan  deleted successfully'), 'admin_flash_success');
            $this->redirect($this->referer());
        }
        $this->Session->setFlash(__('Subscription Plan  was not deleted', 'admin_flash_error'));
        $this->redirect($this->referer());
    }

    /**
     * toggle status existing music file
     */
    public function admin_status($id = null) {
        $this->SubscriptionPlan->id = $id;
        if (!$this->SubscriptionPlan->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
            $blackHoleCallback = $this->Security->blackHoleCallback;
            $this->$blackHoleCallback();
        }
        if ($this->SubscriptionPlan->toggleStatus($id)) {
            $this->Session->setFlash(__('Subscription Plan status has been changed'), 'admin_flash_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Subscription Plan status was not changed', 'admin_flash_error'));
        $this->redirect(array('action' => 'index'));
    }

    

}