<?php

/**
 * Users Controller
 *
 * PHP version 5.4
 *
 */
class UsersController extends AppController {

  /**
   * Controller name
   *
   * @var string
   * @access public
   */
  public $name = 'Users';
  public $components = array(
    'General', 'Upload'
      //, 'Twitter'
  );
  public $helpers = array('General', 'Autosearch', 'Js');
  public $uses = array('User');

  /*
   * beforeFilter
   * @return void
   */

  function beforeRender() {
    $model = Inflector::singularize($this->name);
    foreach ($this->{$model}->hasAndBelongsToMany as $k => $v) {
      if (isset($this->{$model}->validationErrors[$k])) {
        $this->{$model}->{$k}->validationErrors[$k] = $this->{$model}->validationErrors[$k];
      }
    }
  }

  public function beforeFilter() {
    parent::beforeFilter();
    $this->loadModel('User');
    $this->loadModel('UserTextPost');
    $this->loadModel('UserPostComment');
    $this->loadModel('UserPostLike');
    $this->loadModel('DeviceBans');
    $this->Auth->allow('start_access', 'reset_password', 'thankx', 'reset_password_change', 'login', 'register', 'activate', 'success', 'fbconnect', 'forgot_password', 'get_password', 'password_changed', 'linked_connect', 'save_linkedin_data', 'tw_connect', 'tw_response', 'glogin', 'save_google_info', 'social_login', 'tlogin', 'save_cover_photo', 'getTwitterData', 'fb_data', 'fb_logout', 'social_join_mail');
  }

  /*
   * List all users in admin panel
   */

  public function admin_index($defaultTab = 'All') {
    $befor_one_week_date = date("Y-m-d", strtotime("-1 week"));
    $last_month = date("m", strtotime("-1 month"));
    $last_6_month = date('Y-m-d', strtotime('today - 6 month'));
    $last_year = date("Y", strtotime("-1 year"));


    $number_of_record = Configure::read('App.AdminPageLimit');

    if (!isset($this->request->params['named']['page'])) {
      $this->Session->delete('AdminSearch');
      $this->Session->delete('Url');
    }
    $filters_without_status = $filters = array('User.role_id' => Configure::read('App.Role.User'));

    if ($defaultTab != 'All') {
      $filters[] = array('User.status' => array_search($defaultTab, Configure::read('Status')));
    }

    if (!empty($this->request->data)) {

      $this->Session->delete('AdminSearch');
      $this->Session->delete('Url');

      App::uses('Sanitize', 'Utility');
      if (!empty($this->request->data['Number']['number_of_record'])) {
        $number_of_record = Sanitize::escape($this->request->data['Number']['number_of_record']);
        $this->Session->write('number_of_record', $number_of_record);
      }

      if (!empty($this->request->data['User']['email'])) {
        $email = Sanitize::escape($this->request->data['User']['email']);
        $this->Session->write('AdminSearch.email', $email);
      }
      if (!empty($this->request->data['User']['phone_number'])) {
        $phone_number = Sanitize::escape($this->request->data['User']['phone_number']);
        $this->Session->write('AdminSearch.phone_number', $phone_number);
      }
      if (!empty($this->request->data['User']['username'])) {
        $username = Sanitize::escape($this->request->data['User']['username']);
        $this->Session->write('AdminSearch.username', $username);
      }
      if (isset($this->request->data['User']['status']) && $this->request->data['User']['status'] != '') {
        $status = Sanitize::escape($this->request->data['User']['status']);
        $this->Session->write('AdminSearch.status', $status);
        $defaultTab = Configure::read('Status.' . $status);
      }
    }

    if ($this->Session->check('number_of_record')) {
      $number_of_record = $this->Session->read('number_of_record');
      $this->request->data['Number']['number_of_record'] = $number_of_record;
    }
    $search_flag = 0;
    $search_status = '';
    if ($this->Session->check('AdminSearch')) {
      $keywords = $this->Session->read('AdminSearch');

      foreach ($keywords as $key => $values) {
        if ($key == 'status') {
          $search_status = $values;
          $filters[] = array('User.' . $key => $values);
        }
        if ($key == 'email') {
          $filters[] = array('User.' . $key . ' LIKE' => "%" . $values . "%");
          $filters_without_status[] = array('User.' . $key . ' LIKE' => "%" . $values . "%");
        }

        if ($key == 'phone_number') {
          $filters[] = array('User.' . $key . ' LIKE' => "%" . $values . "%");
          $filters_without_status[] = array('User.' . $key . ' LIKE' => "%" . $values . "%");
        }
        if ($key == 'username') {
          $filters[] = array('User.' . $key . ' LIKE' => "%" . $values . "%");
          $filters_without_status[] = array('User.' . $key . ' LIKE' => "%" . $values . "%");
        }
      }
      $search_flag = 1;
    }
    $this->set(compact('search_flag', 'defaultTab'));

    $this->User->bindModel(array(
      'hasOne' => array(
        'UserProfileImage' => array(
          'className' => 'UserImage',
          'foreignKey' => 'user_id',
          'conditions' => array('UserProfileImage.image_type' => 1)
        ),
        'UserCoverImage' => array(
          'className' => 'UserImage',
          'foreignKey' => 'user_id',
          'conditions' => array('UserCoverImage.image_type' => 2)
        )
      )
        ), false);

    $this->paginate = array(
      'User' => array(
        'limit' => $number_of_record,
        'order' => array('User.id' => 'DESC'),
        'conditions' => $filters
      )
    );

    $data = $this->paginate('User');
    $this->set(compact('data'));
    $this->set('title_for_layout', __('Users', true));


    if (isset($this->request->params['named']['page']))
      $this->Session->write('Url.page', $this->request->params['named']['page']);
    if (isset($this->request->params['named']['sort']))
      $this->Session->write('Url.sort', $this->request->params['named']['sort']);
    if (isset($this->request->params['named']['direction']))
      $this->Session->write('Url.direction', $this->request->params['named']['direction']);
    $this->Session->write('Url.type', '');
    $this->Session->write('Url.defaultTab', $defaultTab);

    if ($this->request->is('ajax')) {
      $this->render('ajax/admin_index');
    } else {
      $active = 0;
      $inactive = 0;
      if ($search_status == '' || $search_status == Configure::read('App.Status.active')) {
        $temp = $filters_without_status;
        $temp[] = array('User.status' => 1);
        $active = $this->User->find('count', array('conditions' => $temp));
      }
      if ($search_status == '' || $search_status == Configure::read('App.Status.inactive')) {
        $temp = $filters_without_status;
        $temp[] = array('User.status' => 0);
        $inactive = $this->User->find('count', array('conditions' => $temp));
      }

      $tabs = array('All' => $active + $inactive, 'Active' => $active, 'Inactive' => $inactive);
      $this->set(compact('tabs'));
    }
  }

  /*
   * Dashboard
   */

  public function admin_dashboard() {
    
  }

  /*
   * User location search
   */

  public function admin_location_search() {

    if ($this->request->is('ajax')) {
      $this->layout = false;
      $this->autoRender = false;
      $this->loadModel('Postalcode');

      $results = $this->Postalcode->find('all', array('conditions' => array("Postalcode.placename LIKE '%" . $_POST['query'] . "%'", "Postalcode.status" => 1), 'fields' => array('Postalcode.country_name', 'Postalcode.admin1_state', 'Postalcode.placename'), 'limit' => 20));

      $input_id = $_POST['rand'];

      foreach ($results as $k => $v) {
        $str = strtoupper($v['Postalcode']['placename'] . ' ' . $v['Postalcode']['admin1_state'] . ' ' . $v['Postalcode']['country_name']);
        $start = strpos($str, strtoupper($_POST['query']));
        $end = similar_text($str, strtoupper($_POST['query']));
        $last = substr($str, $end, strlen($str));
        $first = substr($str, $start, $end);
        $final = '<span style="font-weight:bold;">' . $first . '</span>' . $last;
        $final2 = $first . $last;

        echo "<li onclick=\"set_" . $input_id . "('" . $final2 . "')\"><a href='javascript:void(0);' style='color:#000;' style='font-weight:bold;'>" . $final . "</a></li>";
      }
    }
    die();
  }

  /*
   * View existing user
   */

  public function admin_view($id = null) {
    $this->User->id = $id;
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }

    $this->User->recursive = 3;
    $this->User->bindModel(array(
      'hasOne' => array(
        'UserProfileImage' => array(
          'className' => 'UserImage',
          'foreignKey' => 'user_id',
          'conditions' => array('UserProfileImage.image_type' => 1)
        ),
        'UserCoverImage' => array(
          'className' => 'UserImage',
          'foreignKey' => 'user_id',
          'conditions' => array('UserCoverImage.image_type' => 2)
        )
      )
        ), false);
    $data = $this->User->read(null, $id);
    $this->set('user', $data);
  }

  public function admin_add() {
    if ($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id') == Configure::read('App.SubAdmin.role')) {
      $this->Session->setFlash(__('You are not authorizatized for this action'), 'admin_flash_error');
      $this->redirect(array('controller' => 'admins', 'action' => 'dashboard'));
    }
    $this->loadModel('Template');

    if ($this->request->is('post')) {

      if (!empty($this->request->data)) {

        /* unset user skill 0 position value if exist */

        if (!isset($this->request->params['data']['_Token']['key']) || ($this->request->params['data']['_Token']['key'] != $this->request->params['_Token']['key'])) {
          $blackHoleCallback = $this->Security->blackHoleCallback;
          $this->$blackHoleCallback();
        }

        $this->User->set($this->request->data['User']);
        $this->User->setValidation('admin');

        $this->request->data['User']['password'] = Security::hash($this->request->data['User']['password2'], null, true);
        $this->request->data['User']['origional_password'] = $this->request->data['User']['password2'];

        $this->User->create();

        $this->request->data['User']['role_id'] = Configure::read('App.Role.User');
        $file = $this->request->data['User']['profile_image'];
        unset($this->request->data['User']['profile_image']);

        if ($this->User->saveAll($this->request->data)) {
          #user image upload
          $userId = $this->User->id;
          if (!empty($file) && $file['tmp_name'] != '' && $file['size'] > 0) {
            $rules = array('size' => array(USER_THUMB_WIDTH, USER_THUMB_HEIGHT), 'type' => 'resizecrop');
            $tinyrules = array('size' => array(USER_TINY_WIDTH, USER_TINY_HEIGHT), 'type' => 'resizecrop');
            $thumb1 = array('size' => array(USER_THUMB_WIDTH1, USER_THUMB_HEIGHT1), 'type' => 'resizecrop');
            $back = array('size' => array(USER_LARGE_WIDTH, USER_LARGE_HEIGHT), 'type' => 'resizecrop');
            // Upload the image using the Upload component
            $path_info = pathinfo($file['name']);
            $file['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];
            $tinyResult = $this->Upload->upload($file, WWW_ROOT . USER_TINY_DIR
                . DS, '', $tinyrules);
            $result = $this->Upload->upload($file, WWW_ROOT . USER_THUMB_DIR
                . DS, '', $rules);
            $result = $this->Upload->upload($file, WWW_ROOT . USER_THUMB1_DIR
                . DS, '', $thumb1);
            $result = $this->Upload->upload($file, WWW_ROOT . USER_LARGE_DIR
                . DS, '', $back);
            $res1 = $this->Upload->upload($file, WWW_ROOT . USER_ORIGINAL_DIR . DS, '');
            if (!empty($this->Upload->result) && empty($this->Upload->errors)) {
              $this->User->updateAll(array('User.profile_image' => "'" . $this->Upload->result . "'"), array('User.id' => $userId));
            } else {
              $responseData["error"] = $this->Upload->errors[0];
            }
          } else {
            $responseArray["error"] = "Image is empty or Size is Zero.";
          }

          /* send Successful email registration message to user */
          /*  $forgotMail = $this->Template->find('first', array('conditions' => array('Template.slug' => 'user_registration')));

            $email_subject = $forgotMail['Template']['subject'];
            $subject = __('[' . Configure::read('Site.title') . '] ' . $email_subject . '', true);

            $mailMessage = str_replace(array('{NAME}', '{USERNAME}', '{SITE}'), array($this->request->data['User']['first_name'], $this->request->data['User']['username'], Configure::read('Site.title')), $forgotMail['Template']['content']);

            if ($this->sendMail($this->request->data["User"]["email"], $subject, $mailMessage, array(Configure::read('App.AdminMail') => Configure::read('Site.title')), $forgotMail['Template']['id'])) {

            $this->Session->setFlash(__('User has been saved successfully'), 'admin_flash_success');
            } else {

            $this->Session->setFlash('Congrates! You have succesfully registered on ' . Configure::read('Site.title') . '.', 'admin_flash_success');
            } */

          /* send Successful email registration message to Admin */
          /*  $forgotMail = $this->Template->find('first', array('conditions' => array('Template.slug' => 'admin_user_registration')));

            $email_subject = $forgotMail['Template']['subject'];
            $subject = __('[' . Configure::read('Site.title') . '] ' .
            $email_subject . '', true);

            $mailMessage = str_replace(array('{NAME}', '{USERNAME}', '{SITE}'), array($this->request->data['User']['first_name'], $this->request->data['User']['username'], Configure::read('Site.title')), $forgotMail['Template']['content']);

            if ($this->sendMail(Configure::read('App.AdminMail'), $subject, $mailMessage, array(Configure::read('App.AdminMail') => Configure::read('Site.title')), $forgotMail['Template']['id'])) {
            $this->Session->setFlash(__('User has been saved successfully'), 'admin_flash_success');
            } else {
            $this->Session->setFlash('Congrates! You have succesfully registered on ' . Configure::read('Site.title') . '.', 'admin_flash_success');
            } */
          $this->Session->setFlash(__('User has been saved successfully'), 'admin_flash_success');
          $this->redirect(array('action' => 'index'));
        } else {
          $this->Session->setFlash(__('The User could not be saved. Please, try again.'), 'admin_flash_error');
        }
      }
    }
  }

  /**
   * edit existing user
   */
  public function admin_edit($id = null) {

    $this->User->id = $id;

    $imageInfo = $this->User->find('first', array('conditions' => array('User.id' => $id), 'fields' => array('User.profile_image', 'User.fb_id', 'User.id', 'User.twitter_id', 'User.linkdin_id', 'User.social_media_image_url', 'username')));
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {

      if (!empty($this->request->data)) {
        if (!isset($this->request->params['data']['_Token']['key']) || ($this->request->params['data']['_Token']['key'] != $this->request->params['_Token']['key'])) {
          $blackHoleCallback = $this->Security->blackHoleCallback;
          $this->$blackHoleCallback();
        }
        $this->User->set($this->request->data['User']);
        $this->User->setValidation('admin');
        if ($this->User->validates()) {
          $this->User->create();

          //$this->request->data['User']['role_id'] = 2;
          if (!empty($this->request->data['User']['profile_image']['tmp_name'])) {
            $file = $this->request->data['User']['profile_image'];
          } else {
            $file = '';
          }

          unset($this->request->data['User']['profile_image']);
          if (!empty($this->request->data['User']['full_address'])) {
            $addArr = explode(',', $this->request->data['User']['full_address']);

            $this->request->data['User']['city'] = $addArr[0];
            $this->request->data['User']['state'] = $addArr[1];
            $this->request->data['User']['country'] = $addArr[2];
          }
          if ($this->User->saveAll($this->request->data)) {

            if (!empty($file) && $file['tmp_name'] != '' && $file['size'] > 0) {

              $rules = array('size' => array(USER_THUMB_WIDTH, USER_THUMB_HEIGHT), 'type' => 'resizecrop');
              $tinyrules = array('size' => array(USER_TINY_WIDTH, USER_TINY_HEIGHT), 'type' => 'resizecrop');
              $thumb1 = array('size' => array(USER_THUMB_WIDTH1, USER_THUMB_HEIGHT1), 'type' => 'resizecrop');
              $back = array('size' => array(USER_LARGE_WIDTH, USER_LARGE_HEIGHT), 'type' => 'resizecrop');
              //Upload the image using the Upload component
              $path_info = pathinfo($file['name']);
              $file['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];
              $tinyResult = $this->Upload->upload($file, WWW_ROOT . USER_TINY_DIR
                  . DS, '', $tinyrules);
              $result = $this->Upload->upload($file, WWW_ROOT . USER_THUMB_DIR
                  . DS, '', $rules);
              $result = $this->Upload->upload($file, WWW_ROOT . USER_THUMB1_DIR
                  . DS, '', $thumb1);
              $result = $this->Upload->upload($file, WWW_ROOT . USER_LARGE_DIR . DS, '', $back);
              $res1 = $this->Upload->upload($file, WWW_ROOT . USER_ORIGINAL_DIR . DS, '');
              if (!empty($this->Upload->result) && empty($this->Upload->errors)) {
                if (!empty($this->request->data['User']['profile_image_hidden'])) {
                  if (file_exists(WWW_ROOT . DS . USER_TINY_DIR . DS . $this->request->data['User']['profile_image_hidden'])) {
                    @unlink(WWW_ROOT . DS . USER_TINY_DIR . DS . $this->request->data['User']['profile_image_hidden']);
                  }
                  if (file_exists(WWW_ROOT . DS . USER_THUMB_DIR . DS . $this->request->data['User']['profile_image_hidden'])) {
                    @unlink(WWW_ROOT . DS . USER_THUMB_DIR . DS . $this->request->data['User']['profile_image_hidden']);
                  }
                  if (file_exists(WWW_ROOT . DS . USER_THUMB1_DIR . DS . $this->request->data['User']['profile_image_hidden'])) {
                    @unlink(WWW_ROOT . DS . USER_THUMB1_DIR . DS . $this->request->data['User']['profile_image_hidden']);
                  }
                  if (file_exists(WWW_ROOT . DS . USER_LARGE_DIR . DS . $this->request->data['User']['profile_image_hidden'])) {
                    @unlink(WWW_ROOT . DS . USER_LARGE_DIR . DS . $this->request->data['User']['profile_image_hidden']);
                  }
                  if (file_exists(WWW_ROOT . DS . USER_ORIGINAL_DIR . DS . $this->request->data['User']['profile_image_hidden'])) {
                    @unlink(WWW_ROOT . DS . USER_ORIGINAL_DIR . DS . $this->request->data['User']['profile_image_hidden']);
                  }
                }
                $this->User->updateAll(array('User.profile_image' => "'" . $this->Upload->result . "'"), array('User.id' => $this->request->data['User']['id']));
              } else {
                $responseData["error"] = $this->Upload->errors[0];
              }
            } else {
              $responseArray["error"] = "Image is empty or Size is Zero.";
            }

            $this->Session->setFlash(__('The User information has been updated successfully', true), 'admin_flash_success');
            $this->redirect($this->referer());
          } else {

            $this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'admin_flash_error');
          }
        } else {

          $this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'admin_flash_error');
        }
      }
    } else {
      $this->request->data = $this->User->read(null, $id);

      unset($this->request->data['User']['password']);
    }


    if (!empty($imageInfo['User']['profile_image'])) {
      $image = $imageInfo['User']['profile_image'];
    } else {
      $image = "no_image.png";
    }

    $this->set(compact('image', 'imageInfo'));
  }

  public function admin_change_password($id = null) {
    if ($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id') == Configure::read('App.SubAdmin.role')) {
      $this->Session->setFlash(__('You are not authorizatized for this action'), 'admin_flash_error');
      $this->redirect(array('controller' => 'admins', 'action' => 'dashboard'));
    }
    $this->User->id = $id;
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }

    if ($this->request->is('post') || $this->request->is('put')) {

      if (!empty($this->request->data)) {
        if (!isset($this->request->params['data']['_Token']['key']) || ($this->request->params['data']['_Token']['key'] != $this->request->params['_Token']['key'])) {
          $blackHoleCallback = $this->Security->blackHoleCallback;
          $this->$blackHoleCallback();
        }


        //validate user data
        $this->User->set($this->request->data);
        $this->User->setValidation('admin_change_password');
        if ($this->User->validates()) {
          $new_password = $this->request->data['User']['new_password'];
          $this->request->data['User']['password'] = Security::hash($this->request->data['User']['new_password'], null, true);
          $this->request->data['User']['origional_password'] = $this->request->data['User']['new_password'];
          if ($this->User->saveAll($this->request->data)) {

            $this->Session->setFlash(' Password has been changed successfully', 'admin_flash_success');
            $this->redirect($this->referer());
          } else {
            $this->Session->setFlash(__('The Password could not be changed. Please, try again.', true), 'admin_flash_error');
          }
        } else {
          $this->Session->setFlash(__('The Password could not be changed. Please, correct errors.', true), 'admin_flash_error');
        }
      }
    } else {

      $this->request->data = $this->User->read(null, $id);

      unset($this->request->data['User']['password']);
    }
  }

  /**
   * delete existing user
   */
  public function admin_delete($id = null) {
    $user_id = $this->User->id = $id;

    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }
    if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
      $blackHoleCallback = $this->Security->blackHoleCallback;
      $this->$blackHoleCallback();
    }
    /*
      $this->User->bindModel(array(
      'hasMany'=>array(
      'TemplatesUser'=>array(
      'className'=>'TemplatesUser',
      'foreignKey'=>'user_id',
      'dependent'=>true
      )
      ),
      'hasOne'=>array(
      'UserSession'=>array(
      'className'=>'UserSession',
      'foreignKey'=>'user_id',
      'dependent'=>true
      )
      )
      ),false);
      $user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
     */

    $this->User->bindModel(array(
      'hasMany' => array(
        'UserImageLike' => array(
          'className' => 'UserImageLike',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'UserLike' => array(
          'className' => 'UserLike',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'UserLiked' => array(
          'className' => 'UserLike',
          'foreignKey' => 'liked_user_id',
          'dependent' => true
        ),
        'UserChatSetting' => array(
          'className' => 'UserChatSetting',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'FromUserNotification' => array(
          'className' => 'UserNotification',
          'foreignKey' => 'fromid',
          'dependent' => true
        ),
        'ToUserNotification' => array(
          'className' => 'UserNotification',
          'foreignKey' => 'toid',
          'dependent' => true
        ),
        'UserBlock' => array(
          'className' => 'UserBlock',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'UserBlocked' => array(
          'className' => 'UserBlock',
          'foreignKey' => 'blocked_user_id',
          'dependent' => true
        ),
        'UserImage' => array(
          'className' => 'UserImage',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'UserContact' => array(
          'className' => 'UserContact',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'UserContacted' => array(
          'className' => 'UserContact',
          'foreignKey' => 'contact_user_id',
          'dependent' => true
        ),
        'AppMediaFile' => array(
          'className' => 'AppMediaFile',
          'foreignKey' => 'app_user_id',
          'dependent' => true
        ),
        'UserContactRequest' => array(
          'className' => 'UserContactRequest',
          'foreignKey' => 'user_id',
          'dependent' => true
        ),
        'UserContactRequested' => array(
          'className' => 'UserContactRequest',
          'foreignKey' => 'friend_id',
          'dependent' => true
        )
      )
        ), false);

    $userData = $this->User->find('first', array('conditions' => array('User.id' => $id)));

    $userNm = $userData['User']['phone_number'];
    if (!empty($userData['User']['username'])) {
      $userNm = $userData['User']['username'];
    }
    $contactUserArr = array();
    $allDeviceIds = array();
    if (!empty($userData['UserContact'])) {
      foreach ($userData['UserContact'] as $contKey => $contValue) {
        $contactUserArr[] = $contValue['contact_user_id'];
      }
      $allContacts = implode("','", $contactUserArr);
      $allDeviceIds = $this->User->find('list', array('fields' => array('id', 'device_id'), 'conditions' => array("User.id IN('$allContacts')")));
    }

    if (!empty($allDeviceIds)) {
      $sentPns = array();
      foreach ($allDeviceIds as $tokenKey => $tokenValue) {
        if (!in_array($tokenValue, $sentPns)) {
          //$this->send_notification_for_iphone($tokenValue, $userNm." deleted", 9, $userData['User']['id']);
        }
        $sentPns[] = $tokenValue;
      }
    }

    $userImageArr = array();
    if (!empty($userData['UserImage'])) {
      foreach ($userData['UserImage'] as $key => $userImageVal) {
        $userImageArr[] = SITE_URL . "/uploads/user_images/large/" . $userImageVal['image'];
        $userImageArr[] = SITE_URL . "/uploads/user_images/original/" . $userImageVal['image'];
        $userImageArr[] = SITE_URL . "/uploads/user_images/thumb/" . $userImageVal['image'];
      }
    }

    $userMediaArr = array();
    if (!empty($userData['AppMediaFile'])) {
      foreach ($userData['AppMediaFile'] as $key => $userMediaVal) {
        $userMediaArr[] = SITE_URL . "/uploads/media/" . $userData['User']['phone_number'] . "/" . $userMediaVal['file'];
      }
    }

    if ($this->User->delete($userData['User']['id'], true)) {
      $newrs = $this->UserTextPost->find('first', array('fields' => array('user_id'), 'conditions' => array("UserTextPost.user_id" => $userData['User']['id'])));
      if (!empty($newrs)) {
        $this->UserTextPost->deleteAll(array('UserTextPost.user_id' => $userData['User']['id']), true);
      }
      $newrs2 = $this->UserPostComment->find('first', array('fields' => array('user_id'), 'conditions' => array("UserPostComment.user_id" => $userData['User']['id'])));
      if (!empty($newrs2)) {
        $this->UserPostComment->deleteAll(array('UserPostComment.user_id' => $userData['User']['id']), true);
      }
      $newrs3 = $this->UserPostLike->find('first', array('fields' => array('user_id'), 'conditions' => array("UserPostLike.user_id" => $userData['User']['id'])));
      if (!empty($newrs3)) {

        $this->UserPostLike->deleteAll(array('UserPostLike.user_id' => $userData['User']['id']), true);
      }
      if (!empty($userImageArr)) {
        $usrImgArr = array();
        foreach ($userImageArr as $imageKey => $imageVal) {
          @unlink($imageVal);
        }
      }
      if (!empty($userMediaArr)) {
        foreach ($userMediaArr as $mediaKey => $mediaVal) {
          @unlink($imageVal);
        }
      }

      if (!empty($userData['User']['jid'])) {
        $this->loadModel('ChatMessage');
        $this->ChatMessage->query("delete from ofMessageArchive where fromJID='" . $userData['User']['jid'] . "' OR toJID='" . $userData['User']['jid'] . "'");
      }

      if (!empty($userData['User']['phone_number'])) {
        $URL = "https://" . Configure::read('App.open_fire_host') . ":9991/plugins/userService/userservice?type=delete&secret=" . Configure::read('App.OfSecretKey') . "&username=" . $userData['User']['phone_number'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $URL);
        $contents = curl_exec($curl);
        curl_close($curl);
      }

      $this->Session->setFlash(__('User deleted successfully'), 'admin_flash_success');
      $this->redirect($this->referer());
    }
    $this->Session->setFlash(__('User was not deleted', 'admin_flash_error'));
    $this->redirect($this->referer());
  }

  /**
   * ban user
   */
  public function admin_baned($id = null) {
    $user_id = $this->User->id = $id;

    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }
    if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
      $blackHoleCallback = $this->Security->blackHoleCallback;
      $this->$blackHoleCallback();
    }

    //user ban and device ban
    $userData = $this->User->find('first', array('conditions' => array('User.id' => $id)));
    if (!empty($userData)) {
      $userData['User']['login_ban'] = 1;
      if ($this->User->save($userData)) {

        if (!empty($userData['User']['device_unique_id'])) {
          $devicedata = $this->DeviceBans->find('first', array('conditions' => array('DeviceBans.device_unique_id' => $userData['User']['device_unique_id'])));
          if (empty($devicedata)) {
            $devicedata = array(
              'device_unique_id' => $userData['User']['device_unique_id']
            );
            $this->DeviceBans->save($devicedata);
          }
        }

        if ($userData['User']['device_type'] == '2') {
          $token = $userData['User']['device_id'];
          $this->send_notification_to_users($token);
        }
        if ($userData['User']['device_type'] == '1') {
          $token = $userData['User']['device_id'];
          $this->send_notification_to_android_users($token);
        }
        $this->Session->setFlash(__('User blocked successfully'), 'admin_flash_success');

        //remove the device token
        $userData['User']['device_id'] = '';
        $this->User->save($userData);

        $this->redirect($this->referer());
      }
    }

    $this->Session->setFlash(__('User was not deleted', 'admin_flash_error'));
    $this->redirect($this->referer());
  }

  public function admin_status($id = null) {
    if ($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id') == Configure::read('App.SubAdmin.role')) {
      $this->Session->setFlash(__('You are not authorizatized for this action'), 'admin_flash_error');
      $this->redirect(array('controller' => 'admins', 'action' => 'dashboard'));
    }
    $this->User->id = $id;
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }
    if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
      $blackHoleCallback = $this->Security->blackHoleCallback;
      $this->$blackHoleCallback();
    }
    $this->loadModel('Template');
    $this->loadModel('User');
    if ($this->User->toggleStatus($id)) {
      $user_info = $this->User->get_users('first', 'User.email,User.first_name,User.last_name,User.status', array('User.id' => $id));

      $this->Session->setFlash(__('User\'s status has been changed'), 'admin_flash_success');
      $this->redirect($this->referer());
    }
    $this->Session->setFlash(__('User\'s status was not changed', 'admin_flash_error'));
    $this->redirect($this->referer());
  }

  public function admin_process() {

    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }

    if (!isset($this->request->params['named']['token']) || ($this->request->params['named']['token'] != $this->request->params['_Token']['key'])) {
      $blackHoleCallback = $this->Security->blackHoleCallback;
      $this->$blackHoleCallback();
    }

    if (!empty($this->request->data)) {
      App::uses('Sanitize', 'Utility');
      $action = Sanitize::escape($this->request->data['User']['pageAction']);

      $ids = $this->request->data['User']['id'];

      if (count($this->request->data) == 0 || $this->request->data['User'] == null) {
        $this->Session->setFlash('No items selected.', 'admin_flash_error');
        $this->redirect($this->referer());
      }

      if ($action == "delete") {
        $userIds = implode("','", $ids);


        $this->User->bindModel(array(
          'hasMany' => array(
            'UserImageLike' => array(
              'className' => 'UserImageLike',
              'foreignKey' => 'user_id',
              'dependent' => true
            ),
            'UserLike' => array(
              'className' => 'UserLike',
              'foreignKey' => 'user_id',
              'dependent' => true
            ),
            'UserLiked' => array(
              'className' => 'UserLike',
              'foreignKey' => 'liked_user_id',
              'dependent' => true
            ),
            'UserChatSetting' => array(
              'className' => 'UserChatSetting',
              'foreignKey' => 'user_id',
              'dependent' => true
            ),
            'FromUserNotification' => array(
              'className' => 'UserNotification',
              'foreignKey' => 'fromid',
              'dependent' => true
            ),
            'ToUserNotification' => array(
              'className' => 'UserNotification',
              'foreignKey' => 'toid',
              'dependent' => true
            ),
            'UserBlock' => array(
              'className' => 'UserBlock',
              'foreignKey' => 'user_id',
              'dependent' => true
            ),
            'UserBlocked' => array(
              'className' => 'UserBlock',
              'foreignKey' => 'blocked_user_id',
              'dependent' => true
            ),
            'UserImage' => array(
              'className' => 'UserImage',
              'foreignKey' => 'user_id',
              'dependent' => true
            ),
            'UserContact' => array(
              'className' => 'UserContact',
              'foreignKey' => 'user_id',
              'dependent' => true
            ),
            'UserContacted' => array(
              'className' => 'UserContact',
              'foreignKey' => 'contact_user_id',
              'dependent' => true
            ),
            'AppMediaFile' => array(
              'className' => 'AppMediaFile',
              'foreignKey' => 'app_user_id',
              'dependent' => true
            )
          )
            ), false);

        $userData = $this->User->find('all', array('conditions' => array("User.id IN('$userIds')")));
        $userImageArr = array();
        $usernameArr = array();
        $jidArr = array();
        $contactUserArr = array();
        $allDeviceIds = array();

        foreach ($userData as $UserKey => $userVal) {
          if (!empty($userVal['User']['phone_number'])) {
            $usernameArr[] = $userVal['User']['phone_number'];
          }

          if (!empty($userVal['User']['jid'])) {
            $jidArr[] = $userVal['User']['jid'];
          }

          if (!empty($userVal['UserImage'])) {
            foreach ($userVal['UserImage'] as $key => $val) {
              $userImageArr[] = SITE_URL . "/uploads/user_images/large/" . $val['image'];
              $userImageArr[] = SITE_URL . "/uploads/user_images/original/" . $val['image'];
              $userImageArr[] = SITE_URL . "/uploads/user_images/thumb/" . $val['image'];
            }
          }


          /*           * **************** USER DELETION PUSH TO MULTIPLE USERS ******************* */
          $userNm = $userVal['User']['phone_number'];
          if (!empty($userVal['User']['username'])) {
            $userNm = $userVal['User']['username'];
          }

          //$contactUserArr = array();
          if (!empty($userVal['UserContact'])) {
            foreach ($userVal['UserContact'] as $contKey => $contValue) {
              $contactUserArr[] = $contValue['contact_user_id'];
            }
          }
          /*           * **************** USER DELETION PUSH TO MULTIPLE USERS ******************* */
        }

        if (!empty($contactUserArr)) {
          $contactUserArr = array_unique($contactUserArr);
          $allContacts = implode("','", $contactUserArr);
          $allDeviceIds = $this->User->find('list', array('fields' => array('id', 'device_id'), 'conditions' => array("User.id IN('$allContacts')")));
          if (!empty($allDeviceIds)) {
            $sentPns = array();
            foreach ($allDeviceIds as $tokenKey => $tokenValue) {
              if (!in_array($tokenValue, $sentPns)) {
                //$this->send_notification_for_iphone($tokenValue, $userNm." deleted", 9, $userVal['User']['id']);
              }
              $sentPns[] = $tokenValue;
            }
          }
        }

        if (!empty($jidArr)) {
          $this->loadModel('ChatMessage');
          //$jidArr = array('64342541141@115.112.57.155');
          /* INSERT INTO `topchat_openfire`.`ofmessagearchive` (`conversationID`, `fromJID`, `fromJIDResource`, `toJID`, `toJIDResource`, `sentDate`, `body`) VALUES ('11111111', '5555222441@115.112.57.155', 'tfgd', '64342541141@115.112.57.155', 'bdsdgsgsgs', '522425252652', 'gsdtgsgdtwetwett'), ('11111111', '64342541141@115.112.57.155', 'twqqtqq', '5555222441@115.112.57.155', 'dfgsgsg', '245243342', 'sfsfsgwewtww5t5'); */

          $strJids = implode("', '", $jidArr);
          $this->ChatMessage->query("delete from ofmessagearchive where fromJID IN('$strJids') OR toJID IN('$strJids')");
        }
        //die("THNX");

        if ($this->User->deleteAll(array('User.id' => $ids))) {
          if (!empty($userImageArr)) {
            $usrImgArr = array();
            foreach ($userImageArr as $imageKey => $imageVal) {
              @unlink($imageVal);
            }
          }
          if (!empty($usernameArr)) {
            $usrImgArr = array();
            foreach ($usernameArr as $userKey => $userVal) {
              $URL = "http://" . Configure::read('App.open_fire_host') . ":9090/plugins/userService/userservice?type=delete&secret=" . Configure::read('App.OfSecretKey') . "&username=$userVal";

              $curl = curl_init();
              curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($curl, CURLOPT_URL, $URL);
              $contents = curl_exec($curl);
              curl_close($curl);
            }
          }
        }
        $this->Session->setFlash('Users have been deleted successfully', 'admin_flash_success');
        $this->redirect($this->referer());
      }

      if ($action == "activate") {
        $this->User->updateAll(array('User.status' => Configure::read('App.Status.active')), array('User.id' => $ids));
        $this->Session->setFlash('Users have been activated successfully', 'admin_flash_success');
        $this->redirect($this->referer());
      }

      if ($action == "deactivate") {
        $this->User->updateAll(array('User.status' => Configure::read('App.Status.inactive')), array('User.id' => $ids));
        $this->Session->setFlash('Users have been deactivated successfully', 'admin_flash_success');
        $this->redirect($this->referer());
      }
    } else {
      $this->redirect(array('controller' => 'admins', 'action' => 'index'));
    }
  }

  public function reset_password($id = 0) {
    $this->loadModel('Template');
    $this->loadModel('User');
    $this->layout = false;
    if ($this->request->is('post')) {
      if (!empty($this->request->data)) {
        $this->User->set($this->request->data);
        $this->User->validates();
        $this->User->setValidation('reset_password');
        if ($this->User->validates()) {
          $user_id = base64_decode(base64_decode(base64_decode($id)));
          $user_data = $this->User->find('first', array('conditions' => array("User.id" => $user_id, "User.role_id = " => Configure::read('App.User.role'))));


          if (isset($user_data) && !empty($user_data)) {
            $this->User->id = $user_data['User']['id'];
            $new_password = Security::hash($this->request->data['User']['password'], null, true);
            if ($this->User->saveField('password', $new_password)) {
              $password = $this->request->data['User']['password'];
              $this->User->saveField('origional_password', $this->request->data['User']['password']);
              unset($this->request->data['User']['password']);
              unset($this->request->data['User']['password2']);
              /*               * *****Reset Password mail code here** */
              $mailMessage = '';
              $password_confirm = $this->Template->find('first', array('conditions' => array('Template.slug' => 'forgot_password_confirm')));

              $email_subject = $password_confirm['Template']['subject'];
              $subject = '[' . Configure::read('Site.title') . ']' . $email_subject;

              $mailMessage = str_replace(array('{NAME}', '{PASSWORD}'), array($user_data['User']['username'], $password), $password_confirm['Template']['content']);

              if (parent::sendMail($user_data['User']['email'], $subject, $mailMessage, array(Configure::read('App.AdminMail') => Configure::read('Site.title')), 'general')) {

                // Save the data in templates_users table start //
                $this->loadModel('TemplatesUser');
                $templates_users['TemplatesUser']['user_id'] = $user_data['User']['id'];
                $templates_users['TemplatesUser']['template_id'] = $password_confirm['Template']['id'];
                $this->TemplatesUser->save($templates_users);
                // End //
                $this->Session->setFlash('Password change sucessfully.', 'front_flash_good');
                $this->redirect(array('controller' => 'users', 'action' => 'thankx'));
              }
            }
          } else {
            $this->Session->setFlash('invalid url.', 'front_flash_bad');
          }
        }
      }
    }
    $this->set(compact('id'));
  }

  public function reset_password_change($id = 0) {
    $this->loadModel('Template');
    $this->loadModel('User');
    $this->layout = false;
    if ($this->request->is('post')) {
      if (!empty($this->request->data)) {

        $this->User->set($this->request->data);
        $this->User->validates();
        $this->User->setValidation('reset_password');
        if ($this->User->validates()) {
          $user_id = base64_decode(base64_decode(base64_decode($id)));
          $user_data = $this->User->find('first', array('conditions' => array("User.id" => $user_id, "User.role_id = " => Configure::read('App.User.role'))));


          if (isset($user_data) && !empty($user_data)) {
            $this->User->id = $user_data['User']['id'];
            $new_password = Security::hash($this->request->data['User']['password'], null, true);
            if ($this->User->saveField('password', $new_password)) {
              $password = $this->request->data['User']['password'];
              $this->User->saveField('origional_password', $this->request->data['User']['password']);
              unset($this->request->data['User']['password']);
              unset($this->request->data['User']['password2']);
              /*               * *****Reset Password mail code here** */
              $mailMessage = '';
              $password_confirm = $this->Template->find('first', array('conditions' => array('Template.slug' => 'reset_password_confirm')));

              $email_subject = $password_confirm['Template']['subject'];
              $subject = '[' . Configure::read('Site.title') . ']' . $email_subject;

              $mailMessage = str_replace(array('{NAME}', '{PASSWORD}'), array($user_data['User']['username'], $password), $password_confirm['Template']['content']);

              if (parent::sendMail($user_data['User']['email'], $subject, $mailMessage, array(Configure::read('App.AdminMail') => Configure::read('Site.title')), 'general')) {

                // Save the data in templates_users table start //
                $this->loadModel('TemplatesUser');
                $templates_users['TemplatesUser']['user_id'] = $user_data['User']['id'];
                $templates_users['TemplatesUser']['template_id'] = $password_confirm['Template']['id'];
                $this->TemplatesUser->save($templates_users);
                // End //
                $this->Session->setFlash('Password change sucessfully.', 'front_flash_good');
                $this->redirect(array('controller' => 'users', 'action' => 'thankx'));
              }
            }
          } else {
            $this->Session->setFlash('invalid url.', 'front_flash_bad');
          }
        }
      }
    }
    $this->set(compact('id'));
  }

  public function thankx() {
    $this->layout = false;
  }

  public function login() {
    $userCookie = $this->Cookie->read('User');
    if ($this->Auth->login()) {
      $this->redirect($this->Auth->redirect());
    } else {
      if ($this->request->is('post')) {
        $this->request->data['User']['password'] = "";
        $this->Session->setFlash(__('Invalid username or password, try again'));
        $this->redirect(array('controller' => 'users', 'action' => 'register'));
      }
    }
  }

  public function logout() {
    $this->Session->delete('access_token');
    $this->Session->delete('Facebook.User');
    $this->Session->delete('Twitter.User');
    $this->Session->delete('GooglePlus.User');
    $this->Session->delete('LinkedIn.User');
    $this->Session->delete('LinkedIn.referer');
    $this->Session->delete('Google.referer');
    $this->Cookie->delete('User');
    unset($_SESSION['oauth']['linkedin']);
    $this->redirect($this->Auth->logout());
  }

  public function my_account() {
    if (!$this->Auth->user()) {
      //$this->redirect($this->Auth->redirect());
      pr($this->Auth->user());
      die('In');
    }
    $id = $this->Auth->user('id');
    $userInfo = $this->User->find('first', array('conditions' => array('User.id' => $id)));
    $this->set(compact('userInfo'));
  }

  public function register() {
    $this->set('title_for_layout', __('Register'));
    $userCookie = $this->Cookie->read('User');
    if ($this->Auth->login()) {
      if ($this->Cookie->read('User')) {
        $this->request->data = $this->Cookie->read('User');
      }

      $cookie = array();
      if (empty($userCookie)) {
        $cookie = base64_encode(serialize($this->Session->read('Auth.User')));
        $this->Cookie->write('User', $cookie, true, '+1 years');
      }
      $this->redirect($this->Auth->redirect());
    } elseif (!empty($userCookie)) {
      $val = unserialize(base64_decode($this->Cookie->read('User')));
      $userData = $this->User->findById($val['id']);
      if (!empty($userData)) {
        $this->Auth->login($val);
        $this->redirect($this->Auth->redirect());
      }
    } else {
      if ($this->request->is('post')) {
        if (!empty($this->request->data)) {
          $this->User->set($this->request->data['User']);
          $this->User->setValidation('register');

          $name = $this->request->data['User']['display_name'];
          $verification_code = substr(md5(uniqid()), 0, 20);
          $this->request->data['User']['verification_code'] = $verification_code;
          $this->request->data['User']['status'] = '0';

          if ($this->User->validates()) {
            $this->request->data['User']['password'] = Security::hash($this->request->data['User']['password1'], null, true);
            $this->request->data['User']['username'] = $this->request->data['User']['username1'];
            $this->request->data['User']['ip'] = $this->RequestHandler->getClientIp();
            $enter_password = $this->request->data['User']['password1'];
            $username = $this->request->data['User']['username1'];
            $password = $this->request->data['User']['password'];

            if ($this->User->saveAll($this->request->data)) {
              /*               * **************** EMAIL NOTIFICATION MESSAGE ******************* */

              $to = $this->request->data['User']['email'];
              $from = Configure::read('App.AdminMail');
              $mail_message = '';
              $this->loadModel('Template');
              $registrationMail = $this->Template->find('first', array('conditions' => array('Template.slug' => 'user_registration')));
              $email_subject = $registrationMail['Template']['subject'];
              $subject = __('[' . Configure::read('Site.title') . '] ' . $email_subject . '', true);
              $activationCode = $this->request->data['User']['verification_code'];
              $activation_url = Router::url(array(
                    'controller' => 'users',
                    'action' => 'activate',
                    base64_encode($this->request->data['User']['email']),
                    $verification_code,
                      ), true);

              $activation_link = '<a href="' . $activation_url . '">' . $activation_url . '</a>';

              $mail_message = str_replace(array('{USERNAME}', '{PASSWORD}', '{ACTIVATION_LINK}', '{activation_code}', '{NAME}', '{SITE}'), array($username, $enter_password, $activation_link, $activationCode, $name, Configure::read('App.SITENAME')), $registrationMail['Template']['content']);
              $template = 'default';
              $this->set('message', $mail_message);
              //echo $to."_".$subject."_".$mail_message."_".$from."_".$template;die;
              parent::sendMail($to, $subject, $mail_message, $from, $template);
              /*               * **************** EMAIL NOTIFICATION MESSAGE ******************* */
              $this->Session->setFlash(__('The user has been registered successfully. A verification link has been sent to your email account. You will be able to login after successful verification.', true), 'flash_success');
              $this->redirect(array('controller' => 'users', 'action' => 'register'));
            } else {
              $this->Session->setFlash(__('The user could not be registerd. Please, try again.', 'flash_error'));
            }
          } else {
            $this->Session->setFlash(__('Please correct error listed below, try again', 'flash_error'));
          }
        }
      }
    }
  }

  function activate($email = null, $verification_code = null) {
    $this->layout = 'default';
    if ($email == null || $verification_code == null) {
      $this->Session->setFlash(__('Error_Message', true), 'admin_flash_bad');
      $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }
    $email = base64_decode($email);

    if ($this->User->hasAny(
            array(
              'User.email' => $email,
              'User.verification_code' => $verification_code,
            //'User.status' => 0
            )
        )) {
      $user = $this->User->findByEmail($email);
      //activation date code
      $this->User->updateAll(array('User.modified' => "'" . date('Y-m-d H-i-s') . "'"));
      //activation date code close	
      $this->User->id = $user['User']['id'];
      $this->User->saveField('status', 1);
      $this->User->saveField('is_email_verified', 1);
      $this->User->saveField('verification_code', substr(md5(uniqid()), 0, 20));

      $to = $email;
      $from = Configure::read('App.AdminMail');
      $mail_message = '';
      $this->loadModel('Template');
      $notificationMail = $this->Template->find('first', array('conditions' => array('Template.slug' => 'verify_email')));
      $email_subject = $notificationMail['Template']['subject'];
      $subject = __('[' . Configure::read('Site.title') . '] ' . $email_subject . '', true);
      $login_url = Router::url(array(
            'controller' => 'users',
            'action' => 'register'
              ), true);

      $login_link = '<a href="' . $login_url . '">Click Here To Login</a>';
      $mail_message = str_replace(array('{NAME}', '{SITE}', '{LOGIN_LINK}'), array($user['User']['first_name'] . '' . $user['User']['last_name'], Configure::read('App.SITENAME'), $login_link), $notificationMail['Template']['content']);

      $template = 'default';

      $this->set('message', $mail_message);
      parent::sendMail($to, $subject, $mail_message, $from, $template);
      //$this->Session->setFlash(__('Your Email is verified'));
      $this->redirect(array('controller' => 'users', 'action' => 'success'));
    } else {
      $this->Session->setFlash(__('Verification Failed'));
      $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }
  }

  function success() {
    $this->layout = 'default';
    $this->set("title_for_layout", __('Success', true));
  }

  function update_profile($id = null) {
    $id = $this->Auth->User('id');
    $this->User->id = $id;
    $this->loadModel('UserImage');
    $this->User->bindModel(array('hasMany' => array('UserImage')), false);
    $imageInfo = $this->User->find('first', array('conditions' => array('User.id' => $id), 'fields' => array('User.profile_image', 'User.profile_cover_image', 'User.fb_id', 'User.id', 'User.twitter_id', 'User.linkdin_id', 'User.social_media_image_url', 'username')));
    if (!$this->User->exists()) {
      $this->redirect(array('controller' => 'users', 'action' => 'logout'));
      throw new NotFoundException(__('Invalid user'));
    }
    $userData = $this->User->read(null, $id);

    if ((empty($userData['User']['email'])) || (empty($userData['User']['first_name'])) || ($userData['User']['gender'] == 0) || (empty($userData['User']['dob'])) || (empty($userData['User']['username']))) {
      $AllFields = "";
      if (empty($userData['User']['email'])) {
        $AllFields = "email";
      }
      $this->redirect(array('controller' => 'users', 'action' => 'save_missing_fields'));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      if (!empty($this->request->data)) {
        if (!isset($this->request->params['data']['_Token']['key']) || ($this->request->params['data']['_Token']['key'] != $this->request->params['_Token']['key'])) {
          $blackHoleCallback = $this->Security->blackHoleCallback;
          $this->$blackHoleCallback();
        }
        $this->User->set($this->request->data['User']);
        $this->User->setValidation('update_profile');

        if ($this->User->validates()) {
          $this->User->create();

          if (!empty($this->request->data['User']['profile_image']['tmp_name'])) {
            $file = $this->request->data['User']['profile_image'];
          } else {
            $file = '';
          }


          if (isset($this->request->data['UserImage']['extra_image'][0]['tmp_name']) && empty($this->request->data['UserImage']['extra_image'][0]['tmp_name'])) {
            unset($this->request->data['UserImage']);
          }
          $images = array();
          $postExtraImage = 0;
          if (!empty($this->request->data['UserImage']['extra_image'])) {
            $images = $this->request->data['UserImage']['extra_image'];
            $postExtraImage = count($images);
          }
          unset($this->request->data['User']['profile_image']);

          if (!empty($this->request->data['User']['full_address'])) {
            $addArr = explode(',', $this->request->data['User']['full_address']);
            $this->request->data['User']['city'] = $addArr[0];
            $this->request->data['User']['state'] = $addArr[1];
            $this->request->data['User']['country'] = $addArr[2];
          }
          if ($this->User->save($this->request->data)) {

            $this->Session->setFlash(__('The User information has been updated successfully', true), 'admin_flash_success');
            $this->redirect(array('action' => 'update_profile'));
          } else {

            $this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'admin_flash_error');
          }
        } else {
          //pr($this->User->validationErrors);
          $this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'admin_flash_error');
        }
      }
    } else {
      $this->request->data = $this->User->read(null, $id);
      if (empty($this->request->data)) {
        $this->redirect(array('controller' => 'users', 'action' => 'logout'));
      }

      $cookie = array();
      $userCookie = $this->Cookie->read('User');
      //if(empty($userCookie)){
      $cookie = base64_encode(serialize($this->request->data['User']));
      $this->Cookie->write('User', $cookie, true, '+1 years');
      //}
      unset($this->request->data['User']['password']);
    }

    if (!empty($imageInfo['User']['profile_image'])) {
      $image = $imageInfo['User']['profile_image'];
    } else {
      $image = "no_image.png";
    }

    $extraimageInfo = $this->UserImage->find('all', array('fields' => array('UserImage.image', 'UserImage.id', 'UserImage.image_type'), 'conditions' => array('UserImage.user_id' => $id)));
    $totExtraImage = count($extraimageInfo);

    $this->set(compact('id', 'image', 'imageInfo', 'userData', 'extraimageInfo', 'totExtraImage'));
  }

  function forgot_password() {
    if ($this->Auth->user()) {
      $this->redirect(array('controller' => 'users', 'action' => 'update_profile'));
    }
    if (!empty($this->request->data)) {
      $this->User->set($this->request->data);
      $this->User->setValidation('forgot_password');
      if ($this->User->validates($this->request->data)) {
        $userDetail = $this->User->find("first", array('conditions' => array('User.email' => $this->request->data["User"]["email"], 'User.status' => 1, 'User.role_id' => 2)));

        if (!empty($userDetail)) {
          $this->User->id = $userDetail['User']['id'];
          $verification_code = substr(md5(uniqid()), 0, 20);
          $userDetail['User']['verification_code'] = $verification_code;
          if ($this->User->save($userDetail)) {
            $activation_url = Router::url(array(
                  'controller' => 'users',
                  'action' => 'get_password',
                  base64_encode($userDetail['User']['email']),
                  $verification_code
                    ), true);
            $this->loadModel('Template');
            $forgetPassMail = $this->Template->find('first', array('conditions' => array('Template.slug' => 'forgot_password')));
            $subject = $forgetPassMail['Template']['subject'];
            $activation_link = ' <a href="' . $activation_url . '" target="_blank" shape="rect">Change Password</a>';

            $mail_message = str_replace(array('{NAME}', "{ACTIVATION_LINK}"), array($userDetail['User']['display_name'], $activation_link), $forgetPassMail['Template']['content']);
            $to = $userDetail['User']['email'];
            $from = Configure::read('App.AdminMail');
            $template = 'default';
            $this->set('message', $mail_message);
            $template = 'default';
            parent::sendMail($to, $subject, $mail_message, $from, $template);
            $this->Session->setFlash(__('A link has been sent, Please check your inbox'), 'flash_success');
            $this->redirect(array('controller' => 'users', 'action' => 'forgot_password'));
          } else {
            $this->Session->setFlash(__('Email address not found in our record.', 'flash_error'));
          }
          $this->redirect(array('controller' => 'users', 'action' => 'forgot_password'));
        } else {
          $this->Session->setFlash(__('Email address not found in our record.', true), 'flash_error');
        }
      }
    }
  }

  function get_password($email = null, $verification_code = null) {
    $email = base64_decode($email);
    $userDetail = $this->User->find("first", array('conditions' => array('User.email' => $email)));
    if ($this->User->hasAny(
            array(
              'User.email' => $email,
              'User.verification_code' => $verification_code
            )
        )) {
      if (!empty($this->request->data)) {
        $this->User->set($this->request->data);
        $this->User->setValidation('change_password');
        if ($this->User->validates($this->request->data)) {
          $this->request->data['User']['id'] = $userDetail['User']['id'];
          $this->request->data['User']['password'] = Security::hash($this->request->data['User']['password2'], null, true);
          $verification_code = substr(md5(time()), 0, 20);
          $this->request->data['User']['verification_code'] = $verification_code;

          unset($this->request->data['User']['email']);
          if ($this->User->saveAll($this->request->data)) {
            $this->redirect(array('action' => 'password_changed'));
          }
        }
      } else {
        $this->request->data = $this->User->findByEmail($email);
      }
    } else {
      $this->Session->setFlash(__('Invalid Action.'));
    }
    $this->set(compact('email', 'verification_code'));
  }

  function password_changed() {
    $this->set('pageHeading', __('Password changed', true));
  }

  function change_password($id = null) {

    $this->pageTitle = __('Change Password', true);
    if ($this->Auth->user()) {
      if (!empty($this->request->data)) {
        $data = $this->User->findById(array('id' => $this->Auth->user('id')));

        $this->request->data['User']['id'] = $this->Auth->user('id');
        $this->User->set($this->request->data);
        $this->User->setValidation('mobile_change_password');
        if ($this->User->validates()) {
          $new_password = $this->request->data['User']['newpassword2'];
          $this->request->data['User']['password'] = Security::hash($this->request->data['User']['newpassword2'], null, true);
          if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('Password updated successfully', true), 'flash_good');
            $this->redirect(array('controller' => 'users', 'action' => 'update_profile'));
          } else {
            $this->Session->setFlash(__('Please correct the errors listed below.', true), 'flash_error');
            $this->redirect(array('controller' => 'users', 'action' => 'change_password'));
          }
        }
      }
    } else {
      $this->Session->setFlash(__('Please correct the errors listed below.', true), 'flash_error');
      $this->redirect(array('controller' => 'users', 'action' => 'change_password'));
    }
    if ($this->Auth->user('id') != null) {
      $this->request->data = $this->User->findById(array('id' => $this->Auth->user('id')));
      $this->set('profiledata', $this->request->data);
    }
    $this->set('pageHeading', __('Change Password', true));
  }

  public function send_notification_for_iphone($deviceToken = null, $message = null, $type = null, $objectId = null) {
    //$deviceToken = "8e53cb7f723a09dd85c3c60facee1e9305a6449d93b1e967e7e3437c4d723712";
    //$message = "Jai Hind !!!";

    if (strlen($deviceToken) < 40) {
      return false;
    }

    $message = array('aps' => array('alert' => $message, 'type' => $type, 'objId' => $objectId));


    // Put your private key's passphrase here:
    $passphrase = 'octal123';

    // Put your alert message here:			
    $base_path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR;
    //echo $base_path;die;
    //ck_dev.pem
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/push_prod_v2.pem');
    //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/push_production_new.pem');
    //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/apns-ProdRenewedCk.pem');
    //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    //gateway.sandbox.push.apple.com
    // Open a connection to the APNS server

    $fp = stream_socket_client(
        'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx
    );

    if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

    //echo 'Connected to APNS' . PHP_EOL;


    $message['aps']['sound'] = 'default';

    $body = json_encode($message);

    $message = $body;

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($message)) . $message;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));
    // Close the connection to the server
    fclose($fp);
  }

  function send_notification_to_users($deviceToken = null, $type = 100) {
    //$deviceToken = 'f8Z0wXDq4U1Mg8ZJD07cfP:APA91bE_8HVa22Q-wwf8V3sPZBiNgDxFQgE9ivChHnuEn8bpC-0aPPUICGgBVD8_mG7sdKyskR8n5Hlyo8oqBKbusIaIuBDJgUVQ3mLWfI_JDVsEnrYoTH_T7vdMoo4cBgk5-D61o5VJ';
    $url = "https://fcm.googleapis.com/fcm/send";
    $serverKey = 'AAAA1hU4Sr0:APA91bGMiE06_3FPCqhYAOs8XPozornWTbTihv1uItLLY3Ip6d6tpxWRwYJsyd4UlhfPrk-apOXe35tMc9M3eu101szvz2Ip7eElFjYEit1LjoGV59Vgr5k7lFFA_5c1pYdARPvnve8v';

    $notification = array('type' => $type);
    $arrayToSend = array('to' => $deviceToken, 'notification' => $notification, 'priority' => 'high', 'content_available' => true);
    $json = json_encode($arrayToSend);

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=' . $serverKey;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //Send the request
    $response = @curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  function send_notification_to_android_users($deviceToken = null, $type = 100, $objectId = 1, $iindObjectId = 1, $badge = 1) {

    $url = "https://fcm.googleapis.com/fcm/send";
    $serverKey = 'AAAA1hU4Sr0:APA91bGMiE06_3FPCqhYAOs8XPozornWTbTihv1uItLLY3Ip6d6tpxWRwYJsyd4UlhfPrk-apOXe35tMc9M3eu101szvz2Ip7eElFjYEit1LjoGV59Vgr5k7lFFA_5c1pYdARPvnve8v';

    $notification = array('title' => 'banned', 'text' => 'banned', 'body' => 'banned', 'badge' => $badge, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'sound' => $sound);
    $arrayToSend = array('to' => $deviceToken, 'notification' => $notification, 'priority' => 'high');
    $json = json_encode($arrayToSend);

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=' . $serverKey;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //Send the request
    $response = @curl_exec($ch);
    curl_close($ch);
    return $response;
  }

}
