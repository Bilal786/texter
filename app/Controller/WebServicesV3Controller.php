<?php

App::uses('CakeEmail', 'Network/Email');

class WebServicesv3Controller extends AppController {

    var $name = 'Webservicesv3';
    var $components = array('Upload');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = '';
        $this->autoRender = false;
        $this->Auth->allow('*');
    }

    public function archivemessages() {
        try {

            //$con1 = mysqli_connect("dev-applications-rds.co0vummzoqwg.us-east-1.rds.amazonaws.com","dev_txtter","gGjD2C7KdCsuEWQ8","dev_txtter_openfire");
            $con1 = mysqli_connect("mysqltxtterserver.c8ezemdemcoy.us-west-2.rds.amazonaws.com", "db-txtter-octal", "7cKv[JfJvcKH#as1", "openfire_20170103");
            //$con1 = mysqli_connect("mysqltxtterserver.c8ezemdemcoy.us-west-2.rds.amazonaws.com","txt_openfire","JJV7f12Iaz#mGpGX1EW","txtter_openfire_20160809");
            if ($con1) {
                $username = $_POST['username'];
                $toJID = $username;
                $start_date = $_POST['start_date'];

                //echo $dt = date("Y-m-d", strtotime($start_date)."000");

                $date = new DateTime($start_date);
                $start_time = $date->getTimestamp() * 1000;
                $date->add(new DateInterval('PT2M'));
                $end_time = $date->getTimestamp() * 1000;

                $sql = "SELECT conversationID as messageID,fromJID, body, sentDate from ofmessagearchive where toJID = '" . $toJID . "' AND sentDate BETWEEN $start_time AND $end_time";
                $result = mysqli_query($con1, $sql);
                $row_cnt = mysqli_num_rows($result);
                if ($row_cnt > 0) {
                    $resp = array();
                    while ($row = mysqli_fetch_assoc($result)) {
                        $resp[] = $row;
                    }
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = 201;
                    $responseData['message'] = "success";
                    $responseData['response_data'] = $resp;

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else {
                    //no record found
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = 201;
                    $responseData['message'] = "success";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                //fail to connect db
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "Database connection fail. Please try after some time";
                $responseData['response_data'] = array();

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } catch (Exception $e) {
            
        }
    }

    public function register($test_data = null) {
        //return 'rrrS';
        $decoded['email'] = $_POST['email'];
        $decoded['userName'] = $_POST['userName'];
        $decoded['fullName'] = $_POST['fullName'];
        $decoded['password'] = $_POST['password'];
        $decoded['country'] = $_POST['country'];
        $decoded['device_type'] = $_POST['device_type'];
        $decoded['device_id'] = $_POST['device_id'];
        $decoded['interests'] = $_POST['interests'];
        $decoded['gender'] = $_POST['gender'];
        $decoded['age'] = $_POST['age'];
        $decoded['profile_status_message'] = $_POST['profile_status_message'];
        $decoded['device_unique_id'] = $_POST['device_unique_id'];

        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded)) {
            if (!empty($decoded['email']) && !empty($decoded['fullName']) && !empty($decoded['userName']) && !empty($decoded['password'])) {
                $decoded['email'] = ltrim($decoded['email'], '0');

                $decoded['fullName'] = ltrim($decoded['fullName'], '0');

                $decoded['userName'] = ltrim($decoded['userName'], '0');

                $decoded['password'] = ltrim($decoded['password'], '0');

                $email_check = $this->check_email($decoded['email'], null);

                $user_data = $this->User->find('first', array('conditions' => array('User.email' => $decoded['email'])));
                $user_data_username = $this->User->find('first', array('conditions' => array('User.username' => $decoded['userName'])));

                $already_exist_id = $user_data['User']['id'];
                $already_exist_username_id = $user_data_username['User']['id'];

                if (!empty($already_exist_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Email already registered.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else if (!empty($already_exist_username_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "The username has been taken already.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }

            $authKey = $this->auth_key(9);
            $email = $decoded['email'];
            $password = $decoded['password'];
            $userName = $decoded['userName'];
            $fullName = $decoded['fullName'];
            $country = $decoded['country'];
            $interests = $decoded['interests'];
            $age = $decoded['age'];
            $gender = $decoded['gender'];
            $profile_status_message = $decoded['profile_status_message'];
            $device_unique_id = $decoded['device_unique_id'];
            $userdata = array(
                'email' => $email,
                'jid' => '',
                'full_name' => $fullName,
                'username' => $userName,
                'country' => $country,
                'user_registration_date' => date("Y-m-d H:i:s", strtotime('now')),
                'service_expire_date' => date("Y-m-d H:i:s", (strtotime('now') + (365 * 24 * 60 * 60))),
                'status' => 0,
                'password' => Security::hash($password, null, true),
                'interests' => $interests,
                'age' => $age,
                'gender' => $gender,
                'profile_status_message' => $profile_status_message,
                'device_unique_id' => $device_unique_id,
                'created' => date("Y-m-d H:i:s", strtotime('now')),
                'modified' => date("Y-m-d H:i:s", strtotime('now'))
            );

            if (!empty($already_exist_id)) {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "Username or email not available";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }

            if ($this->User->save($userdata)) {
                $UserId = $this->User->getLastInsertId();

                $responseData['response_data']['is_already_exist'] = 0;

                if (empty($UserId)) {
                    $UserId = $already_exist_id;
                    $responseData['response_data']['is_already_exist'] = 1;
                }

                $jid = $UserId . "@" . Configure::read('App.open_fire_host');

                $jidData['User']['jid'] = $jid;
                $this->User->save($jidData);

                $device_type = $decoded['device_type'];
                $device_id = $decoded['device_id'];

                $devicedData['User']['device_type'] = $device_type;
                $devicedData['User']['device_id'] = $device_id;
                $this->User->save($devicedData);

                /* -------------------user location -------------------- */
                $this->loadModel('UserLocation');
                $userLocationData = array();
                $userLocationData['UserLocation']['user_id'] = $UserId;
                $userLocationData['UserLocation']['lat'] = $_POST['lat'];
                $userLocationData['UserLocation']['lon'] = $_POST['lon'];
                $userLocationData['UserLocation']['hide_from_map'] = 0;
                $this->UserLocation->save($userLocationData);
                /* -------------------user avatar upload part ----------------------------- */
                App::import('Lib', 'S3');
                if (!empty($_FILES)) {
                    if ($_FILES['image']['size'] < 6291456) {
                        $thumbRules = array('size' => array(100, 100), 'type' => 'resizecrop');
                        $largeRules = array('size' => array(500, 500), 'type' => 'resizecrop');
                        $path_info = pathinfo($_FILES['image']['name']);

                        $_FILES['image']['name'] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                        $res1 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS, '', $thumbRules, array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));
                        $res2 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS, '', $largeRules, array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));
                        $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                        $this->loadModel('UserImage');

                        if (!empty($this->Upload->result)) {
                            $userImage = $this->Upload->result;

                            if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result)) {
                                @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $userImage);
                            }
                            if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result)) {
                                @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $userImage);
                            }
                            if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result)) {
                                @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $userImage);
                            }

                            $result['response_data']['image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/large/" . $userImage; //SITE_URL . "/uploads/user_images/large/" . $userImage;

                            $profileData = array();
                            $profileData['UserImage']['user_id'] = $UserId;
                            $profileData['UserImage']['image_type'] = $_POST['image_type'];
                            $profileData['UserImage']['image'] = $userImage;

                            if ($this->UserImage->save($profileData)) {
                                $result['response_data']['image_type'] = $_POST['image_type'];
                            }
                        }
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $_FILES['image']['name']);
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $_FILES['image']['name']);
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $_FILES['image']['name']);
                    }
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 404;
                    $responseData['message'] = "No profile photo data found in the request";
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $user_data_new = $this->User->find('first', array('conditions' => array('User.id' => $UserId)));

                $responseData['response_data']['userId'] = $UserId;
                $responseData['response_data']['username'] = $userName;
                $responseData['response_data']['jid'] = $UserId . "@" . Configure::read('App.open_fire_host');
                $responseData['response_data']['email'] = $email;
                $responseData['response_data']['full_name'] = $fullName;
                $responseData['response_data']['country'] = $country;
                $responseData['response_data']['is_verify'] = $user_data_new['User']['is_verify'];
                $responseData['response_data']['age'] = $user_data_new['User']['age'];
                $responseData['response_data']['gender'] = $user_data_new['User']['gender'];

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function stagging_register($test_data = null) {

        $decoded['email'] = $_POST['email'];
        $decoded['userName'] = $_POST['userName'];
        $decoded['fullName'] = $_POST['fullName'];
        $decoded['password'] = $_POST['password'];
        $decoded['country'] = $_POST['country'];
        $decoded['device_type'] = $_POST['device_type'];
        $decoded['device_id'] = $_POST['device_id'];
        $decoded['interests'] = $_POST['interests'];
        $decoded['gender'] = $_POST['gender'];
        $decoded['age'] = $_POST['age'];
        $decoded['profile_status_message'] = $_POST['profile_status_message'];
        $decoded['device_unique_id'] = $_POST['device_unique_id'];
        $decoded['personality_type'] = $_POST['personality_type'];
        $decoded['looking_for'] = $_POST['looking_for'];

        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded)) {
            if (!empty($decoded['email']) && !empty($decoded['fullName']) && !empty($decoded['userName']) && !empty($decoded['password'])) {
                $decoded['email'] = ltrim($decoded['email'], '0');

                $decoded['fullName'] = ltrim($decoded['fullName'], '0');

                $decoded['userName'] = ltrim($decoded['userName'], '0');

                $decoded['password'] = ltrim($decoded['password'], '0');

                $email_check = $this->check_email($decoded['email'], null);

                $user_data = $this->User->find('first', array('conditions' => array('User.email' => $decoded['email'])));
                $user_data_username = $this->User->find('first', array('conditions' => array('User.username' => $decoded['userName'])));

                $already_exist_id = $user_data['User']['id'];
                $already_exist_username_id = $user_data_username['User']['id'];

                if (!empty($already_exist_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Email already registered.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else if (!empty($already_exist_username_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "The username has been taken already.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }

            $authKey = $this->auth_key(9);
            $email = $decoded['email'];
            $password = $decoded['password'];
            $userName = $decoded['userName'];
            $fullName = $decoded['fullName'];
            $country = $decoded['country'];
            $interests = $decoded['interests'];
            $age = $decoded['age'];
            $gender = $decoded['gender'];
            $profile_status_message = $decoded['profile_status_message'];
            $device_unique_id = $decoded['device_unique_id'];
            $personality_type = $decoded['personality_type'];
            $looking_for = $decoded['looking_for'];
            $userdata = array(
                'email' => $email,
                'jid' => '',
                'full_name' => $fullName,
                'username' => $userName,
                'country' => $country,
                'user_registration_date' => date("Y-m-d H:i:s", strtotime('now')),
                'service_expire_date' => date("Y-m-d H:i:s", (strtotime('now') + (365 * 24 * 60 * 60))),
                'status' => 0,
                'password' => Security::hash($password, null, true),
                'interests' => $interests,
                'age' => $age,
                'gender' => $gender,
                'profile_status_message' => $profile_status_message,
                'device_unique_id' => $device_unique_id,
                'personality_type' => $personality_type,
                'looking_for' => $looking_for,
                'created' => date("Y-m-d H:i:s", strtotime('now')),
                'modified' => date("Y-m-d H:i:s", strtotime('now'))
            );

            if (!empty($already_exist_id)) {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "Username or email not available";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }

            if ($this->User->save($userdata)) {

                $UserId = $this->User->getLastInsertId();

                $responseData['response_data']['is_already_exist'] = 0;

                if (empty($UserId)) {
                    $UserId = $already_exist_id;
                    $responseData['response_data']['is_already_exist'] = 1;
                }

                $jid = $UserId . "@" . Configure::read('App.open_fire_host');

                $jidData['User']['jid'] = $jid;
                $this->User->save($jidData);

                $device_type = $decoded['device_type'];
                $device_id = $decoded['device_id'];

                $devicedData['User']['device_type'] = $device_type;
                $devicedData['User']['device_id'] = $device_id;
                $this->User->save($devicedData);

                /* -------------------user location -------------------- */
                $this->loadModel('UserLocation');

                $userLocationData = array();
                $userLocationData['UserLocation']['user_id'] = $UserId;
                $userLocationData['UserLocation']['lat'] = $_POST['lat'];
                $userLocationData['UserLocation']['lon'] = $_POST['lon'];
                $userLocationData['UserLocation']['hide_from_map'] = 0;

                $this->UserLocation->save($userLocationData);

                /* -------------------user avatar upload part ----------------------------- */
                App::import('Lib', 'S3');
                if (!empty($_FILES)) {
                    if ($_FILES['image']['size'] < 6291456) {
                        $thumbRules = array('size' => array(100, 100), 'type' => 'resizecrop');
                        $largeRules = array('size' => array(500, 500), 'type' => 'resizecrop');
                        $path_info = pathinfo($_FILES['image']['name']);

                        $_FILES['image']['name'] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                        $res1 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS, '', $thumbRules, array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));
                        $res2 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS, '', $largeRules, array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));
                        $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                        $this->loadModel('UserImage');

                        if (!empty($this->Upload->result)) {
                            $userImage = $this->Upload->result;

                            if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result)) {
                                @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $userImage);
                            }
                            if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result)) {
                                @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $userImage);
                            }
                            if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result)) {
                                @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $userImage);
                            }

                            $result['response_data']['image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/large/" . $userImage; //SITE_URL . "/uploads/user_images/large/" . $userImage;

                            $profileData = array();
                            $profileData['UserImage']['user_id'] = $UserId;
                            $profileData['UserImage']['image_type'] = $_POST['image_type'];
                            $profileData['UserImage']['image'] = $userImage;

                            if ($this->UserImage->save($profileData)) {
                                $result['response_data']['image_type'] = $_POST['image_type'];
                            }
                        }
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $_FILES['image']['name']);
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $_FILES['image']['name']);
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $_FILES['image']['name']);
                    }
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 404;
                    $responseData['message'] = "No profile photo data found in the request";
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $user_data_new = $this->User->find('first', array('conditions' => array('User.id' => $UserId)));

                $responseData['response_data']['userId'] = $UserId;
                $responseData['response_data']['username'] = $userName;
                $responseData['response_data']['jid'] = $UserId . "@" . Configure::read('App.open_fire_host');
                $responseData['response_data']['email'] = $email;
                $responseData['response_data']['full_name'] = $fullName;
                $responseData['response_data']['country'] = $country;
                $responseData['response_data']['is_verify'] = $user_data_new['User']['is_verify'];
                $responseData['response_data']['age'] = $user_data_new['User']['age'];
                $responseData['response_data']['gender'] = $user_data_new['User']['gender'];

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function forget_password($test_data = null) {

        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded) && !empty($decoded['email'])) {
            $user_data = $this->User->find('first', array('conditions' => array('User.email' => $decoded['email'])));

            if (!empty($user_data)) {
                $user_data['User']['id'] = $user_data['User']['id'];
                $user_data['User']['password_token'] = Security::hash($user_data['User']['id'] . $user_data['User']['email'], null, true);
                $this->User->save($user_data);

                $Email = new CakeEmail();
                //$from = Configure::read('App.AdminMail');
                $Email->config('gmail1');
                $Email->emailFormat('html');
                //$Email->from(array('signup@mapchatapp.com' => 'Mapchat Team'));
                $Email->from(array('help@mapbuzz.com' => 'Mapchat Team'));
                //$email->from($from);
                $Email->to($user_data['User']['email']);
                $Email->subject('Mapchat Reset Password');
                $facebookLink = "https://www.facebook.com/mapmate";
                $instagramLink = "";
                $twitterLink = "https://www.twitter.com/mapmateapp";
                $youtubeLink = "";

                $facebook = 'https://d35ugz1sdahij6.cloudfront.net/img/facebook_new.png';
                $twitter = 'https://d35ugz1sdahij6.cloudfront.net/img/twitter_new.png';
                $instagram = 'https://d35ugz1sdahij6.cloudfront.net/img/instagram_new.png';
                $youtube = 'https://d35ugz1sdahij6.cloudfront.net/img/youtube_new.png';

                // $user_image = 'https://d35ugz1sdahij6.cloudfront.net/uploads/img/'. "Placeholder.png";
                $link = "https://admin.mapbuzz.com/reset?token=" . $user_data['User']['password_token'];
                $mainbg = 'https://d35ugz1sdahij6.cloudfront.net/img/main-bg.png';
                $Brand = 'https://d35ugz1sdahij6.cloudfront.net/img/14.png';
                $lg = 'https://d35ugz1sdahij6.cloudfront.net/img/reset-logo.png';
                $user_fullname = $user_data['User']['full_name'];
                $module = <<<EOD
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mapmeta App</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="$lg" sizes="32x32">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
    <style type="text/css">
        @media (max-device-width: 620px) {
            .container { width: 90% !important; }
        }
        .main {
            background-repeat: no-repeat;
            background-color: #00aef0;
            background-position: center top;
            background-size: 100% 100%;
        }
        .brand { max-width: 40%; }
        @media (max-device-width: 568px) {
            .main { padding: 40px 0 !important; }
            .brand { max-width: 60% !important; }
            .user-box { padding-top: 30px !important; }
            .user-img { width: 140px !important;height: 140px !important; }
            .name-title { font-size: 26px !important; }
            .forgot-text { font-size: 18px !important;padding: 20px 0 !important; }
            .button { width: 100% !important; }
            .social-title { padding: 20px 0 !important;font-size: 16px !important; }
            .footer table { width: 100% !important; }
            .footer table tr td img { max-width: 70% !important; }
        }
    </style>

</head>

<body style=" width: 100%; text-align: center; margin: 0px;">
    <table cellpadding="0" cellspacing="0" border="0" style="font-family: 'Montserrat', sans-serif; padding-top: 120px; padding-bottom: 120px;" background="$mainbg" width="100%" class="main">
        <tr>
            <td align="center">
                <table cellspacing="0" cellpadding="0" border="0" width="600px" class="container">
                    <tr>
                        <td style="text-align: center;">
                            <img class="brand" src="$Brand">
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="text-align: center; font-size:36px; padding-top: 20px; font-weight: 400; color: #fff;" class="name-title">Welcome $user_fullname</td>
                    </tr>
                    <tr>
                        <td style="text-align: center; padding: 40px 0; font-size: 22px; color: #fff;" class="forgot-text">You can reset your password by Mapmeta <br> Click on the below button.
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <a href="$link" target="_blank">
                                <button class="button" style="display: inline-block; width: 35%; height: 58px; box-shadow: none; border: 0px; background: #ed008c; color: #fff; font-size: 26px; cursor: pointer;">
                                    Forget Password
                                </button>
                            </a>
                        </td>
					</tr>
					<tr>
					<td style="text-align: center; color: #fff; font-size: 20px; padding: 40px 0;" class="social-title">Connect with us on social media</td>
				</tr>
				<tr>
					<td class="footer" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="50%">
							<tr>
								<td style="text-align:center">
									<a href="$facebookLink"><img src="$facebook" /></a>
									<a href="$twitterLink"><img src="$twitter" /></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
EOD;

                // $Email->send('You can reset your password by clicking the following <a href="https://admin.mapbuzz.com/reset?token=' . $user_data['User']['password_token'] . '" style="color: rgb(9, 129, 190);" target="_blank">link</a>');
                $Email->send($module);

                //success
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "Plase check your email inbox";

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "Email not found in our database";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function change_password() {
        $this->loadModel('User');

        $token = $_POST['token'];
        $password = $_POST['password'];
        $password_confirm = $_POST['password_confirm'];

        if (!empty($token) && $password == $password_confirm) {
            if (strlen($password) > 5) {
                $user_data = $this->User->find('first', array('conditions' => array('User.password_token' => $token)));

                if (!empty($user_data)) {
                    $user_data['User']['id'] = $user_data['User']['id'];
                    $user_data['User']['password'] = Security::hash($password, null, true);
                    $user_data['User']['password_token'] = NULL;
                    $this->User->save($user_data);
                    $this->redirect(array("controller" => "Passwordreset", "action" => "index?success=1"), $status, $exit);
                } else {
                    $this->redirect(array("controller" => "Passwordreset", "action" => "index?error=99&token=" . $_POST['token']), $status, $exit);
                }
            } else {
                $this->redirect(array("controller" => "Passwordreset", "action" => "index?error=2&token=" . $_POST['token']), $status, $exit);
            }
        } else {
            $this->redirect(array("controller" => "Passwordreset", "action" => "index?error=1&token=" . $_POST['token']), $status, $exit);
        }
    }

    public function merge_account($test_data = null) {

        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $this->loadModel('User');

        $responseData = array();

        //merge by username !!!
        if (!empty($decoded['id']) && !empty($decoded['email']) && !empty($decoded['userName']) && !empty($decoded['password'])) {
            $user_data = $this->User->find('first', array('conditions' => array('User.userName' => $decoded['userName'])));

            if (!empty($user_data)) {
                $profileData['User']['id'] = $decoded['id'];
                $profileData['User']['email'] = $decoded['email'];
                $profileData['User']['password'] = $decoded['password'];

                $this->User->save($profileData); //success
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "User data updated successfully";

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "User not found in our database";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function logout($test_data = null) {

        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded) && !empty($decoded['id'])) {
            $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['id'])));

            if (!empty($user_data)) {
                $profileData['User']['id'] = $user_data['User']['id'];
                $profileData['User']['device_id'] = '';
                //$profileData['User']['device_type'] = '';
                $profileData['User']['voip_token'] = '';
                $this->User->save($profileData);

                //success
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "Logout successfull";

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "Email not found in our database";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function update_email() {
        $this->loadModel('User');

        $result = array();
        $result['response_status'] = 1;
        $result['response_code'] = 200;
        $result['response_message_code'] = '';
        $result['message'] = 'success';
        $result['response_data'] = array();

        $email = trim($_POST['email']);
        $userid = $_POST['userid'];

        $user_data = $this->User->find('first', array('conditions' => array('User.email' => $email, 'NOT' => array('User.id' => $userid))));
        $already_exist_id = $user_data['User']['id'];

        if (isset($already_exist_id) && $already_exist_id != "") {
            $responseData['response_data']['is_already_exist'] = 1;
            $result['message'] = 'Email already exist!';
        } else {
            $profileData['User']['id'] = $userid;
            $profileData['User']['email'] = $email;
            $this->User->save($profileData);
        }

        $jsonEncode = json_encode($result);
        return $jsonEncode;
    }

    public function e_register($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded)) {

            if (!empty($decoded['email'])) {
                $decoded['email'] = ltrim($decoded['email'], '0');

                $decoded['fullName'] = ltrim($decoded['fullName'], '0');

                $decoded['userName'] = ltrim($decoded['userName'], '0');

                $decoded['password'] = ltrim($decoded['fullName'], '0');

                $email_check = $this->check_email($decoded['email'], null);
                $user_data = $this->User->find('first', array('conditions' => array('User.email' => $decoded['email'])));
                $already_exist_id = $user_data['User']['id'];
            }

            $authKey = $this->auth_key(9);
            $email = $decoded['email'];
            $password = $decoded['password'];
            $userName = $decoded['userName'];
            $fullName = $decoded['fullName'];
            $userdata = array(
                'email' => $email,
                'jid' => $email,
                'full_name' => $fullName,
                'username' => $userName,
                'user_registration_date' => date("Y-m-d H:i:s", strtotime('now')),
                'service_expire_date' => date("Y-m-d H:i:s", (strtotime('now') + (365 * 24 * 60 * 60))),
                'status' => 0,
                'password' => Security::hash($password, null, true),
                'created' => date("Y-m-d H:i:s", strtotime('now')),
                'modified' => date("Y-m-d H:i:s", strtotime('now'))
            );

            if (!empty($already_exist_id)) {
                $userdata['id'] = $already_exist_id;
            }

            if ($this->User->save($userdata)) {
                $UserId = $this->User->getLastInsertId();

                $responseData['response_data']['is_already_exist'] = 0;

                if (empty($UserId)) {
                    $UserId = $already_exist_id;
                    $responseData['response_data']['is_already_exist'] = 1;
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";

                $responseData['response_data']['userId'] = $UserId;
                $responseData['response_data']['username'] = $userName;
                $responseData['response_data']['jid'] = $email;
                $responseData['response_data']['email'] = $email;
                $responseData['response_data']['full_name'] = $fullName;

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function login($test_data = null) {

        $decoded = array(
            "userName" => $_POST["userName"],
            "password" => $_POST["password"],
            "country" => $_POST['country'],
            "device_id" => $_POST['device_id'],
            "device_type" => $_POST['device_type'],
            "device_unique_id" => $_POST['device_unique_id'],
        );
        $this->loadModel('User');
        $this->loadModel('DeviceBans');

        $responseData = array();

        if (!empty($decoded)) {

            if (!empty($decoded['userName']) && !empty($decoded['password'])) {

                $decoded['userName'] = ltrim($decoded['userName'], '0');
                $decoded['password'] = ltrim($decoded['password'], '0');
                $decoded['device_id'] = $decoded['device_id'] ? $decoded['device_id'] : "";
                $decoded['device_type'] = $decoded['device_type'] ? $decoded['device_type'] : 0;
                $decoded['device_unique_id'] = $decoded['device_unique_id'] ? $decoded['device_unique_id'] : '';
                //$user_data = $this->User->find('first', array('conditions' => array('User.username' => $decoded['userName'], 'User.password' => Security::hash($decoded['password'], null, true))));
                $user_data = $this->User->find('first', array('conditions' => array('User.password' => Security::hash($decoded['password'], null, true), "OR" => array('User.username' => $decoded['userName'], 'User.email' => $decoded['userName']))));
                if (!empty($user_data['User']['id'])) {

                    $login_ban = $user_data['User']['login_ban'];
                    //check user ban
                    if (!empty($login_ban) && $login_ban == '1') {
                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 401;
                        $responseData['message'] = "User is baned to access the app.";
                        $responseData['response_data'] = [];

                        $data = array('response' => $responseData);
                        $jsonEncode = json_encode($data);
                        return $jsonEncode;
                    }

                    //check device ban
                    $user_device_status = $this->DeviceBans->find('first', array(
                        'conditions' => array(
                            'DeviceBans.device_unique_id' => $_POST['device_unique_id']
                        )
                    ));
                    if (!empty($user_device_status)) {
                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 401;
                        $responseData['message'] = "This device is baned to access the app.";
                        $responseData['response_data'] = [];

                        $data = array('response' => $responseData);
                        $jsonEncode = json_encode($data);
                        return $jsonEncode;
                    }

                    $profileData['User']['id'] = $user_data['User']['id'];
                    $profileData['User']['country'] = $decoded['country'];
                    $profileData['User']['device_id'] = $decoded['device_id'] ? $decoded['device_id'] : "";
                    $profileData['User']['device_type'] = $decoded['device_type'] ? $decoded['device_type'] : 0;
                    $profileData['User']['device_unique_id'] = $decoded['device_unique_id'] ? $decoded['device_unique_id'] : '';

                    $this->User->save($profileData);

                    //if($user_data['User']['is_email_verified']==1){
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['message'] = "success";

                    $responseData['response_data']['userId'] = $user_data['User']['id'];
                    $responseData['response_data']['username'] = $user_data['User']['username'];
                    $responseData['response_data']['jid'] = $user_data['User']['jid'];
                    $responseData['response_data']['email'] = $user_data['User']['email'];
                    $responseData['response_data']['full_name'] = $user_data['User']['full_name'];
                    $responseData['response_data']['country'] = $user_data['User']['country'];
                    $responseData['response_data']['status'] = $user_data['User']['status'];
                    $responseData['response_data']['is_email_verified'] = $user_data['User']['is_email_verified'];
                    $responseData['response_data']['registrationStatus'] = ($user_data['User']['email'] != "") ? "email" : "phone";
                    $responseData['response_data']['is_verify'] = $user_data['User']['is_verify'];
                    $responseData['response_data']['age'] = $user_data['User']['age'];
                    $responseData['response_data']['gender'] = $user_data['User']['gender'];
                    // $responseData['response_data'][] = $responseuserData;
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Username or password is invalid.";
                    $responseData['response_data'] = [];

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = "Please provide username and password.";
                $responseData['response_data'] = array();

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function UpdateDeviceToken($test_data = null) {

        $decoded = array(
            "userId" => $_POST['userId'],
            "device_id" => $_POST['device_id'],
            "device_type" => $_POST['device_type']
        );
        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded)) {

            if (!empty($decoded['device_id']) && !empty($decoded['device_type'])) {

                $decoded['device_id'] = $decoded['device_id'] ? $decoded['device_id'] : "";
                $decoded['device_type'] = $decoded['device_type'] ? $decoded['device_type'] : 0;
                //$user_data = $this->User->find('first', array('conditions' => array('User.username' => $decoded['userName'], 'User.password' => Security::hash($decoded['password'], null, true))));
                $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['userId'])));
                if (!empty($user_data['User']['id'])) {

                    $profileData['User']['id'] = $user_data['User']['id'];
                    $profileData['User']['device_id'] = $decoded['device_id'] ? $decoded['device_id'] : "";
                    $profileData['User']['device_type'] = $decoded['device_type'] ? $decoded['device_type'] : 0;

                    $this->User->save($profileData);

                    //if($user_data['User']['is_email_verified']==1){
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['message'] = "success";

                    $responseData['response_data']['userId'] = $user_data['User']['id'];
                    $responseData['response_data']['username'] = $user_data['User']['username'];
                    $responseData['response_data']['jid'] = $user_data['User']['jid'];
                    $responseData['response_data']['email'] = $user_data['User']['email'];
                    $responseData['response_data']['full_name'] = $user_data['User']['full_name'];
                    $responseData['response_data']['status'] = $user_data['User']['status'];
                    $responseData['response_data']['is_email_verified'] = $user_data['User']['is_email_verified'];
                    $responseData['response_data']['registrationStatus'] = ($user_data['User']['email'] != "") ? "email" : "phone";
                    // $responseData['response_data'][] = $responseuserData;
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "UserId is invalid.";
                    $responseData['response_data'] = [];

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = "Please provide deviceid and devicetype.";
                $responseData['response_data'] = array();

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function email_register($test_data = null) {

        $decoded = array(
            "email" => $_POST["email"],
            "fullName" => $_POST["fullName"],
            "userName" => $_POST["userName"],
            "password" => $_POST["password"],
            "phoneNumber" => $_POST["phoneNumber"]
        );

        $this->loadModel('User');

        $responseData = array();

        if (!empty($decoded)) {

            if (!empty($decoded['email']) && !empty($decoded['userName'])) {
                $decoded['email'] = ltrim($decoded['email'], '0');
                $decoded['fullName'] = ltrim($decoded['fullName'], '0');
                $decoded['userName'] = ltrim($decoded['userName'], '0');
                $decoded['password'] = ltrim($decoded['password'], '0');
                $decoded['phoneNumber'] = ltrim($decoded['phoneNumber'], '0');

                $email_check = $this->check_email($decoded['email'], null);
                $user_data = $this->User->find('first', array('conditions' => array('User.email' => $decoded['email'])));
                $user_data_username = $this->User->find('first', array('conditions' => array('User.username' => $decoded['userName'])));

                $decoded['phoneNumber'] = str_replace("+", "", $decoded['phoneNumber']);

                $user_data_phonenumber = $this->User->find('first', array('conditions' => array('User.phone_number' => $decoded['phoneNumber'])));
                $already_exist_id = $user_data['User']['id'];
                $already_exist_username_id = $user_data_username['User']['id'];
                $already_exist_phonenumber_id = $user_data_phonenumber['User']['id'];

                $authKey = $this->auth_key(9);
                $email = $decoded['email'];
                $password = $decoded['password'];
                $userName = $decoded['userName'];
                $fullName = $decoded['fullName'];
                $phone_number = $decoded['phoneNumber'];
                $verification_code = $this->verification_code(4);
                $userdata = array(
                    'email' => $email,
                    'jid' => $userName . "@" . Configure::read('App.open_fire_host'),
                    'full_name' => $fullName,
                    'username' => $userName,
                    'phone_number' => $phone_number,
                    'user_registration_date' => date("Y-m-d H:i:s", strtotime('now')),
                    'service_expire_date' => date("Y-m-d H:i:s", (strtotime('now') + (365 * 24 * 60 * 60))),
                    'status' => 1,
                    'is_email_verified' => 0,
                    'verification_code' => $verification_code,
                    'password' => Security::hash($password, null, true),
                    'created' => date("Y-m-d H:i:s", strtotime('now')),
                    'modified' => date("Y-m-d H:i:s", strtotime('now'))
                );

                if (!empty($already_exist_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Email already registered.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else if (!empty($already_exist_username_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Username already registered.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else if (!empty($already_exist_phonenumber_id)) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Phone already registered.";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else {

                    if ($this->User->save($userdata)) {
                        $UserId = $this->User->getLastInsertId();

                        try {

                            $this->send_sms($phone_number, $verification_code);
                        } catch (Exception $e) {
                            
                        }


                        $responseData['response_data']['is_already_exist'] = 0;

                        if (empty($UserId)) {
                            $UserId = $already_exist_id;
                            $responseData['response_data']['is_already_exist'] = 1;
                        }

                        $responseData['response_status'] = 1;
                        $responseData['response_code'] = 200;
                        $responseData['response_message_code'] = '';
                        $responseData['message'] = "success";

                        $responseData['response_data']['userId'] = $UserId;
                        $responseData['response_data']['username'] = $userName;
                        $responseData['response_data']['jid'] = $userName . "@" . Configure::read('App.open_fire_host');
                        $responseData['response_data']['email'] = $email;
                        $responseData['response_data']['full_name'] = $fullName;
                        $responseData['response_data']['registrationStatus'] = "email";
                        $responseData['response_data']['is_email_verified'] = 0;

                        $data = array('response' => $responseData);
                        $jsonEncode = json_encode($data);
                        return $jsonEncode;
                    } else {

                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 401;
                        $responseData['message'] = "Something went wrong. User registration fail. Please try again.";
                        $responseData['response_data'] = array();

                        $data = array('response' => $responseData);
                        $jsonEncode = json_encode($data);
                        return $jsonEncode;
                    }
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = "Please provide email and username.";
                $responseData['response_data'] = array();

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    //get profile pic
    public function get_user_profilepic($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $contactArr = array();
        $responseData = array();
        $decoded = array("jid" => $_POST["jid"]);
        if (!empty($decoded)) {
            $this->loadModel('User');
            $this->loadModel('UserImage');

            $profiledata = $this->User->find('first', array('fields' => array('id', 'jid'), 'conditions' => array('User.jid' => $decoded['jid'])));

            $profile_user_id = $profiledata['User']['id'];
            if (!empty($profile_user_id)) {
                $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $profiledata['User']['id'], 'UserImage.image_type' => array(1, 2))));
                $profiledata['User']['profile_image'] = "";
                $profiledata['User']['profile_cover_image'] = "";
                foreach ($userImages as $key => $value) {
                    if ($value['UserImage']['image_type'] == 1) {
                        $profiledata['User']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                    }
                    if ($value['UserImage']['image_type'] == 2) {
                        $profiledata['User']['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'success';
                $responseData['response_data']['data'] = $profiledata['User'];

                $now = time(); // or your date as well
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = 'No user data found for provided user JID.';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'No data found';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_group_notofication($test_data = null) {
        //App::import('Lib', 'S3');

        $array = array("groupJabberID" => "groupJabberID", "group_userjabberid" => "group_userjabberid", "message" => "message");

        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');
        $this->loadModel('User');
        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"])));

        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $userids = $group_data["ChatGroup"]["group_userjabberid"];

            $array_ids = explode(",", $userids);
            $to_remove = array($form_data['group_userjabberid']);
            $user_ids = array_diff($array_ids, $to_remove);

            if ($group_data["ChatGroup"]["mute_userjabberid"] != '') {
                $to_remove = explode(",", $group_data["ChatGroup"]["mute_userjabberid"]);
                $user_ids = array_diff($user_ids, $to_remove);
            }
            if (!empty($user_ids)) {
                $user_data = $this->User->find('all', array('fields' => array('User.full_name', 'User.jid', 'User.device_id', 'User.device_type'), 'conditions' => array('User.jid' => $user_ids)));
                foreach ($user_data as $userdata_detail) {
                    // $this->send_notification_for_iphone($userdata_detail['User']['device_id'], $form_data['message']);
                    if ($userdata_detail['User']['device_type'] == 1) {
                        $this->send_notification_for_android($userdata_detail['User']['device_id'], $form_data['message']);
                    } else if ($userdata_detail['User']['device_type'] == 2) {
                        $this->send_notification_for_iphone_test($userdata_detail['User']['device_id'], $form_data['message']);
                    } else {
                        $this->send_notification_for_iphone_test($userdata_detail['User']['device_id'], $form_data['message']);
                    }
                }
            }


            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = 201;
            $responseData['message'] = "Success";
            $responseData['response_data'] = $user_data;

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = "Provided Group id is not found.";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function create_group($test_data = null) {
        App::import('Lib', 'S3');
        $array = array("adminjabberID" => "adminjabberID", "groupname" => "groupname", "groupJabberID" => "groupJabberID", "group_userjabberid" => "group_userjabberid", "group_userName" => "group_userName", "group_userNumber" => "group_userNumber", "group_user_emailId" => "group_user_emailId", "admin_name" => "admin_name", "admin_number" => "admin_number");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');

        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"], 'ChatGroup.adminjabberID' => $form_data["adminjabberID"])));

        if (isset($_FILES['media']) && count($_FILES['media']['error']) == 1 && $_FILES['media']['error'][0] > 0) {
            
        } else {
            if (isset($_FILES['media'])) {
                $icon_name = explode("@", $form_data["groupJabberID"]);
                $image_upload_msg = "";
                if (!$this->save_image_s3bucket($_FILES['media']['tmp_name'], 'groups/' . $icon_name[0] . ".jpg")) {
                    $image_upload_msg = "Group icon not updated";
                }
            }
        }


        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = "Provided Group id is already exist, Please try with different name or call add member service.";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }

        $rt = $this->ChatGroup->save($form_data);

        $this->loadModel('User');
        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"])));
        $form_data['message'] = $group_data['ChatGroup']['admin_name'] . ' added you to a group';
        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $userids = $group_data["ChatGroup"]["group_userjabberid"];

            $array_ids = explode(",", $userids);
            $to_remove = array($form_data['adminjabberID']);
            $user_ids = array_diff($array_ids, $to_remove);

            $user_data = $this->User->find('all', array('fields' => array('User.full_name', 'User.jid', 'User.device_id', 'User.device_type'), 'conditions' => array('User.jid' => $user_ids)));
            foreach ($user_data as $userdata_detail) {
                // $this->send_notification_for_iphone($userdata_detail['User']['device_id'], $form_data['message'],101);
                if ($userdata_detail['User']['device_type'] == 1) {
                    $this->send_notification_for_android($userdata_detail['User']['device_id'], $form_data['message'], 101);
                } else if ($userdata_detail['User']['device_type'] == 2) {
                    $this->send_notification_for_iphone_test($userdata_detail['User']['device_id'], $form_data['message'], 101);
                } else {
                    $this->send_notification_for_iphone_test($userdata_detail['User']['device_id'], $form_data['message'], 101);
                }
            }
        }

        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = 201;
        $responseData['message'] = "Success";
        $responseData['response_data'] = $form_data;

        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function delete_group() {

        $array = array("groupJabberID" => "groupJabberID");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');
        $this->ChatGroup->deleteAll(array('ChatGroup.groupJabberID' => $form_data["groupJabberID"]), false);

        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = 201;
        $responseData['message'] = "Success";
        $responseData['response_data'] = $form_data;

        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function add_user() {
        $array = array("groupJabberID" => "groupJabberID", "adminjabberID" => "adminjabberID", "userJabberID" => "userJabberID");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');
        $this->loadModel('User');

        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"], 'ChatGroup.adminjabberID' => $form_data["adminjabberID"])));

        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $members = $group_data["ChatGroup"]["group_userjabberid"];
            $new_data = array();
            $new_data["ChatGroup"]["id"] = $group_id;
            $new_data["ChatGroup"]["group_userjabberid"] = $group_data["ChatGroup"]["group_userjabberid"] = $members . "," . $form_data["userJabberID"];
            $this->ChatGroup->save($new_data);

            //Notification for admin added user in group start
            $user_data = $this->User->find('first', array('conditions' => array('User.jid' => $form_data["userJabberID"])));
            $message = $group_data['ChatGroup']['admin_name'] . ' added you to a group';

            // $this->send_notification_for_iphone($user_data['User']['device_id'],$message,101);
            if ($user_data['User']['device_type'] == 1) {
                $this->send_notification_for_android($user_data['User']['device_id'], $message, 101);
            } else if ($user_data['User']['device_type'] == 2) {
                $this->send_notification_for_iphone_test($user_data['User']['device_id'], $message, 101);
            } else {
                $this->send_notification_for_iphone_test($user_data['User']['device_id'], $message, 101);
            }
            //Notification for admin added user in group start

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = 201;
            $responseData['message'] = "Success";
            $responseData['response_data'] = $group_data;

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = "Group Id not found.";
            $responseData['response_data'] = [];

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function delete_group_user() {
        $array = array("groupJabberID" => "groupJabberID", "adminjabberID" => "adminjabberID", "userJabberID" => "userJabberID");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');
        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"], 'ChatGroup.adminjabberID' => $form_data["adminjabberID"])));

        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $members = $group_data["ChatGroup"]["group_userjabberid"];
            $members_array = explode(",", $members);
            $about_delete_members = explode(",", $form_data["userJabberID"]);
            $final_array_members = array_diff($members_array, $about_delete_members);
            $final_members = implode(",", $final_array_members);

            $new_data = array();
            $new_data["ChatGroup"]["id"] = $group_id;
            $new_data["ChatGroup"]["group_userjabberid"] = $group_data["ChatGroup"]["group_userjabberid"] = $final_members;
            $this->ChatGroup->save($new_data);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = 201;
            $responseData['message'] = "Success";
            $responseData['response_data'] = $group_data;

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = "Group Id not found.";
            $responseData['response_data'] = [];

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function getgroups() {
        $array = array("userJabberID" => "userJabberID");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');
        $group_data = $this->ChatGroup->find('all', array('conditions' => array('ChatGroup.group_userjabberid LIKE' => "%" . $form_data["userJabberID"] . "%")));

        //print_r($group_data);
        $this->loadModel('User');
        $response_data = array();
        $groupdata_len = count($group_data);
        for ($i = 0; $i < $groupdata_len; $i++) {
            //foreach($group_data as $group){
            $data = $group_data[$i]['ChatGroup'];
            $array = explode(",", $group_data[$i]['ChatGroup']['group_userjabberid']);
            $user_data = $this->User->find('all', array('fields' => array('User.full_name', 'User.jid'), 'conditions' => array('User.jid' => $array)));
            $data['members'] = array();
            foreach ($user_data as $user) {
                array_push($data['members'], $user['User']);
            }
            $response_data[$i] = $data;
        }


        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = 201;
        $responseData['message'] = "Success";
        $responseData['response_data'] = $response_data;

        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;

        //from server
        //https://chat.txtter.com:9991/plugins/restapi/v1/users/918866663190/groups
    }

    public function muteuser() {
        $array = array("groupJabberID" => "groupJabberID", "userJabberID" => "userJabberID");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');

        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"])));

        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $members = $group_data["ChatGroup"]["mute_userjabberid"];
            $new_data = array();

            $groupusers = $group_data["ChatGroup"]["group_userjabberid"];

            if (strpos($groupusers, $form_data["userJabberID"]) !== false) {
                $new_data["ChatGroup"]["id"] = $group_id;

                if (strpos($members, $form_data["userJabberID"]) !== false) {
                    
                } else {
                    if ($members != "")
                        $new_data["ChatGroup"]["mute_userjabberid"] = $group_data["ChatGroup"]["mute_userjabberid"] = $members . "," . $form_data["userJabberID"];
                    else
                        $new_data["ChatGroup"]["mute_userjabberid"] = $group_data["ChatGroup"]["mute_userjabberid"] = $form_data["userJabberID"];

                    $this->ChatGroup->save($new_data);
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = 201;
                $responseData['message'] = "Success";
                $responseData['response_data'] = $group_data;

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {

                //no user found in this group
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = "No user found in provided group.";
                $responseData['response_data'] = [];

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = "Group Id not found.";
            $responseData['response_data'] = [];

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function unmuteuser() {
        $array = array("groupJabberID" => "groupJabberID", "userJabberID" => "userJabberID");
        $form_data = array_intersect_key($_POST, $array);

        $this->loadModel('ChatGroup');
        $group_data = $this->ChatGroup->find('first', array('conditions' => array('ChatGroup.groupJabberID' => $form_data["groupJabberID"])));

        $group_id = $group_data["ChatGroup"]["id"];
        if (!empty($group_id)) {
            $members = $group_data["ChatGroup"]["mute_userjabberid"];
            $members_array = explode(",", $members);
            $about_delete_members = explode(",", $form_data["userJabberID"]);
            $final_array_members = array_diff($members_array, $about_delete_members);
            $final_members = implode(",", $final_array_members);

            $new_data = array();
            $new_data["ChatGroup"]["id"] = $group_id;
            $new_data["ChatGroup"]["mute_userjabberid"] = $group_data["ChatGroup"]["mute_userjabberid"] = $final_members;
            $this->ChatGroup->save($new_data);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = 201;
            $responseData['message'] = "Success";
            $responseData['response_data'] = $group_data;

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = "Group Id not found.";
            $responseData['response_data'] = [];

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function delete_user($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');
            $this->loadModel('UserTextPost');
            $this->loadModel('UserPostComment');
            $this->loadModel('UserPostLike');
            $this->User->bindModel(array(
                'hasMany' => array(
                    'UserImageLike' => array(
                        'className' => 'UserImageLike',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserLike' => array(
                        'className' => 'UserLike',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserLiked' => array(
                        'className' => 'UserLike',
                        'foreignKey' => 'liked_user_id',
                        'dependent' => true
                    ),
                    'UserChatSetting' => array(
                        'className' => 'UserChatSetting',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'FromUserNotification' => array(
                        'className' => 'UserNotification',
                        'foreignKey' => 'fromid',
                        'dependent' => true
                    ),
                    'ToUserNotification' => array(
                        'className' => 'UserNotification',
                        'foreignKey' => 'toid',
                        'dependent' => true
                    ),
                    'UserBlock' => array(
                        'className' => 'UserBlock',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserBlocked' => array(
                        'className' => 'UserBlock',
                        'foreignKey' => 'blocked_user_id',
                        'dependent' => true
                    ),
                    'UserImage' => array(
                        'className' => 'UserImage',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserLocation' => array(
                        'className' => 'UserLocation',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserContact' => array(
                        'className' => 'UserContact',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserContacted' => array(
                        'className' => 'UserContact',
                        'foreignKey' => 'contact_user_id',
                        'dependent' => true
                    ),
                    'UserContactRequest' => array(
                        'className' => 'UserContactRequest',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
                    'UserContactRequested' => array(
                        'className' => 'UserContactRequest',
                        'foreignKey' => 'friend_id',
                        'dependent' => true
                    )
                )
                    ), false);

            $userData = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
            // print_r($userData);die;
            $userNm = $userData['User']['phone_number'];
            if (!empty($userData['User']['username'])) {
                $userNm = $userData['User']['username'];
            }
            $contactUserArr = array();
            $allDeviceIds = array();
            $contUserArr = array();

            if (!empty($userData['UserContact'])) {
                foreach ($userData['UserContact'] as $contKey => $contValue) {
                    $contactUserArr[] = $contValue['contact_user_id'];
                }
                $allContacts = implode("','", $contactUserArr);
                $contUserArr = $this->User->find('all', array('fields' => array('id', 'jid'), 'conditions' => array("User.id IN('$allContacts')")));
            }

            if (!empty($contUserArr)) {

                $body = base64_encode($userNm . " deleted");
                $userContData = base64_encode(json_encode($contUserArr));
                $postFields = array('userContctacts' => $userContData, 'body' => $body);
                // $ch = curl_init();
                // $callUrl = Router::url(array('controller' => 'web_services', 'action' => 'send_messages', 43, 23, 'deleteAccount', 'YES', $userData['User']['id']), true);
                // $callUrl = str_replace("http://", "https://", $callUrl);
                // //echo $callUrl;die;
                // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                // curl_setopt($ch, CURLOPT_URL, $callUrl);
                // curl_setopt($ch, CURLOPT_POST, count($postFields));
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                // curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                // echo $contents = curl_exec($ch);
                // curl_close($ch);
            }

            $userImageArr = array();
            if (!empty($userData['UserImage'])) {
                foreach ($userData['UserImage'] as $key => $userImageVal) {
                    $userImageArr[] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/large/" . $userImageVal['image'];
                    $userImageArr[] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $userImageVal['image'];
                    $userImageArr[] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userImageVal['image'];
                }
            }

            if ($this->User->delete($userData['User']['id'], true)) {

                $newrs = $this->UserTextPost->find('first', array('fields' => array('user_id'), 'conditions' => array("UserTextPost.user_id" => $userData['User']['id'])));
                if (!empty($newrs)) {
                    $this->UserTextPost->deleteAll(array('UserTextPost.user_id' => $userData['User']['id']), true);
                }
                $newrs2 = $this->UserPostComment->find('first', array('fields' => array('user_id'), 'conditions' => array("UserPostComment.user_id" => $userData['User']['id'])));
                if (!empty($newrs2)) {
                    $this->UserPostComment->deleteAll(array('UserPostComment.user_id' => $userData['User']['id']), true);
                }
                $newrs3 = $this->UserPostLike->find('first', array('fields' => array('user_id'), 'conditions' => array("UserPostLike.user_id" => $userData['User']['id'])));
                if (!empty($newrs3)) {
                    $this->UserPostLike->deleteAll(array('UserPostLike.user_id' => $userData['User']['id']), true);
                }

                // $this->UserTextPost->deleteAll(array('UserTextPost.user_id' => $userData['User']['id']), true);
                // $this->UserPostComment->deleteAll(array('UserPostComment.user_id' => $userData['User']['id']), true);
                // if (!empty($userData['User']['phone_number'])) {
                // 	$URL = "https://" . Configure::read('App.open_fire_host') . ":9991/plugins/restapi/v1/users/" . $userData['User']['phone_number'];
                // 	$secret = '876RmLfeU1Yfs5Aa';
                // 	$curl = curl_init();
                // 	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                // 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                // 	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                // 		'Authorization:' . $secret
                // 	));
                // 	curl_setopt($curl, CURLOPT_URL, $URL);
                // 	$contents = curl_exec($curl);
                // 	curl_close($curl);
                // }

                if (!empty($userImageArr)) {
                    $usrImgArr = array();
                    foreach ($userImageArr as $imageKey => $imageVal) {
                        @unlink($imageVal);
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'error in deletion';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_sms($phonenumber = null, $activation_code = null) {
        $smsApiKey = Configure::read('App.SMS_API_KEY');
        $smsApiSecret = Configure::read('App.SMS_API_SECRET');
        $smsFrom = Configure::read('App.SMS_FROM');
        $URL = "https://rest.nexmo.com/sms/json?api_key=$smsApiKey&api_secret=$smsApiSecret&from=$smsFrom&to=$phonenumber&text=ActivationCode:$activation_code";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $contents = curl_exec($curl);

        curl_close($curl);
        //echo $contents;die;
        return false;
    }

    public function send_sms_test() {

        $phonenumber = $_POST['phone'];
        $activation_code = urlencode($_POST['msg']);

        $smsApiKey = Configure::read('App.SMS_API_KEY');
        $smsApiSecret = Configure::read('App.SMS_API_SECRET');
        $smsFrom = Configure::read('App.SMS_FROM');
        echo $URL = "https://rest.nexmo.com/sms/json?api_key=$smsApiKey&api_secret=$smsApiSecret&from=$smsFrom&to=$phonenumber&text=ActivationCode:$activation_code";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $contents = curl_exec($curl);

        curl_close($curl);
        //echo $contents;die;
        print_r($curl);
    }

    public function get_user_registration_status($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = array();
        //$data = '{"msisdn":"9154346434336"}';

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {

            $this->loadModel("AppUser");

            $decoded['msisdn'] = str_replace("+", "", $decoded['msisdn']);

            if (substr($decoded['msisdn'], 0, 3) == '966') {
                $decoded['msisdn'] = "9660" . substr($decoded['msisdn'], 3);
            }

            $res = $this->AppUser->find('first', array('fields' => array('id', 'email', 'phone', 'device_id', 'device_type', 'country_dial_code', 'full_phone_number', 'msisdn', 'subscription_access_type', 'subscription_date', 'profile_status', 'ksa_registration_status', 'user_registration_date', 'service_expire_date'), 'conditions' => array("AppUser.msisdn" => $decoded['msisdn'])));

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $res['AppUser'];
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function verify_email_registration($test_data = null) {

        $decoded = array(
            "UserId" => $_POST["UserId"],
            "verification_code" => $_POST["verification_code"],
            "device_id" => $_POST["device_id"],
            "device_type" => $_POST["device_type"],
            "email" => $_POST["email"],
        );
        $responseData = array();

        if (!empty($decoded)) {

            $verification_code = $decoded['verification_code'];
            $UserId = $decoded['UserId'];
            $device_id = $decoded['device_id'];
            $device_type = $decoded['device_type'];
            $subscription_date = date('Y-m-d H:i:s');

            $this->loadModel('User');
            $verification_code_check = $this->User->find('count', array('conditions' => array('User.id' => $UserId)));

            if ($verification_code_check == 1) {
                $AppUserData = array();
                $AppUserData['User']['id'] = $UserId;
                $AppUserData['User']['status'] = 1;
                $AppUserData['User']['is_email_verified'] = 1;

                if ($verification_code == "9999")
                    $AppUserData['User']['skip_verification'] = 1;

                $AppUserData['User']['device_id'] = $device_id ? $device_id : "";
                $AppUserData['User']['device_type'] = $device_type ? $device_type : 0;
                $AppUserData['User']['subscription_date'] = $subscription_date;

                $xmppPass = $decoded['email'];

                $this->User->save($AppUserData);

                $hostname = XMPP_HOSTNAME;
                $xmppuser = $decoded['email'];
                $xmpppassword = $xmppPass;

                $resc = Configure::read('App.siteFolder');
                App::import('Vendor', 'XMPPHP', array('file' => 'XMPPHP/XMPP.php'));
                $conn = new XMPPHP_XMPP($hostname, XMPP_PORT, XMPP_USERNAME, XMPP_PASSWORD, 'xmpphp', XMPP_SERVER, $printlog = true, $loglevel = XMPPHP_Log::LEVEL_VERBOSE);

                try {
                    $conn->connect(30, true);
                    $conn->registerNewUser($xmppuser, $xmpppassword); // User name who need to register  $xmppUserName, $xmppPassword
                    $conn->disconnect();

                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['message'] = "success";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } catch (XMPPHP_Exception $e) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 500;
                    $responseData['message'] = $e->getMessage();
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = 'Wrong verification';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function verify_registration($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {

            $verification_code = $decoded['verification_code'];
            $UserId = $decoded['UserId'];
            $device_id = $decoded['device_id'];
            $device_type = $decoded['device_type'];

            $subscription_date = date('Y-m-d H:i:s');
            $this->loadModel('User');
            $verification_code_check = $this->User->find('count', array('conditions' => array('User.verification_code' => $verification_code, 'User.id' => $UserId)));

            if ($verification_code_check == 1) {
                $AppUserData = array();
                $AppUserData['User']['id'] = $UserId;
                $AppUserData['User']['status'] = 1;
                $AppUserData['User']['device_id'] = $device_id;
                $AppUserData['User']['device_type'] = $device_type;
                $AppUserData['User']['subscription_date'] = $subscription_date;

                $xmppPass = $decoded['phone'];

                $this->User->save($AppUserData);

                $UserDetail = $this->User->find('first', array('conditions' => array('User.verification_code' => $verification_code)));
                $hostname = XMPP_HOSTNAME;
                $xmppuser = str_replace("+", "", $UserDetail['User']['phone_number']);
                $xmpppassword = $xmppPass;
                $resc = Configure::read('App.siteFolder');
                App::import('Vendor', 'XMPPHP', array('file' => 'XMPPHP/XMPP.php'));

                $conn = new XMPPHP_XMPP($hostname, XMPP_PORT, XMPP_USERNAME, XMPP_PASSWORD, 'xmpphp', XMPP_SERVER, $printlog = true, $loglevel = XMPPHP_Log::LEVEL_VERBOSE);

                try {
                    $conn->connect(30, true);
                    $conn->registerNewUser($xmppuser, $xmpppassword);
                    $conn->disconnect();

                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['message'] = "success";
                    $responseData['response_data'] = array();

                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } catch (XMPPHP_Exception $e) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 500;
                    $responseData['message'] = $e->getMessage();
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = 'Wrong verification';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_exist_contacts($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $contactArr = array();
        $responseData = array();
        $decoded = json_decode($data, true);
        if (!empty($decoded)) {
            $this->loadModel('AppUser');
            $phoneNumbers = implode("','", $decoded['Contacts']);
            $data = $this->AppUser->find('all', array('fields' => array('AppUser.id', 'AppUser.phone', 'AppUser.username', 'AppUser.profile_status_message', 'AppUser.country_dial_code', 'AppUser.jid', 'AppUser.profile_image'), 'conditions' => array("AppUser.phone IN('$phoneNumbers')")));

            $returnContactArr = array();
            $returnContactArr['userid'] = $decoded['userid'];
            $returnContactArr['phonenumber'] = $decoded['phonenumber'];
            $returnContactArr['usercontacts'] = array();

            foreach ($data as $key => $value) {
                $returnContactArr['usercontacts'][$value['AppUser']['phone']] = $value['AppUser'];
                $returnContactArr['usercontacts'][$value['AppUser']['phone']]['profile_image'] = SITE_URL . "/uploads/user_profile_images/" . $value['AppUser']['profile_image'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data'] = $returnContactArr;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'No data found';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_hide_map_info($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $responseData = array();
        $decoded = json_decode($data, true);

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');

            $this->loadModel('UserLocation');

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";

                $responseData['response_data']['userId'] = $user_id;
                $responseData['response_data']['hide_from_map'] = $location_data['UserLocation']['hide_from_map'];

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";

                $responseData['response_data']['userId'] = $user_id;
                $responseData['response_data']['hide_from_map'] = "0";

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_hide_map_info($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');

            $this->loadModel('UserLocation');
            $userLocationData = array();

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
            }

            $userLocationData['UserLocation']['user_id'] = $user_id;
            $userLocationData['UserLocation']['lat'] = $decoded['lat'];
            $userLocationData['UserLocation']['lon'] = $decoded['lon'];
            $userLocationData['UserLocation']['hide_from_map'] = $decoded['hide_from_map'];

            $this->UserLocation->save($userLocationData);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = array();
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_friend_see_on_map($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $responseData = array();
        $decoded = json_decode($data, true);

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');

            $this->loadModel('UserLocation');

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";

                $responseData['response_data']['userId'] = $user_id;
                $responseData['response_data']['friend_see_on_map'] = $location_data['UserLocation']['friend_see_on_map'];

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";

                $responseData['response_data']['userId'] = $user_id;
                $responseData['response_data']['friend_see_on_map'] = "0";

                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_friend_see_on_map($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');

            $this->loadModel('UserLocation');
            $userLocationData = array();

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));

            if (!empty($location_data['UserLocation']['user_id'])) {
                $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
            }

            $userLocationData['UserLocation']['user_id'] = $user_id;
            $userLocationData['UserLocation']['lat'] = $decoded['lat'];
            $userLocationData['UserLocation']['lon'] = $decoded['lon'];
            $userLocationData['UserLocation']['friend_see_on_map'] = $decoded['friend_see_on_map'];

            $this->UserLocation->save($userLocationData);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = array();
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_wave($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');
            $target_id = ltrim($decoded['target_id'], '0');

            $this->loadModel('User');
            $this->loadModel('UserNotification');

            $userData = $this->User->find('first', array('conditions' => array("User.id" => $user_id), 'fields' => array('id', 'username', 'phone_number', 'full_name', 'jid', 'device_id', 'device_type')));
            $userNm = $userData['User']['phone_number'];

            if (!empty($userData['User']['full_name'])) {
                $userNm = $userData['User']['full_name'];
            }

            $userArr = array();

            $userArr['UserNotification']['fromid'] = $user_id;
            $userArr['UserNotification']['toid'] = $target_id;
            $userArr['UserNotification']['image_url'] = "wave";
            $userArr['UserNotification']['message'] = $userNm . " waved at you";
            $userArr['UserNotification']['type'] = 0;
            //pr($userArr);die;
            $this->UserNotification->save($userArr);
            unset($userArr);

            //send push notification
            $user_profile = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status', 'is_private', 'device_id'), 'conditions' => array('User.id' => $target_id)));
            // $this->send_notification_for_iphone($user_profile['User']['device_id'], $userNm . " waved at you");

            if ($user_profile['User']['device_type'] == 1) {
                $this->send_notification_for_android($user_profile['User']['device_id'], $userNm . " waved at you");
            } else if ($user_profile['User']['device_type'] == 2) {
                $this->send_notification_for_iphone_test($user_profile['User']['device_id'], $userNm . " waved at you");
            } else {
                $this->send_notification_for_iphone_test($user_profile['User']['device_id'], $userNm . " waved at you");
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = array();
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function update_user_location($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');

            $this->loadModel('UserLocation');
            $this->loadModel('User');
            $userLocationData = array();

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
            }

            $userLocationData['UserLocation']['user_id'] = $user_id;
            $userLocationData['UserLocation']['lat'] = $decoded['lat'];
            $userLocationData['UserLocation']['lon'] = $decoded['lon'];
            $this->UserLocation->save($userLocationData);

            $userData = $this->User->find('first', array('conditions' => array('User.id' => $decoded['id'])));
            $city = isset($decoded['city']) ? $decoded['city'] : '';
            if (strtolower($city) == 'great' || strtolower($city) == 'great manchester' || strtolower($city) == 'greater') {
                $city = 'Manchester';
            }
            if (strtolower($city) == 'great london') {
                $city = 'London';
            }
            /* send notification to nearby people in same city */
            if (isset($decoded['city']) && !empty($decoded['city']) && empty($userData['User']['city']) && !empty($userData) && !empty($userData['User']['username'])) {
                //        $city = $decoded['city'];
                $msg = base64_encode($city . '_' . $user_id);
                $proc_command = "wget https://admin.mapbuzz.com/app_notification.php?t=$msg -q -O - -b";
                $proc = popen($proc_command, "r");
                pclose($proc);
            }

            /* save city */
            if (isset($decoded['city']) && !empty($decoded['city']) && !empty($userData) && !empty($userData['User']['username'])) {
                $userData['User']['city'] = $city;
                $this->User->save($userData);
            }



            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = array();
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_profile_info($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');
            if (isset($decoded['username'])) {
                $userId = $decoded['id'];
                $isAlready = $this->User->find('count', array('conditions' => array("User.id!=$userId", 'User.username' => $decoded['username'])));
                if ($isAlready > 0) {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = "Sorry, this username isn't available";
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }

            $this->User->findById($decoded['id']);
            if (!empty($decoded['profile_status_message'])) {
                $decoded['profile_status_message_updated'] = date('Y-m-d H:i:s', strtotime('now'));
            }


            if (((isset($decoded['full_name'])) && (!empty($decoded['full_name']))) || ((isset($decoded['online_status'])) && (!empty($decoded['online_status']) || ($decoded['online_status'] == 0))) || ((isset($decoded['profile_status_message'])) && (!empty($decoded['profile_status_message']))) || ((isset($decoded['is_private'])) && (!empty($decoded['is_private'])))) {
                // SEND PUSH NOTIFICATION
                $userId = $decoded['id'];
                $this->loadModel('User');
                $this->User->Behaviors->load('Containable');
                $this->User->bindModel(array('hasMany' => array('UserContact' => array('foreignKey' => 'contact_user_id', 'fields' => array('id', 'user_id', 'contact_user_id', 'status')))));
                $this->User->UserContact->bindModel(array('belongsTo' => array('User' => array('foreignKey' => 'user_id', 'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status')))));

                $userData = $this->User->find('first', array('contain' => array('UserContact' => array('User')), 'conditions' => array('User.id' => $userId)));

                $userNm = $userData['User']['phone_number'];
                if (!empty($userData['User']['username'])) {
                    $userNm = $userData['User']['username'];
                }

                $body = "";
                $action = "";
                $actValue = "YES";
                // online_status
                if (isset($decoded['full_name']) && (!empty($decoded['full_name']))) {
                    $body = $userNm . " updated name";
                    $action = "nameChange";
                }

                if (isset($decoded['online_status']) || (!empty($decoded['online_status']) && ($decoded['online_status'] == 0))) {
                    $body = $userNm . " updated online status";
                    $action = "onlineStatus";
                    $actValue = "YES";
                    if ($decoded['online_status'] == 0) {
                        $actValue = "NO";
                    }
                }

                if (isset($decoded['profile_status_message']) && (!empty($decoded['profile_status_message']))) {
                    $body = $userNm . " updated status message";
                    $action = "statusChange";
                }


                if (!empty($userData['UserContact']) && (!empty($body))) {

                    $body = base64_encode($body);
                    $userContData = base64_encode(json_encode($userData['UserContact']));
                    $postFields = array('userContctacts' => $userContData, 'body' => $body);
                    $ch = curl_init();
                    $callUrl = Router::url(array('controller' => 'web_services', 'action' => 'send_messages', 43, 23, $action, $actValue, $userData['User']['id']), true);
                    $callUrl = str_replace("http://", "https://", $callUrl);

                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_URL, $callUrl);
                    curl_setopt($ch, CURLOPT_POST, count($postFields));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                    echo $contents = curl_exec($ch);
                    curl_close($ch);
                }
            }


            $Data['User'] = $decoded;
            if ($this->User->save($Data)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = array();
                $responseData['message'] = 'success';
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_user_chat_setting($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }
        $data = '{"user_id":"19", "text_size":"12", "last_login_status":"1"}';
        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('UserChatSetting');
            $userSettingData = $this->UserChatSetting->find('first', array('conditions' => array('UserChatSetting.user_id' => $decoded['user_id'])));
            $savableData = array();
            $savableData['UserChatSetting'] = $decoded;
            $savableData['UserChatSetting']['id'] = $userSettingData['UserChatSetting']['id'];
            if ($this->UserChatSetting->save($savableData)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = array();
                $responseData['message'] = 'success';
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = array();
                $responseData['message'] = 'failed';
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_profile_point($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $this->loadModel('UserLocation');

            $point = 0;
            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['user_id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $point = $location_data['UserLocation']['score'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = array();
            $responseData['user_score'] = $point;
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);

            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);

            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_profile_info($test_data = null) {

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');
            $this->loadModel('UserImage');
            $this->loadModel('UserBlock');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');
            $this->loadModel('Interests');
            $this->loadModel('SubInterests');

            if (strlen($decoded['user_id']) > 10) {
                $userInfo = $this->User->find('first', array('conditions' => array('User.apple_identifier' => $decoded['user_id'])));
                $decoded['user_id'] = $userInfo['User']['id'];
            }

            $profiledata = $this->User->find('first', array('fields' => array(
                    'id', 'is_private', 'username', 'email', 'jid', 'phone_number', 'profile_status_message',
                    'push_notification_status ', 'notification_preview_status', 'full_name', 'profile_image', 'profile_cover_image', 'hide_username_search',
                    'device_type', 'status', 'is_verify', 'interests', 'age', 'gender', 'country', 'city', 'hide_random_chat', 'random_video_allow', 'voip_token',
                    'near_city_notification_status', 'near_map_photo_notification_status', 'chat_room_notification_status'
                ), 'conditions' => array('User.id' => $decoded['user_id'])));
            $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $decoded['user_id'], 'UserImage.image_type' => array(1, 2, 6))));

            $profiledata['User']['profile_image'] = "";
            $profiledata['User']['profile_cover_image'] = "";
            $profiledata['User']['dating_pic'] = "";
            foreach ($userImages as $key => $value) {
                if ($value['UserImage']['image_type'] == 1) {
                    $profiledata['User']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $value['UserImage']['image'];
                    $profiledata['User']['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                }
                if ($value['UserImage']['image_type'] == 2) {
                    $profiledata['User']['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                }
                if ($value['UserImage']['image_type'] == 6) {
                    $profiledata['User']['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $value['UserImage']['image'];
                }
            }

            //user interests
            $user_interests = explode(',', $profiledata['User']['interests']);
            $interests_data = array();
            if (!empty($user_interests)) {
                foreach ($user_interests as $k => $val) {
                    $sub_interests = $this->SubInterests->find('first', array('fields' => array('SubInterests.name', 'SubInterests.interest', 'SubInterests.image'), 'conditions' => array('SubInterests.id' => $val)));
                    if (!empty($sub_interests)) {
                        $interest_name = $sub_interests['SubInterests']['name'];
                        $subinterest_image = $sub_interests['SubInterests']['image'];
                        $interest_id = $sub_interests['SubInterests']['interest'];
                        if (!empty($subinterest_image)) {
                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $subinterest_image;
                        } else {
                            $interestDetails = $this->Interests->find('first', array('fields' => array('Interests.image'), 'conditions' => array('Interests.id' => $interest_id)));
                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $interestDetails['Interests']['image'];
                        }

                        $interests_data[] = array('id' => $val, 'name' => $interest_name, 'image' => $interest_url);
                    }
                }
            }
            $profiledata['User']['interests'] = $interests_data;

            //get location and score
            $this->loadModel('UserLocation');

            $user_lat = "0.0";
            $user_lon = "0.0";
            $user_score = 0;

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['user_id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $user_lat = $location_data['UserLocation']['lat'];
                $user_lon = $location_data['UserLocation']['lon'];
                $user_score = $location_data['UserLocation']['score'];
            }

            $profiledata['User']['lat'] = $user_lat;
            $profiledata['User']['lon'] = $user_lon;
            $profiledata['User']['score'] = $user_score;

            $profiledata['User']['UserImage'] = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $decoded['user_id'], 'UserImage.image_type' => 1), 'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')));
            $profiledata['User']['UserBlock'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['user_id'])));
            $profiledata['User']['UserBlocked'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.blocked_user_id' => $decoded['user_id'])));

            $profiledata['User']['UserContact'] = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $decoded['user_id'], 'UserContact.contact_user_id' => $decoded['userid'])));
            $profiledata['User']['UserContactRequest'] = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.user_id' => $decoded['user_id'], 'UserContactRequest.friend_id' => $decoded['userid'])));

            $user_id = $decoded['userid'];
            $profiledata['User']['contact_request'] = 0;
            $profiledata['User']['is_contact'] = 0;
            $profiledata['User']['is_sender'] = 0;

            $this->loadModel('UserContactRequest');
            $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $decoded['user_id'], 'UserContactRequest.friend_id' => $user_id)));

            if (!empty($contact_request)) {
                $profiledata['User']['contact_request'] = 1;
            }

            $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $decoded['user_id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

            if (!empty($contact_request_is_sender)) {
                $profiledata['User']['is_sender'] = 1;
            }

            $this->loadModel('UserContact');
            $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $decoded['user_id'])));

            if (!empty($contact)) {
                $profiledata['User']['is_contact'] = 1;
            }
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $profiledata['User'];

            $now = time(); // or your date as well
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_staging_profile_info($test_data = null) {

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');
            $this->loadModel('UserImage');
            $this->loadModel('UserBlock');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');
            $this->loadModel('Interests');
            $this->loadModel('SubInterests');

            if (strlen($decoded['user_id']) > 10) {
                $userInfo = $this->User->find('first', array('conditions' => array('User.apple_identifier' => $decoded['user_id'])));
                $decoded['user_id'] = $userInfo['User']['id'];
            }

            $profiledata = $this->User->find('first', array('fields' => array(
                    'id', 'is_private', 'username', 'email', 'jid', 'phone_number', 'profile_status_message',
                    'push_notification_status ', 'notification_preview_status', 'full_name', 'profile_image', 'profile_cover_image', 'hide_username_search',
                    'device_type', 'status', 'is_verify', 'interests', 'age', 'gender', 'country', 'city', 'hide_random_chat', 'random_video_allow', 'voip_token',
                    'near_city_notification_status', 'near_map_photo_notification_status', 'chat_room_notification_status', 'personality_type', 'looking_for'
                ), 'conditions' => array('User.id' => $decoded['user_id'])));
            $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $decoded['user_id'], 'UserImage.image_type' => array(1, 2, 6))));

            $profiledata['User']['profile_image'] = "";
            $profiledata['User']['profile_cover_image'] = "";
            $profiledata['User']['dating_pic'] = "";
            foreach ($userImages as $key => $value) {
                if ($value['UserImage']['image_type'] == 1) {
                    $profiledata['User']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $value['UserImage']['image'];
                    $profiledata['User']['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                }
                if ($value['UserImage']['image_type'] == 2) {
                    $profiledata['User']['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                }
                if ($value['UserImage']['image_type'] == 6) {
                    $profiledata['User']['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $value['UserImage']['image'];
                }
            }

            //user interests
            $user_interests = explode(',', $profiledata['User']['interests']);
            $interests_data = array();
            if (!empty($user_interests)) {
                foreach ($user_interests as $k => $val) {
                    $sub_interests = $this->SubInterests->find('first', array('fields' => array('SubInterests.name', 'SubInterests.interest', 'SubInterests.image'), 'conditions' => array('SubInterests.id' => $val)));
                    if (!empty($sub_interests)) {
                        $interest_name = $sub_interests['SubInterests']['name'];
                        $subinterest_image = $sub_interests['SubInterests']['image'];
                        $interest_id = $sub_interests['SubInterests']['interest'];
                        if (!empty($subinterest_image)) {
                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $subinterest_image;
                        } else {
                            $interestDetails = $this->Interests->find('first', array('fields' => array('Interests.image'), 'conditions' => array('Interests.id' => $interest_id)));
                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $interestDetails['Interests']['image'];
                        }

                        $interests_data[] = array('id' => $val, 'name' => $interest_name, 'image' => $interest_url);
                    }
                }
            }
            $profiledata['User']['interests'] = $interests_data;

            //get location and score
            $this->loadModel('UserLocation');

            $user_lat = "0.0";
            $user_lon = "0.0";
            $user_score = 0;

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['user_id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $user_lat = $location_data['UserLocation']['lat'];
                $user_lon = $location_data['UserLocation']['lon'];
                $user_score = $location_data['UserLocation']['score'];
            }

            $profiledata['User']['lat'] = $user_lat;
            $profiledata['User']['lon'] = $user_lon;
            $profiledata['User']['score'] = $user_score;

            $profiledata['User']['UserImage'] = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $decoded['user_id'], 'UserImage.image_type' => 1), 'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')));
            $profiledata['User']['UserBlock'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['user_id'])));
            $profiledata['User']['UserBlocked'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.blocked_user_id' => $decoded['user_id'])));

            $profiledata['User']['UserContact'] = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $decoded['user_id'], 'UserContact.contact_user_id' => $decoded['user_id'])));
            $profiledata['User']['UserContactRequest'] = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.user_id' => $decoded['user_id'], 'UserContactRequest.friend_id' => $decoded['user_id'])));

            $user_id = $decoded['user_id'];
            $profiledata['User']['contact_request'] = 0;
            $profiledata['User']['is_contact'] = 0;
            $profiledata['User']['is_sender'] = 0;

            $this->loadModel('UserContactRequest');
            $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $decoded['user_id'], 'UserContactRequest.friend_id' => $user_id)));

            if (!empty($contact_request)) {
                $profiledata['User']['contact_request'] = 1;
            }

            $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $decoded['user_id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

            if (!empty($contact_request_is_sender)) {
                $profiledata['User']['is_sender'] = 1;
            }

            $this->loadModel('UserContact');
            $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $decoded['user_id'])));

            if (!empty($contact)) {
                $profiledata['User']['is_contact'] = 1;
            }
            $this->loadModel('Followers');
            $profiledata['User']['follower_count'] = $this->Followers->find('count', array(
                'fields' => 'DISTINCT Followers.follower_id',
                'conditions' => array('Followers.user_id' => $user_id, 'is_approvad' => true)
            ));
            $profiledata['User']['following_count'] = $this->Followers->find('count', array(
                'fields' => 'DISTINCT Followers.user_id',
                'conditions' => array('Followers.follower_id' => $user_id, 'is_approvad' => true)
            ));
            $profiledata['User']['pending_follower_count'] = $this->Followers->find('count', array(
                'fields' => 'DISTINCT Followers.follower_id',
                'conditions' => array('Followers.user_id' => $user_id, 'is_approvad' => false)
            ));
            $profiledata['User']['pending_following_count'] = $this->Followers->find('count', array(
                'fields' => 'DISTINCT Followers.user_id',
                'conditions' => array('Followers.follower_id' => $user_id, 'is_approvad' => false)
            ));
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $profiledata['User'];

            $now = time(); // or your date as well
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_profile_device_type($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');

            $profiledata = $this->User->find('first', array('fields' => array('id', 'device_type'), 'conditions' => array('User.id' => $decoded['user_id'])));

            $profiledata['User']['device_type'] = $profiledata['User']['device_type'] ? (int) $profiledata['User']['device_type'] : 0;

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $profiledata['User'];

            $now = time(); // or your date as well
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_static_page($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('Page');
            $contentData = $this->Page->find('first', array('fields' => array('id', 'title', 'heading', 'content'), 'conditions' => array('Page.slug' => $decoded['page_slug'])));
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $contentData['Page'];
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_global_single_inputs($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = array();

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            if (!empty($decoded['model_name'])) {
                $this->loadModel($decoded['model_name']);
                $Reqfields = array();
                $ReqConditions = array();
                if ($decoded['response_type'] == 'custom') {
                    if (!empty($decoded['fields'])) {
                        $Reqfields = $decoded['fields'];
                    }
                }

                if (!empty($decoded['conditions'])) {
                    $ReqConditions = $decoded['conditions'];
                }

                $res = $this->$decoded['model_name']->find('first', array('fields' => $Reqfields, 'conditions' => array($ReqConditions)));
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'success';
                $responseData['response_data']['data'] = $res[$decoded['model_name']];

                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['message'] = 'error';
                $responseData['response_data']['data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_global_inputs($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }
        $decoded = array();

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            if (!empty($decoded['model_name'])) {
                $this->loadModel($decoded['model_name']);
                $savData[$decoded['model_name']] = $decoded['data'];
                if ($this->$decoded['model_name']->save($savData)) {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['message'] = 'success';
                    $responseData['response_data']['data'] = $decoded['data'];
                    $data = array('response' => $responseData);
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 500;
                    $responseData['message'] = 'error';
                    $responseData['response_data']['data'] = array();
                    $data = array('response' => $responseData);
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['message'] = 'error';
                $responseData['response_data']['data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function sendsms($verification_code, $phone) {
        $this->loadModel('AppUser');
        App::import('Vendor', 'twilio', array('file' => 'twilio/Services/Twilio.php'));
        $accountSid = Configure::read('App.Twilio.AccountId');
        $authToken = Configure::read('App.Twilio.AuthToken');
        $toArray = $phone;
        $from = Configure::read('App.Twilio.FromNo');
        $smsVerifyCode = $verification_code;
        $message = 'topchat.com:Your verification code is ' . $smsVerifyCode . '.Please enter it in the space provided in the login on website.Thank you for using Topchat.';
        $checkValid = "https://api.twilio.com/" . date('Y-m-d') . "/Accounts/{$accountSid}/" . $toArray;

        $client = new Services_Twilio($accountSid, $authToken);
        try {
            $sms = $client->account->sms_messages->create($from, $toArray, $message, array('StatusCallback' => SITE_URL . 'web_services/sms_response'));
        } catch (Exception $e) {
            $this->AppUser->validationErrors['phone'][0] = 'Invalid Phone Number.';
        }



        if (!empty($sms->client->last_response->sid)) {
            return $smsVerifyCode;
        } else {
            return false;
        }
    }

    public function sms_response() {
        pr($this);
        die;
    }

    public function email_verify_resent($test_data = null) {
        $responseData = array();
        $this->loadModel('User');
        if (isset($_POST['email'])) {
            $user_data = $this->User->find('first', array('conditions' => array('User.email' => $_POST['email'])));
            if (!empty($user_data)) {

                $message_line = "Your verification code is " . $user_data['User']['verification_code'];
                if ($this->send_mail_check($user_data['User']['email'], "Email verification code resent", $message_line)) {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = 201;
                    $responseData['message'] = 'Verification code resent successfully.';
                    $responseData['response_data']['data'] = array();
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 400;
                    $responseData['message'] = 'Resent verification fail.';
                    $responseData['response_data']['data'] = array();
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['message'] = 'No record found for provided email.';
                $responseData['response_data']['data'] = array();
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 400;
            $responseData['message'] = 'Please provide email.';
            $responseData['response_data']['data'] = array();
        }

        $jsonEncode = json_encode($responseData);
        return $jsonEncode;
    }

    public function forgot_password($test_data = null) {
        $responseData = array();
        $this->loadModel('User');
        if (isset($_POST['email'])) {
            $user_data = $this->User->find('first', array('conditions' => array('User.email' => $_POST['email'])));
            if (!empty($user_data)) {
                $new_token = $user_data['User']['id'] . "T" . rand(100000000, 9999999999);

                $AppUserData = array();
                $AppUserData['User']['id'] = $user_data['User']['id'];
                $AppUserData['User']['token'] = "" . $new_token . "";

                $this->User->save($AppUserData);
                $message_line = "We have received your request to reset your password.<br/><br/> <a href='" . SITE_URL_ABS . "passwordreset?token=" . $new_token . "'>click here</a> to reset your password.<br/><br/>or copy below link:<br/>" . SITE_URL_ABS . "passwordreset?token=" . $new_token;

                if ($this->send_mail_check($user_data['User']['email'], "Txtter - Password reset link", $message_line)) {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response__query'] = $this->getLastQuery($this->User);
                    $responseData['response_message_code'] = 201;
                    $responseData['message'] = 'Password reset link sent on your email.';
                    $responseData['response_data']['data'] = array();
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_query'] = $this->getLastQuery($this->User);
                    $responseData['response_message_code'] = 400;
                    $responseData['message'] = 'Resent verification fail.';
                    $responseData['response_data']['data'] = array();
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['response_query'] = $this->getLastQuery($this->User);
                $responseData['message'] = 'No record found for provided email.';
                $responseData['response_data']['data'] = array();
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 400;
            $responseData['response_query'] = $this->getLastQuery($this->User);
            $responseData['message'] = 'Please provide email.';
            $responseData['response_data']['data'] = array();
        }

        $jsonEncode = json_encode($responseData);
        return $jsonEncode;
    }

    public function getLastQuery($model) {
        $dbo = $model->getDatasource();
        $logData = $dbo->getLog();
        $getLog = end($logData['log']);
        return $getLog['query'];
    }

    public function password_reset_page($test_data = null) {
        $responseData = array();
        $this->loadModel('User');
        if (isset($_POST['token']) && trim($_POST['token']) != "" && isset($_POST['password']) && trim($_POST['password'] != "")) {
            $user_data = $this->User->find('first', array('conditions' => array('User.token' => $_POST['token'])));
            if (!empty($user_data)) {

                $AppUserData = array();
                $AppUserData['User']['id'] = $user_data['User']['id'];
                $AppUserData['User']['password'] = Security::hash($_POST['password'], null, true);

                $this->User->save($AppUserData);

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = 201;
                $responseData['message'] = 'Password reset successfully.';
                $responseData['response_data']['data'] = array();
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['message'] = 'Provided token is invalid.';
                $responseData['response_data']['data'] = array();
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 400;
            $responseData['message'] = 'Please provide token and password.';
            $responseData['response_data']['data'] = array();
        }

        $jsonEncode = json_encode($responseData);
        return $jsonEncode;
    }

    public function send_mail_check($to, $subject, $message_line) {
        $email = new CakeEmail();
        //$from = Configure::read('App.AdminMail');
        $email->config('gmail1');
        $email->emailFormat('html');
        //$email->from(array('signup@mapchatapp.com' => 'Mapchat Team'));
        $email->from(array('help@mapbuzz.com' => 'Mapchat Team'));
        //$email->from($from);
        $email->to($to);
        $email->subject($subject);

        $message = '<!DOCTYPE HTML>
    <html>
    <head>
    <meta charset="utf-8">
    <title>Txtter</title>
    </head>

    <body style="margin:0; padding:0; font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#000; background:#ddd;">

    <table align="center" cellspacing="0" cellpadding="0" width="600" border="0" style="padding:0; background-size:100% 100%; background-color:#191919; font-family:Arial, Helvetica, sans-serif;">
    <tr>
    <td align="center" style="padding:20px 0;"><img src="https://d35ugz1sdahij6.cloudfront.net/logo.png" alt=""></td>
    </tr>
    <tr>
    <td width="540px;" style="padding:0px 30px 20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td align="center" style="background:#fff; padding:25px 30px; border-radius:10px; -webkit-border-radius:10px; -o-border-radius:10px; -moz-border-radius:10px; -ms-border-radius:10px; box-shadow:0px 6px 10px rgba(0, 0, 0, 0.3);">
    <table width="100%" style="margin:0;">
    <tr>
    <td align="left" style="font-size:14px; color:#000; padding:0 0 15px; font-family:Arial, Helvetica, sans-serif;">Hi,</td>
    </tr>
    <tr>
    <td align="left" style="font-size:13px; line-height:23px; color:#000; padding:0 0 12px; font-family:Arial, Helvetica, sans-serif;">' . $message_line . '</td>
    </tr>
    <tr>
    <td style="padding:15px 0 5px; font-family:Arial, Helvetica, sans-serif;" align="left">
    <p style="margin:0; font-size:13px; line-height:20px; font-family:Arial, Helvetica, sans-serif; color:#000;">Thank You, <br>Txtter</p>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align:center; padding:0; border-radius:10px 10px 0 0; margin:0;font-size:12px;">
    <tr>
    <td align="center" style="padding:20px 0 0 0;"><p style="margin:0; font-family:Arial, Helvetica, sans-serif; color:#fff">Copyright &copy; ' . date('Y') . '. All Rights Reserved. </p></td>
    </tr>
    </table>

    </td>
    </tr>
    </table>

    </body>
    </html>';

        $result = $email->send($message);
        if ($result)
            return true;
        else
            return false;
    }

    public function get_sendEmail($to, $subject, $message) {

        $email = new CakeEmail();
        $from = Configure::read('App.AdminMail');
        $email->config('gmail1');
        $email->emailFormat('html');
        $email->from($from);
        $email->to($to);
        $email->subject($subject);
        if ($email->send($message))
            return true;
        else
            return false;
    }

    function phone_check($phone = null) {
        if (!empty($phone)) {
            $this->loadModel('User');
            $PhoneCheck = $this->User->find('count', array('conditions' => array('User.phone_number' => $phone)));
            if ($PhoneCheck == 1) {
                return true;
            }
        }
        return false;
    }

    function check_email($email = null, $userId = null) {
        if (!empty($email)) {
            $this->loadModel('User');
            $result = array();
            if (!empty($userId)) {
                $emailCheck = $this->User->find('count', array('conditions' => array('User.email' => urldecode(trim($email)), 'User.id !=' => $userId, 'User.role_id' => 2)));
            } else {
                $emailCheck = $this->User->find('count', array('conditions' => array('User.email' => urldecode(trim($email)), 'User.role_id' => 2)));
            }

            if ($emailCheck == 1) {
                return true;
            }
        }
        return false;
    }

    function check_username($username = null, $userId = null) {
        if (!empty($username)) {
            $this->loadModel('User');
            $result = array();
            if (!empty($userId)) {
                $usernameCheck = $this->User->find('count', array('conditions' => array('User.username' => urldecode(trim($username)), 'User.id !=' => $userId, 'User.role_id' => 2)));
            } else {
                $usernameCheck = $this->User->find('count', array('conditions' => array('User.username' => urldecode(trim($username)), 'User.role_id' => 2)));
            }
            if ($usernameCheck == 1) {
                return true;
            }
        }
        return false;
    }

    function verification_code($length) {
        $key = '';
        $keys = array_merge(range(0, 9));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    function auth_key($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    function testxmpp() {
        $responseData = array();
        $hostname = XMPP_HOSTNAME;
        $xmppuser = '918675464458';
        $xmpppass = '123456';
        $resc = Configure::read('App.siteFolder');
        App::import('Vendor', 'XMPPHP', array('file' => 'XMPPHP/XMPP.php'));
        $conn = new XMPPHP_XMPP($hostname, XMPP_PORT, XMPP_USERNAME, XMPP_PASSWORD, 'xmpphp', XMPP_SERVER, $printlog = true, $loglevel = XMPPHP_Log::LEVEL_VERBOSE);
        try {
            $conn->connect(30, true);
            $conn->registerNewUser($xmppuser, $xmpppass);
            $conn->disconnect();

            $responseData['success'] = 'You have registered successfully';

            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } catch (XMPPHP_Exception $e) {
            $responseData['error'] = $e->getMessage();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function send_xmpp_message($to = null, $body = null, $action = null, $value = null, $id = null) {
        $responseData = array();

        $hostname = XMPP_HOSTNAME;
        $resc = Configure::read('App.siteFolder');
        App::import('Vendor', 'XMPPHP', array('file' => 'XMPPHP/XMPP.php'));
        $conn = new XMPPHP_XMPP($hostname, XMPP_PORT, XMPP_USERNAME, XMPP_PASSWORD, 'xmpphp', XMPP_SERVER, $printlog = true, $loglevel = XMPPHP_Log::LEVEL_VERBOSE);

        try {
            $conn->connect();
            $conn->processUntil('session_start');
            $conn->presence();
            $conn->message($to, $body, $action, $value, $id);
            return true;
            $conn->disconnect();
        } catch (XMPPHP_Exception $e) {
            return true;
        }
    }

    public function read_doc($test = null, $request_action = null) {
        $this->autoRender = true;
        $this->layout = false;
        $response_data = "";

        $this->LoadModel("TestWebService");
        $data = $this->TestWebService->find('first', array('conditions' => array('TestWebService.title' => $request_action)));

        if ($test == 'test') {
            if (!empty($this->request->data)) {
                if (!empty($request_action)) {
                    $dataArr = array();
                    $readDoc = "";
                    if (isset($this->data['TestWebService']['request'])) {
                        $readDoc = $this->data['TestWebService']['request'];
                    }

                    $response_data = $this->$request_action(base64_encode($readDoc));
                }
            } else {
                $this->request->data = $data;
            }
        }
        if (!empty($data)) {
            //$this->request->data = $data;
        }
        $this->set(compact('test', 'request_action', 'response_data', 'data'));
    }

    function upload_profile_image() {
        App::import('Lib', 'S3');
        if (!empty($_FILES)) {


            if ($_FILES['image']['size'] < 6291456) {

                $thumbRules = array('size' => array(100, 100), 'type' => 'resizecrop');
                $largeRules = array('size' => array(500, 500), 'type' => 'resizecrop');

                $path_info = pathinfo($_FILES['image']['name']);
                $_FILES['image']['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];
                //echo WWW_ROOT . PROFILE_PICS . DS ."thumb". DS, '';die;
                $res1 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_PICS . DS . "thumb" . DS, '', $thumbRules, array('png', 'jpg', 'jpeg', 'gif'));
                $res2 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_PICS . DS . "large" . DS, '', $largeRules, array('png', 'jpg', 'jpeg', 'gif'));
                $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_PICS . DS . "original" . DS, '', '', array('png', 'jpg', 'jpeg', 'gif'));

                if (!empty($this->Upload->result)) {

                    $message['image'] = $this->Upload->result;
                    $userImage = $this->Upload->result;
                    $result['response_status'] = 1;
                    $result['response_code'] = 200;
                    $result['response_message_code'] = '';
                    $result['message'] = 'success';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = $userImage;

                    $profileData = array();
                    $this->loadModel('User');
                    $data = $this->User->find('first', array('fields' => array('User.id', 'User.profile_image'), 'conditions' => array('User.id' => $_POST['user_id'])));
                    @unlink(WWW_ROOT . PROFILE_PICS . DS . "original" . DS . $data['User']['profile_image']);
                    @unlink(WWW_ROOT . PROFILE_PICS . DS . "thumb" . DS . $data['User']['profile_image']);
                    @unlink(WWW_ROOT . PROFILE_PICS . DS . "large" . DS . $data['User']['profile_image']);
                    $profileData['User']['id'] = $_POST['user_id'];
                    $profileData['User']['profile_image'] = $userImage;
                    $this->User->save($profileData);
                    $jsonEncode = json_encode($result);
                    return $jsonEncode;
                } else {
                    $result['response_status'] = 0;
                    $result['response_code'] = 400;
                    $result['response_message_code'] = 500;
                    $result['message'] = 'error';
                    $result['response_data'] = $this->Upload->error;
                    $result['response_data']['image'] = '';
                    $jsonEncode = json_encode($result);

                    return $jsonEncode;
                }
            } else {
                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = 413;
                $result['message'] = 'cannot upload image of size more than 8MB.';
                $result['response_data'] = array();
                $result['response_data']['image'] = '';
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            }
        } else {
            $result['response_status'] = 0;
            $result['response_code'] = 400;
            $result['response_message_code'] = 404;
            $result['message'] = 'file not found';
            $result['response_data'] = array();
            $result['response_data']['image'] = '';
            $jsonEncode = json_encode($result);
            return $jsonEncode;
        }
    }

    function upload_user_images() { //echo SITE_URL;die;
        App::import('Lib', 'S3');
        if (!empty($_FILES)) {
            if ($_FILES['image']['size'] < 6291456) {
                $thumbRules = array('size' => array(100, 100), 'type' => 'resizecrop');
                $largeRules = array('size' => array(500, 500), 'type' => 'resizecrop');
                $path_info = pathinfo($_FILES['image']['name']);

                $_FILES['image']['name'] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                $res1 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS, '', $thumbRules, array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));
                $res2 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS, '', $largeRules, array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));
                $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                $this->loadModel('UserImage');
                if ($_POST['image_type'] == 2) {
                    $oldCoverImage = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $_POST['user_id'], 'UserImage.image_type' => 2)));
                }

                if (!empty($this->Upload->result)) {
                    $this->Upload->result;

                    if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result)) {
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "thumb" . DS . $this->Upload->result);
                    }
                    if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result)) {
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "large" . DS . $this->Upload->result);
                    }
                    if ($this->save_image_s3bucket(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result, PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result)) {
                        @unlink(WWW_ROOT . PROFILE_EXTRA_PICS . DS . "original" . DS . $this->Upload->result);
                    }

                    $message['image'] = $this->Upload->result;
                    $userImage = $this->Upload->result;
                    $result['response_status'] = 1;
                    $result['response_code'] = 200;
                    $result['response_message_code'] = '';
                    $result['message'] = 'success';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/large/" . $userImage; //SITE_URL . "/uploads/user_images/large/" . $userImage;
                    $img_path = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $userImage; //SITE_URL . "/uploads/user_images/large/" . $userImage;

                    if ($_POST['image_type'] == 0) {
                        //add point
                        $this->loadModel('UserLocation');
                        $userLocationData = array();

                        $priorPoint = 0;
                        $user_lat = "0.0";
                        $user_lon = "0.0";
                        $user_hide_from_map = 0;

                        $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $_POST['user_id'])));
                        if (!empty($location_data['UserLocation']['user_id'])) {
                            $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];

                            $priorPoint = $location_data['UserLocation']['score'];
                            $user_lat = $location_data['UserLocation']['lat'];
                            $user_lon = $location_data['UserLocation']['lon'];
                            $user_hide_from_map = $location_data['UserLocation']['hide_from_map'];
                        }

                        $newPoint = $priorPoint + 1;
                        $userLocationData['UserLocation']['user_id'] = $_POST['user_id'];
                        $userLocationData['UserLocation']['score'] = $newPoint;
                        $userLocationData['UserLocation']['lat'] = $user_lat;
                        $userLocationData['UserLocation']['lon'] = $user_lon;
                        $userLocationData['UserLocation']['hide_from_map'] = $user_hide_from_map;
                        $this->UserLocation->save($userLocationData);

                        $bGotScore = false;
                        $scoreMessage = "";

                        if ($newPoint >= 10000) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a President";
                        } else if ($newPoint >= 2000) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a Governor";
                        } else if ($newPoint >= 1000) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a Ambassador";
                        } else if ($newPoint >= 500) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a Mayor";
                        }

                        if ($bGotScore) {
                            $bAlreadyGotMessage = false;

                            $this->loadModel('UserNotification');

                            $notification_data = $this->UserNotification->find('first', array('conditions' => array('UserNotification.toid' => $_POST['user_id'], 'UserNotification.image_url' => 'score_notification', 'UserNotification.message' => $scoreMessage)));
                            if (!empty($notification_data['UserNotification']['toid'])) {
                                $bAlreadyGotMessage = true;
                            }

                            /*
                              $notification_data = $this->UserNotification->find('first', array('conditions' => array('UserNotification.toid' => $_POST['user_id'], 'UserNotification.image_url' => 'score_notification')));
                              if(!empty($notification_data['UserNotification']['toid'])){
                              $userArr['UserNotification']['id'] = $notification_data['UserNotification']['id'];
                              $priorScoreMessage = $notification_data['UserNotification']['message'];
                              if ($priorScoreMessage == $scoreMessage) {
                              $bScoreMessageUpdated = false;
                              }
                              }
                             */

                            if ($bAlreadyGotMessage == false) {
                                $userArr = array();
                                $userArr['UserNotification']['fromid'] = $_POST['user_id'];
                                $userArr['UserNotification']['toid'] = $_POST['user_id'];
                                $userArr['UserNotification']['image_id'] = 0;
                                $userArr['UserNotification']['image_url'] = "score_notification";
                                $userArr['UserNotification']['message'] = $scoreMessage;
                                $userArr['UserNotification']['type'] = 0;
                                //pr($userArr);die;
                                $this->UserNotification->save($userArr);
                                unset($userArr);

                                $this->loadModel('User');
                                $user_data = $this->User->find('first', array('conditions' => array('User.id' => $_POST['user_id'])));
                                $user_data['User']['notification_flag'] = 1;
                                $this->User->save($user_data);
                            }
                        }
                        //---------------------------------------------
                    }

                    $profileData = array();
                    $this->loadModel('User');
                    $userData = $this->User->find('first', array('conditions' => array('User.id' => $_POST['user_id']), 'fields' => array('id', 'username', 'jid', 'phone_number', 'push_notification_status', 'device_id', 'device_type')));

                    $userNm = $userData['User']['phone_number'];
                    if (!empty($userData['User']['username'])) {
                        $userNm = $userData['User']['username'];
                    }

                    $profileData['UserImage']['user_id'] = $_POST['user_id'];
                    $profileData['UserImage']['image_type'] = $_POST['image_type'];
                    $profileData['UserImage']['image'] = $userImage;
                    $profileData['UserImage']['lat'] = $_POST['lat'];
                    $profileData['UserImage']['lon'] = $_POST['lon'];
                    $profileData['UserImage']['category_idx'] = $_POST['category_idx'];

                    $this->loadModel('User');
                    $this->User->Behaviors->load('Containable');
                    $this->User->bindModel(array('hasMany' => array('UserContact' => array('foreignKey' => 'contact_user_id', 'fields' => array('id', 'user_id', 'contact_user_id', 'status'),))));
                    $this->User->UserContact->bindModel(array('belongsTo' => array('User' => array('foreignKey' => 'user_id', 'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status'),))));

                    $userData = $this->User->find('first', array('contain' => array('UserContact' => array('User')), 'conditions' => array('User.id' => $_POST['user_id'])));
                    $userNm = $userData['User']['phone_number'];
                    if (!empty($userData['User']['full_name'])) {
                        $userNm = $userData['User']['full_name'];
                    }

                    if ($this->UserImage->save($profileData)) {

                        $imgId = $result['response_data']['id'] = $this->UserImage->id;
                        $result['response_data']['image_type'] = $_POST['image_type'];
                        // if ($_POST['image_type'] != 0) {
                        //   $this->UserImage->updateAll(array('UserImage.image_type' => 0), array('UserImage.image_type' => $_POST['image_type'], "UserImage.user_id" => $userData['User']['id'], "UserImage.id!=$imgId"));
                        // }

                        if ($_POST['image_type'] == 1) {
                            $userdata = array();
                            $this->UserImage->updateAll(array('status' => '0'), array('user_id' => $_POST['user_id'], 'image_type' => '1'));
                            $userdata['UserImage']['id'] = $this->UserImage->id;
                            $userdata['UserImage']['status'] = 1;
                            $this->UserImage->save($userdata);
                        }

                        if (($_POST['image_type'] == 2) && (!empty($oldCoverImage['UserImage']['id']))) {
                            //REMOVE OLD COVER IMAGE
                            if ($this->UserImage->delete($oldCoverImage['UserImage']['id'])) {
                                $success_status = 1;
                            }
                        }
                    }

                    /* sending in app notification to same city users */
                    $userProfileData = $this->User->find('first', array('conditions' => array('User.id' => $_POST['user_id']), 'fields' => array('id', 'username', 'city')));
                    $image_category_id = $_POST['category_idx'];
                    $city = isset($_POST['city']) ? $_POST['city'] : '';
                    if (strtolower($city) == 'great' || strtolower($city) == 'great manchester' || strtolower($city) == 'greater') {
                        $city = 'Manchester';
                    }
                    if (strtolower($city) == 'great london') {
                        $city = 'London';
                    }

                    // if ($image_category_id != '-1' && !empty($city) && ($userProfileData['city'] == $city)) {
                    if ($image_category_id != '-1' && !empty($city)) {
                        $image_path = $img_path;
                        $sender_id = $_POST['user_id'];
                        $msg = base64_encode($city . '|' . $sender_id . '|' . $image_category_id . '|' . $image_path);
                        $proc_command = "wget https://admin.mapbuzz.com/app2_notification.php?t=$msg -q -O - -b";
                        $proc = popen($proc_command, "r");
                        pclose($proc);
                    }

                    $jsonEncode = json_encode($result);
                    return $jsonEncode;
                } else {
                    $result['response_status'] = 0;
                    $result['response_code'] = 400;
                    $result['response_message_code'] = 500;
                    $result['message'] = 'error';
                    $result['response_data']['error'] = $this->Upload->error;
                    $result['response_data']['image'] = $_FILES['image']['name'];
                    $jsonEncode = json_encode($result);

                    return $jsonEncode;
                }
            } else {

                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = 413;
                $result['message'] = 'cannot upload image of size more than 8MB.';
                $result['response_data'] = array();
                $result['response_data']['image'] = '';
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            }
        } else {
            $result['response_status'] = 0;
            $result['response_code'] = 400;
            $result['response_message_code'] = 404;
            $result['message'] = 'file not found';
            $result['response_data'] = array();
            $result['response_data']['image'] = '';
            $jsonEncode = json_encode($result);
            return $jsonEncode;
        }
    }

    public function get_user_images($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('UserImage');
            $this->UserImage->bindModel(array(
                'hasOne' => array(
                    'UserImageLike' => array(
                        'className' => 'UserImageLike',
                        'foreignKey' => 'user_image_id',
                        'conditions' => array('UserImageLike.user_id' => $decoded['viewer_id'], 'UserImageLike.status' => 1),
                        'fields' => array('id', 'user_id', 'user_image_id', 'status')
                    )
                )
                    ), false);

            $data = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $decoded['user_id']), 'order' => 'UserImage.id ASC'));

            $user_images = array();
            foreach ($data as $key => $val) {
                $user_images[$key]['id'] = $val['UserImage']['id'];
                $user_images[$key]['user_id'] = $val['UserImage']['user_id'];
                $user_images[$key]['image_type'] = $val['UserImage']['image_type'];
                $user_images[$key]['image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image']; //SITE_URL . "/uploads/user_images/thumb/" . $val['UserImage']['image'];
                $user_images[$key]['org_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                $user_images[$key]['viewer_like_status'] = 0;
                $user_images[$key]['lat'] = $val['UserImage']['lat'];
                $user_images[$key]['lon'] = $val['UserImage']['lon'];
                $user_images[$key]['category_idx'] = $val['UserImage']['category_idx'];
                $user_images[$key]['added_on'] = $val['UserImage']['created'];

                if (!empty($val['UserImageLike']['id'])) {
                    $user_images[$key]['viewer_like_status'] = 1;
                }
                $user_images[$key]['likes'] = $val['UserImage']['total_likes'];
            }
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = $user_images;
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_user_image_likes($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $this->loadModel('UserImageLike');
            $this->UserImageLike->Behaviors->load('Containable');
            $this->UserImageLike->bindModel(array(
                'belongsTo' => array(
                    'User' => array(
                        'className' => 'User',
                        'foreignKey' => 'user_id',
                        'fields' => array('id', 'username', 'username', 'first_name', 'last_name', 'profile_image', 'status')
                    )
                )
                    ), false);

            $this->UserImageLike->User->bindModel(array(
                'hasOne' => array(
                    'UserImage' => array(
                        'className' => 'UserImage',
                        'foreignKey' => 'user_id',
                        'conditions' => array('UserImage.image_type' => 1),
                        'fields' => array('id', 'user_id', 'image', 'status')
                    )
                )
                    ), false);

            $data = $this->UserImageLike->find('all', array(
                'contain' => array('User' => array(
                        'UserImage',
                    )),
                'conditions' => array('UserImageLike.user_image_id' => $decoded['user_image_id'], 'UserImageLike.status' => 1)
            ));

            $user_images = array();
            foreach ($data as $key => $val) {
                $user_images[$key]['user_id'] = $val['User']['id'];
                $user_images[$key]['username'] = $val['User']['username'];
                $user_images[$key]['first_name'] = $val['User']['first_name'];
                $user_images[$key]['last_name'] = $val['User']['last_name'];
                $user_images[$key]['image_path'] = "";
                if (!empty($val['User']['UserImage']['image'])) {
                    $user_images[$key]['image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['User']['UserImage']['image'];
                }
            }
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = $user_images;
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_image_type($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            $profileData = array();
            $this->loadModel('UserImage');
            $profileData['UserImage']['id'] = $decoded['image_id'];
            $profileData['UserImage']['image_type'] = $decoded['image_type'];

            $this->loadModel('User');
            $this->User->Behaviors->load('Containable');
            $this->User->bindModel(array('hasMany' => array('UserContact' => array('foreignKey' => 'contact_user_id', 'fields' => array('id', 'user_id', 'contact_user_id', 'status'),))));
            $this->User->UserContact->bindModel(array('belongsTo' => array('User' => array('foreignKey' => 'user_id', 'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status'),))));

            $userData = $this->User->find('first', array('contain' => array('UserContact' => array('User')), 'conditions' => array('User.id' => $decoded['user_id'])));
            $userNm = $userData['User']['phone_number'];

            if (!empty($userData['User']['full_name'])) {
                $userNm = $userData['User']['full_name'];
            }

            if ($this->UserImage->save($profileData)) {
                $imgId = $decoded['image_id'];
                if ($decoded['image_type'] != 0) {
                    $this->UserImage->updateAll(array('UserImage.image_type' => 0), array('UserImage.image_type' => $decoded['image_type'], 'UserImage.user_id' => $userData['User']['id'], "UserImage.id!=$imgId"));
                }

                if ($decoded['image_type'] == 1) {
                    
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'success';
                $responseData['response_data'] = $profileData['UserImage'];
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 500;
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function update_image_like_status($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('UserImageLike');
            $this->loadModel('UserImage');
            $this->loadModel('User');
            $this->UserImage->bindModel(array('belongsTo' => array('User' => array('foreignKey' => 'user_id', 'fields' => array('id', 'username', 'jid', 'push_notification_status', 'device_id', 'device_type'),))));

            $userLikeArr = array();
            $userLikeArr['UserImageLike']['user_id'] = $decoded['user_id'];

            $likedData = $this->UserImageLike->find('first', array('conditions' => array("UserImageLike.user_image_id" => $decoded['image_id'], "UserImageLike.user_id" => $decoded['user_id'])));

            if (!empty($likedData)) {
                $userLikeArr['UserImageLike']['id'] = $likedData['UserImageLike']['id'];
            }
            $userLikeArr['UserImageLike']['user_image_id'] = $decoded['image_id'];
            $userLikeArr['UserImageLike']['status'] = 1;
            $updateUseImgTbl = "total_likes+1";
            if ($likedData['UserImageLike']['status'] == 1) {
                $userLikeArr['UserImageLike']['status'] = 0;
                $updateUseImgTbl = "total_likes-1";
            }

            if ($updateUseImgTbl < 0) {
                $updateUseImgTbl = 0;
            }

            $responseData = array();
            if ($this->UserImageLike->save($userLikeArr)) {
                if ($userLikeArr['UserImageLike']['status'] == 1) {
                    $imageData = $this->UserImage->find('first', array('conditions' => array("UserImage.id" => $decoded['image_id'])));
                    $userData = $this->User->find('first', array('conditions' => array("User.id" => $decoded['user_id']), 'fields' => array('id', 'username', 'phone_number', 'full_name', 'jid', 'device_id', 'device_type')));
                    $userNm = $userData['User']['phone_number'];

                    if (!empty($userData['User']['full_name'])) {
                        $userNm = $userData['User']['full_name'];
                    }

                    if (!empty($imageData) && (!empty($userData)) && ($imageData['User']['id'] != $userData['User']['id'])) {
                        $userArr = array();
                        $this->loadModel('UserNotification');
                        $userArr['UserNotification']['fromid'] = $userData['User']['id'];
                        $userArr['UserNotification']['toid'] = $imageData['User']['id'];
                        $userArr['UserNotification']['image_id'] = $imageData['UserImage']['id'];

                        $userArr['UserNotification']['image_url'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $imageData['UserImage']['image'];

                        $userArr['UserNotification']['message'] = $userNm . " liked your moment";
                        $userArr['UserNotification']['type'] = 1;
                        $this->UserNotification->save($userArr);
                        unset($userArr);
                    }

                    if (!empty($imageData['User']['device_id']) && ($imageData['User']['push_notification_status'] == 1) && ($imageData['User']['id'] != $userData['User']['id'])) {
                        // $this->send_notification_for_iphone($imageData['User']['device_id'], $userNm . " liked your moment", 7, $userData['User']['id'], $imageData['UserImage']['id']);
                        if ($imageData['User']['device_type'] == 1) {
                            $this->send_notification_for_android($imageData['User']['device_id'], $userNm . " liked your moment", 7, $userData['User']['id'], $imageData['UserImage']['id']);
                        } else if ($imageData['User']['device_type'] == 2) {
                            $this->send_notification_for_iphone_test($imageData['User']['device_id'], $userNm . " liked your moment", 7, $userData['User']['id'], $imageData['UserImage']['id']);
                        } else {
                            $this->send_notification_for_iphone_test($imageData['User']['device_id'], $userNm . " liked your moment", 7, $userData['User']['id'], $imageData['UserImage']['id']);
                        }
                    }

                    if ($imageData['User']['id'] != $userData['User']['id']) {
                        //add point
                        $targetUserId = $imageData['User']['id'];
                        $this->loadModel('UserLocation');
                        $userLocationData = array();

                        $priorPoint = 0;
                        $user_lat = "0.0";
                        $user_lon = "0.0";
                        $user_hide_from_map = 0;

                        $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $targetUserId)));
                        if (!empty($location_data['UserLocation']['user_id'])) {
                            $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];

                            $priorPoint = $location_data['UserLocation']['score'];
                            $user_lat = $location_data['UserLocation']['lat'];
                            $user_lon = $location_data['UserLocation']['lon'];
                            $user_hide_from_map = $location_data['UserLocation']['hide_from_map'];
                        }

                        $newPoint = $priorPoint + 3;
                        $userLocationData['UserLocation']['user_id'] = $targetUserId;
                        $userLocationData['UserLocation']['score'] = $newPoint;
                        $userLocationData['UserLocation']['lat'] = $user_lat;
                        $userLocationData['UserLocation']['lon'] = $user_lon;
                        $userLocationData['UserLocation']['hide_from_map'] = $user_hide_from_map;

                        $this->UserLocation->save($userLocationData);

                        $bGotScore = false;
                        $scoreMessage = "";

                        if ($newPoint >= 10000) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a President";
                        } else if ($newPoint >= 2000) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a Governor";
                        } else if ($newPoint >= 1000) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a Ambassador";
                        } else if ($newPoint >= 500) {
                            $bGotScore = true;
                            $scoreMessage = "Congratulations, you are now a Mayor";
                        }

                        if ($bGotScore) {
                            $bAlreadyGotMessage = false;

                            $this->loadModel('UserNotification');

                            $notification_data = $this->UserNotification->find('first', array('conditions' => array('UserNotification.toid' => $targetUserId, 'UserNotification.image_url' => 'score_notification', 'UserNotification.message' => $scoreMessage)));

                            if (!empty($notification_data['UserNotification']['toid'])) {
                                $bAlreadyGotMessage = true;
                            }

                            if ($bAlreadyGotMessage == false) {

                                $userArr = array();
                                $userArr['UserNotification']['fromid'] = $targetUserId;
                                $userArr['UserNotification']['toid'] = $targetUserId;
                                $userArr['UserNotification']['image_id'] = 0;
                                $userArr['UserNotification']['image_url'] = "score_notification";
                                $userArr['UserNotification']['message'] = $scoreMessage;
                                $userArr['UserNotification']['type'] = 0;
                                //pr($userArr);die;
                                $this->UserNotification->save($userArr);
                                unset($userArr);

                                //send push notification
                                //$this->loadModel('User');
                                $target_user_data = $this->User->find('first', array('conditions' => array('User.id' => $targetUserId)));
                                $target_user_data['User']['notification_flag'] = 1;
                                $this->User->save($target_user_data);

                                $user_profile = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status', 'is_private', 'device_id'), 'conditions' => array('User.id' => $targetUserId)));
                                // $this->send_notification_for_iphone($user_profile['User']['device_id'], $scoreMessage);
                                if ($user_profile['User']['device_type'] == 1) {
                                    $this->send_notification_for_android($user_profile['User']['device_id'], $scoreMessage);
                                } else if ($user_profile['User']['device_type'] == 2) {
                                    $this->send_notification_for_iphone_test($user_profile['User']['device_id'], $scoreMessage);
                                } else {
                                    $this->send_notification_for_iphone_test($user_profile['User']['device_id'], $scoreMessage);
                                }
                            }
                        }
                        //---------------------------------------------
                    }
                } else {
                    $imageData = $this->UserImage->find('first', array('conditions' => array("UserImage.id" => $decoded['image_id'])));
                    $userData = $this->User->find('first', array('conditions' => array("User.id" => $decoded['user_id']), 'fields' => array('id', 'username', 'phone_number', 'full_name', 'jid', 'device_id', 'device_type')));
                    $userNm = $userData['User']['phone_number'];

                    if (!empty($userData['User']['full_name'])) {
                        $userNm = $userData['User']['full_name'];
                    }

                    // $body = base64_encode($userNm . " disliked you");
                    // $userContData = base64_encode(json_encode(array($imageData)));
                    // $postFields = array('userContctacts' => $userContData, 'body' => $body);
                    // $ch = curl_init();
                    // $callUrl = Router::url(array('controller' => 'web_services', 'action' => 'send_messages', 43, 23, 'photoDislike', 'YES', $imageData['UserImage']['id']), true);
                    // $callUrl = str_replace("http://", "https://", $callUrl);
                    // //echo $callUrl;die;
                    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    // curl_setopt($ch, CURLOPT_URL, $callUrl);
                    // curl_setopt($ch, CURLOPT_POST, count($postFields));
                    // curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                    // curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    // curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                    // echo $contents = curl_exec($ch);
                    // curl_close($ch);


                    if (!empty($imageData['User']['device_id']) && ($imageData['User']['push_notification_status'] == 1)) {
                        
                    }

                    $image_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $imageData['UserImage']['image'];

                    $this->loadModel('UserNotification');
                    $this->UserNotification->deleteAll(array('UserNotification.fromid' => $userData['User']['id'], 'UserNotification.toid' => $imageData['User']['id'], 'UserNotification.image_id' => $imageData['UserImage']['id'], 'UserNotification.type' => 1));
                }

                $this->loadModel('UserImage');
                $this->UserImage->updateAll(array("UserImage.total_likes" => $updateUseImgTbl), array('UserImage.id' => $decoded['image_id']));
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userLikeArr['UserImageLike']['status'];
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 500;
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function upload_user_cover_image() {
        if (!empty($_FILES)) {
            if ($_FILES['image']['size'] < 6291456) {

                $thumbRules = array('size' => array(200, 100), 'type' => 'resizecrop');
                $largeRules = array('size' => array(600, 300), 'type' => 'resizecrop');

                $path_info = pathinfo($_FILES['image']['name']);
                $_FILES['image']['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];
                $res1 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_COVER_PICS . DS . "thumb" . DS, '', $thumbRules, array('png', 'jpg', 'jpeg', 'gif'));
                $res2 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_COVER_PICS . DS . "large" . DS, '', $largeRules, array('png', 'jpg', 'jpeg', 'gif'));
                $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . PROFILE_COVER_PICS . DS . "original" . DS, '', '', array('png', 'jpg', 'jpeg', 'gif'));

                if (!empty($this->Upload->result)) {
                    $message['image'] = $this->Upload->result;
                    $userImage = $this->Upload->result;

                    $result['response_status'] = 1;
                    $result['response_code'] = 200;
                    $result['response_message_code'] = '';
                    $result['message'] = 'success';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = $userImage;

                    $profileData = array();
                    $this->loadModel('User');
                    $data = $this->User->find('first', array('conditions' => array('User.id' => $_POST['user_id'])));
                    @unlink(WWW_ROOT . PROFILE_COVER_PICS . DS . "original" . DS . $data['User']['profile_cover_image']);
                    @unlink(WWW_ROOT . PROFILE_COVER_PICS . DS . "thumb" . DS . $data['User']['profile_cover_image']);
                    @unlink(WWW_ROOT . PROFILE_COVER_PICS . DS . "large" . DS . $data['User']['profile_cover_image']);
                    $profileData['User']['id'] = $_POST['user_id'];
                    $profileData['User']['profile_cover_image'] = $userImage;
                    $this->User->save($profileData);
                    $jsonEncode = json_encode($result);
                    return $jsonEncode;
                } else {
                    $result['response_status'] = 0;
                    $result['response_code'] = 400;
                    $result['response_message_code'] = 500;
                    $result['message'] = 'error';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = '';
                    $jsonEncode = json_encode($result);

                    return $jsonEncode;
                }
            } else {
                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = 413;
                $result['message'] = 'cannot upload image of size more than 8MB.';
                $result['response_data'] = array();
                $result['response_data']['image'] = '';
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            }
        } else {
            $result['response_status'] = 0;
            $result['response_code'] = 400;
            $result['response_message_code'] = 404;
            $result['message'] = 'file not found';
            $result['response_data'] = array();
            $result['response_data']['image'] = '';
            $jsonEncode = json_encode($result);
            return $jsonEncode;
        }
    }

    function upload_media_file() {
        App::import('Lib', 'S3');
        if (!empty($_FILES)) {
            $this->loadModel('User');
            $app_data = $this->User->find('first', array('fields' => array('User.id', 'User.jid'), 'conditions' => array('User.id' => $_POST['id'])));
            if (empty($app_data)) {
                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = '';
                $result['message'] = 'invalid user';
                $result['response_data'] = array();
                $result['response_data']['media'] = "";
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            }

            if ($_FILES['media']['size'] < 16777216) {
                $path_info = pathinfo($_FILES['media']['name']);
                $_FILES['media']['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];
                //$jidNo = explode("@", $_POST['jid']);
                $jidNo = explode("@", $app_data['User']['jid']);
                $imgpath = "uploads/media/" . $jidNo[0];
                $path = WWW_ROOT . "uploads" . DS . "media" . DS . $jidNo[0];
                if (!is_dir("$path")) {
                    if (!mkdir($path, 0777, true)) {
                        die('Failed to create folders...');
                    }
                }

                if (!is_dir("$path/thumb")) {
                    if (!mkdir("$path/thumb", 0777, true)) {
                        die('Failed to create folders...');
                    }
                }

                $userMedia = $_FILES['media']['name'];
                $thumbMedia = substr($userMedia, 0, strrpos($userMedia, '.')) . ".jpg";

                if ($this->save_image_s3bucket($_FILES['media']['tmp_name'], $imgpath . '/' . $userMedia)) {


                    $image = MOBILE_MEDIA . DS . $jidNo[0] . DS . "thumb/$thumbMedia";
                    if (isset($_FILES['thumb']) && ($_FILES['thumb']['tmp_name'] < 1)) {
                        $this->save_image_s3bucket($_FILES['thumb']['tmp_name'], $imgpath . '/thumb/' . $thumbMedia);
                    }
                    $result['response_status'] = 1;
                    $result['response_code'] = 200;
                    $result['response_message_code'] = '';
                    $result['message'] = 'success';
                    $result['response_data'] = array();
                    $result['response_data']['media'] = "https://d35ugz1sdahij6.cloudfront.net/" . $imgpath . "/" . $userMedia; //txtter-upload-api.s3.amazonaws.com

                    $thumbPath = "https://d35ugz1sdahij6.cloudfront.net/" . $imgpath . "/thumb/" . $thumbMedia;

                    $result['response_data']['thumb'] = $thumbPath;
                    $result['response_data']['jid'] = $app_data['User']['jid'];
                    $result['response_data']['message_id'] = $_POST['message_id'];

                    $mediaData = array();

                    $this->loadModel('AppMediaFile');
                    $mediaData['AppMediaFile']['jid'] = $app_data['User']['jid'];
                    $mediaData['AppMediaFile']['app_user_id'] = $app_data['User']['id'];
                    $mediaData['AppMediaFile']['file'] = $userMedia;
                    $this->AppMediaFile->save($mediaData);

                    $result['response_data']['media_id'] = $this->AppMediaFile->id;
                    $jsonEncode = json_encode($result);
                    return $jsonEncode;
                } else {
                    $result['response_status'] = 0;
                    $result['response_code'] = 400;
                    $result['response_message_code'] = 500;
                    $result['message'] = 'error';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = '';
                    $jsonEncode = json_encode($result);

                    return $jsonEncode;
                }
            } else {
                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = 413;
                $result['message'] = 'cannot upload image of size more than 16MB.';
                $result['response_data'] = array();
                $result['response_data']['image'] = '';
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            }
        } else {
            $result['response_status'] = 0;
            $result['response_code'] = 400;
            $result['response_message_code'] = 404;
            $result['message'] = 'file not found';
            $result['response_data'] = array();
            $result['response_data']['image'] = '';
            $jsonEncode = json_encode($result);
            return $jsonEncode;
        }
    }

    function save_profile_image($ImgBinary = null) {
        $filename = "GAME_" . time() . rand(7, 15) . ".png";
        $path_names = array(GAME_DIR, GAME_TINY_DIR);
        foreach ($path_names as $val) {
            $f = fopen($val . "/" . $filename, "wb");
            if (!fwrite($f, $this->base64url_decode($ImgBinary), (int) strlen($ImgBinary))) {
                fclose($f);
            }
        }
        return $filename;
    }

    public function update_like_status($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('UserLike');

            $userLikeArr = array();
            $userLikeArr['UserLike']['user_id'] = $decoded['user_id'];

            $this->loadModel('User');
            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status'), 'conditions' => array('User.phone_number' => $decoded['phone_number'])));

            $likedData = $this->UserLike->find('first', array('conditions' => array("UserLike.liked_user_id" => $userData['User']['id'], "UserLike.user_id" => $decoded['user_id'])));

            $likedBy = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status'), 'conditions' => array('User.id' => $decoded['user_id'])));

            $userNm = $likedBy['User']['phone_number'];

            if (!empty($likedBy['User']['username'])) {
                $userNm = $likedBy['User']['username'];
            }

            if (!empty($likedData)) {
                $userLikeArr['UserLike']['id'] = $likedData['UserLike']['id'];
            }
            $userLikeArr['UserLike']['liked_user_id'] = $userData['User']['id'];

            $userLikeArr['UserLike']['status'] = 1;
            $likeVal = "YES";
            $body = $userNm . " liked you";
            if ($likedData['UserLike']['status'] == 1) {
                $userLikeArr['UserLike']['status'] = 0;
                $body = $userNm . " disliked you";
                $likeVal = "NO";
            }

            $responseData = array();
            if ($this->UserLike->save($userLikeArr)) {

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userLikeArr['UserLike']['status'];
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 500;
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }

            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {

            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_liked($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = array();

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $Reqfields = array();
            $ReqConditions = array();
            $this->loadModel('User');
            $this->loadModel('UserLike');
            $this->UserLike->bindModel(
                    array(
                        'belongsTo' => array(
                            'User' => array(
                                'foreignKey' => 'liked_user_id',
                                'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status'),
                            )
                        )
                    )
            );

            $res = $this->UserLike->find('all', array('conditions' => array('UserLike.user_id' => $decoded['user_id'], 'UserLike.status' => 1)));

            $contactData = array();
            if (!empty($res)) {
                foreach ($res as $key => $value) {
                    $contactData[$key] = $value['User'];
                }
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $contactData;
            $data = array('response' => $responseData);
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 400;
            $responseData['message'] = 'error';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
        }
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function update_block_status($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('UserBlock');

            $userBlockArr = array();
            $userBlockArr['UserBlock']['user_id'] = $decoded['user_id'];

            $this->loadModel('User');
            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status'), 'conditions' => array('User.id' => $decoded['other_user_id'])));

            $blockedData = $this->UserBlock->find('first', array('conditions' => array("UserBlock.blocked_user_id" => $userData['User']['id'], "UserBlock.user_id" => $decoded['user_id'])));
            $blockedBy = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status'), 'conditions' => array('User.id' => $decoded['user_id'])));

            //$userNm = $blockedBy['User']['phone_number'];
            //if (!empty($blockedBy['User']['username'])) {
            $userNm = $blockedBy['User']['username'];
            //}

            if (!empty($blockedData)) {
                $userBlockArr['UserBlock']['id'] = $blockedData['UserBlock']['id'];
            }
            $userBlockArr['UserBlock']['blocked_user_id'] = $userData['User']['id'];
            $userBlockArr['UserBlock']['status'] = 1;
            $body = $userNm . " blocked you";
            $blockVal = "YES";
            if ($blockedData['UserBlock']['status'] == 1) {
                $userBlockArr['UserBlock']['status'] = 0;
                $body = $userNm . " unblocked you";
                $blockVal = "NO";
            }

            $responseData = array();
            if ($this->UserBlock->save($userBlockArr)) {

                $body = base64_encode($body);
                $userContData = base64_encode(json_encode(array($userData)));
                $postFields = array('userContctacts' => $userContData, 'body' => $body);
                // $ch = curl_init();
                // $callUrl = Router::url(array('controller' => 'web_services', 'action' => 'send_messages', 43, 23, 'block', $blockVal, $blockedBy['User']['id']), true);
                // $callUrl = str_replace("http://", "https://", $callUrl);
                // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                // curl_setopt($ch, CURLOPT_URL, $callUrl);
                // curl_setopt($ch, CURLOPT_POST, count($postFields));
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                // curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                // echo $contents = curl_exec($ch);
                // curl_close($ch);


                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userBlockArr['UserBlock']['status'];
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 500;
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_blocked($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = array();

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            if (!empty($decoded)) {
                $Reqfields = array();
                $ReqConditions = array();
                $this->loadModel('User');

                $this->loadModel('UserBlock');
                $this->UserBlock->bindModel(
                        array(
                            'belongsTo' => array(
                                'User' => array(
                                    'foreignKey' => 'blocked_user_id',
                                    'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status'),
                                )
                            )
                        )
                );

                $res = $this->UserBlock->find('all', array('conditions' => array('UserBlock.user_id' => $decoded['user_id'], 'UserBlock.status' => 1)));

                $contactData = array();
                if (!empty($res)) {
                    foreach ($res as $key => $value) {
                        $contactData[$key] = $value['User'];
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'success';
                $responseData['response_data']['data'] = $contactData;
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['message'] = 'error';
                $responseData['response_data']['data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function add_favorite($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('AppUser');
            $this->loadModel('AppUserContact');
            $phoneNo = implode("|", $decoded['phones']);
            $app_contact_data = $this->AppUserContact->find('list', array('fields' => array('AppUserContact.id', 'AppUserContact.contact_app_user_id'), 'conditions' => array("AppUserContact.app_user_id" => $decoded['user_id'], "AppUserContact.user_phone_number REGEXP  '($phoneNo)$'")));

            $app_data = $this->AppUser->find('all', array('fields' => array('AppUser.id', 'AppUser.phone', 'AppUser.full_phone_number', 'AppUser.username', 'AppUser.profile_status_message', 'AppUser.country_dial_code', 'AppUser.profile_image', 'AppUser.email'), 'conditions' => array("AppUser.full_phone_number REGEXP '($phoneNo)$'")));

            $TopChatAvailableIds = array();
            $TopChatUserInfos = array();

            foreach ($app_data as $key => $appVal) {
                $TopChatAvailableIds[] = $appVal['AppUser']['id'];
                $TopChatUserInfos[] = $appVal['AppUser'];
            }

            $NotInContactList = array_diff($TopChatAvailableIds, $app_contact_data);
            $appContactData = array();

            foreach ($app_data as $key => $appVal) {
                if (in_array($appVal['AppUser']['id'], $NotInContactList)) {
                    $appContactData[$key]['AppUserContact']['app_user_id'] = $decoded['user_id'];
                    $appContactData[$key]['AppUserContact']['contact_app_user_id'] = $appVal['AppUser']['id'];
                    $appContactData[$key]['AppUserContact']['device_contact_name'] = $appVal['AppUser']['country_dial_code'] . $appVal['AppUser']['phone'];
                    $appContactData[$key]['AppUserContact']['contact_row_id'] = 0;
                    $appContactData[$key]['AppUserContact']['contact_type'] = '';
                    $appContactData[$key]['AppUserContact']['contact_email_id'] = $appVal['AppUser']['phone'];
                    $appContactData[$key]['AppUserContact']['favorite_status'] = 1;
                    $appContactData[$key]['AppUserContact']['user_phone_number'] = str_replace("+", "", $appVal['AppUser']['country_dial_code']) . $appVal['AppUser']['phone'];
                }
            }
            if (!empty($appContactData)) {
                $this->AppUserContact->saveAll($appContactData);
            }

            $responseData = array();
            if ($this->AppUserContact->updateAll(array('AppUserContact.favorite_status' => 1), array("AppUserContact.app_user_id" => $decoded['user_id'], "AppUserContact.user_phone_number REGEXP '($phoneNo)$'"))) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $TopChatUserInfos;
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 500;
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_favorites($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = array();
        $data = '{"phone":"919928121212"}';

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $Reqfields = array();
            $ReqConditions = array();
            $this->loadModel('AppUser');
            $appData = $this->AppUser->find('first', array('id' => array('id'), 'conditions' => array('AppUser.full_phone_number' => $decoded['phone'])));
            $userId = $appData['AppUser']['id'];
            $ReqConditions = array('AppUserContact.app_user_id' => $userId, 'AppUserContact.favorite_status' => '1');

            $this->loadModel('AppUserContact');
            $this->AppUserContact->bindModel(
                    array(
                        'belongsTo' => array(
                            'AppUser' => array(
                                'foreignKey' => 'contact_app_user_id',
                                'fields' => array('id', 'username', 'email', 'profile_status_message', 'profile_image', 'country_dial_code', 'phone', 'full_phone_number', 'jid'),
                            )
                        )
                    )
            );

            $res = $this->AppUserContact->find('all', array('fields' => $Reqfields, 'conditions' => array($ReqConditions)));
            $resData = array();
            foreach ($res as $key => $value) {
                $resData[$key]['id'] = $value['AppUserContact']['id'];
                $resData[$key]['username'] = $value['AppUser']['username'];
                $resData[$key]['email'] = $value['AppUser']['email'];
                $resData[$key]['profile_status_message'] = $value['AppUser']['profile_status_message'];
                $resData[$key]['jid'] = $value['AppUser']['jid'];
                $resData[$key]['app_user_id'] = $value['AppUserContact']['app_user_id'];
                $resData[$key]['contact_app_user_id'] = $value['AppUserContact']['contact_app_user_id'];
                $resData[$key]['device_contact_name'] = $value['AppUserContact']['device_contact_name'];
                $resData[$key]['contact_row_id'] = $value['AppUserContact']['contact_row_id'];
                $resData[$key]['contact_type'] = $value['AppUserContact']['contact_type'];
                $resData[$key]['contact_email_id'] = $value['AppUserContact']['contact_email_id'];
                $resData[$key]['user_phone_number'] = $value['AppUserContact']['user_phone_number'];
                $resData[$key]['favorite_status'] = $value['AppUserContact']['favorite_status'];
                $resData[$key]['created'] = $value['AppUserContact']['created'];
                $resData[$key]['modified'] = $value['AppUserContact']['modified'];
            }
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data']['data'] = $resData;
            $data = array('response' => $responseData);
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
        }
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function add_contacts_temp($test_data = null) {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $this->loadModel('User');
        $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['userid'])));
        print_r($user_data);
        die();
    }

    public function add_follower() {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $user_id = $decoded['user_id'];
        $follower_id = $decoded['follower_id'];
        $this->loadModel('User');
        $user_data = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
        if ($user_data['User']['is_private']) {
            $is_approvad = 0;
        } else {
            $is_approvad = 1;
        }
        $this->loadModel('Followers');

        $userdata = array(
            'user_id' => $user_id,
            'follower_id' => $follower_id,
            'is_approvad' => $is_approvad,
            'created' => date("Y-m-d H:i:s", strtotime('now')),
            'modified' => date("Y-m-d H:i:s", strtotime('now'))
        );
        if ($this->Followers->save($userdata)) {
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "somthing wrong";
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_followers() {
       
        $user_id = $_GET['user_id'];
        $this->loadModel('Followers');
        $user_data = $this->Followers->find('list', array(
            'conditions' => array('Followers.user_id' => $user_id), // array of conditions
            // array of field names
            'fields' => array('Followers.follower_id', 'Followers.is_approvad'),
        ));
        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = '';
        $responseData['message'] = "success";
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($user_data);
        return $jsonEncode;
    }

    public function get_following() {

        $follower_id = $_GET['follower_id'];
        $this->loadModel('Followers');
        $user_data = $this->Followers->find('list', array(
            'conditions' => array('Followers.follower_id' => $follower_id), // array of conditions
            // array of field names
            'fields' => array('Followers.user_id', 'Followers.is_approvad'),
        ));
        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = '';
        $responseData['message'] = "success";
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($user_data);
        return $jsonEncode;
    }

    public function add_contacts($test_data = null) {
        $test = "";
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }
        $decoded = json_decode($data, true);
        $this->loadModel('User');
        $this->loadModel('UserContact');
        $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['userid'])));
        $already_exist_id = $user_data['User']['id'];
        $ContactUserIds = $decoded['user_ids'];
        foreach ($ContactUserIds as $key => &$ContactUserId) {
            if ($ContactUserId == $decoded['userid']) {
                unset($ContactUserIds[$key]);
            }
        }
        $contactNumIds = implode("','", $decoded['user_ids']);
        $responseData = array();
        if (!empty($decoded)) {
            $contactData = array();
            $contactRequestData = array();
            $this->loadModel('User');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');

            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status'), 'conditions' => array('User.id' => $decoded['userid'])));

            if (!empty($userData['User']['username'])) {
                $message = $userData['User']['username'] . " added you";
            } else {
                $message = $userData['User']['phone_number'] . " added you";
            }
            if (!empty($userData['User']['full_name'])) {
                $message = $userData['User']['full_name'] . " added you";
            } else {
                $message = $userData['User']['phone_number'] . " added you";
            }
            $isAlreadyExist = $this->UserContact->find('list', array('fields' => array('id', 'contact_user_id'), 'conditions' => array('UserContact.user_id' => $decoded['userid'], "UserContact.contact_user_id IN('$contactNumIds')")));
            $insertableUserIds = array_diff($ContactUserIds, $isAlreadyExist);
            $keyVal1 = 0;
            $newContactIds = array();
            $keyVal2 = 0;
            $newContactRequestIds = array();
            foreach ($insertableUserIds as $ContKey => $ContVal) {
                $other_user_profile = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status', 'is_private'), 'conditions' => array('User.id' => $ContVal)));
                if ($other_user_profile['User']['is_private'] == 0) {
                    $contactData[$keyVal1]['UserContact']['user_id'] = $decoded['userid'];
                    $contactData[$keyVal1]['UserContact']['contact_user_id'] = $ContVal;
                    $contactData[$keyVal1]['UserContact']['type'] = $decoded['add_type'];
                    $newContactIds[] = $ContVal;
                    $keyVal1++;
                    $this->loadModel('UserNotification');
                    $userNotificationData['UserNotification']['fromid'] = $decoded['userid'];
                    $userNotificationData['UserNotification']['toid'] = $ContVal;
                    $userNotificationData['UserNotification']['image_id'] = 0;
                    $userNotificationData['UserNotification']['image_url'] = '';
                    $userNotificationData['UserNotification']['message'] = $user_data['User']['full_name'] . " added you.";
                    $userNotificationData['UserNotification']['type'] = 0;
                    $this->UserNotification->save($userNotificationData);
                    $contactData[$keyVal1]['UserContact']['user_id'] = $ContVal;
                    $contactData[$keyVal1]['UserContact']['contact_user_id'] = $decoded['userid'];
                    $contactData[$keyVal1]['UserContact']['type'] = $decoded['add_type'];
                    $newContactIds[] = $ContVal;
                    $keyVal1++;
                }
                if ($other_user_profile['User']['is_private'] == 1) {
                    // $UserContactRequestisAlreadyExist = $this->UserContactRequest->find('first', array('fields' => array('id'), 'conditions' => array('UserContactRequest.user_id' => $decoded['userid'], "UserContactRequest.friend_id" => $ContVal)));
                    $UserContactRequestisAlreadyExist = $contact_request = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.friend_id' => $ContVal, 'UserContactRequest.user_id' => $decoded['userid'])));

                    if (empty($UserContactRequestisAlreadyExist)) {
                        $contactRequestData[$keyVal2]['UserContactRequest']['user_id'] = $decoded['userid'];
                        $contactRequestData[$keyVal2]['UserContactRequest']['friend_id'] = $ContVal;
                        $contactRequestData[$keyVal2]['UserContactRequest']['is_sender'] = 1;
                        $newContactRequestIds[] = $ContVal;
                        $keyVal2++;
                        $friend_request_message = $userData['User']['full_name'] . " sent you a friend request";
                        $test = "Notify";
                        if ($other_user_profile['User']['device_type'] == 1) {
                            $test = "Android Notify * " . "Device ID" . $other_user_profile['User']['device_id'] . "text" . $friend_request_message;

                            $this->send_notification_for_android($other_user_profile['User']['device_id'], $friend_request_message);
                        } else if ($other_user_profile['User']['device_type'] == 2) {
                            $test = "iphone Notify * " . "Device ID" . $other_user_profile['User']['device_id'] . "text" . $friend_request_message;
                            // if($ContVal==57186){
                            // 	$this->send_notification_for_iphone_test($other_user_profile['User']['device_id'], $friend_request_message);
                            // }else{
                            $this->send_notification_for_iphone_test($other_user_profile['User']['device_id'], $friend_request_message);
                            // }
                        } else {

                            // if($ContVal==57186){
                            // 	$test="iphone Notify * "."Device ID".$other_user_profile['User']['device_id']."text".$friend_request_message."with 57186";
                            // 	$this->send_notification_for_iphone_test($other_user_profile['User']['device_id'], $friend_request_message);
                            // }else{
                            $test = "iphone Notify * " . "Device ID" . $other_user_profile['User']['device_id'] . "text" . $friend_request_message;
                            $this->send_notification_for_iphone_test($other_user_profile['User']['device_id'], $friend_request_message);
                            // }
                        }
                        $this->loadModel('UserNotification');
                        $userNotificationData['UserNotification']['fromid'] = $decoded['userid'];
                        $userNotificationData['UserNotification']['toid'] = $ContVal;
                        $userNotificationData['UserNotification']['image_id'] = 0;
                        $userNotificationData['UserNotification']['image_url'] = '';
                        $userNotificationData['UserNotification']['message'] = $user_data['User']['full_name'] . " sent you a friend request.";
                        $userNotificationData['UserNotification']['type'] = 0;
                        $this->UserNotification->save($userNotificationData);
                    }
                    $UserContactRequestisAlreadyExist = $this->UserContactRequest->find('first', array('fields' => array('id'), 'conditions' => array('UserContactRequest.user_id' => $ContVal, "UserContactRequest.friend_id" => $decoded['userid'])));
                    if (empty($UserContactRequestisAlreadyExist)) {
                        $contactRequestData[$keyVal2]['UserContactRequest']['user_id'] = $ContVal;
                        $contactRequestData[$keyVal2]['UserContactRequest']['friend_id'] = $decoded['userid'];
                        $contactRequestData[$keyVal2]['UserContactRequest']['is_sender'] = 0;
                        $newContactRequestIds[] = $decoded['userid'];
                        $keyVal2++;
                    }
                }
            }

            if (!empty($contactData) || !empty($contactRequestData)) {
                if (!empty($contactData)) {
                    if ($this->UserContact->saveAll($contactData)) {
                        $this->loadModel('UserContact');
                        $this->UserContact->Behaviors->load('Containable');
                        $this->UserContact->bindModel(
                                array(
                                    'belongsTo' => array(
                                        'User' => array(
                                            'foreignKey' => 'contact_user_id',
                                            'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status'),
                                        )
                                    )
                                )
                        );
                        $this->UserContact->User->bindModel(array(
                            'hasOne' => array(
                                'UserImage' => array(
                                    'className' => 'UserImage',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserImage.image_type' => 1),
                                ),
                                'UserBlock' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'blocked_user_id',
                                    'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['userid'])
                                ),
                                'UserBlocked' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['userid'])
                                )
                            )
                                ), false);
                        $res = $this->UserContact->find(
                                'all',
                                array(
                                    'contain' => array(
                                        'User' => array(
                                            'UserImage',
                                            'UserBlock',
                                            'UserBlocked'
                                        )
                                    ),
                                    'conditions' => array('UserContact.user_id' => $decoded['userid'], 'User.status' => 1, "User.id!=''", "User.full_name IS NOT NULL", "User.username IS NOT NULL")
                                )
                        );
                        $contactData = array();
                        $adminMsgTo = array();
                        $this->loadModel('UserNotification');
                        if (!empty($res)) {
                            foreach ($res as $key => $value) {
                                $contactData[$key] = $value['User'];
                                $contactData[$key]['profile_image_path'] = "";
                                $contactData[$key]['add_type'] = $value['UserContact']['type'];
                                if (!empty($value['User']['UserImage']['image'])) {
                                    $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                                }
                                $contactData[$key]['block_status'] = 0;
                                if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                                    $contactData[$key]['block_status'] = 1;
                                }
                                $contactData[$key]['blocked_status'] = 0;
                                if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                                    $contactData[$key]['blocked_status'] = 1;
                                }
                                if (in_array($value['User']['id'], $newContactIds)) {
                                    $adminMsgTo[] = $value;
                                    if (!empty($value['User']['jid'])) {
                                        $userArr = array();
                                        $userArr['UserNotification']['fromid'] = $decoded['userid'];
                                        $userArr['UserNotification']['toid'] = $value['User']['id'];
                                        $userArr['UserNotification']['image_url'] = "";
                                        $userArr['UserNotification']['message'] = $message;
                                        $userArr['UserNotification']['type'] = 0;
                                        $this->UserNotification->save($userArr);
                                        unset($userArr);
                                        if ($value['User']['push_notification_status'] == 1) {
                                            if ($value['User']['device_type'] == 1) {
                                                $this->send_notification_for_android($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else if ($value['User']['device_type'] == 2) {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            }
                                        }
                                    }
                                }
                            }
                            $body = base64_encode($message);
                            $userContData = base64_encode(json_encode($adminMsgTo));
                            $postFields = array('userContctacts' => $userContData, 'body' => $body);
                            $ch = curl_init();
                            $callUrl = Router::url(array('controller' => 'web_services_v2', 'action' => 'send_messages', 43, 23, 'addAccount', 'YES', $userData['User']['id']), true);
                            $callUrl = str_replace("http://", "https://", $callUrl);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_URL, $callUrl);
                            curl_setopt($ch, CURLOPT_POST, count($postFields));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                            echo $contents = curl_exec($ch);
                            curl_close($ch);
                        }
                        $responseData['response_status'] = 1;
                        $responseData['response_code'] = 200;
                        $responseData['response_message_code'] = "";
                        $responseData['message'] = 'successfully';
                        $responseData['response_data'] = $contactData;
                        $data = array('response' => $responseData);
                    } else {
                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 500;
                        $responseData['message'] = 'failed';
                        $responseData['response_data'] = array();
                        $data = array('response' => $responseData);
                    }
                }
                if (!empty($contactRequestData)) {
                    $message = $friend_request_message;

                    if ($this->UserContactRequest->saveAll($contactRequestData)) {

                        $this->loadModel('UserContact');
                        $this->UserContact->Behaviors->load('Containable');
                        $this->UserContact->bindModel(
                                array(
                                    'belongsTo' => array(
                                        'User' => array(
                                            'foreignKey' => 'contact_user_id',
                                            'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status'),
                                        )
                                    )
                                )
                        );
                        $this->UserContact->User->bindModel(array(
                            'hasOne' => array(
                                'UserImage' => array(
                                    'className' => 'UserImage',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserImage.image_type' => 1),
                                ),
                                'UserBlock' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'blocked_user_id',
                                    'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['userid'])
                                ),
                                'UserBlocked' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['userid'])
                                )
                            )
                                ), false);
                        $res = $this->UserContact->find(
                                'all',
                                array(
                                    'contain' => array(
                                        'User' => array(
                                            'UserImage',
                                            'UserBlock',
                                            'UserBlocked'
                                        )
                                    ),
                                    'conditions' => array('UserContact.user_id' => $decoded['userid'], 'User.status' => 1, "User.id!=''", "User.full_name IS NOT NULL", "User.username IS NOT NULL")
                                )
                        );
                        $contactData = array();
                        $adminMsgTo = array();
                        $this->loadModel('UserNotification');
                        if (!empty($res)) {
                            foreach ($res as $key => $value) {
                                $contactData[$key] = $value['User'];
                                $contactData[$key]['profile_image_path'] = "";
                                $contactData[$key]['add_type'] = $value['UserContact']['type'];
                                if (!empty($value['User']['UserImage']['image'])) {
                                    $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                                }

                                $contactData[$key]['block_status'] = 0;
                                if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                                    $contactData[$key]['block_status'] = 1;
                                }
                                $contactData[$key]['blocked_status'] = 0;
                                if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                                    $contactData[$key]['blocked_status'] = 1;
                                }
                                if (in_array($value['User']['id'], $newContactIds)) {

                                    $adminMsgTo[] = $value;
                                    if (!empty($value['User']['jid'])) {
                                        $userArr = array();
                                        $userArr['UserNotification']['fromid'] = $decoded['userid'];
                                        $userArr['UserNotification']['toid'] = $value['User']['id'];
                                        $userArr['UserNotification']['image_url'] = "";
                                        $userArr['UserNotification']['message'] = $message;
                                        $userArr['UserNotification']['type'] = 0;
                                        $this->UserNotification->save($userArr);
                                        unset($userArr);
                                        if ($value['User']['push_notification_status'] == 1) {
                                            if ($value['User']['device_type'] == 1) {
                                                $this->send_notification_for_android($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else if ($value['User']['device_type'] == 2) {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            }
                                        }
                                    }
                                }
                            }
                            $body = base64_encode($message);
                            $userContData = base64_encode(json_encode($adminMsgTo));
                            $postFields = array('userContctacts' => $userContData, 'body' => $body);
                            $ch = curl_init();
                            $callUrl = Router::url(array('controller' => 'web_services_v2', 'action' => 'send_messages', 43, 23, 'addAccount', 'YES', $userData['User']['id']), true);
                            $callUrl = str_replace("http://", "https://", $callUrl);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_URL, $callUrl);
                            curl_setopt($ch, CURLOPT_POST, count($postFields));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                            echo $contents = curl_exec($ch);
                            curl_close($ch);
                        }
                        $responseData['response_status'] = 1;
                        $responseData['test'] = $test;
                        $responseData['response_code'] = 200;
                        $responseData['response_message_code'] = "";
                        $responseData['message'] = 'successfully';
                        $responseData['response_data'] = $contactData;
                        $data = array('response' => $responseData);
                    } else {
                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 500;
                        $responseData['message'] = 'failed';
                        $responseData['response_data'] = array();
                        $data = array('response' => $responseData);
                    }
                }
            } else {

                $this->loadModel('UserContact');
                $this->UserContact->Behaviors->load('Containable');
                $this->UserContact->bindModel(
                        array(
                            'belongsTo' => array(
                                'User' => array(
                                    'foreignKey' => 'contact_user_id',
                                    'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'profile_cover_image', 'status'),
                                )
                            )
                        )
                );
                $this->UserContact->User->bindModel(array(
                    'hasOne' => array(
                        'UserImage' => array(
                            'className' => 'UserImage',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserImage.image_type' => 1),
                        ),
                        'UserBlock' => array(
                            'className' => 'UserBlock',
                            'foreignKey' => 'blocked_user_id',
                            'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['userid'])
                        ),
                        'UserBlocked' => array(
                            'className' => 'UserBlock',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['userid'])
                        )
                    )
                        ), false);
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'contain' => array(
                                'User' => array(
                                    'UserImage',
                                    'UserBlock',
                                    'UserBlocked'
                                )
                            ),
                            'conditions' => array('UserContact.user_id' => $decoded['userid'], 'User.status' => 1, "User.id!=''", "User.full_name IS NOT NULL", "User.username IS NOT NULL")
                        )
                );
                $contactData = array();
                if (!empty($res)) {
                    foreach ($res as $key => $value) {
                        $contactData[$key] = $value['User'];
                        $contactData[$key]['profile_image_path'] = "";
                        $contactData[$key]['add_type'] = $value['UserContact']['type'];
                        if (!empty($value['User']['UserImage']['image'])) {
                            $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                        }
                        $contactData[$key]['block_status'] = 0;
                        if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                            $contactData[$key]['block_status'] = 1;
                        }

                        $contactData[$key]['blocked_status'] = 0;
                        if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                            $contactData[$key]['blocked_status'] = 1;
                        }
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $contactData;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    // ==============================================================================================
    public function add_contacts_2($test_data = null) {

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }
        $decoded = json_decode($data, true);
        $this->loadModel('User');
        $this->loadModel('UserContact');
        $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['userid'])));
        $already_exist_id = $user_data['User']['id'];
        $ContactUserIds = $decoded['user_ids'];
        foreach ($ContactUserIds as $key => &$ContactUserId) {
            if ($ContactUserId == $decoded['userid']) {
                unset($ContactUserIds[$key]);
            }
        }
        $contactNumIds = implode("','", $decoded['user_ids']);
        $responseData = array();
        if (!empty($decoded)) {
            $contactData = array();
            $contactRequestData = array();
            $this->loadModel('User');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');

            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status'), 'conditions' => array('User.id' => $decoded['userid'])));

            if (!empty($userData['User']['username'])) {
                $message = $userData['User']['username'] . " added you";
            } else {
                $message = $userData['User']['phone_number'] . " added you";
            }
            if (!empty($userData['User']['full_name'])) {
                $message = $userData['User']['full_name'] . " added you";
            } else {
                $message = $userData['User']['phone_number'] . " added you";
            }
            $isAlreadyExist = $this->UserContact->find('list', array('fields' => array('id', 'contact_user_id'), 'conditions' => array('UserContact.user_id' => $decoded['userid'], "UserContact.contact_user_id IN('$contactNumIds')")));

            $insertableUserIds = array_diff($ContactUserIds, $isAlreadyExist);
            $keyVal1 = 0;
            $newContactIds = array();
            $keyVal2 = 0;
            $newContactRequestIds = array();

            foreach ($insertableUserIds as $ContKey => $ContVal) {
                $id = $ContVal;

                $other_user_profile = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status', 'is_private'), 'conditions' => array('User.id' => $ContVal)));

                if ($other_user_profile['User']['is_private'] == 0) {

                    $contactData[$keyVal1]['UserContact']['user_id'] = $decoded['userid'];
                    $contactData[$keyVal1]['UserContact']['contact_user_id'] = $ContVal;
                    $contactData[$keyVal1]['UserContact']['type'] = $decoded['add_type'];
                    $newContactIds[] = $ContVal;
                    $keyVal1++;
                    $this->loadModel('UserNotification');
                    $userNotificationData['UserNotification']['fromid'] = $decoded['userid'];
                    $userNotificationData['UserNotification']['toid'] = $ContVal;
                    $userNotificationData['UserNotification']['image_id'] = 0;
                    $userNotificationData['UserNotification']['image_url'] = '';
                    $userNotificationData['UserNotification']['message'] = $user_data['User']['full_name'] . " added you.";
                    $userNotificationData['UserNotification']['type'] = 0;
                    $this->UserNotification->save($userNotificationData);
                    $contactData[$keyVal1]['UserContact']['user_id'] = $ContVal;
                    $contactData[$keyVal1]['UserContact']['contact_user_id'] = $decoded['userid'];
                    $contactData[$keyVal1]['UserContact']['type'] = $decoded['add_type'];
                    $newContactIds[] = $ContVal;
                    $keyVal1++;
                }

                if ($other_user_profile['User']['is_private'] == 1) {
                    // $UserContactRequestisAlreadyExist = $this->UserContactRequest->find('first', array('fields' => array('id'), 'conditions' => array('UserContactRequest.user_id' => $decoded['userid'], "UserContactRequest.friend_id" => $ContVal)));
                    $UserContactRequestisAlreadyExist = $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.friend_id' => $ContVal, 'UserContactRequest.user_id' => $decoded['user_id'])));

                    if (empty($UserContactRequestisAlreadyExist)) {
                        $contactRequestData[$keyVal2]['UserContactRequest']['user_id'] = $decoded['userid'];
                        $contactRequestData[$keyVal2]['UserContactRequest']['friend_id'] = $ContVal;
                        $contactRequestData[$keyVal2]['UserContactRequest']['is_sender'] = 1;
                        $newContactRequestIds[] = $ContVal;
                        $keyVal2++;
                        $friend_request_message = $userData['User']['full_name'] . " sent you a friend request";

                        if ($other_user_profile['User']['device_type'] == 1) {
                            $this->send_notification_for_android($other_user_profile['User']['device_id'], $friend_request_message);
                        } else if ($other_user_profile['User']['device_type'] == 2) {
                            $this->send_notification_for_iphone_test($other_user_profile['User']['device_id'], $friend_request_message);
                        } else {
                            $this->send_notification_for_iphone_test($other_user_profile['User']['device_id'], $friend_request_message);
                        }
                        $this->loadModel('UserNotification');
                        $userNotificationData['UserNotification']['fromid'] = $decoded['userid'];
                        $userNotificationData['UserNotification']['toid'] = $ContVal;
                        $userNotificationData['UserNotification']['image_id'] = 0;
                        $userNotificationData['UserNotification']['image_url'] = '';
                        $userNotificationData['UserNotification']['message'] = $user_data['User']['full_name'] . " sent you a friend request.";
                        $userNotificationData['UserNotification']['type'] = 0;
                        $this->UserNotification->save($userNotificationData);
                    }
                    $UserContactRequestisAlreadyExist = $this->UserContactRequest->find('first', array('fields' => array('id'), 'conditions' => array('UserContactRequest.user_id' => $ContVal, "UserContactRequest.friend_id" => $decoded['userid'])));

                    if (empty($UserContactRequestisAlreadyExist)) {
                        $contactRequestData[$keyVal2]['UserContactRequest']['user_id'] = $ContVal;
                        $contactRequestData[$keyVal2]['UserContactRequest']['friend_id'] = $decoded['userid'];
                        $contactRequestData[$keyVal2]['UserContactRequest']['is_sender'] = 0;
                        $newContactRequestIds[] = $decoded['userid'];
                        $keyVal2++;
                    }
                }
            }

            if (!empty($contactData) || !empty($contactRequestData)) {
                if (!empty($contactData)) {
                    if ($this->UserContact->saveAll($contactData)) {
                        $this->loadModel('UserContact');
                        $this->UserContact->Behaviors->load('Containable');
                        $this->UserContact->bindModel(
                                array(
                                    'belongsTo' => array(
                                        'User' => array(
                                            'foreignKey' => 'contact_user_id',
                                            'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status'),
                                        )
                                    )
                                )
                        );
                        $this->UserContact->User->bindModel(array(
                            'hasOne' => array(
                                'UserImage' => array(
                                    'className' => 'UserImage',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserImage.image_type' => 1),
                                ),
                                'UserBlock' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'blocked_user_id',
                                    'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['userid'])
                                ),
                                'UserBlocked' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['userid'])
                                )
                            )
                                ), false);
                        $res = $this->UserContact->find(
                                'all',
                                array(
                                    'contain' => array(
                                        'User' => array(
                                            'UserImage',
                                            'UserBlock',
                                            'UserBlocked'
                                        )
                                    ),
                                    'conditions' => array('UserContact.user_id' => $decoded['userid'], 'User.status' => 1, "User.id!=''", "User.full_name IS NOT NULL", "User.username IS NOT NULL")
                                )
                        );
                        $contactData = array();
                        $adminMsgTo = array();
                        $this->loadModel('UserNotification');
                        if (!empty($res)) {
                            foreach ($res as $key => $value) {
                                $contactData[$key] = $value['User'];
                                $contactData[$key]['profile_image_path'] = "";
                                $contactData[$key]['add_type'] = $value['UserContact']['type'];
                                if (!empty($value['User']['UserImage']['image'])) {
                                    $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                                }
                                $contactData[$key]['block_status'] = 0;
                                if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                                    $contactData[$key]['block_status'] = 1;
                                }
                                $contactData[$key]['blocked_status'] = 0;
                                if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                                    $contactData[$key]['blocked_status'] = 1;
                                }
                                if (in_array($value['User']['id'], $newContactIds)) {
                                    $adminMsgTo[] = $value;
                                    if (!empty($value['User']['jid'])) {
                                        $userArr = array();
                                        $userArr['UserNotification']['fromid'] = $decoded['userid'];
                                        $userArr['UserNotification']['toid'] = $value['User']['id'];
                                        $userArr['UserNotification']['image_url'] = "";
                                        $userArr['UserNotification']['message'] = $message;
                                        $userArr['UserNotification']['type'] = 0;
                                        $this->UserNotification->save($userArr);
                                        unset($userArr);
                                        if ($value['User']['push_notification_status'] == 1) {
                                            if ($value['User']['device_type'] == 1) {
                                                $this->send_notification_for_android($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else if ($value['User']['device_type'] == 2) {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            }
                                        }
                                    }
                                }
                            }
                            $body = base64_encode($message);
                            $userContData = base64_encode(json_encode($adminMsgTo));
                            $postFields = array('userContctacts' => $userContData, 'body' => $body);
                            $ch = curl_init();
                            $callUrl = Router::url(array('controller' => 'web_services_v2', 'action' => 'send_messages', 43, 23, 'addAccount', 'YES', $userData['User']['id']), true);
                            $callUrl = str_replace("http://", "https://", $callUrl);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_URL, $callUrl);
                            curl_setopt($ch, CURLOPT_POST, count($postFields));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                            echo $contents = curl_exec($ch);
                            curl_close($ch);
                        }
                        $responseData['response_status'] = 1;
                        $responseData['response_code'] = 200;
                        $responseData['response_message_code'] = "";
                        $responseData['message'] = 'successfully';
                        $responseData['response_data'] = $contactData;
                        $data = array('response' => $responseData);
                    } else {
                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 500;
                        $responseData['message'] = 'failed';
                        $responseData['response_data'] = array();
                        $data = array('response' => $responseData);
                    }
                }
                if (!empty($contactRequestData)) {
                    $message = $friend_request_message;

                    if ($this->UserContactRequest->saveAll($contactRequestData)) {

                        $this->loadModel('UserContact');
                        $this->UserContact->Behaviors->load('Containable');
                        $this->UserContact->bindModel(
                                array(
                                    'belongsTo' => array(
                                        'User' => array(
                                            'foreignKey' => 'contact_user_id',
                                            'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'push_notification_status', 'profile_cover_image', 'status'),
                                        )
                                    )
                                )
                        );
                        $this->UserContact->User->bindModel(array(
                            'hasOne' => array(
                                'UserImage' => array(
                                    'className' => 'UserImage',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserImage.image_type' => 1),
                                ),
                                'UserBlock' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'blocked_user_id',
                                    'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['userid'])
                                ),
                                'UserBlocked' => array(
                                    'className' => 'UserBlock',
                                    'foreignKey' => 'user_id',
                                    'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['userid'])
                                )
                            )
                                ), false);
                        $res = $this->UserContact->find(
                                'all',
                                array(
                                    'contain' => array(
                                        'User' => array(
                                            'UserImage',
                                            'UserBlock',
                                            'UserBlocked'
                                        )
                                    ),
                                    'conditions' => array('UserContact.user_id' => $decoded['userid'], 'User.status' => 1, "User.id!=''", "User.full_name IS NOT NULL", "User.username IS NOT NULL")
                                )
                        );
                        $contactData = array();
                        $adminMsgTo = array();
                        $this->loadModel('UserNotification');
                        if (!empty($res)) {
                            foreach ($res as $key => $value) {
                                $contactData[$key] = $value['User'];
                                $contactData[$key]['profile_image_path'] = "";
                                $contactData[$key]['add_type'] = $value['UserContact']['type'];
                                if (!empty($value['User']['UserImage']['image'])) {
                                    $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                                }

                                $contactData[$key]['block_status'] = 0;
                                if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                                    $contactData[$key]['block_status'] = 1;
                                }
                                $contactData[$key]['blocked_status'] = 0;
                                if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                                    $contactData[$key]['blocked_status'] = 1;
                                }
                                if (in_array($value['User']['id'], $newContactIds)) {

                                    $adminMsgTo[] = $value;
                                    if (!empty($value['User']['jid'])) {
                                        $userArr = array();
                                        $userArr['UserNotification']['fromid'] = $decoded['userid'];
                                        $userArr['UserNotification']['toid'] = $value['User']['id'];
                                        $userArr['UserNotification']['image_url'] = "";
                                        $userArr['UserNotification']['message'] = $message;
                                        $userArr['UserNotification']['type'] = 0;
                                        $this->UserNotification->save($userArr);
                                        unset($userArr);
                                        if ($value['User']['push_notification_status'] == 1) {
                                            if ($value['User']['device_type'] == 1) {
                                                $this->send_notification_for_android($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else if ($value['User']['device_type'] == 2) {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            } else {
                                                $this->send_notification_for_iphone_test($value['User']['device_id'], $message, 8, $userData['User']['id']);
                                            }
                                        }
                                    }
                                }
                            }
                            $body = base64_encode($message);
                            $userContData = base64_encode(json_encode($adminMsgTo));
                            $postFields = array('userContctacts' => $userContData, 'body' => $body);
                            $ch = curl_init();
                            $callUrl = Router::url(array('controller' => 'web_services_v2', 'action' => 'send_messages', 43, 23, 'addAccount', 'YES', $userData['User']['id']), true);
                            $callUrl = str_replace("http://", "https://", $callUrl);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_URL, $callUrl);
                            curl_setopt($ch, CURLOPT_POST, count($postFields));
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                            echo $contents = curl_exec($ch);
                            curl_close($ch);
                        }
                        $responseData['response_status'] = 1;
                        $responseData['response_code'] = 200;
                        $responseData['response_message_code'] = "";
                        $responseData['message'] = 'successfully';
                        $responseData['response_data'] = $contactData;
                        $data = array('response' => $responseData);
                    } else {
                        $responseData['response_status'] = 0;
                        $responseData['response_code'] = 400;
                        $responseData['response_message_code'] = 500;
                        $responseData['message'] = 'failed';
                        $responseData['response_data'] = array();
                        $data = array('response' => $responseData);
                    }
                }
            } else {

                $this->loadModel('UserContact');
                $this->UserContact->Behaviors->load('Containable');
                $this->UserContact->bindModel(
                        array(
                            'belongsTo' => array(
                                'User' => array(
                                    'foreignKey' => 'contact_user_id',
                                    'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'profile_cover_image', 'status'),
                                )
                            )
                        )
                );
                $this->UserContact->User->bindModel(array(
                    'hasOne' => array(
                        'UserImage' => array(
                            'className' => 'UserImage',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserImage.image_type' => 1),
                        ),
                        'UserBlock' => array(
                            'className' => 'UserBlock',
                            'foreignKey' => 'blocked_user_id',
                            'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['userid'])
                        ),
                        'UserBlocked' => array(
                            'className' => 'UserBlock',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['userid'])
                        )
                    )
                        ), false);
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'contain' => array(
                                'User' => array(
                                    'UserImage',
                                    'UserBlock',
                                    'UserBlocked'
                                )
                            ),
                            'conditions' => array('UserContact.user_id' => $decoded['userid'], 'User.status' => 1, "User.id!=''", "User.full_name IS NOT NULL", "User.username IS NOT NULL")
                        )
                );
                $contactData = array();
                if (!empty($res)) {
                    foreach ($res as $key => $value) {
                        $contactData[$key] = $value['User'];
                        $contactData[$key]['profile_image_path'] = "";
                        $contactData[$key]['add_type'] = $value['UserContact']['type'];
                        if (!empty($value['User']['UserImage']['image'])) {
                            $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                        }
                        $contactData[$key]['block_status'] = 0;
                        if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                            $contactData[$key]['block_status'] = 1;
                        }

                        $contactData[$key]['blocked_status'] = 0;
                        if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                            $contactData[$key]['blocked_status'] = 1;
                        }
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $contactData;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    // ==============================================================================================

    public function get_contacts($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }


        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $contactData = array();
            $this->loadModel('User');
            $this->loadModel('UserContact');

            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status'), 'conditions' => array('User.id' => $decoded['user_id']))); {
                /*                 * ************ USER CONTACT ************ */
                $this->loadModel('UserContact');
                $this->UserContact->Behaviors->load('Containable');
                $this->UserContact->bindModel(
                        array(
                            'belongsTo' => array(
                                'User' => array(
                                    'foreignKey' => 'contact_user_id',
                                    'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'profile_cover_image', 'status'),
                                )
                            )
                        )
                );

                $this->UserContact->User->bindModel(array(
                    'hasOne' => array(
                        'UserImage' => array(
                            'className' => 'UserImage',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserImage.image_type' => 1),
                            'order' => array('UserImage.id DESC'),
                        //'fields' => array('id', 'user_id', 'user_image_id', 'status')
                        ),
                        'UserBlock' => array(
                            'className' => 'UserBlock',
                            'foreignKey' => 'blocked_user_id',
                            'conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $decoded['user_id'])
                        ),
                        'UserBlocked' => array(
                            'className' => 'UserBlock',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserBlocked.status' => 1, 'UserBlocked.blocked_user_id' => $decoded['user_id'])
                        )
                    )
                        ), false);

                $conditions = array('UserContact.user_id' => $decoded['user_id']);
                if (isset($decoded['contact_user_id']) && (!empty($decoded['contact_user_id']))) {
                    $conditions = array('UserContact.user_id' => $decoded['user_id'], 'UserContact.contact_user_id' => $decoded['contact_user_id'], "User.id!=''");
                }

                $res = $this->UserContact->find(
                        'all',
                        array(
                            'contain' => array(
                                'User' => array(
                                    'UserImage',
                                    'UserBlock',
                                    'UserBlocked'
                                )
                            ),
                            'conditions' => $conditions
                        )
                );

                $contactData = array();
                if (!empty($res)) {
                    foreach ($res as $key => $value) {
                        $contactData[$key] = $value['User'];
                        $contactData[$key]['profile_image_path'] = "";
                        if (!empty($value['User']['UserImage']['image'])) {
                            $contactData[$key]['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['User']['UserImage']['image'];
                        }

                        $contactData[$key]['block_status'] = 0;
                        if (isset($value['User']['UserBlock']['status']) && ($value['User']['UserBlock']['status'] == 1)) {
                            $contactData[$key]['block_status'] = 1;
                        }

                        $contactData[$key]['blocked_status'] = 0;
                        if (isset($value['User']['UserBlocked']['status']) && ($value['User']['UserBlocked']['status'] == 1)) {
                            $contactData[$key]['blocked_status'] = 1;
                        }
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $contactData;
                $data = array('response' => $responseData);
            }

            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function search_contacts($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = array();

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $user_id = "90000000000";
            if (isset($decoded['user_id']) && (!empty($decoded['user_id']))) {
                $user_id = $decoded['user_id'];
            }

            if (!empty($decoded)) {
                $Reqfields = array();
                $ReqConditions = array();
                $this->loadModel('User');

                $this->loadModel('UserBlock');
                $blockedQryString = "";
                $blocked_user = $this->UserBlock->find('all', array('fields' => array('user_id', 'blocked_user_id'), 'conditions' => array('OR' => array('UserBlock.user_id' => $decoded['user_id'], 'UserBlock.blocked_user_id' => $decoded['user_id']), 'UserBlock.status' => 1)));
                $blockedAllArr = array();
                if (!empty($blocked_user)) {
                    foreach ($blocked_user as $blockKey => $blockValue) {
                        $blockedAllArr[] = $blockValue['UserBlock']['user_id'];
                        $blockedAllArr[] = $blockValue['UserBlock']['blocked_user_id'];
                    }
                }
                $blockedAllArr = array_unique($blockedAllArr);
                $blockedString = implode("','", $blockedAllArr);
                if (!empty($blockedString)) {
                    $blockedQryString = "User.id NOT IN('$blockedString')";
                }



                $this->User->bindModel(array(
                    'hasOne' => array(
                        'UserImage' => array(
                            'className' => 'UserImage',
                            'foreignKey' => 'user_id',
                            'conditions' => array('UserImage.image_type' => 1),
                            'order' => array('UserImage.id DESC'),
                        // 'having' => array('UserImage.id ' => MAX('UserImage.id')),
                        //'fields' => array('id', 'user_id', 'user_image_id', 'status')
                        )
                    )
                        ), false);
                $userData = $this->User->find('all', array('fields' => array('id', 'phone_number', 'username', 'full_name', 'jid', 'UserImage.*', 'is_private'), 'conditions' => array(
                        'OR' => array(
                            //'User.phone_number LIKE'=>"%".$decoded['keyword']."%",
                            // 'AND' => array(
                            // 	'User.username LIKE' => '%' . $decoded['keyword'] . "%",
                            // 	'User.hide_username_search' => 0
                            // )
                            'AND' => array(
                                'User.username LIKE' => $decoded['keyword'],
                                'User.hide_username_search' => 0
                            )
                        ),
                        "User.id != $user_id",
                        $blockedQryString
                )));

                $userArr = array();
                if (!empty($userData)) {

                    foreach ($userData as $userKey => $userValue) {

                        $userArr[$userKey] = $userValue['User'];
                        $userArr[$userKey]['contact_request'] = 0;
                        $userArr[$userKey]['is_contact'] = 0;
                        $userArr[$userKey]['is_sender'] = 0;

                        $this->loadModel('UserContactRequest');
                        $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $userValue['User']['id'], 'UserContactRequest.friend_id' => $user_id)));

                        if (!empty($contact_request)) {
                            $userArr[$userKey]['contact_request'] = 1;
                        }

                        $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $userValue['User']['id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

                        if (!empty($contact_request_is_sender)) {
                            $userArr[$userKey]['is_sender'] = 1;
                        }

                        $this->loadModel('UserContact');
                        $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $userValue['User']['id'])));

                        if (!empty($contact)) {
                            $userArr[$userKey]['is_contact'] = 1;
                        }

                        $userArr[$userKey]['profile_image'] = "";
                        if (!empty($userValue['UserImage']['image'])) {
                            $userArr[$userKey]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userValue['UserImage']['image'];
                        }
                    }
                }
                // print_r($userArr);
                $newArr = [];
                $tempArr = array_unique(array_column($userArr, 'id'));
                $newArr = array_intersect_key($userArr, $tempArr);
                $reindexed_array = array_values($newArr);

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'success';
                $responseData['response_data'] = $reindexed_array;
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 400;
                $responseData['message'] = 'error';
                $responseData['response_data']['data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data']['data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function accept_decline_contact_request($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $contactRequestData = array();

        if (!empty($decoded) && isset($decoded['user_id']) && isset($decoded['other_user_id']) && isset($decoded['action'])) {
            if ($decoded['action'] == 'accept') {
                $this->loadModel('User');
                $this->loadModel('UserContact');
                $this->loadModel('UserContactRequest');

                $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
                $other_user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['other_user_id'])));

                $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.friend_id' => $decoded['user_id'], 'UserContactRequest.user_id' => $decoded['other_user_id'])));
                if (!empty($contact_request)) {
                    $contactData = array();

                    $contactData[0]['UserContact']['user_id'] = $decoded['user_id'];
                    $contactData[0]['UserContact']['contact_user_id'] = $decoded['other_user_id'];
                    $contactData[0]['UserContact']['type'] = 0;

                    $contactData[1]['UserContact']['user_id'] = $decoded['other_user_id'];
                    $contactData[1]['UserContact']['contact_user_id'] = $decoded['user_id'];
                    $contactData[1]['UserContact']['type'] = 0;

                    $this->UserContact->saveAll($contactData);

                    $message = $user_data['User']['full_name'] . " has accepted your friend request!";
                    if ($other_user_data['User']['device_type'] == 1) {
                        $this->send_notification_for_android($other_user_data['User']['device_id'], $message);
                    } else if ($other_user_data['User']['device_type'] == 2) {
                        $this->send_notification_for_iphone_test($other_user_data['User']['device_id'], $message);
                    } else {
                        $this->send_notification_for_iphone_test($other_user_data['User']['device_id'], $message);
                    }

                    $this->loadModel('UserNotification');
                    $userNotificationData['UserNotification']['fromid'] = $decoded['user_id'];
                    $userNotificationData['UserNotification']['toid'] = $decoded['other_user_id'];
                    $userNotificationData['UserNotification']['image_id'] = 0;
                    $userNotificationData['UserNotification']['image_url'] = '';
                    $userNotificationData['UserNotification']['message'] = $user_data['User']['full_name'] . " has accepted your friend request!";
                    $userNotificationData['UserNotification']['type'] = 0;
                    $this->UserNotification->save($userNotificationData);

                    $this->UserContactRequest->delete($contact_request['UserContactRequest']['id']);

                    $contact_request_2 = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.friend_id' => $decoded['other_user_id'], 'UserContactRequest.user_id' => $decoded['user_id'])));
                    if (!empty($contact_request_2)) {

                        $this->UserContactRequest->delete($contact_request_2['UserContactRequest']['id']);
                    }

                    $message = $user_data['User']['full_name'] . " has accepted your friend request!";

                    $adminMsgTo = array();
                    $adminMsgTo[] = $other_user_data;

                    $body = base64_encode($message);
                    $userContData = base64_encode(json_encode($adminMsgTo));
                    $postFields = array('userContctacts' => $userContData, 'body' => $body);
                    $ch = curl_init();
                    $callUrl = Router::url(array('controller' => 'web_services_v2', 'action' => 'send_messages', 43, 23, 'addAccount', 'YES', $other_user_data['User']['id']), true);
                    $callUrl = str_replace("http://", "https://", $callUrl);

                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_URL, $callUrl);
                    curl_setopt($ch, CURLOPT_POST, count($postFields));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                    echo $contents = curl_exec($ch);
                    curl_close($ch);
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 500;
                    $responseData['message'] = 'failed - nothing to accept';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $this->loadModel('User');
                $this->loadModel('UserContact');
                $this->loadModel('UserContactRequest');
                $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
                $other_user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['other_user_id'])));

                $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.friend_id' => $decoded['user_id'], 'UserContactRequest.user_id' => $decoded['other_user_id'])));
                $this->UserContactRequest->delete($contact_request['UserContactRequest']['id']);

                $contact_request_2 = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.friend_id' => $decoded['other_user_id'], 'UserContactRequest.user_id' => $decoded['user_id'])));
                $this->UserContactRequest->delete($contact_request_2['UserContactRequest']['id']);
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['data'] = $contact_request_2;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = $contactRequestData;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_contact_requests_count($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $this->loadModel('UserContactRequest');

        if (!empty($decoded)) {
            $contactRequestData = array();

            $contactRequestRawData = array();
            $contactRequestRawData = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.friend_id' => $decoded['user_id'], 'UserContactRequest.is_sender' => 1)));

            $contactRequestData['count'] = 0;

            if (!empty($contactRequestRawData)) {
                $contactRequestData['count'] = count($contactRequestRawData);
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = $contactRequestData;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_contact_requests($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $this->loadModel('UserContactRequest');

        if (!empty($decoded)) {
            $contactRequestData = array();

            $contactRequestRawData = array();
            $contactRequestRawData = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.friend_id' => $decoded['user_id'], 'UserContactRequest.is_sender' => 1)));

            if (!empty($contactRequestRawData)) {
                foreach ($contactRequestRawData as $key => $contactRequestRawDataSingle) {
                    $contactRequestRawDataSingleTemp = array();

                    $this->loadModel('User');
                    $contactRawData = $this->User->find('first', array('conditions' => array('User.id' => $contactRequestRawDataSingle['UserContactRequest']['user_id'])));
                    if (!empty($contactRawData['User']['username'])) {
                        $contactRequestRawDataSingleTemp['id'] = $contactRawData['User']['id'];
                        $contactRequestRawDataSingleTemp['name'] = $contactRawData['User']['full_name'];
                        $contactRequestRawDataSingleTemp['username'] = $contactRawData['User']['username'];

                        $this->loadModel('UserImage');
                        $contactImageData = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $contactRawData['User']['id'], 'UserImage.image_type' => 1), 'order' => array('id' => 'DESC')));
                        if (!empty($contactImageData)) {
                            $contactRequestRawDataSingleTemp['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $contactImageData['UserImage']['image'];
                        } else {
                            $contactRequestRawDataSingleTemp['profile_image_path'] = '';
                        }
                    }

                    if (!empty($contactRequestRawDataSingleTemp)) {
                        $contactRequestData[] = $contactRequestRawDataSingleTemp;
                    }
                }
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = $contactRequestData;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_contacts_2($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $this->loadModel('UserContact');

        if (!empty($decoded)) {
            $contactData = array();

            $contactRawData = array();
            $contactRawData = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $decoded['user_id'])));

            if (!empty($contactRawData)) {
                foreach ($contactRawData as $key => $contactRawDataSingle) {
                    $contactRawDataSingle = array();

                    $this->loadModel('User');
                    $contactRawData = $this->User->find('first', array('conditions' => array('User.id' => $contactRequestRawDataSingle['UserContactRequest']['user_id'])));

                    $contactRequestRawDataSingleTemp['id'] = $contactRawData['User']['id'];
                    $contactRequestRawDataSingleTemp['name'] = $contactRawData['User']['full_name'];
                    $contactRequestRawDataSingleTemp['username'] = $contactRawData['User']['username'];

                    $this->loadModel('UserImage');
                    $contactImageData = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $contactRawData['User']['id'], 'UserImage.image_type' => 1)));
                    if (!empty($contactImageData)) {
                        $contactRequestRawDataSingleTemp['profile_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $contactImageData['UserImage']['image'];
                    } else {
                        $contactRequestRawDataSingleTemp['profile_image_path'] = '';
                    }


                    $contactRequestData[] = $contactRequestRawDataSingleTemp;
                }
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = $contactRequestData;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function unfriend($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $contactRequestData = array();

        if (!empty($decoded) && isset($decoded['user_id']) && isset($decoded['other_user_id'])) {
            $this->loadModel('User');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');
            $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
            $other_user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['other_user_id'])));

            $contact_request = $this->UserContact->find('first', array('conditions' => array('UserContact.contact_user_id' => $decoded['user_id'], 'UserContact.user_id' => $decoded['other_user_id'])));

            $this->UserContact->delete($contact_request['UserContact']['id']);

            $contact_request = $this->UserContact->find('first', array('conditions' => array('UserContact.contact_user_id' => $decoded['other_user_id'], 'UserContact.user_id' => $decoded['user_id'])));
            $this->UserContact->delete($contact_request['UserContact']['id']);

            $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.friend_id' => $decoded['other_user_id'], 'UserContactRequest.user_id' => $decoded['user_id'])));
            $this->UserContactRequest->delete($contact_request['UserContactRequest']['id']);

            $responseData['response_status'] = 1;
            $responseData['data'] = $contact_request;
            $responseData['response_query'] = $this->getLastQuery($this->UserContactRequest);
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function set_full_phone_no() {
        $this->loadModel("AppUser");
        $data = $this->AppUser->find('all', array('fields' => array('id', 'phone', 'country_dial_code')));
        $savDt = array();
        foreach ($data as $key => $value) {
            $savDt[$key]['AppUser']['id'] = $value['AppUser']['id'];
            $savDt[$key]['AppUser']['full_phone_number'] = str_replace("+", "", $value['AppUser']['country_dial_code']) . $value['AppUser']['phone'];
            $this->AppUser->save($savDt[$key]);
        }
        die;
    }

    public function set_crash_info($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            foreach ($decoded as $key => $value) {
                if (is_array($value)) {
                    $decoded[$key] = json_encode($value);
                }
            }
            $Data['CrashInformation'] = $decoded;
            $this->loadModel('CrashInformation');
            if ($this->CrashInformation->save($Data)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = $Data;
                $responseData['message'] = 'success';
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_language_label($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }
        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->Session->write('Config.language', $decoded['language']);
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = __($decoded['msgid']);
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function upload_group_pic() {
        if (!empty($_FILES)) {
            $newArrayDt = array_merge($_FILES, $_POST);

            if ($_FILES['image']['size'] < 6291456) {

                $thumbRules = array('size' => array(50, 75), 'type' => 'resizecrop');
                $waterRules = array('size' => array(100, 150), 'type' => 'resizecrop');
                $bigRules = array('size' => array(150, 225), 'type' => 'resizecrop');
                $OrgRules = array('size' => array(150, 225), 'type' => 'resizecrop');

                $path_info = pathinfo($_FILES['image']['name']);

                $_FILES['image']['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];
                $file = $_FILES['image'];
                $thumb = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/thumb" . DS, '', $thumbRules);
                $water = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/water" . DS, '', $waterRules);
                $big = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/big" . DS, '', $bigRules);
                $org = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/org" . DS, '', $OrgRules);

                if (!empty($this->Upload->result)) {
                    $message['image'] = $this->Upload->result;
                    $userImage = $this->Upload->result;

                    $profileData = array();
                    $this->loadModel('OfUserInformation');
                    $data = $this->OfUserInformation->find('first', array(
                        'conditions' => array('OfUserInformation.jid' => $_POST['jid'])
                    ));

                    $profileData['OfUserInformation']['id'] = $data['OfUserInformation']['id'];
                    $profileData['OfUserInformation']['jid'] = $_POST['jid'];
                    $profileData['OfUserInformation']['image'] = $userImage;
                    if ($this->OfUserInformation->save($profileData)) {
                        if (!empty($data)) {
                            @unlink(WWW_ROOT . "img/ofprofilepic/org" . DS . $data['OfUserInformation']['image']);
                            @unlink(WWW_ROOT . "img/ofprofilepic/thumb" . DS . $data['OfUserInformation']['image']);
                            @unlink(WWW_ROOT . "img/ofprofilepic/water" . DS . $data['OfUserInformation']['image']);
                            @unlink(WWW_ROOT . "img/ofprofilepic/big" . DS . $data['OfUserInformation']['image']);
                        }
                    }

                    if (!empty($userImage)) {
                        $userImage = SITE_URL . "/img/ofprofilepic/org/" . $userImage;
                    }


                    $result['response_status'] = 1;
                    $result['response_code'] = 200;
                    $result['response_message_code'] = '';
                    $result['message'] = 'success';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = $userImage;
                    $result['response_data']['jid'] = $_POST['jid'];
                    $jsonEncode = json_encode($result);
                    return $jsonEncode;
                } else {
                    $result['response_status'] = 0;
                    $result['response_code'] = 400;
                    $result['response_message_code'] = 500;
                    $result['message'] = 'error';
                    $result['response_data'] = array();
                    $result['response_data']['image'] = '';
                    $result['response_data']['jid'] = '';
                    $jsonEncode = json_encode($result);

                    return $jsonEncode;
                }
            } else {
                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = 413;
                $result['message'] = 'cannot upload image of size more than 8MB.';
                $result['response_data'] = array();
                $result['response_data']['image'] = '';
                $result['response_data']['jid'] = '';
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            }
        } else {
            $result['response_status'] = 0;
            $result['response_code'] = 400;
            $result['response_message_code'] = 404;
            $result['message'] = 'file not found';
            $result['response_data'] = array();
            $result['response_data']['image'] = '';
            $result['response_data']['jid'] = '';
            $jsonEncode = json_encode($result);
            return $jsonEncode;
        }
    }

    public function get_group_pic($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $profileData = array();
            $this->loadModel('OfUserInformation');
            $data = $this->OfUserInformation->find('first', array(
                'fields' => array('OfUserInformation.id', 'OfUserInformation.jid', 'OfUserInformation.image'),
                'conditions' => array('OfUserInformation.jid' => $decoded['jid'])
            ));
            if (!empty($data)) {
                $data['OfUserInformation']['image'] = SITE_URL . "/img/ofprofilepic/org/" . $data['OfUserInformation']['image'];
                $profileData = $data['OfUserInformation'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = $profileData;
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function uploadImage($file, $FileRules = array(), $FileFolders = array()) {

        if (!empty($file) && $file['tmp_name'] != '' && $file['size'] > 0) {
            $thumbRules = array('size' => array(50, 75), 'type' => 'resizecrop');
            $waterRules = array('size' => array(100, 150), 'type' => 'resizecrop');
            $bigRules = array('size' => array(150, 225), 'type' => 'resizecrop');
            $path_info = pathinfo($file['name']);

            $file['name'] = $path_info['filename'] . "_" . time() . "." . $path_info['extension'];

            $thumb = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/thumb" . DS, '', $thumbRules);
            $water = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/water" . DS, '', $thumbRules);
            $big = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/big" . DS, '', $thumb1);
            $org = $this->Upload->upload($file, WWW_ROOT . "img/ofprofilepic/org" . DS, '', $back);

            if (!empty($this->Upload->result) && empty($this->Upload->errors)) {
                return $this->Upload->result;
            } else {
                $responseData["error"] = $this->Upload->errors[0];
            }
        } else {
            $responseArray["error"] = "Image is empty or Size is Zero.";
        }
    }

    public function send_notifications($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('AppUser');
            $senderData = $this->AppUser->find('first', array('fields' => array('id', 'username', 'full_phone_number'), 'conditions' => array('AppUser.jid' => $decoded['sender_jid'])));

            $receiverJids = implode("', '", $decoded['receiver_jids']);
            $receiverAndroidData = $this->AppUser->find('list', array('fields' => array('id', 'device_id'), 'conditions' => array("AppUser.jid IN('$receiverJids')", "AppUser.device_type" => 'android')));
            $receiverIphoneData = $this->AppUser->find('list', array('fields' => array('id', 'device_id'), 'conditions' => array("AppUser.jid IN('$receiverJids')", "AppUser.device_type" => 'iphone')));

            $msg = $senderData['AppUser']['full_phone_number'] . ":" . substr($decoded['msg'], 0, 50) . "...";

            if (!empty($receiverIphoneData)) {
                $this->iphone_notification($receiverIphoneData, $msg);
            }

            if (!empty($receiverAndroidData)) {
                $this->android_notification($receiverAndroidData, $msg);
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);

            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function iphone_notification($registatoin_ids = array(), $msg = null) {
        $this->loadModel('User');
        foreach ($registatoin_ids as $key => $value) {
            $this->User->apple_push_notification($value, $msg, "TOPCHAT");
        }
        return true;
    }

    function android_notification($registatoin_ids = array(), $msg = null) {
        App::import('Vendor', 'send_notification', array('file' => 'gcm_server_php/GCM.php'));
        $gcm = new GCM();
        $result = $gcm->send_notification($registatoin_ids, $msg);
        return true;
    }

    public function change_phone_number($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');
            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'phone_number'), 'conditions' => array('User.phone_number' => $decoded['old_phone_number'])));

            $userData['User']['phone_number'] = $decoded['new_phone_number'];

            if ($this->User->save($userData)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'error in save';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function delete_phone_number($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $this->loadModel('User');
            $userData = $this->User->find('first', array('fields' => array('id', 'username', 'phone_number'), 'conditions' => array('User.phone_number' => $decoded['phone_number'])));

            if ($this->User->delete($userData['User']['id'])) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'error in save';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function delete_image($test_data = null) {
        $data = file_get_contents("php://input");

        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        if (!empty($decoded)) {
            $success_status = 0;

            $this->loadModel('UserImage');
            $data = $this->UserImage->find('first', array('fields' => array('id', 'image', 'image_type'), 'conditions' => array('UserImage.id' => $decoded['image_id'])));

            if ($data['UserImage']['image_type'] == 1) {
                $this->loadModel('User');
                $this->User->bindModel(array('hasMany' => array('UserContact' => array('className' => 'UserContact', 'foreignKey' => 'user_id', 'dependent' => true)), false));
                $userData = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
                $userNm = $userData['User']['phone_number'];
                if (!empty($userData['User']['username'])) {
                    $userNm = $userData['User']['username'];
                }
                if (!empty($userData['User']['full_name'])) {
                    $userNm = $userData['User']['full_name'];
                }
                $contactUserArr = array();
                $allDeviceIds = array();
                if (!empty($userData['UserContact'])) {
                    foreach ($userData['UserContact'] as $contKey => $contValue) {
                        $contactUserArr[] = $contValue['contact_user_id'];
                    }
                    $allContacts = implode("','", $contactUserArr);
                    $allDeviceIds = $this->User->find('list', array('fields' => array('id', 'device_id', 'device_type'), 'conditions' => array("User.id IN('$allContacts')")));
                }
            }

            if ($this->UserImage->delete($decoded['image_id'])) {
                @unlink(SITE_URL . "/uploads/user_images/large/" . $data['UserImage']['image']);
                @unlink(SITE_URL . "/uploads/user_images/original/" . $data['UserImage']['image']);
                @unlink(SITE_URL . "/uploads/user_images/thumb/" . $data['UserImage']['image']);
                $success_status = 1;
            }
            //}
            if ($success_status == 1) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function test_charity_api($test_data = null) {
        $data = file_get_contents("https://projects.propublica.org/nonprofits/api/v1/search.json?q=charity&page=3");
        $decoded = json_decode($data, true);
    }

    public function send_msg_noti($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        print_r($decoded);
        exit;
    }

    public function send_message_notification($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        $friendIds = "";
        $this->loadModel('User');
        $fromUsername = "Someone";
        if (!empty($decoded)) {
            if (isset($decoded['from_id']) && (!empty($decoded['from_id']))) {
                $from_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['from_id']), 'fields' => array('id', 'username', 'email', 'jid', 'device_id', 'device_type', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'profile_cover_image', 'status')));
                $fromUsername = $from_user_data['User']['phone_number'];

                if (!empty($from_user_data['User']['full_name'])) {
                    $fromUsername = $from_user_data['User']['full_name'];
                }
            }

            $contents = "";
            if (isset($decoded['to_id']) && (!empty($decoded['to_id']))) {
                $to_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['to_id']), 'fields' => array('id', 'username', 'email', 'jid', 'device_id', 'device_type', 'phone_number', 'profile_status_message', 'full_name', 'profile_image', 'push_notification_status', 'notification_preview_status', 'profile_cover_image', 'status', 'chat_room_notification_status')));
            }

            //check if user blocked notification 
            if (isset($decoded['group_name']) && !empty($decoded['group_name']) && $to_user_data['User']['chat_room_notification_status'] != '1') {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'This user blocked the group notifications';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }

            if (!empty($to_user_data['User']['device_id'])) {

                $message = "";
                if ($decoded['message_type'] == '0') {

                    $message = $fromUsername;

                    if ($to_user_data['User']['notification_preview_status'] == 1) {
                        if (!empty($decoded['message'])) {
                            $previewMsg = $decoded['message'];
                            if (strlen($decoded['message']) > 20) {
                                $previewMsg = substr($decoded['message'], 0, 20) . "...";
                            }

                            $message = $message . ' sent you a message';
                        }
                    } else {
                        $message = $message . ' sent you a message';
                    }
                }

                if ($decoded['message_type'] == '1') {
                    $message = $fromUsername . " sent you a photo";
                }

                if ($decoded['message_type'] == '2') {
                    $message = $fromUsername . " sent you a LiveSnap";
                }

                if ($decoded['message_type'] == '3') {
                    $message = $fromUsername . " sent you a video";
                }

                if ($decoded['message_type'] == '4') {
                    $message = $fromUsername . " sent you a voice clip";
                }

                if ($decoded['message_type'] == '5') {
                    $message = $fromUsername . " buzzed you!";
                }

                if ($decoded['message_type'] == '6') {
                    $message = $fromUsername . " is reading your message!";
                }

                if ($decoded['message_type'] == '7') {
                    if (isset($decoded['group_name'])) {
                        $message = $fromUsername . " sent a message in " . $decoded['group_name'] . " group";
                    }
                }

                if ($decoded['message_type'] == '8') {
                    if (isset($decoded['group_name'])) {
                        $message = $fromUsername . " sent an image in " . $decoded['group_name'] . " group";
                    }
                }

                if ($decoded['message_type'] == '9') {
                    if (isset($decoded['group_name'])) {
                        $message = $fromUsername . " sent a buzz in " . $decoded['group_name'] . " group";
                    }
                }

                if ($decoded['message_type'] == '10') {
                    if (isset($decoded['group_name'])) {
                        $message = $fromUsername . " sent an audio message in " . $decoded['group_name'] . " group";
                    }
                }

                if ($decoded['message_type'] == '11') {
                    if (isset($decoded['group_name'])) {
                        $message = $fromUsername . " sent a group live image " . $decoded['group_name'] . " group";
                    }
                }

                if ($decoded['message_type'] == '12') {
                    $message = "Missed call from " . $fromUsername;
                }

                if ($decoded['message_type'] == '13') {
                    if (isset($decoded['group_name'])) {
                        $message = $fromUsername . " joined the " . $decoded['group_name'] . " group";
                    }
                }

                if ($to_user_data['User']['push_notification_status'] == 1) {

                    // $this->send_notification_for_iphone($to_user_data['User']['device_id'], $message, $decoded['message_type']);

                    if ($to_user_data['User']['device_type'] == 1) {
                        $this->send_notification_for_android($to_user_data['User']['device_id'], $message, $decoded['message_type']);
                    } else if ($to_user_data['User']['device_type'] == 2) {
                        $this->send_notification_for_iphone_test($to_user_data['User']['device_id'], $message, $decoded['message_type']);
                    } else {
                        $this->send_notification_for_iphone_test($to_user_data['User']['device_id'], $message, $decoded['message_type']);
                    }
                }
                //}
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = $message;
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_call_notification($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        $friendIds = "";
        $this->loadModel('User');
        $fromUsername = "Someone";
        if (!empty($decoded)) {
            if (isset($decoded['from_id']) && (!empty($decoded['from_id']))) {
                $from_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['from_id']), 'fields' => array('id', 'device_id', 'device_type', 'phone_number', 'profile_image', 'full_name')));
                $fromUsername = $from_user_data['User']['phone_number'];
                if (!empty($from_user_data['User']['full_name'])) {
                    $fromUsername = $from_user_data['User']['full_name'];
                }
            }

            $contents = "";
            if (isset($decoded['to_id']) && (!empty($decoded['to_id']))) {
                $to_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['to_id']), 'fields' => array('id', 'device_id', 'device_type', 'push_notification_status')));
            }

            if (!empty($to_user_data['User']['device_id'])) {
                $message = $fromUsername;
                if ($decoded['call'] == 'audio') {
                    $message = $message . ' is calling you.';
                } else {
                    $message = $message . ' is video calling you.';
                }

                if ($to_user_data['User']['push_notification_status'] == 1) {
                    $extra = array(
                        'call' => $decoded['call'],
                        'from_id' => $decoded['from_id'],
                        'from_name' => $decoded['from_name'],
                        'from_image_uri' => $decoded['from_image_uri'],
                        'to_id' => $decoded['to_id'],
                        'to_name' => $decoded['to_name'],
                        'to_image_uri' => $decoded['to_image_uri']
                    );
                    // $this->send_notification_for_iphone($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                    if ($to_user_data['User']['device_type'] == 1) {
                        $this->send_notification_for_android($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                    } else if ($to_user_data['User']['device_type'] == 2) {
                        $this->send_notification_for_iphone_test($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                    } else {
                        $this->send_notification_for_iphone_test($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message'] = $decoded['sound'];
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_call_notification_new($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        $friendIds = "";
        $this->loadModel('User');
        $fromUsername = "Someone";
        if (!empty($decoded)) {
            if (isset($decoded['to_id']) && (!empty($decoded['to_id']))) {
                $to_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['to_id']), 'fields' => array('id', 'device_id', 'device_type', 'push_notification_status', 'voip_token')));
            }

            if (!empty($to_user_data['User']['voip_token'])) {

                $callAccepted = isset($decoded['callAccepted']) && !empty($decoded['callAccepted']) ? $decoded['callAccepted'] : '0';
                $callDecline = isset($decoded['callDecline']) && !empty($decoded['callDecline']) ? $decoded['callDecline'] : '0';
                $extra = array(
                    'aps' => array(
                        "content-available" => 1,
                        'priority' => 'high',
                        'apns-expiration' => 0
                    ),
                    'extra' => array(
                        'call' => $decoded['call'],
                        'from_id' => $decoded['from_id'],
                        'from_name' => $decoded['from_name'],
                        'from_image_uri' => $decoded['from_image_uri'],
                        'to_id' => $decoded['to_id'],
                        'to_name' => $decoded['to_name'],
                        'to_image_uri' => $decoded['to_image_uri'],
                        'fromUUID' => $decoded['fromUUID'],
                        'room_id' => $decoded['room_id'],
                        'callAccepted' => $callAccepted,
                        'callDecline' => $callDecline,
                        'receiver_token' => $to_user_data['User']['voip_token']
                    )
                );

                $url = "https://rapidsofts.com/voippush/send_push.php";
                //$url = "https://mapbuzz.herokuapp.com/send_push.php";

                $base_path = getcwd();
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $headers = array(
                    "Content-Type: application/json",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($extra));
                //for debug only!
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $resp = curl_exec($curl);
                curl_close($curl);

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message'] = "calling";
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_call_notification_newdev($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        $friendIds = "";
        $this->loadModel('User');
        $fromUsername = "Someone";
        if (!empty($decoded)) {
            if (isset($decoded['to_id']) && (!empty($decoded['to_id']))) {
                $to_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['to_id']), 'fields' => array('id', 'device_id', 'device_type', 'push_notification_status', 'voip_token')));
            }

            if (!empty($to_user_data['User']['voip_token'])) {

                $callAccepted = isset($decoded['callAccepted']) && !empty($decoded['callAccepted']) ? $decoded['callAccepted'] : '0';
                $callDecline = isset($decoded['callDecline']) && !empty($decoded['callDecline']) ? $decoded['callDecline'] : '0';
                $extra = array(
                    'aps' => array(
                        "content-available" => 1,
                        'priority' => 'high',
                        'apns-expiration' => 0
                    ),
                    'extra' => array(
                        'call' => $decoded['call'],
                        'from_id' => $decoded['from_id'],
                        'from_name' => $decoded['from_name'],
                        'from_image_uri' => $decoded['from_image_uri'],
                        'to_id' => $decoded['to_id'],
                        'to_name' => $decoded['to_name'],
                        'to_image_uri' => $decoded['to_image_uri'],
                        'fromUUID' => $decoded['fromUUID'],
                        'room_id' => $decoded['room_id'],
                        'callAccepted' => $callAccepted,
                        'callDecline' => $callDecline,
                        'receiver_token' => $to_user_data['User']['voip_token']
                    )
                );

                $url = "https://rapidsofts.com/voippush/send_push_dev.php";
                //$url = "https://mapbuzz.herokuapp.com/send_push_dev.php";

                $base_path = getcwd();
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $headers = array(
                    "Content-Type: application/json",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($extra));
                //for debug only!
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $resp = curl_exec($curl);
                curl_close($curl);

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message'] = "calling";
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_missed_call_notification($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        //$friendIds = "";
        $this->loadModel('User');
        $fromUsername = "Someone";
        if (!empty($decoded)) {
            if (isset($decoded['from_id']) && (!empty($decoded['from_id']))) {
                $from_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['from_id']), 'fields' => array('id', 'device_id', 'device_type', 'phone_number', 'profile_image', 'full_name')));
                $fromUsername = $from_user_data['User']['phone_number'];
                if (!empty($from_user_data['User']['full_name'])) {
                    $fromUsername = $from_user_data['User']['full_name'];
                }
            }

            $contents = "";
            if (isset($decoded['to_id']) && (!empty($decoded['to_id']))) {
                $to_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['to_id']), 'fields' => array('id', 'device_id', 'device_type', 'push_notification_status')));
            }

            if (!empty($to_user_data['User']['device_id'])) {
                $message = $fromUsername;
                $message = $message . ' missed call.';

                if ($to_user_data['User']['push_notification_status'] == 1) {
                    // $this->send_notification_for_iphone($to_user_data['User']['device_id'], $message);
                    if ($to_user_data['User']['device_type'] == 1) {
                        $this->send_notification_for_android($to_user_data['User']['device_id'], $message);
                    } else if ($to_user_data['User']['device_type'] == 2) {
                        $this->send_notification_for_iphone_test($to_user_data['User']['device_id'], $message);
                    } else {
                        $this->send_notification_for_iphone_test($to_user_data['User']['device_id'], $message);
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function broadcastMessage() {
        $this->loadModel('User');
        if (isset($_GET['t']) && $_GET['t'] != "") {

            $msg = base64_decode($_GET['t']);
            $users = $this->User->find('list', array(
                'fields' => array('User.device_id', 'User.device_type'),
                'conditions' => array('User.status' => 1, 'User.device_id !=' => '', 'User.device_id !=' => 'Null'),
                'recursive' => 0
            ));

            $txt = "user id date" . $msg;
            $myfile = file_put_contents('./datacheck.txt', $txt . PHP_EOL, FILE_APPEND | LOCK_EX);

            $data = WWW_ROOT . UPLOAD_FOLDER . DS . "datacheck.txt";

            $txt = "user id date" . $msg;
            $myfile = file_put_contents($data, $txt . PHP_EOL, FILE_APPEND | LOCK_EX);

            $this->send_notification_for_iphone_test('52a2eada5cf48b55971beaa15d913569e908373b49d687f03bedb7680b2e3410', "sample");
        }
    }

    public function send_notification($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        $friendIds = "";
        $this->loadModel('User');
        if (!empty($decoded)) {
            if (isset($decoded['jid']) && (!empty($decoded['jid']))) {
                $user_data = $this->User->find('first', array('conditions' => array('jid' => $decoded['jid']), 'fields' => array('id', 'jid', 'device_id', 'device_type')));
            }

            if (isset($decoded['user_id']) && (!empty($decoded['user_id']))) {
                $user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['user_id']), 'fields' => array('id', 'jid', 'device_id', 'device_type')));
            }

            if (!empty($user_data['User']['device_id'])) {
                // $this->send_notification_for_iphone($user_data['User']['device_id'], $decoded['message']);

                if ($user_data['User']['device_type'] == 1) {
                    $this->send_notification_for_android($user_data['User']['device_id'], $decoded['message']);
                } else if ($user_data['User']['device_type'] == 2) {
                    $this->send_notification_for_iphone_test($user_data['User']['device_id'], $decoded['message']);
                } else {
                    $this->send_notification_for_iphone_test($user_data['User']['device_id'], $decoded['message']);
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'failed';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_notification_for_android($deviceToken = null, $message = null, $type = 99, $objectId = 1, $iindObjectId = 1, $badge = 1, $extraObject = null, $sound = null) {
        if (strlen($deviceToken) < 40) {
            return false;
        }

        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => $deviceToken,
            'data' => array("message" => $message),
        );

        $headers = array(
            'Authorization: key=' . "AIzaSyB75XZEHf0LPuRnrt7a0JbDkhjGfyX2mdc",
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        if ($result === FALSE) {

            die('Curl failed: ' . curl_error($ch));
        }
    }

    public function send_notification_for_iphone(
            $deviceToken = null,
            $message = null,
            $type = 99,
            $objectId = 1,
            $iindObjectId = 1,
            $badge = 1,
            $extraObject = null,
            $sound = null
    ) {

        if (strlen($deviceToken) < 40) {
            return false;
        }

        if ($extraObject == null) {
            $message = array('aps' => array('alert' => $message, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'badge' => $badge));
        } else {
            $message = array(
                'aps' => array('alert' => $message, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'badge' => $badge),
                'extra' => $extraObject
            );
        }

        //$passphrase = "123456";
        $passphrase = "qwer";

        $base_path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR;
        // $base_path = dirname(__FILE__);

        $ctx = stream_context_create();
        // stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/push_production_new.pem');
        //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/push_prod_v2.pem');
        //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/push_development_zhang.pem');
        //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/push_prod_zhang.pem');
        // stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/ck.pem');
        //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/mapmateAPNDist.pem');
        stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/CertificatesProdPush.pem');
        //stream_context_set_option($ctx, 'ssl', 'local_cert', $base_path . '/apns-ProdRenewedCk.pem');
        //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        stream_context_set_option($ctx, 'ssl', 'cafile', $base_path . '/entrust_2048_ca.cer');

        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        //$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);


        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        if ($sound == null) {
            $message['aps']['sound'] = 'default';
        } else {
            $message['aps']['sound'] = $sound;
        }
        $message['aps']['content_available'] = 1;
        $body = json_encode($message);

        $message = $body;

        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($message)) . $message;

        $result = fwrite($fp, $msg, strlen($msg));

        //print_r($result); exit;
        fclose($fp);
    }

    public function send_notification_for_iphone_test(
            $deviceToken = null,
            $title = null,
            $type = 99,
            $objectId = 1,
            $iindObjectId = 1,
            $badge = 1,
            $extraObject = null,
            $sound = null
    ) {

        if (strlen($deviceToken) < 40) {
            return false;
        }

        if ($extraObject == null) {
            $message = array(
                'aps' => array('alert' => $title, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'badge' => $badge)
            );
        } else {
            $message = array(
                'aps' => array('alert' => $title, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'badge' => $badge),
                'extra' => $extraObject
            );
        }

        if ($sound == null) {
            $sound = 'default';
            $message['aps']['sound'] = 'default';
        } else {
            $message['aps']['sound'] = $sound;
        }
        $message['aps']['content_available'] = 1;

        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAA1hU4Sr0:APA91bGMiE06_3FPCqhYAOs8XPozornWTbTihv1uItLLY3Ip6d6tpxWRwYJsyd4UlhfPrk-apOXe35tMc9M3eu101szvz2Ip7eElFjYEit1LjoGV59Vgr5k7lFFA_5c1pYdARPvnve8v';

        $notification = array('title' => $title, 'text' => $title, 'badge' => $badge, 'type' => $type, 'objId' => $objectId, 'iindObjId' => $iindObjectId, 'sound' => $sound, 'extra' => $extraObject);
        $arrayToSend = array('to' => $deviceToken, 'notification' => $notification, 'priority' => 'high');
        $json = json_encode($arrayToSend);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = @curl_exec($ch);
        if ($response === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }

    function user_notifications($test_data = null) {
        $data = file_get_contents("php://input");
        //echo time();die;
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $length = "";
        $offset = "";
        $length_new = 10;

        $decoded = json_decode($data, true);
        $responseData = array();

        if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
            $offset = $decoded['offset'];
        }

        if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
            $length_new = $decoded['limit'];
        }

        if (!isset($offset) || $offset == 0 || $offset < 0) {
            $offset = 0;
        }

        $this->loadModel('UserNotification');
        $this->UserNotification->Behaviors->load('Containable');
        $this->UserNotification->bindModel(array(
            'belongsTo' => array(
                'User' => array(
                    'className' => 'User',
                    'foreignKey' => 'fromid',
                    'fields' => array('id', 'username', 'username', 'first_name', 'last_name', 'profile_image', 'status')
                )
            )
                ), false);

        $this->UserNotification->User->bindModel(array(
            'hasOne' => array(
                'UserImage' => array(
                    'className' => 'UserImage',
                    'foreignKey' => 'user_id',
                    'conditions' => array('UserImage.image_type' => 1),
                    'fields' => array('id', 'user_id', 'image', 'status'),
                    'order' => array('UserImage.id DESC'),
                )
            )
                ), false);

        $this->loadModel('UserBlock');
        $this->loadModel('UserImage');
        $blockedQryString = "";
        $blocked_user = $this->UserBlock->find('all', array('fields' => array('user_id', 'blocked_user_id'), 'conditions' => array('OR' => array('UserBlock.user_id' => $decoded['user_id'], 'UserBlock.blocked_user_id' => $decoded['user_id']), 'UserBlock.status' => 1)));
        $blockedAllArr = array();
        if (!empty($blocked_user)) {
            foreach ($blocked_user as $blockKey => $blockValue) {
                $blockedAllArr[] = $blockValue['UserBlock']['user_id'];
                $blockedAllArr[] = $blockValue['UserBlock']['blocked_user_id'];
            }
        }
        $blockedAllArr = array_unique($blockedAllArr);
        $blockedString = implode("','", $blockedAllArr);
        if (!empty($blockedString)) {
            $blockedQryString = "UserNotification.fromid NOT IN('$blockedString')";
        }

        $total_records = $this->UserNotification->find('count', array(
            'contain' => array('User' => array(
                    'UserImage',
                )),
            'conditions' => array('UserNotification.toid' => $decoded['user_id'], "User.id!=''", $blockedQryString)
        ));

        $data = $this->UserNotification->find('all', array(
            'limit' => "$offset, $length_new",
            'contain' => array('User' => array(
                    'UserImage',
                )),
            'conditions' => array('UserNotification.toid' => $decoded['user_id'], "User.id!=''", $blockedQryString),
            'order' => 'UserNotification.created DESC'
        ));

        if (!empty($data)) {

            $user_notifications = array();
            foreach ($data as $key => $val) {
                if (!empty($val['User']['id']) && !empty($val['User']['username'])) {
                    $user_notifications[$key]['type'] = "user_added";
                    if ($val['UserNotification']['type'] == 1) {
                        $user_notifications[$key]['type'] = "image_like";
                    }
                    if ($val['UserNotification']['type'] == 51) {
                        $user_notifications[$key]['type'] = "someone_near";
                    }
                    if ($val['UserNotification']['type'] == 52) {
                        $user_notifications[$key]['type'] = "new_photo";
                    }
                    $user_notifications[$key]['user_id'] = $val['UserNotification']['fromid'];
                    $user_notifications[$key]['message'] = $val['UserNotification']['message'];
                    $user_notifications[$key]['created'] = $val['UserNotification']['created'];
                    $user_notifications[$key]['image'] = $val['UserNotification']['image_url'];
                    $user_notifications[$key]['image_path'] = '';
                    if (!empty($val['UserNotification']['image_path'])) {
                        $user_notifications[$key]['image_path'] = $val['UserNotification']['image_path'];
                    }

                    $user_notifications[$key]['user_image_path'] = "";
                    if (!empty($val['User']['UserImage']['image'])) {
                        $user_notifications[$key]['user_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['User']['UserImage']['image'];
                    }
                    if ($val['UserNotification']['type'] == 99) {
                        $user_notifications[$key]['user_image_path'] = "";
                        $user_notifications[$key]['type'] = "user_match";

                        $userImages = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $val['UserNotification']['fromid'], 'UserImage.image_type' => 1)));
                        if (!empty($userImages)) {
                            if ($userImages['UserImage']['image_type'] == 1 && !empty($userImages['UserImage']['image'])) {
                                $user_notifications[$key]['user_image_path'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userImages['UserImage']['image'];
                            }
                        }
                    }
                }
            }

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['offset'] = $offset;
            $responseData['total_records'] = $total_records;
            $responseData['response_data'] = array_values($user_notifications);
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function user_feeds($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $length = "";
        $offset = "";
        $length_new = 10;

        $decoded = json_decode($data, true);
        $responseData = array();

        if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
            $offset = $decoded['offset'];
        }

        if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
            $length_new = $decoded['limit'];
        }

        if (!isset($offset) || $offset == 0 || $offset < 0) {
            $offset = 0;
        }

        $this->loadModel("UserContact");
        $this->loadModel("User");
        $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);

        $this->UserContact->bindModel(array('belongsTo' => array('User' => array('className' => 'User', 'foreignKey' => 'contact_user_id', 'fields' => array('id', 'status')))), false);

        $friendIds = "";
        if (!empty($decoded)) {
            $userId = $decoded['user_id'];

            $this->loadModel('UserBlock');
            $blockedQryString = "";
            $blocked_user = $this->UserBlock->find('all', array('fields' => array('user_id', 'blocked_user_id'), 'conditions' => array('OR' => array('UserBlock.user_id' => $decoded['user_id'], 'UserBlock.blocked_user_id' => $decoded['user_id']), 'UserBlock.status' => 1)));
            $blockedAllArr = array();
            if (!empty($blocked_user)) {
                foreach ($blocked_user as $blockKey => $blockValue) {
                    $blockedAllArr[] = $blockValue['UserBlock']['user_id'];
                    $blockedAllArr[] = $blockValue['UserBlock']['blocked_user_id'];
                }
            }
            $blockedAllArr = array_unique($blockedAllArr);
            $blockedString = implode("','", $blockedAllArr);
            if (!empty($blockedString)) {
                $blockedQryString = "UserContact.contact_user_id NOT IN('$blockedString')";
            }


            $data = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $decoded['user_id'], "User.id!=''", $blockedQryString)));

            $contArr = array();
            if (!empty($data)) {
                foreach ($data as $ContKey => $ContVal) {
                    $contArr[$ContVal['UserContact']['id']] = $ContVal['UserContact']['contact_user_id'];
                }
            }

            $contArr[] = $userId;
            $friendIds = implode("','", $contArr);
            $user_data = $this->User->find('all', array('conditions' => array("User.id IN('$friendIds')"), 'fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $this->loadModel("UserImageLike");
            $this->loadModel('UserImage');
            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $decoded['user_id'], 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;
            if (!empty($friendIds)) {
                $userArr = array();
                $existFriendList = array();
                foreach ($user_data as $userKey => $userVal) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                    if (!empty($userVal['User']['full_name'])) {
                        $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                    }

                    $userArr[$userVal['User']['id']]['profile_image'] = "";
                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $userVal['User']['id'], 'UserImage.image_type' => 1)));

                    foreach ($userImages as $key => $value) {
                        if ($value['UserImage']['image_type'] == 1) {
                            $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                    }
                }

                $this->loadModel("Message");

                $countData = $this->Message->query("select count(*) as totalRec, 'image' as type from user_images as UserImage
            where UserImage.image !='' and UserImage.user_id IN('$friendIds')");

                foreach ($countData as $countKey => $countVal) {
                    $totalRec += $countVal[0]['totalRec'];
                }

                $data = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, 
            UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx
            from user_images as UserImage 
            where UserImage.image !='' AND UserImage.image_type NOT IN (1,2,4,6) and UserImage.user_id IN('$friendIds')  
            order by updated DESC limit $offset, $length_new");

                $feedArr = array();
                $count = 0;
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal['UserImage']['value'])) {

                        $feedArr[$count]['feed_id'] = $FeedVal['UserImage']['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal['UserImage']['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        $feedArr[$count]['message'] = $userArr[$FeedVal['UserImage']['userId']]['USERNM'] . " added a new photo";
                        $feedArr[$count]['user_name'] = $userArr[$FeedVal['UserImage']['userId']]['USERNM'];
                        $feedArr[$count]['added_on'] = $FeedVal['UserImage']['updated'];
                        $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal['UserImage']['value'];

                        if (in_array($FeedVal['UserImage']['id'], $user_liked_images)) {
                            $feedArr[$count]['liked_by_me'] = 1;
                        }

                        $feedArr[$count]['type'] = 'Image';
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal['UserImage']['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal['UserImage']['userId'];
                        $feedArr[$count]['lat'] = $FeedVal['UserImage']['lat'];
                        $feedArr[$count]['lon'] = $FeedVal['UserImage']['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal['UserImage']['category_idx'];
                        $count++;
                    }
                }

                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $feedArr;
                $responseData['offset'] = $offset;
                $responseData['total_records'] = $totalRec;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $responseData['offset'] = $offset;
                $responseData['total_records'] = $totalRec;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function user_feeds_discover($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $length = "";
        $offset = "";
        $length_new = 10;
        $current_lat = "";
        $current_lon = "";

        $decoded = json_decode($data, true);
        $responseData = array();

        if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
            $offset = $decoded['offset'];
        }

        if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
            $length_new = $decoded['limit'];
        }

        if ((isset($decoded['current_lat'])) && (!empty($decoded['current_lat']))) {
            $current_lat = $decoded['current_lat'];
        }

        if ((isset($decoded['current_lon'])) && (!empty($decoded['current_lon']))) {
            $current_lon = $decoded['current_lon'];
        }

        if (!isset($offset) || $offset == 0 || $offset < 0) {
            $page = 1;
        } else {
            $page = $decoded['offset'];
        }


        $start_from = ($page - 1) * $length_new;

        if (true) {
            $this->loadModel('UserLocation');
            $userLocationData = array();

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['user_id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
            }

            $userLocationData['UserLocation']['user_id'] = $decoded['user_id'];
            $userLocationData['UserLocation']['lat'] = $decoded['current_lat'];
            $userLocationData['UserLocation']['lon'] = $decoded['current_lon'];

            $this->UserLocation->save($userLocationData);

            $this->loadModel("User");
            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => '1'), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $this->loadModel("UserImageLike");
            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $decoded['user_id'], 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            $existFriendList = array();
            foreach ($user_data as $userKey => $userVal) {

                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";
                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }

            $this->loadModel("Message");

            $user_id = $decoded['user_id'];
            $data = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND UserImage.image !='' AND UserImage.image_type NOT IN (1,2,4,5,6) AND UserImage.status = 1 UNION select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
							from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( user_locations.lat ) )
							  * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                              where User.hide_from_map = 0 and User.distance <= 5 * 1.609 and User.id != $user_id and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) order by updated DESC limit $start_from, $length_new");

            $feedArr = array();
            $count = 0;

            foreach ($data as $FeedKey => &$FeedVal) {
                if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                    if ($FeedVal[0]['type'] == 'Image') {
                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;
                    }
                    if ($FeedVal[0]['type'] == 'Image') {
                        if (!empty($FeedVal[0]['value'])) {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                        }
                        if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                            $feedArr[$count]['liked_by_me'] = 1;
                        }
                    }

                    if ($FeedVal[0]['type'] == 'Image') {
                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        $count++;
                    }
                }
            }

            /* get users amount around 5 miles */

            $aroundUsers = $this->Message->query("select count(*) as amount from ((SELECT *,
							  IFNULL(( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ), 0) AS distance FROM user_locations) as UserLocation) where UserLocation.distance <= 5 * 1.609"); // AND UserLocation.user_id != $user_id

            $totalAroundUsers = 0;
            foreach ($aroundUsers as $countKey => $countVal) {
                $totalAroundUsers += $countVal[0]['amount'];
            }

            if (!empty($decoded['user_id'])) {
                if (!empty($decoded['country'])) {
                    $country = $decoded['country'];
                } else {
                    $userData = $this->User->find('first', array('fields' => array('country'), 'conditions' => array('User.id' => $decoded['user_id'])));
                    $country = $userData['User']['country'];
                }


                $this->loadModel('UserImage');
                $this->loadModel('UserBlock');
                $this->loadModel('UserContact');
                $this->loadModel('UserContactRequest');

                $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
							from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( user_locations.lat ) )
							  * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
							  where User.distance <= 100 AND User.id != " . $decoded['user_id'] . " ORDER BY id DESC limit 12");

                if (!empty($profiledata)) {
                    foreach ($profiledata as $ukey => $uvalue) {

                        $profiledata['User'][$ukey] = $uvalue['User'];

                        $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $uvalue['User']['id'], 'UserImage.image_type' => array(1, 2))));

                        $profiledata['User'][$ukey]['profile_image'] = "";
                        $profiledata['User'][$ukey]['profile_cover_image'] = "";
                        foreach ($userImages as $key => $value) {
                            if ($value['UserImage']['image_type'] == 1) {
                                $profiledata['User'][$ukey]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                            }
                            if ($value['UserImage']['image_type'] == 2) {
                                $profiledata['User'][$ukey]['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                            }
                        }

                        /* get location and score */
                        $this->loadModel('UserLocation');

                        $user_lat = "0.0";
                        $user_lon = "0.0";
                        $user_score = 0;
                        $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $uvalue['User']['id'])));

                        if (!empty($location_data['UserLocation']['user_id'])) {
                            $user_lat = $location_data['UserLocation']['lat'];
                            $user_lon = $location_data['UserLocation']['lon'];
                            $user_score = $location_data['UserLocation']['score'];
                        }

                        $profiledata['User'][$ukey]['lat'] = $user_lat;
                        $profiledata['User'][$ukey]['lon'] = $user_lon;
                        $profiledata['User'][$ukey]['score'] = $user_score;

                        $profiledata['User'][$ukey]['UserBlock'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $uvalue['User']['id'])));
                        $profiledata['User'][$ukey]['UserBlocked'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.blocked_user_id' => $uvalue['User']['id'])));

                        $profiledata['User'][$ukey]['UserContact'] = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $uvalue['User']['id'], 'UserContact.contact_user_id' => $uvalue['User']['id'])));
                        $profiledata['User'][$ukey]['UserContactRequest'] = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $uvalue['User']['id'])));

                        $user_id = $uvalue['User']['id'];
                        $profiledata['User'][$ukey]['contact_request'] = 0;
                        $profiledata['User'][$ukey]['is_contact'] = 0;
                        $profiledata['User'][$ukey]['is_sender'] = 0;

                        $this->loadModel('UserContactRequest');
                        $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id)));

                        if (!empty($contact_request)) {
                            $profiledata['User'][$ukey]['contact_request'] = 1;
                        }

                        $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

                        if (!empty($contact_request_is_sender)) {
                            $profiledata['User'][$ukey]['is_sender'] = 1;
                        }

                        $this->loadModel('UserContact');
                        $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $uvalue['User']['id'])));

                        if (!empty($contact)) {
                            $profiledata['User'][$ukey]['is_contact'] = 1;
                        }
                    }
                }

                if (!empty($profiledata)) {
                    $profiledata = $profiledata['User'];
                } else {
                    $profiledata = [];
                }
            } else {
                $profiledata = [];
            }

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = $feedArr;
            $responseData['residence'] = $profiledata;
            $responseData['offset'] = $offset;
            $responseData['total_records'] = $totalRec;
            $responseData['around_users_amount'] = $totalAroundUsers;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $responseData['residence'] = array();
            $responseData['offset'] = $offset;
            $responseData['total_records'] = $totalRec;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function user_feeds_discoverNew($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $length = 20;
        $offset = "";
        $current_lat = "";
        $current_lon = "";

        $decoded = json_decode($data, true);
        $responseData = array();

        if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
            $offset = $decoded['offset'];
        }

        if ((isset($decoded['current_lat'])) && (!empty($decoded['current_lat']))) {
            $current_lat = $decoded['current_lat'];
        }

        if ((isset($decoded['current_lon'])) && (!empty($decoded['current_lon']))) {
            $current_lon = $decoded['current_lon'];
        }

        if (!isset($offset) || $offset == 0 || $offset < 0) {
            $page = 1;
        } else {
            $page = $decoded['offset'];
        }

        $start_from = ($page - 1) * $length;

        /* user location update */
        $this->loadModel('UserLocation');
        $userLocationData = array();

        $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['user_id'])));
        if (!empty($location_data['UserLocation']['user_id'])) {
            $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
        }
        $userLocationData['UserLocation']['user_id'] = $decoded['user_id'];
        $userLocationData['UserLocation']['lat'] = $decoded['current_lat'];
        $userLocationData['UserLocation']['lon'] = $decoded['current_lon'];

        $this->UserLocation->save($userLocationData);

        $this->loadModel("User");
        $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => '1'), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);

        if (!empty($decoded['user_id'])) {
            $user_id = $decoded['user_id'];
            $location = $decoded['location'];
            $subCategory = $decoded['subCategory'];
            $age_min = isset($decoded['age']['min']) ? $decoded['age']['min'] : '';
            $age_max = isset($decoded['age']['max']) ? $decoded['age']['max'] : '';

            /* get user age */
            $user_profile = $this->User->find('first', array(
                'fields' => array('id', 'age'),
                'conditions' => array('User.id' => $user_id)
            ));

            $age = 18;
            if (!empty($user_profile)) {
                $age = isset($user_profile['User']['age']) ? $user_profile['User']['age'] : 18;
                if (empty($age)) {
                    $age = 18;
                }
            }

            $age_condition = "";
            if (!empty($age_max)) {
                $age_condition = "User.age >= $age_min AND User.age <= $age_max AND";
            } else {
                if ($age >= 18) {
                    $age_condition = "User.age >= 18 AND";
                }
            }

            $interests_condition = "";
            if (!empty($subCategory)) {
                $interests_condition = "CONCAT(',', `interests`, ',') REGEXP ',($subCategory),' AND";
            }

            if (!empty($decoded['country'])) {
                $country = $decoded['country'];
            } else {
                $userData = $this->User->find('first', array('fields' => array('country'), 'conditions' => array('User.id' => $decoded['user_id'])));
                $country = $userData['User']['country'];
            }


            $this->loadModel('UserImage');
            $this->loadModel('UserBlock');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');

            if ($location == 1) {
                $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
                          from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
                            * COS( RADIANS( user_locations.lat ) )
                            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
                            + SIN( RADIANS($current_lat) )
                            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                            where User.distance <= 100 AND User.username != '' AND " . $age_condition . " " . $interests_condition . " User.id != " . $decoded['user_id'] . " ORDER BY User.distance ASC limit $start_from, $length");
                if (empty($profiledata)) {
                    $location = 2;
                    $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
                          from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
                            * COS( RADIANS( user_locations.lat ) )
                            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
                            + SIN( RADIANS($current_lat) )
                            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                            where User.distance <= 12714 AND User.username != '' AND " . $age_condition . " " . $interests_condition . " User.id != " . $decoded['user_id'] . " ORDER BY User.distance ASC limit $start_from, $length");
                }
            } else {
                $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
                          from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
                            * COS( RADIANS( user_locations.lat ) )
                            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
                            + SIN( RADIANS($current_lat) )
                            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                            where User.distance <= 12714 AND User.username != '' AND " . $age_condition . " " . $interests_condition . " User.id != " . $decoded['user_id'] . " ORDER BY id DESC limit $start_from, $length");
            }

            if (!empty($profiledata)) {
                foreach ($profiledata as $ukey => $uvalue) {

                    $profiledata['User'][$ukey] = $uvalue['User'];

                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $uvalue['User']['id'], 'UserImage.image_type' => array(1, 2))));

                    $profiledata['User'][$ukey]['profile_image'] = "";
                    $profiledata['User'][$ukey]['profile_cover_image'] = "";
                    foreach ($userImages as $key => $value) {
                        if ($value['UserImage']['image_type'] == 1) {
                            $profiledata['User'][$ukey]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                        if ($value['UserImage']['image_type'] == 2) {
                            $profiledata['User'][$ukey]['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                    }

                    /* get location and score */
                    $this->loadModel('UserLocation');

                    $user_lat = "0.0";
                    $user_lon = "0.0";
                    $user_score = 0;
                    $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $uvalue['User']['id'])));

                    if (!empty($location_data['UserLocation']['user_id'])) {
                        $user_lat = $location_data['UserLocation']['lat'];
                        $user_lon = $location_data['UserLocation']['lon'];
                        $user_score = $location_data['UserLocation']['score'];
                    }
                    /* get destance */
                    $profiledata['User'][$ukey]['distance'] = $this->distance($current_lat, $current_lon, $user_lat, $user_lon, 'K');
                    $profiledata['User'][$ukey]['lat'] = $user_lat;
                    $profiledata['User'][$ukey]['lon'] = $user_lon;
                    $profiledata['User'][$ukey]['score'] = $user_score;

                    $profiledata['User'][$ukey]['UserBlock'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $uvalue['User']['id'])));
                    $profiledata['User'][$ukey]['UserBlocked'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.blocked_user_id' => $uvalue['User']['id'])));

                    $profiledata['User'][$ukey]['UserContact'] = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $uvalue['User']['id'], 'UserContact.contact_user_id' => $uvalue['User']['id'])));
                    $profiledata['User'][$ukey]['UserContactRequest'] = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $uvalue['User']['id'])));

                    $user_id = $uvalue['User']['id'];
                    $profiledata['User'][$ukey]['contact_request'] = 0;
                    $profiledata['User'][$ukey]['is_contact'] = 0;
                    $profiledata['User'][$ukey]['is_sender'] = 0;

                    $this->loadModel('UserContactRequest');
                    $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id)));

                    if (!empty($contact_request)) {
                        $profiledata['User'][$ukey]['contact_request'] = 1;
                    }

                    $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

                    if (!empty($contact_request_is_sender)) {
                        $profiledata['User'][$ukey]['is_sender'] = 1;
                    }

                    $this->loadModel('UserContact');
                    $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $uvalue['User']['id'])));

                    if (!empty($contact)) {
                        $profiledata['User'][$ukey]['is_contact'] = 1;
                    }
                }
            }

            if (!empty($profiledata)) {
                $profiledata = $profiledata['User'];
                /* $this->array_sort_by_column($profiledata, 'distance'); */
            } else {
                $profiledata = [];
            }
        } else {
            $profiledata = [];
        }

        $responseData = array();
        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = "";
        $responseData['message'] = 'successfully';
        $responseData['response_data'] = array('offset' => $offset, 'location' => $location, 'data' => $profiledata);
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function stagging_user_feeds_discoverNew($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $length = 20;
        $offset = "";
        $current_lat = "";
        $current_lon = "";

        $decoded = json_decode($data, true);
        $responseData = array();

        if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
            $offset = $decoded['offset'];
        }

        if ((isset($decoded['current_lat'])) && (!empty($decoded['current_lat']))) {
            $current_lat = $decoded['current_lat'];
        }

        if ((isset($decoded['current_lon'])) && (!empty($decoded['current_lon']))) {
            $current_lon = $decoded['current_lon'];
        }

        if (!isset($offset) || $offset == 0 || $offset < 0) {
            $page = 1;
        } else {
            $page = $decoded['offset'];
        }

        $start_from = ($page - 1) * $length;

        /* user location update */
        $this->loadModel('UserLocation');
        $userLocationData = array();

        $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['user_id'])));
        if (!empty($location_data['UserLocation']['user_id'])) {
            $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
        }
        $userLocationData['UserLocation']['user_id'] = $decoded['user_id'];
        $userLocationData['UserLocation']['lat'] = $decoded['current_lat'];
        $userLocationData['UserLocation']['lon'] = $decoded['current_lon'];

        $this->UserLocation->save($userLocationData);

        $this->loadModel("User");
        $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => '1'), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);

        if (!empty($decoded['user_id'])) {
            $user_id = $decoded['user_id'];
            $location = $decoded['location'];
            $subCategory = $decoded['subCategory'];
            $age_min = isset($decoded['age']['min']) ? $decoded['age']['min'] : '';
            $age_max = isset($decoded['age']['max']) ? $decoded['age']['max'] : '';

            /* get user age */
            $user_profile = $this->User->find('first', array(
                'fields' => array('id', 'age'),
                'conditions' => array('User.id' => $user_id)
            ));

            $age = 18;
            if (!empty($user_profile)) {
                $age = isset($user_profile['User']['age']) ? $user_profile['User']['age'] : 18;
                if (empty($age)) {
                    $age = 18;
                }
            }

            $age_condition = "";
            if (!empty($age_max)) {
                $age_condition = "User.age >= $age_min AND User.age <= $age_max AND";
            } else {
                if ($age >= 18) {
                    $age_condition = "User.age >= 18 AND";
                }
            }

            $interests_condition = "";
            if (!empty($subCategory)) {
                $interests_condition = "CONCAT(',', `interests`, ',') REGEXP ',($subCategory),' AND";
            }

            if (!empty($decoded['country'])) {
                $country = $decoded['country'];
            } else {
                $userData = $this->User->find('first', array('fields' => array('country'), 'conditions' => array('User.id' => $decoded['user_id'])));
                $country = $userData['User']['country'];
            }


            $this->loadModel('UserImage');
            $this->loadModel('UserBlock');
            $this->loadModel('UserContact');
            $this->loadModel('UserContactRequest');

            if ($location == 1) {
                $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
                          from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
                            * COS( RADIANS( user_locations.lat ) )
                            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
                            + SIN( RADIANS($current_lat) )
                            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                            where User.distance <= 100 AND User.username != '' AND " . $age_condition . " " . $interests_condition . " User.id != " . $decoded['user_id'] . " ORDER BY User.distance ASC limit $start_from, $length");
                if (empty($profiledata)) {
                    $location = 2;
                    $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
                          from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
                            * COS( RADIANS( user_locations.lat ) )
                            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
                            + SIN( RADIANS($current_lat) )
                            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                            where User.distance <= 12714 AND User.username != '' AND " . $age_condition . " " . $interests_condition . " User.id != " . $decoded['user_id'] . " ORDER BY User.distance ASC limit $start_from, $length");
                }
            } else {
                $profiledata = $this->User->query("select User.id, User.is_private,User.username, User.email, User.jid, User.phone_number,User.push_notification_status, User.notification_preview_status, User.full_name, User.profile_image, User.profile_cover_image, User.hide_username_search, User.device_type, User.country, User.status, User.is_verify, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
                          from (select users.*, user_locations.lat, user_locations.lon, ( 3959 * ACOS( COS( RADIANS($current_lat) )
                            * COS( RADIANS( user_locations.lat ) )
                            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
                            + SIN( RADIANS($current_lat) )
                            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                            where User.distance <= 12714 AND User.username != '' AND " . $age_condition . " " . $interests_condition . " User.id != " . $decoded['user_id'] . " ORDER BY id DESC limit $start_from, $length");
            }

            if (!empty($profiledata)) {
                foreach ($profiledata as $ukey => $uvalue) {

                    $profiledata['User'][$ukey] = $uvalue['User'];

                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $uvalue['User']['id'], 'UserImage.image_type' => array(1, 2))));

                    $profiledata['User'][$ukey]['profile_image'] = "";
                    $profiledata['User'][$ukey]['profile_cover_image'] = "";
                    foreach ($userImages as $key => $value) {
                        if ($value['UserImage']['image_type'] == 1) {
                            $profiledata['User'][$ukey]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                        if ($value['UserImage']['image_type'] == 2) {
                            $profiledata['User'][$ukey]['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                    }

                    /* get location and score */
                    $this->loadModel('UserLocation');

                    $user_lat = "0.0";
                    $user_lon = "0.0";
                    $user_score = 0;
                    $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $uvalue['User']['id'])));

                    if (!empty($location_data['UserLocation']['user_id'])) {
                        $user_lat = $location_data['UserLocation']['lat'];
                        $user_lon = $location_data['UserLocation']['lon'];
                        $user_score = $location_data['UserLocation']['score'];
                    }
                    /* get destance */
                    $profiledata['User'][$ukey]['distance'] = $this->distance($current_lat, $current_lon, $user_lat, $user_lon, 'K');
                    $profiledata['User'][$ukey]['lat'] = $user_lat;
                    $profiledata['User'][$ukey]['lon'] = $user_lon;
                    $profiledata['User'][$ukey]['score'] = $user_score;

                    $profiledata['User'][$ukey]['UserBlock'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $uvalue['User']['id'])));
                    $profiledata['User'][$ukey]['UserBlocked'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.blocked_user_id' => $uvalue['User']['id'])));

                    $profiledata['User'][$ukey]['UserContact'] = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $uvalue['User']['id'], 'UserContact.contact_user_id' => $uvalue['User']['id'])));
                    $profiledata['User'][$ukey]['UserContactRequest'] = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $uvalue['User']['id'])));

                    $user_id = $uvalue['User']['id'];
                    $profiledata['User'][$ukey]['contact_request'] = 0;
                    $profiledata['User'][$ukey]['is_contact'] = 0;
                    $profiledata['User'][$ukey]['is_sender'] = 0;

                    $this->loadModel('UserContactRequest');
                    $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id)));

                    if (!empty($contact_request)) {
                        $profiledata['User'][$ukey]['contact_request'] = 1;
                    }

                    $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

                    if (!empty($contact_request_is_sender)) {
                        $profiledata['User'][$ukey]['is_sender'] = 1;
                    }

                    $this->loadModel('UserContact');
                    $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $uvalue['User']['id'])));

                    if (!empty($contact)) {
                        $profiledata['User'][$ukey]['is_contact'] = 1;
                    }

                    $this->loadModel('Followers');
                    $profiledata['User'][$ukey]['follower_count'] = $this->Followers->find('count', array(
                        'fields' => 'DISTINCT Followers.follower_id',
                        'conditions' => array('Followers.user_id' => $user_id, 'is_approvad' => true)
                    ));
                    $profiledata['User'][$ukey]['following_count'] = $this->Followers->find('count', array(
                        'fields' => 'DISTINCT Followers.user_id',
                        'conditions' => array('Followers.follower_id' => $user_id, 'is_approvad' => true)
                    ));
                    $profiledata['User'][$ukey]['pending_follower_count'] = $this->Followers->find('count', array(
                        'fields' => 'DISTINCT Followers.follower_id',
                        'conditions' => array('Followers.user_id' => $user_id, 'is_approvad' => false)
                    ));
                    $profiledata['User'][$ukey]['pending_following_count'] = $this->Followers->find('count', array(
                        'fields' => 'DISTINCT Followers.user_id',
                        'conditions' => array('Followers.follower_id' => $user_id, 'is_approvad' => false)
                    ));
                    $follower_status=$this->Followers->find('first', array(
                        'fields' => 'Followers.user_id, Followers.follower_id, is_approvad',
                        'conditions' => array('Followers.follower_id' => $user_id,'user_id'=>$decoded['user_id'])
                    ));
                    if($follower_status){
                        $profiledata['User'][$ukey]['follower_status']=0;
                    }else{
                        if($follower_status['is_approvad']){
                            $profiledata['User'][$ukey]['follower_status']=2;
                        }else{
                            $profiledata['User'][$ukey]['follower_status']=1;
                        }
                        
                    }
                    $following_status=$this->Followers->find('first', array(
                        'fields' => 'Followers.user_id, Followers.follower_id, is_approvad',
                        'conditions' => array('Followers.user_id' => $user_id, 'follower_id'=>$decoded['user_id'])
                    ));
                    if($following_status){
                        $profiledata['User'][$ukey]['following_status']=0;
                    }else{
                        if($following_status['is_approvad']){
                            $profiledata['User'][$ukey]['following_status']=2;
                        }else{
                            $profiledata['User'][$ukey]['following_status']=1;
                        }
                        
                    }
                }
            }

            if (!empty($profiledata)) {
                $profiledata = $profiledata['User'];
                /* $this->array_sort_by_column($profiledata, 'distance'); */
            } else {
                $profiledata = [];
            }
        } else {
            $profiledata = [];
        }

        $responseData = array();
        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = "";
        $responseData['message'] = 'successfully';
        $responseData['response_data'] = array('offset' => $offset, 'location' => $location, 'data' => $profiledata);
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

    public function get_image_feed_details($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $user_id = $decoded["id"];
        $image_id = $decoded["image_id"];

        if (true) {
            $this->loadModel("User");
            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $this->loadModel("UserImageLike");
            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            foreach ($user_data as $userKey => $userVal) {
                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";
                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }

            $this->loadModel("UserImage");

            $image_data = array();

            $data = $this->UserImage->find('first', array('conditions' => array('UserImage.id' => $image_id)));

            $image_data['feed_id'] = $data['UserImage']['id'];
            $image_data['updated'] = $data['UserImage']['modified'];
            $image_data['user_id'] = $data['UserImage']['user_id'];
            $image_data['value'] = $data['UserImage']['image'];
            $image_data['feed_likes'] = $data['UserImage']['total_likes'];
            $image_data['lat'] = $data['UserImage']['lat'];
            $image_data['lon'] = $data['UserImage']['lon'];
            $image_data['category_idx'] = $data['UserImage']['category_idx'];
            $image_data['liked_by_me'] = 0;

            $image_data['message'] = $userArr[$data['UserImage']['user_id']]['USERNM'] . " added a new photo";
            $image_data['added_on'] = $data['UserImage']['modified'];
            $image_data['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $data['UserImage']['image'];

            if (in_array($data['UserImage']['id'], $user_liked_images)) {
                $image_data['liked_by_me'] = 1;
            }

            $image_data['type'] = 'Image';
            $image_data['user_profile_pic'] = $userArr[$data['UserImage']['user_id']]['profile_image'];

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = $image_data;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function map_feeds_discover($test_data = null) {
        $data_n = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data_n = $testData;
        }

        $decoded = json_decode($data_n, true);
        $responseData = array();

        $current_lat = $decoded['current_lat'];
        $current_lon = $decoded['current_lon'];
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];

        $this->loadModel("User");
        $this->loadModel('UserImage');
        $this->loadModel("UserImageLike");
        $this->loadModel("Message");

        if (!empty($decoded)) {
            $user_id = $decoded['user_id'];

            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            foreach ($user_data as $userKey => $userVal) {
                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";

                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }

            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            $data = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 7 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) 
							  
							  UNION select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
							from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( user_locations.lat ) )
							  * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                              where User.distance <= 7 * 1.609 ");

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');

            $feedArr = array();
            $count = 0;
            $res = [];
            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                if (empty($res)) {
                    $responseData = array();
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }

            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        if ($show_me == 1) {
                            $conditions2 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions2
                                    )
                            );
                            if (!$res) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions3 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions3
                                )
                        );
                        if ($Location[0]['UserLocation']['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions4
                                    )
                            );
                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions1 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.friend_see_on_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions1
                                )
                        );
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1 && $Location[0]['UserLocation']['hide_from_map'] === 1) {
                            if ($Location[0]['UserLocation']['user_id'] == $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        $count++;
                    }
                }
            }
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array_values($feedArr);
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function map_feeds_discover_new($test_data = null) {
        $data_n = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data_n = $testData;
        }

        $offset = "";
        $length_new = 40;

        $decoded = json_decode($data_n, true);
        $responseData = array();

        $current_lat = $decoded['current_lat'];
        $current_lon = $decoded['current_lon'];
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];

        $this->loadModel("User");
        $this->loadModel('UserImage');
        $this->loadModel("UserImageLike");
        $this->loadModel("Message");

        if (!empty($decoded)) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                $length_new = $decoded['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            $user_id = $decoded['user_id'];

            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            foreach ($user_data as $userKey => $userVal) {
                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";

                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }

            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            $data = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 7 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) 
							  
							  UNION select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
							from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( user_locations.lat ) )
							  * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                              where User.distance <= 7 * 1.609 ORDER BY id DESC limit 1, 0");
            /*   where User.distance <= 7 * 1.609 ORDER BY id DESC limit $offset, $length_new"); */
            /* ORDER BY id DESC */
            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');

            $feedArr = array();
            $count = 0;
            $res = [];
            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                if (empty($res)) {
                    $responseData = array();
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }

            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        if ($show_me == 1) {
                            $conditions2 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions2
                                    )
                            );
                            if (!$res) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions3 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions3
                                )
                        );
                        if ($Location[0]['UserLocation']['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions4
                                    )
                            );
                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions1 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.friend_see_on_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions1
                                )
                        );
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1 && $Location[0]['UserLocation']['hide_from_map'] === 1) {
                            if ($Location[0]['UserLocation']['user_id'] == $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        $count++;
                    }
                }
            }
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array_values($feedArr);
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function map_feeds_discover_2($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $current_lat = $decoded['current_lat'];
        $current_lon = $decoded['current_lon'];
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];

        if (true) {
            $user_id = $decoded['user_id'];

            $this->loadModel("User");
            $this->loadModel('UserImage');

            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $this->loadModel("UserImageLike");
            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            foreach ($user_data as $userKey => $userVal) {
                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";

                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }

            $this->loadModel("Message");
            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            $data = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 7 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) 
							  
							  UNION select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
							from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( user_locations.lat ) )
							  * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                              where User.distance <= 7 * 1.609 ");
            /*  User.hide_from_map = 0 and and User.id != $user_id */

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');

            $feedArr = array();
            $count = 0;
            $res = [];
            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                if (empty($res)) {
                    $responseData = array();
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }

            if (!empty($data)) {

                foreach ($data as $FeedKey => $FeedVal) {

                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {


                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        if ($show_me == 1) {
                            $conditions2 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions2
                                    )
                            );
                            if (!$res) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions3 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions3
                                )
                        );

                        if ($Location[0]['UserLocation']['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions4
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions1 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.friend_see_on_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions1
                                )
                        );

                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1 && $Location[0]['UserLocation']['hide_from_map'] === 1) {
                            if ($Location3[0]['UserLocation']['user_id'] == $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        $count++;
                    }
                }
            }
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfullyy';
            $responseData['response_data'] = array_values($feedArr);
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public function map_feeds_discover_test($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $current_lat = $decoded['current_lat'];
        $current_lon = $decoded['current_lon'];
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];

        if (true) {
            $user_id = $decoded['user_id'];

            $this->loadModel("User");
            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $this->loadModel("UserImageLike");
            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            foreach ($user_data as $userKey => $userVal) {
                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";
                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }
            $this->loadModel("Message");
            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            $data = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 7 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (1,2,4,5,6) 
							  
							  UNION select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
							from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( user_locations.lat ) )
							  * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
                              where User.distance <= 7 * 1.609 ");
            /*  User.hide_from_map = 0 and and User.id != $user_id */

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');

            $feedArr = array();
            $count = 0;
            $res = [];
            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                if (empty($res)) {
                    $responseData = array();
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            }


            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {

                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        if ($show_me == 1) {
                            $conditions2 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions2
                                    )
                            );
                            if (!$res) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions3 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions3
                                )
                        );
                        if ($Location[0]['UserLocation']['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions4
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions1 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.friend_see_on_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions1
                                )
                        );
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        $count++;
                    }
                }
            }
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array_values($feedArr);
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_who_can_see_on_map($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');
            $flag = $decoded['flag'];

            $this->loadModel('UserLocation');
            $userLocationData = array();

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));

            if (!empty($location_data['UserLocation']['user_id'])) {
                $userLocationData['UserLocation']['id'] = $location_data['UserLocation']['id'];
            }

            $userLocationData['UserLocation']['user_id'] = $user_id;
            $userLocationData['UserLocation']['lat'] = $decoded['lat'];
            $userLocationData['UserLocation']['lon'] = $decoded['lon'];
            if ($flag == 1) {
                $userLocationData['UserLocation']['hide_from_map'] = 0;
                $userLocationData['UserLocation']['friend_see_on_map'] = 0;
                $userLocationData['UserLocation']['hide_from_profile'] = 0;
            } else if ($flag == 2) {
                $userLocationData['UserLocation']['hide_from_map'] = 1;
                $userLocationData['UserLocation']['friend_see_on_map'] = 0;
                $userLocationData['UserLocation']['hide_from_profile'] = 0;
            } else {
                $userLocationData['UserLocation']['hide_from_profile'] = 1;
                $userLocationData['UserLocation']['hide_from_map'] = 0;
                $userLocationData['UserLocation']['friend_see_on_map'] = 0;
            }

            $this->UserLocation->save($userLocationData);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = array();
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_who_can_see_on_map($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $user_id = ltrim($decoded['id'], '0');

            $this->loadModel('UserLocation');

            $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $decoded['id'])));
            if (!empty($location_data['UserLocation']['user_id'])) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['response_data'] = array();

                /* $responseData['response_data']['userId'] = $user_id; */

                if ($location_data['UserLocation']['hide_from_map'] == 0 && $location_data['UserLocation']['friend_see_on_map'] == 0 && $location_data['UserLocation']['hide_from_profile'] == 0) {
                    $responseData['flag'] = 1;
                } else if ($location_data['UserLocation']['hide_from_map'] == 1 && $location_data['UserLocation']['friend_see_on_map'] == 0 && $location_data['UserLocation']['hide_from_profile'] == 0) {
                    $responseData['flag'] = 2;
                } else {
                    $responseData['flag'] = 3;
                }
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['response_data'] = array();
                $responseData['flag'] = 0;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $responseData['flag'] = 0;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    function send_messages($userContData = null, $body = null, $action = null, $actVal = null, $id = null) {
        $userContData = $_POST['userContctacts'];
        $body = $_POST['body'];
        $userContData = json_decode(base64_decode($userContData), true);
        $body = base64_decode($body);
        $sentDeviceTo = array();
        foreach ($userContData as $userContKey => $userContVal) {
            $this->send_xmpp_message($userContVal['User']['jid'], $body, $action, $actVal, $id);
        }
    }

    function uploadmedia() {
        App::import('Lib', 'S3');
        if (!empty($_FILES)) {
            $mediatype = $_POST["uploadtype"];

            $path_info = pathinfo($_FILES['media']['name']);
            $_FILES['media']['name'] = $path_info['filename'] . "." . $path_info['extension'];

            $userMedia = $_FILES['media']['name'];

            if ($this->save_image_s3bucket($_FILES['media']['tmp_name'], 'uploads/media/' . $userMedia)) {

                /*                 * **** THUMBNAIL CREATION **** */
                $result['response_status'] = 1;
                $result['response_code'] = 200;
                $result['response_message_code'] = '';
                $result['message'] = 'success';
                $result['response_data'] = array();
                $result['response_data']['media'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/media/" . $userMedia;
                $jsonEncode = json_encode($result);
                return $jsonEncode;
            } else {
                $result['response_status'] = 0;
                $result['response_code'] = 400;
                $result['response_message_code'] = 500;
                $result['message'] = 'Media upload error.';
                $result['response_data'] = array();
                $result['response_data']['media'] = '';
                $jsonEncode = json_encode($result);

                return $jsonEncode;
            }
        } else {
            $result['response_status'] = 0;
            $result['response_code'] = 400;
            $result['response_message_code'] = 404;
            $result['message'] = 'file not found';
            $result['response_data'] = array();
            $result['response_data']['media'] = '';
            $jsonEncode = json_encode($result);
            return $jsonEncode;
        }
    }

    function save_image_s3bucket($image, $destination) {

        if (!defined('awsAccessKey'))
            define('awsAccessKey', 'AKIATR22M2SLHQHJB6MZ');
        if (!defined('awsSecretKey'))
            define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);

        if ($s3->putObjectFile($image, "txtter-upload-api-new", $destination, S3::ACL_PUBLIC_READ)) {
            return true;
        } else {
            return false;
        }
    }

    public function missed_call_notification($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded) && isset($decoded['user_id']) && isset($decoded['other_user_id'])) {
            $this->loadModel('User');
            $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
            $other_user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['other_user_id'])));

            $this->loadModel('UserNotification');
            $userNotificationData['UserNotification']['fromid'] = $user_data['User']['id'];
            $userNotificationData['UserNotification']['toid'] = $other_user_data['User']['id'];
            $userNotificationData['UserNotification']['image_id'] = 0;
            $userNotificationData['UserNotification']['image_url'] = '';
            $userNotificationData['UserNotification']['message'] = $user_data['User']['full_name'] . " called you.";
            $userNotificationData['UserNotification']['type'] = 0;
            $this->UserNotification->save($userNotificationData);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function read_notification_flag($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $notification_flag = 0;

        if (!empty($decoded) && isset($decoded['user_id'])) {

            $this->loadModel('User');

            $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));

            if ($user_data['User']['notification_flag']) {
                $notification_flag = 1;
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $responseData['response_data']['notification_flag'] = $user_data['User']['notification_flag'];
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_notification_flag($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded) && isset($decoded['user_id']) && isset($decoded['notification_flag'])) {
            $this->loadModel('User');

            $user_data = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));

            $user_data['User']['notification_flag'] = $decoded['notification_flag'];

            $this->User->save($user_data);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $responseData['response_data']['notification_flag'] = $decoded['notification_flag'];
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_user_blocks($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $notification_flag = 0;

        if (!empty($decoded) && isset($decoded['user_id'])) {

            $this->loadModel('User');
            $this->loadModel('UserBlock');
            $this->loadModel('UserImage');

            $blocked_users = array();

            $user_block = $this->UserBlock->find('all', array('conditions' => array('UserBlock.user_id' => $decoded['user_id'], 'UserBlock.status' => 1)));

            if (count($user_block)) {
                foreach ($user_block as $key => $blocked_user) {
                    $user = array();
                    $user = $this->User->find('first', array('fields' => array('id', 'username', 'full_name'), 'conditions' => array('User.id' => $blocked_user['UserBlock']['blocked_user_id'])));
                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $user['User']['id'], 'UserImage.image_type' => array(1, 2))));
                    $user['User']['profile_image'] = "";
                    $user['User']['profile_cover_image'] = "";
                    foreach ($userImages as $key => $value) {
                        if ($value['UserImage']['image_type'] == 1) {
                            $user['User']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                        if ($value['UserImage']['image_type'] == 2) {
                            $user['User']['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                        }
                    }
                    $user['User']['UserBlock'] = $blocked_user['UserBlock'];
                    $blocked_users[] = $user;
                }
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $responseData['response_data']['blocked_users'] = $blocked_users;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 500;
            $responseData['message'] = 'failed';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_categories($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $offset = "";
        $length_new = 10;

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserImage');
        $this->loadModel("User");
        $this->loadModel("UserImageLike");
        if (!empty($decoded) && isset($decoded['category_idx'])) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                $length_new = $decoded['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            $catPost = array();

            $this->User->bindModel(array('hasMany' => array(
                    'UserImage' => array(
                        'className' => 'UserImage',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
            )));

            $this->UserImage->bindModel(array('belongsTo' => array(
                    'User' => array(
                        'className' => 'User',
                        'foreignKey' => 'user_id'
                    )
                ),));

            $categoryPosts = $this->UserImage->find('all', array(
                'conditions' => array(
                    'UserImage.status =' => '1',
                    'UserImage.image_type =' => '0',
                    'UserImage.category_idx' => $decoded['category_idx']
                ),
                'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc'),
                'page' => $offset,
                'limit' => $length_new,
                'recursive' => 0
            ));

            foreach ($categoryPosts as $key => $categoryPost) {
                $PostUserProfile = '';

                $categoryPostUsers = $this->UserImage->find('first', array(
                    'conditions' => array(
                        'UserImage.user_id =' => $categoryPost['UserImage']['user_id'],
                        'UserImage.status =' => '1',
                        'UserImage.image_type =' => '1',
                    /* 'UserImage.category_idx' => $decoded['category_idx'] */
                    ),
                    'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
                ));
                if (!empty($categoryPostUsers['UserImage']['image'])) {
                    $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $categoryPostUsers['UserImage']['image'];
                }
                $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $categoryPost['UserImage']['user_id'], 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));

                if (in_array($categoryPost['UserImage']['id'], $user_liked_images)) {
                    $categoryPost['UserImage']['liked_by_me'] = 1;
                } else {
                    $categoryPost['UserImage']['liked_by_me'] = 0;
                }


                $categoryPost['UserImage']['type'] = ($categoryPost['UserImage']['image_type'] == '0' ? 'Image' : 'User');
                unset($categoryPost['UserImage']['image_type']);
                $categoryPost['UserImage']['feed_likes'] = $categoryPost['UserImage']['total_likes'];
                unset($categoryPost['UserImage']['total_likes']);
                unset($categoryPost['UserImage']['status']);
                unset($categoryPost['UserImage']['created']);
                $categoryPost['UserImage']['message'] = $categoryPost['User']['full_name'] . " added a new photo";
                $categoryPost['UserImage']['feed_id'] = $categoryPost['UserImage']['id'];
                unset($categoryPost['UserImage']['id']);
                $categoryPost['UserImage']['username'] = $categoryPost['User']['username'];
                $categoryPost['UserImage']['full_name'] = $categoryPost['User']['full_name'];
                $categoryPost['UserImage']['added_on'] = $categoryPost['UserImage']['modified'];
                unset($categoryPost['UserImage']['modified']);
                $categoryPost['UserImage']['shared_pic'] = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/' . $categoryPost['UserImage']['image'];
                unset($categoryPost['UserImage']['image']);
                unset($categoryPost['User']);
                $categoryPost['UserImage']['user_profile_pic'] = $PostUserProfile;
                $catPost[$key] = $categoryPost['UserImage'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";

            $responseData['offset'] = $offset;
            $responseData['response_data'] = $catPost;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else if ($this->request->isGet()) {

            $cat = array();
            $categoryIds = $this->UserImage->find('all', array(
                'fields' => 'DISTINCT UserImage.category_idx'
            ));
            foreach ($categoryIds as $key => $categoryId) {
                $cat[$key] = $categoryId['UserImage'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $responseData['response_data'] = $cat;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_categoriesNew($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $offset = "";
        $length_new = 10;

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserImage');
        $this->loadModel("User");
        $this->loadModel("UserImageLike");
        /* $this->loadModel("Message"); */
        if (!empty($decoded) && isset($decoded['category_idx'])) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                $length_new = $decoded['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            if (!isset($decoded['offset']) || $decoded['offset'] == 1 || $decoded['offset'] == 0 || $decoded['offset'] < 0) {
                $page = 1;
                $start_from = ($page - 1) * $length_new;
            } else {
                $start_from = ($decoded['offset'] - 1) * $length_new;
            }
            $catPost = array();

            $this->User->bindModel(array('hasMany' => array(
                    'UserImage' => array(
                        'className' => 'UserImage',
                        'foreignKey' => 'user_id',
                        'dependent' => true
                    ),
            )));

            $this->UserImage->bindModel(array('belongsTo' => array(
                    'User' => array(
                        'className' => 'User',
                        'foreignKey' => 'user_id'
                    )
                ),));

            $categoryPosts = $this->UserImage->find('all', array(
                'conditions' => array(
                    'UserImage.status =' => '1',
                    'UserImage.image_type =' => '0',
                    'UserImage.category_idx' => $decoded['category_idx']
                ),
                'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc'),
                'page' => $offset,
                'limit' => $length_new,
                'recursive' => 0
            ));

            $current_lat = $decoded['current_lat'];
            $current_lon = $decoded['current_lon'];

            $data = $this->UserImage->query("select * from ((SELECT *,
			( 6371 * ACOS( COS( RADIANS($current_lat) )
			* COS( RADIANS( lat ) )
			* COS( RADIANS( lon ) - RADIANS($current_lon) )
			+ SIN( RADIANS($current_lat) )
			* SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 6 * 1.609 AND UserImage.image_type = 0 AND UserImage.status = 1 AND UserImage.category_idx = " . $decoded['category_idx'] . " ORDER BY id DESC limit $start_from, $length_new");

            foreach ($data as $key => $categoryPost) {
                $PostUserProfile = '';

                $categoryPostUsers = $this->UserImage->find('first', array(
                    'conditions' => array(
                        'UserImage.user_id =' => $categoryPost['UserImage']['user_id'],
                        'UserImage.status =' => '1',
                        'UserImage.image_type =' => '1',
                    /* 'UserImage.category_idx' => $decoded['category_idx'] */
                    ),
                    'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
                ));
                if (!empty($categoryPostUsers['UserImage']['image'])) {
                    $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $categoryPostUsers['UserImage']['image'];
                }
                $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $categoryPost['UserImage']['user_id'], 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));

                if (in_array($categoryPost['UserImage']['id'], $user_liked_images)) {
                    $categoryPost['UserImage']['liked_by_me'] = 1;
                } else {
                    $categoryPost['UserImage']['liked_by_me'] = 0;
                }

                $categoryPsUsers = $this->User->find('first', array(
                    'conditions' => array(
                        'User.id =' => $categoryPost['UserImage']['user_id'],
                    ),
                    'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
                ));

                $categoryPost['UserImage']['type'] = ($categoryPost['UserImage']['image_type'] == '0' ? 'Image' : 'User');
                unset($categoryPost['UserImage']['image_type']);
                $categoryPost['UserImage']['feed_likes'] = $categoryPost['UserImage']['total_likes'];
                unset($categoryPost['UserImage']['total_likes']);
                unset($categoryPost['UserImage']['status']);
                unset($categoryPost['UserImage']['created']);
                $categoryPost['UserImage']['message'] = $categoryPsUsers['User']['full_name'] . " added a new photo";
                $categoryPost['UserImage']['feed_id'] = $categoryPost['UserImage']['id'];
                unset($categoryPost['UserImage']['id']);
                $categoryPost['UserImage']['username'] = $categoryPsUsers['User']['username'];
                $categoryPost['UserImage']['full_name'] = $categoryPsUsers['User']['full_name'];
                $categoryPost['UserImage']['added_on'] = $categoryPost['UserImage']['modified'];
                unset($categoryPost['UserImage']['modified']);
                $categoryPost['UserImage']['shared_pic'] = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/' . $categoryPost['UserImage']['image'];
                unset($categoryPost['UserImage']['image']);
                unset($categoryPost['User']);
                $categoryPost['UserImage']['user_profile_pic'] = $PostUserProfile;
                $catPost[$key] = $categoryPost['UserImage'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";

            $responseData['offset'] = $offset;
            $responseData['response_data'] = $catPost;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else if ($this->request->isGet()) {

            $cat = array();
            $categoryIds = $this->UserImage->find('all', array(
                'fields' => 'DISTINCT UserImage.category_idx'
            ));
            foreach ($categoryIds as $key => $categoryId) {
                $cat[$key] = $categoryId['UserImage'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $responseData['response_data'] = $cat;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_user_text_post($test_data = null) {
        date_default_timezone_set('Asia/Calcutta');

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserTextPost');
        if (!empty($decoded)) {
            $userPostdata = array();
            $fg = 0;
            if (isset($decoded['post_id'])) {
                $fg = 1;
                $userPostdata['UserTextPost']['id'] = $decoded['post_id'];
                $userPosts = $this->UserTextPost->find('all', array(
                    'conditions' => array(
                        'UserTextPost.id =' => $decoded['post_id']
                    ),
                    'order' => array('UserTextPost.id' => 'desc')
                ));

                if (isset($decoded['status'])) {
                    $userPostdata['UserTextPost']['status'] = $decoded['status'];
                }

                $userPostdata['UserTextPost']['user_id'] = empty($decoded['user_id']) ? $userPosts[0]['UserTextPost']['user_id'] : $decoded['user_id'];
                $userPostdata['UserTextPost']['title'] = empty($decoded['title']) ? $userPosts[0]['UserTextPost']['title'] : $decoded['title'];
                $userPostdata['UserTextPost']['content'] = empty($decoded['content']) ? $userPosts[0]['UserTextPost']['content'] : $decoded['content'];
                $userPostdata['UserTextPost']['category_idx'] = empty($decoded['category_idx']) ? $userPosts[0]['UserTextPost']['category_idx'] : $decoded['category_idx'];
                $userPostdata['UserTextPost']['lat'] = empty($decoded['current_lat']) ? $userPosts[0]['UserTextPost']['lat'] : $decoded['current_lat'];
                $userPostdata['UserTextPost']['lon'] = empty($decoded['current_lon']) ? $userPosts[0]['UserTextPost']['lon'] : $decoded['current_lon'];
            } else {
                $userPostdata['UserTextPost']['user_id'] = $decoded['user_id'];
                $userPostdata['UserTextPost']['title'] = $decoded['title'];
                $userPostdata['UserTextPost']['content'] = $decoded['content'];
                $userPostdata['UserTextPost']['category_idx'] = $decoded['category_idx'];
                $userPostdata['UserTextPost']['lat'] = $decoded['current_lat'];
                $userPostdata['UserTextPost']['lon'] = $decoded['current_lon'];
            }

            if ($this->UserTextPost->save($userPostdata)) {

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'Post ' . ($fg == 1 ? 'update' : 'add') . ' success';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = 'Post not ' . ($fg == 1 ? 'updated' : 'added');
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_user_text_post($test_data = null) {
        $offset = "";
        $length_new = 10;

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserTextPost');
        $this->loadModel('User');
        $this->loadModel('UserImage');

        if (!empty($decoded) && isset($decoded['post_id']) && isset($decoded['other_user_id'])) {
            if (!empty($decoded['user_id']) || !empty($decoded['post_id'])) {
                $conditions = array();
                $userPos = array();
                if (!empty($decoded['user_id']) && !empty($decoded['post_id'])) {
                    $conditions = array(
                        'UserTextPost.user_id =' => $decoded['user_id'],
                        'UserTextPost.id =' => $decoded['post_id'],
                        'UserTextPost.status =' => '1',
                    );
                } else if (!empty($decoded['user_id'])) {

                    $conditions = array(
                        'UserTextPost.user_id =' => $decoded['user_id'],
                        'UserTextPost.status =' => '1',
                    );
                } else if (!empty($decoded['post_id'])) {

                    $conditions = array(
                        'UserTextPost.id =' => $decoded['post_id'],
                        'UserTextPost.status =' => '1',
                    );
                }

                $userPosts = $this->UserTextPost->find('all', array(
                    'conditions' => $conditions,
                    'order' => array('UserTextPost.modified' => 'desc', 'UserTextPost.created' => 'desc')
                        /* 'order' => array('UserTextPost.id' => 'asc') */
                ));
                foreach ($userPosts as $key => $userPost) {
                    $PostUserProfile = "";
                    $categoryPostUsers = $this->UserImage->find('first', array(
                        'conditions' => array(
                            'UserImage.user_id =' => $userPost['UserTextPost']['user_id'],
                            'UserImage.status =' => '1',
                            'UserImage.image_type =' => '1',
                            'UserImage.category_idx' => $userPost['UserTextPost']['category_idx']
                        ),
                        'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
                    ));
                    if (!empty($categoryPostUsers['UserImage']['image'])) {
                        $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $categoryPostUsers['UserImage']['image'];
                    }
                    $userPost['UserTextPost']['username'] = $userPost['User']['username'];
                    $userPost['UserTextPost']['full_name'] = $userPost['User']['full_name'];
                    $userPost['UserTextPost']['post_id'] = $userPost['UserTextPost']['id'];
                    $userPost['UserTextPost']['lat'] = empty($userPost['UserTextPost']['lat']) ? "" : $userPost['UserTextPost']['lat'];
                    $userPost['UserTextPost']['lon'] = empty($userPost['UserTextPost']['lon']) ? "" : $userPost['UserTextPost']['lon'];
                    $userLikeUsers = array();
                    foreach ($userPost['UserPostLike'] as $userLk) {
                        if ($userLk['status'] == 1) {
                            $userLikeUsers[] = $userLk['user_id'];
                        }
                    }

                    /* $userPost['UserTextPost']['post_like'] = ($userPost['UserPostLike'][0]['status'] == 1 ? 1 : 0); */
                    $userPost['UserTextPost']['post_like'] = (in_array($decoded['other_user_id'], $userLikeUsers) ? 1 : 0);
                    $userPost['UserTextPost']['user_profile_pic'] = $PostUserProfile;

                    unset($userPost['UserTextPost']['id']);
                    unset($userPost['User']);
                    $userPos[$key] = $userPost['UserTextPost'];
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['response_data'] = $userPos;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else if ($this->request->isGet()) {

            if ((isset($this->request->query['offset'])) && (!empty($this->request->query['offset']))) {
                $offset = $this->request->query['offset'];
            }

            if ((isset($this->request->query['limit'])) && (!empty($this->request->query['limit']))) {
                $length_new = $this->request->query['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            $userPos = array();
            $userPosts = $this->UserTextPost->find('all', array(
                'conditions' => array(
                    'UserTextPost.status =' => '1',
                ),
                'order' => array('UserTextPost.modified' => 'desc', 'UserTextPost.created' => 'desc'),
                /* 'order' => array('UserTextPost.id' => 'asc'), */
                'page' => $offset,
                'limit' => $length_new,
            ));
            foreach ($userPosts as $key => $userPost) {
                $PostUserProfile = "";
                $categoryPostUsers = $this->UserImage->find('first', array(
                    'conditions' => array(
                        'UserImage.user_id =' => $userPost['UserTextPost']['user_id'],
                        'UserImage.status =' => '1',
                        'UserImage.image_type =' => '1',
                    /* 'UserImage.category_idx' => $userPost['UserTextPost']['category_idx'] */
                    ),
                    'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
                ));
                if (!empty($categoryPostUsers['UserImage']['image'])) {
                    $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $categoryPostUsers['UserImage']['image'];
                }
                $userPost['UserTextPost']['username'] = $userPost['User']['username'];
                $userPost['UserTextPost']['full_name'] = $userPost['User']['full_name'];
                $userPost['UserTextPost']['post_id'] = $userPost['UserTextPost']['id'];
                $userPost['UserTextPost']['lat'] = empty($userPost['UserTextPost']['lat']) ? "" : $userPost['UserTextPost']['lat'];
                $userPost['UserTextPost']['lon'] = empty($userPost['UserTextPost']['lon']) ? "" : $userPost['UserTextPost']['lon'];
                $userPost['UserTextPost']['user_profile_pic'] = $PostUserProfile;

                $userLikeUsers = array();

                if (!empty($userPost['UserPostLike'])) {

                    foreach ($userPost['UserPostLike'] as $userLk) {
                        if ($userLk['status'] == 1) {
                            $userLikeUsers[] = $userLk['user_id'];
                        }
                    }
                }

                /* $userPost['UserTextPost']['post_like'] = ($userPost['UserPostLike'][0]['status'] == 1 ? 1 : 0); */
                $userPost['UserTextPost']['post_like'] = (in_array($this->request->query['other_user_id'], $userLikeUsers) ? 1 : 0);

                unset($userPost['UserTextPost']['id']);
                unset($userPost['User']);
                $userPos[$key] = $userPost['UserTextPost'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $responseData['offset'] = $offset;
            $responseData['response_data'] = $userPos;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_user_text_postNew($test_data = null) {
        $offset = "";
        $length_new = 10;

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserTextPost');
        $this->loadModel('User');
        $this->loadModel('UserImage');
        $this->loadModel('UserLocation');

        if (!empty($decoded) && isset($decoded['post_id']) && isset($decoded['other_user_id'])) {
            if (!empty($decoded['user_id']) || !empty($decoded['post_id'])) {
                $conditions = array();
                $userPos = array();
                if (!empty($decoded['user_id']) && !empty($decoded['post_id'])) {
                    $conditions = array(
                        'UserTextPost.user_id =' => $decoded['user_id'],
                        'UserTextPost.id =' => $decoded['post_id'],
                        'UserTextPost.status =' => '1',
                    );
                } else if (!empty($decoded['user_id'])) {

                    $conditions = array(
                        'UserTextPost.user_id =' => $decoded['user_id'],
                        'UserTextPost.status =' => '1',
                    );
                } else if (!empty($decoded['post_id'])) {

                    $conditions = array(
                        'UserTextPost.id =' => $decoded['post_id'],
                        'UserTextPost.status =' => '1',
                    );
                }

                $userPosts = $this->UserTextPost->find('all', array(
                    'conditions' => $conditions,
                    'order' => array('UserTextPost.modified' => 'desc', 'UserTextPost.created' => 'desc')
                        /* 'order' => array('UserTextPost.id' => 'asc') */
                ));
                foreach ($userPosts as $key => $userPost) {
                    $PostUserProfile = "";
                    $categoryPostUsers = $this->UserImage->find('first', array(
                        'conditions' => array(
                            'UserImage.user_id =' => $userPost['UserTextPost']['user_id'],
                            'UserImage.status =' => '1',
                            'UserImage.image_type =' => '1',
                            'UserImage.category_idx' => $userPost['UserTextPost']['category_idx']
                        ),
                        'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
                    ));
                    if (!empty($categoryPostUsers['UserImage']['image'])) {
                        $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $categoryPostUsers['UserImage']['image'];
                    }
                    $userPost['UserTextPost']['username'] = $userPost['User']['username'];
                    $userPost['UserTextPost']['full_name'] = $userPost['User']['full_name'];
                    $userPost['UserTextPost']['post_id'] = $userPost['UserTextPost']['id'];
                    $userPost['UserTextPost']['lat'] = empty($userPost['UserTextPost']['lat']) ? "" : $userPost['UserTextPost']['lat'];
                    $userPost['UserTextPost']['lon'] = empty($userPost['UserTextPost']['lon']) ? "" : $userPost['UserTextPost']['lon'];
                    $userLikeUsers = array();
                    foreach ($userPost['UserPostLike'] as $userLk) {
                        if ($userLk['status'] == 1) {
                            $userLikeUsers[] = $userLk['user_id'];
                        }
                    }

                    /* $userPost['UserTextPost']['post_like'] = ($userPost['UserPostLike'][0]['status'] == 1 ? 1 : 0); */
                    $userPost['UserTextPost']['post_like'] = (in_array($decoded['other_user_id'], $userLikeUsers) ? 1 : 0);
                    $userPost['UserTextPost']['user_profile_pic'] = $PostUserProfile;

                    unset($userPost['UserTextPost']['id']);
                    unset($userPost['User']);
                    $userPos[$key] = $userPost['UserTextPost'];
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['response_data'] = $userPos;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else if ($this->request->isGet()) {



            if ((isset($this->request->query['offset'])) && (!empty($this->request->query['offset']))) {
                $offset = $this->request->query['offset'];
            }

            if ((isset($this->request->query['limit'])) && (!empty($this->request->query['limit']))) {
                $length_new = $this->request->query['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            if (!isset($this->request->query['offset']) || $this->request->query['offset'] == 1 || $this->request->query['offset'] == 0 || $this->request->query['offset'] < 0) {
                $page = 1;
                $start_from = ($page - 1) * $length_new;
            } else {
                $start_from = ($this->request->query['offset'] - 1) * $length_new;
            }

            $current_lat = $this->request->query['current_lat'];
            $current_lon = $this->request->query['current_lon'];

            $datas = $this->UserLocation->query("select * from ((SELECT *,
			( 6371 * ACOS( COS( RADIANS($current_lat) )
			* COS( RADIANS( lat ) )
			* COS( RADIANS( lon ) - RADIANS($current_lon) )
			+ SIN( RADIANS($current_lat) )
			* SIN( RADIANS( lat ) ) ) ) AS distance FROM user_locations) as UserLocation) where UserLocation.distance <= 6 * 1.609  ORDER BY id DESC");
            /*             * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_locations) as UserLocation) where UserLocation.distance <= 6 * 1.609  ORDER BY id DESC limit $start_from, $length_new"); */

            $datanew = array();
            foreach ($datas as $data) {
                array_push($datanew, $data['UserLocation']['user_id']);
            }

            $userPos = array();

            $userPosts = $this->UserTextPost->query("select * from ((SELECT *,
			( 6371 * ACOS( COS( RADIANS($current_lat) )
			* COS( RADIANS( lat ) )
			* COS( RADIANS( lon ) - RADIANS($current_lon) )
			+ SIN( RADIANS($current_lat) )
			* SIN( RADIANS( lat ) ) ) ) AS distance FROM user_text_posts) as UserTextPost) where UserTextPost.distance <= 5 * 1.609 and UserTextPost.status = '1' ORDER BY id DESC limit $start_from, $length_new");
            /*             * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_text_posts) as UserTextPost) where UserTextPost.distance <= 6 * 1.609  ORDER BY id DESC"); */


            foreach ($userPosts as $key => $userPost) {
                $PostUserProfile = "";
                $categoryPostUsers = $this->UserImage->find('first', array(
                    'conditions' => array(
                        'UserImage.user_id =' => $userPost['UserTextPost']['user_id'],
                        'UserImage.status =' => '1',
                        'UserImage.image_type =' => '1',
                    /* 'UserImage.category_idx' => $userPost['UserTextPost']['category_idx'] */
                    ),
                    'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
                ));
                if (!empty($categoryPostUsers['UserImage']['image'])) {
                    $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $categoryPostUsers['UserImage']['image'];
                }
                $userDs = $this->User->find('first', array(
                    'conditions' => array(
                        'User.id =' => $userPost['UserTextPost']['user_id'],
                    ),
                    'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
                ));

                $userPost['UserTextPost']['username'] = $userDs['User']['username'];
                $userPost['UserTextPost']['full_name'] = $userDs['User']['full_name'];
                $userPost['UserTextPost']['post_id'] = $userPost['UserTextPost']['id'];
                $userPost['UserTextPost']['lat'] = empty($userPost['UserTextPost']['lat']) ? "" : $userPost['UserTextPost']['lat'];
                $userPost['UserTextPost']['lon'] = empty($userPost['UserTextPost']['lon']) ? "" : $userPost['UserTextPost']['lon'];
                $userPost['UserTextPost']['user_profile_pic'] = $PostUserProfile;

                $userLikeUsers = array();

                if (!empty($userPost['UserPostLike'])) {

                    foreach ($userPost['UserPostLike'] as $userLk) {
                        if ($userLk['status'] == 1) {
                            $userLikeUsers[] = $userLk['user_id'];
                        }
                    }
                }

                /* $userPost['UserTextPost']['post_like'] = ($userPost['UserPostLike'][0]['status'] == 1 ? 1 : 0); */
                $userPost['UserTextPost']['post_like'] = (in_array($this->request->query['other_user_id'], $userLikeUsers) ? 1 : 0);

                unset($userPost['UserTextPost']['id']);
                unset($userPost['User']);
                $userPos[$key] = $userPost['UserTextPost'];
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $responseData['offset'] = $offset;
            $responseData['response_data'] = $userPos;
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_user_post_likeNew($test_data = null) {
        date_default_timezone_set('Asia/Calcutta');

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserTextPost');
        $this->loadModel('UserPostLike');

        if (!empty($decoded) && isset($decoded['post_id']) && isset($decoded['other_user_id'])) {
            $userPostLk = $this->UserPostLike->find('first', array(
                'conditions' => array(
                    'UserPostLike.post_id =' => $decoded['post_id'],
                    'UserPostLike.user_id' => $decoded['other_user_id']
                ),
                'order' => array('UserPostLike.modified' => 'desc', 'UserPostLike.created' => 'desc')
            ));
            $userPostLkdata = array();
            if (empty($userPostLk)) {

                $userPostLkdata['UserPostLike']['user_id'] = $decoded['other_user_id'];
                $userPostLkdata['UserPostLike']['post_id'] = $decoded['post_id'];
                $this->UserPostLike->save($userPostLkdata);
            } else {
                $userPostLkdata['UserPostLike']['id'] = $userPostLk['UserPostLike']['id'];
                $userPostLkdata['UserPostLike']['user_id'] = $decoded['other_user_id'];
                $userPostLkdata['UserPostLike']['post_id'] = $decoded['post_id'];
                $userPostLkdata['UserPostLike']['status'] = ($userPostLk['UserPostLike']['status'] == 1 ? 0 : 1);
                $this->UserPostLike->save($userPostLkdata);
            }

            $userPostLkNew = $this->UserPostLike->find('first', array(
                'conditions' => array(
                    'UserPostLike.post_id =' => $decoded['post_id'],
                    'UserPostLike.user_id' => $decoded['other_user_id']
                ),
                'order' => array('UserPostLike.modified' => 'desc', 'UserPostLike.created' => 'desc')
            ));

            $userPostLkNew['UserPostLike']['post_like'] = $userPostLkNew['UserPostLike']['status'];

            unset($userPostLkNew['UserPostLike']['id']);
            unset($userPostLkNew['UserPostLike']['status']);
            unset($userPostLkNew['UserPostLike']['created']);
            unset($userPostLkNew['UserPostLike']['modified']);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $responseData['response_data'] = $userPostLkNew['UserPostLike'];
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_user_post_like($test_data = null) {
        date_default_timezone_set('Asia/Calcutta');

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('User');
        $this->loadModel('UserTextPost');
        $this->loadModel('UserPostLike');
        $this->loadModel('UserNotification');

        if (!empty($decoded) && isset($decoded['post_id']) && isset($decoded['other_user_id'])) {

            $conditionsns = array(
                'UserTextPost.id =' => $decoded['post_id'],
                'UserTextPost.status =' => '1',
            );

            $userTextps = $this->UserTextPost->find('first', array(
                'conditions' => $conditionsns,
                'order' => array('UserTextPost.modified' => 'desc', 'UserTextPost.created' => 'desc')
            ));

            $userPostns = $this->User->find('first', array(
                'conditions' => array(
                    'User.id =' => $userTextps['User']['id'],
                ),
                'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
            ));

            $userPostLk = $this->UserPostLike->find('first', array(
                'conditions' => array(
                    'UserPostLike.post_id =' => $decoded['post_id'],
                    'UserPostLike.user_id' => $decoded['other_user_id']
                ),
                'order' => array('UserPostLike.modified' => 'desc', 'UserPostLike.created' => 'desc')
            ));
            $userPostLkdata = array();
            $userNotificationData = array();
            if (empty($userPostLk)) {

                $userPostLkdata['UserPostLike']['user_id'] = $decoded['other_user_id'];
                $userPostLkdata['UserPostLike']['post_id'] = $decoded['post_id'];
                $this->UserPostLike->save($userPostLkdata);

                $userPostlk = $this->User->find('first', array(
                    'conditions' => array(
                        'User.id =' => $decoded['other_user_id'],
                    ),
                    'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
                ));
                if ($decoded['other_user_id'] === $userTextps['User']['id']) {
                    
                } else {
                    $message = $userPostlk['User']['username'] . " liked your post.";

                    $userNotificationData['UserNotification']['fromid'] = $userPostlk['User']['id'];
                    $userNotificationData['UserNotification']['toid'] = $userPostns['User']['id'];
                    $userNotificationData['UserNotification']['image_id'] = 0;
                    $userNotificationData['UserNotification']['image_url'] = '';
                    $userNotificationData['UserNotification']['message'] = $userPostlk['User']['username'] . " liked your post.";
                    $userNotificationData['UserNotification']['type'] = 104;
                    $this->UserNotification->save($userNotificationData);

                    if ($userPostns['User']['device_type'] == 1) {
                        $this->send_notification_for_android($userPostns['User']['device_id'], $message, 104);
                    } else if ($userPostns['User']['device_type'] == 2) {
                        $this->send_notification_for_iphone_test($userPostns['User']['device_id'], $message, 104);
                    } else {
                        $this->send_notification_for_iphone_test($userPostns['User']['device_id'], $message, 104);
                    }
                }
            } else {
                $userPostLkdata['UserPostLike']['id'] = $userPostLk['UserPostLike']['id'];
                $userPostLkdata['UserPostLike']['user_id'] = $decoded['other_user_id'];
                $userPostLkdata['UserPostLike']['post_id'] = $decoded['post_id'];
                $userPostLkdata['UserPostLike']['status'] = ($userPostLk['UserPostLike']['status'] == 1 ? 0 : 1);
                $this->UserPostLike->save($userPostLkdata);

                if ($userPostLkdata['UserPostLike']['status'] == 1) {

                    $userPostlk = $this->User->find('first', array(
                        'conditions' => array(
                            'User.id =' => $decoded['other_user_id'],
                        ),
                        'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
                    ));
                    if ($decoded['other_user_id'] === $userTextps['User']['id']) {
                        
                    } else {
                        $message = $userPostlk['User']['username'] . " liked your post.";

                        $userNotificationData['UserNotification']['fromid'] = $userPostlk['User']['id'];
                        $userNotificationData['UserNotification']['toid'] = $userPostns['User']['id'];
                        $userNotificationData['UserNotification']['image_id'] = 0;
                        $userNotificationData['UserNotification']['image_url'] = '';
                        $userNotificationData['UserNotification']['message'] = $userPostlk['User']['username'] . " liked your post.";
                        $userNotificationData['UserNotification']['type'] = 104;
                        $this->UserNotification->save($userNotificationData);

                        if ($userPostns['User']['device_type'] == 1) {
                            $this->send_notification_for_android($userPostns['User']['device_id'], $message, 104);
                        } elseif ($userPostns['User']['device_type'] == 2) {
                            $this->send_notification_for_iphone_test($userPostns['User']['device_id'], $message, 104);
                        } else {
                            $this->send_notification_for_iphone_test($userPostns['User']['device_id'], $message, 104);
                        }
                    }
                }
            }

            $userPostLkNew = $this->UserPostLike->find('first', array(
                'conditions' => array(
                    'UserPostLike.post_id =' => $decoded['post_id'],
                    'UserPostLike.user_id' => $decoded['other_user_id']
                ),
                'order' => array('UserPostLike.modified' => 'desc', 'UserPostLike.created' => 'desc')
            ));

            $userPostLkNew['UserPostLike']['post_like'] = $userPostLkNew['UserPostLike']['status'];

            unset($userPostLkNew['UserPostLike']['id']);
            unset($userPostLkNew['UserPostLike']['status']);
            unset($userPostLkNew['UserPostLike']['created']);
            unset($userPostLkNew['UserPostLike']['modified']);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = "success";
            $responseData['response_data'] = $userPostLkNew['UserPostLike'];
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_user_post_commentNew($test_data = null) {
        date_default_timezone_set('Asia/Calcutta');

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserPostComment');

        if (!empty($decoded)) {
            $postComment = array();

            $fg = 0;
            if (isset($decoded['post_comment_id'])) {
                $postComment['UserPostComment']['id'] = $decoded['post_comment_id'];
                if (isset($decoded['status'])) {
                    $postComment['UserPostComment']['status'] = $decoded['status'];
                }
                $fg = 1;
            }

            $postComment['UserPostComment']['user_id'] = $decoded['user_id'];
            $postComment['UserPostComment']['post_id'] = $decoded['post_id'];
            $postComment['UserPostComment']['category_idx'] = $decoded['category_idx'];
            $postComment['UserPostComment']['comment'] = $decoded['comment'];

            if ($this->UserPostComment->save($postComment)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'Post comment ' . ($fg == 1 ? 'update' : 'add') . ' success';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = 'Post comment not ' . ($fg == 1 ? 'updated' : 'added');
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_user_post_comment($test_data = null) {
        date_default_timezone_set('Asia/Calcutta');

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();
        $this->loadModel('User');
        $this->loadModel('UserTextPost');
        $this->loadModel('UserPostComment');

        if (!empty($decoded)) {
            $postComment = array();

            $fg = 0;
            if (isset($decoded['post_comment_id']) && !empty($decoded['post_comment_id'])) {
                $postComment['UserPostComment']['id'] = $decoded['post_comment_id'];
                if (isset($decoded['status'])) {
                    $postComment['UserPostComment']['status'] = $decoded['status'];
                }
                $fg = 1;
            }

            $postComment['UserPostComment']['user_id'] = $decoded['user_id'];
            $postComment['UserPostComment']['post_id'] = $decoded['post_id'];
            $postComment['UserPostComment']['category_idx'] = $decoded['category_idx'];
            $postComment['UserPostComment']['comment'] = $decoded['comment'];

            $conditionsns = array(
                'UserTextPost.id =' => $decoded['post_id'],
                'UserTextPost.status =' => '1',
            );

            $userTextps = $this->UserTextPost->find('first', array(
                'conditions' => $conditionsns,
                'order' => array('UserTextPost.modified' => 'desc', 'UserTextPost.created' => 'desc')
            ));

            $userPostns = $this->User->find('first', array(
                'conditions' => array(
                    'User.id =' => $userTextps['User']['id'],
                ),
                'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
            ));

            $userNotificationData = array();
            if ($this->UserPostComment->save($postComment)) {

                if ($fg == 0) {

                    $userPostlk = $this->User->find('first', array(
                        'conditions' => array(
                            'User.id =' => $decoded['user_id'],
                        ),
                        'order' => array('User.modified' => 'desc', 'User.created' => 'desc')
                    ));
                    if ($decoded['user_id'] == $userTextps['User']['id']) {
                        
                    } else {

                        $message = $userPostlk['User']['username'] . " commented on your post.";

                        $this->loadModel('UserNotification');
                        $userNotificationData['UserNotification']['fromid'] = $userPostlk['User']['id'];
                        $userNotificationData['UserNotification']['toid'] = $userPostns['User']['id'];
                        $userNotificationData['UserNotification']['image_id'] = 0;
                        $userNotificationData['UserNotification']['image_url'] = '';
                        $userNotificationData['UserNotification']['message'] = $message;
                        $userNotificationData['UserNotification']['type'] = 105;
                        $this->UserNotification->save($userNotificationData);

                        if ($userPostns['User']['device_type'] == 1) {
                            $this->send_notification_for_android($userPostns['User']['device_id'], $message, 105);
                        } else if ($userPostns['User']['device_type'] == 2) {
                            $this->send_notification_for_iphone_test($userPostns['User']['device_id'], $message, 105);
                        } else {
                            $this->send_notification_for_iphone_test($userPostns['User']['device_id'], $message, 105);
                        }
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = 'Post comment ' . ($fg == 1 ? 'update' : 'add') . ' success';

                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = 'Post comment not ' . ($fg == 1 ? 'updated' : 'added');
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_user_post_comment($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $offset = "";
        $length_new = 10;

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserPostComment');
        $this->loadModel('User');
        $this->loadModel('UserImage');
        if (!empty($decoded) && isset($decoded['post_id'])) {
            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                $length_new = $decoded['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            $postCt = array();
            if (!empty($decoded['post_id'])) {

                $this->UserPostComment->bindModel(array('belongsTo' => array(
                        'User' => array(
                            'className' => 'User',
                            'foreignKey' => 'user_id',
                            'fields' => array('id', 'username', 'full_name')
                        )
                    ),));
                $this->User->bindModel(array('hasMany' => array(
                        'UserImage' => array(
                            'className' => 'UserImage',
                            'foreignKey' => 'user_id',
                            'dependent' => true,
                            'fields' => array('id', 'image', 'category_idx'),
                            'conditions' => array('UserImage.image_type' => '1'),
                            'order' => array('UserImage.created DESC')
                        )
                )));

                $postComments = $this->UserPostComment->find('all', array(
                    'conditions' => array(
                        'UserPostComment.status =' => '1',
                        'UserPostComment.post_id' => $decoded['post_id']
                    ),
                    'order' => array('UserPostComment.modified' => 'desc', 'UserPostComment.created' => 'desc'),
                    /* 'order' => array('UserPostComment.id' => 'asc'), */
                    'page' => $offset,
                    'limit' => $length_new,
                    'recursive' => 2
                ));
                $PostUserProfile = '';
                foreach ($postComments as $key => $postComment) {
                    $postComment['UserPostComment']['post_comment_id'] = $postComment['UserPostComment']['id'];
                    unset($postComment['UserPostComment']['id']);
                    $postCt[$key] = $postComment['UserPostComment'];
                    $postCt[$key]['username'] = $postComment['User']['username'];
                    $postCt[$key]['full_name'] = $postComment['User']['full_name'];
                    $postCt[$key]['full_name'] = $postComment['User']['full_name'];
                    if (!empty($postComment['User']['UserImage'][0]['image'])) {
                        $PostUserProfile = 'https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/' . $postComment['User']['UserImage'][0]['image'];
                    }
                    $postCt[$key]['user_profile_pic'] = $PostUserProfile;
                }
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['offset'] = $offset;
                $responseData['response_data'] = $postCt;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function delete_user_text_post($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserTextPost');
        $this->loadModel('UserPostLike');
        $this->loadModel('UserPostComment');

        if (!empty($decoded) && isset($decoded['post_id']) && isset($decoded['action_id'])) {
            $userPostData = $this->UserTextPost->find('first', array('conditions' => array('UserTextPost.id' => $decoded['post_id'])));

            if (!empty($userPostData)) {

                $rs = 0;
                if ($decoded['action_id'] == 0) {
                    $userPost['UserTextPost']['id'] = $decoded['post_id'];
                    $userPost['UserTextPost']['status'] = 0;
                    $rs = $this->UserTextPost->save($userPost);
                } else {
                    $rs = $this->UserTextPost->deleteAll(array('UserTextPost.id' => $decoded['post_id']), false);

                    $newrs2 = $this->UserPostComment->find('first', array('fields' => array('post_id'), 'conditions' => array('UserPostComment.post_id' => $decoded['post_id'])));
                    if (!empty($newrs2)) {
                        $this->UserPostComment->deleteAll(array('UserPostComment.post_id' => $decoded['post_id']), true);
                    }
                    $newrs3 = $this->UserPostLike->find('first', array('fields' => array('user_id'), 'conditions' => array('UserPostLike.post_id' => $decoded['post_id'])));
                    if (!empty($newrs3)) {
                        $this->UserPostLike->deleteAll(array('UserPostLike.post_id' => $decoded['post_id']), true);
                    }
                    /* $this->UserPostComment->deleteAll(array('UserPostComment.post_id' => $decoded['post_id']), true);
                      $this->UserPostLike->deleteAll(array('UserPostLike.post_id' => $decoded['post_id']), true); */
                }
                if ($rs) {

                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                } else {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'error in deletion';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                }
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "No data found";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found. Parameter not found.";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function delete_user_post_comment($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserTextPost');
        $this->loadModel('UserPostLike');
        $this->loadModel('UserPostComment');

        if (!empty($decoded) && isset($decoded['post_comment_id']) && isset($decoded['action_id'])) {
            $userCommentData = $this->UserPostComment->find('first', array('conditions' => array('UserPostComment.id' => $decoded['post_comment_id'])));

            if (!empty($userCommentData)) {

                $rs = 0;
                if ($decoded['action_id'] == 0) {
                    $userPost['UserPostComment']['id'] = $decoded['post_comment_id'];
                    $userPost['UserPostComment']['status'] = 0;
                    $rs = $this->UserPostComment->save($userPost);
                } else {
                    $rs = $this->UserPostComment->deleteAll(array('UserPostComment.id' => $decoded['post_comment_id']), true);
                }
                if ($rs) {

                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                } else {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'error in deletion';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                }
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "No data found.";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found. Parameter not found.";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function user_set_post_report($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('User');
        $this->loadModel('UserTextPost');
        $this->loadModel('UserImage');
        $this->loadModel('UserPostLike');
        $this->loadModel('UserPostComment');
        $this->loadModel('UserPostReport');

        /* $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'order' => array('UserImage.id DESC'),))), false);
          $this->UserImage->bindModel(array('belongsTo' => array('User' => array('className' => 'User', 'foreignKey' => 'user_id','fields' => array('id', 'full_name', 'username')))), false); */

        if (!empty($decoded) && isset($decoded['post_id']) && isset($decoded['other_user_id']) && isset($decoded['user_id'])) {

            $post_type = (isset($decoded['post_type']) && !empty($decoded['post_type']) && ($decoded['post_type'] == 'moment')) ? 'moment' : 'text_post';

            if ($post_type == 'moment') {
                $textPostData = $this->UserImage->find(
                        'first',
                        array(
                            'conditions' => array('UserImage.id' => $decoded['post_id']),
                            'recursive' => 1
                        )
                );
                /* https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/  */
            } else {
                $textPostData = $this->UserTextPost->find('first', array('conditions' => array('UserTextPost.id' => $decoded['post_id'])));
            }
            if (!empty($textPostData)) {

                $html = '';
                $postReport['UserPostReport']['user_id'] = $decoded['user_id'];
                $postReport['UserPostReport']['post_id'] = $decoded['post_id'];
                $postReport['UserPostReport']['post_type'] = $post_type;
                $postReport['UserPostReport']['other_user_id'] = $decoded['other_user_id'];

                $reportData = $this->UserPostReport->save($postReport);

                $Email = new CakeEmail();
                /* $from = Configure::read('App.AdminMail'); */
                $Email->config('gmail1');
                $Email->emailFormat('html');
                $Email->from(array('signup@mapchatapp.com' => 'Mapchat Team'));
                /* $toEmail = 'iblinfotech.android@gmail.com'; */
                $toEmail = 'help@mapbuzz.com';
                /* $toEmail = 'paras.chodavadiya@gmail.com'; */
                $Email->to($toEmail);

                if ($post_type == 'moment') {
                    $otherData = $this->User->find('first', array('conditions' => array('User.id' => $decoded['other_user_id'])));
                    $psData = $this->User->find('first', array('conditions' => array('User.id' => $textPostData['UserImage']['user_id'])));
                    $Email->subject('User Report Post : Moment Post');
                    $html .= '<!DOCTYPE html><html><body>';
                    $html .= '<div style="background-color:lightblue">';
                    $html .= '<h3>User ' . $otherData['User']['full_name'] . ' reported user ' . $textPostData['User']['full_name'] . ' moment </h3>';
                    $html .= '</div><div><table border="1"><tr><th colspan="2">User Reported</th></tr>';
                    $html .= '<tr><td>Post ID:</td><td>' . $textPostData['UserImage']['id'] . '</td></tr>';
                    $html .= '<tr><td>Post Title:</td><td>User Monent Post</td></tr>';
                    $html .= '<tr><td>Post Content:</td><td><a href="https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/' . $textPostData['UserImage']['image'] . '">Post Image</a></td></tr>';
                    $html .= '<tr><td>Posted UserId:</td><td>' . $textPostData['UserImage']['user_id'] . '</td></tr>';
                    $html .= '<tr><td>Posted User:</td><td>' . $psData['User']['full_name'] . '</td></tr>';
                    $html .= '<tr><td>Reported UserId:</td><td>' . $otherData['User']['id'] . '</td></tr>';
                    $html .= '<tr><td>Reported User:</td><td>' . $otherData['User']['full_name'] . '</td></tr>';
                    $html .= '</table></div><p>Thank you.</p></body></html>';
                } else {
                    $otherData = $this->UserTextPost->find('first', array('conditions' => array('User.id' => $decoded['other_user_id'])));
                    $Email->subject('User Report Post : ' . $textPostData['UserTextPost']['title']);
                    $html .= '<!DOCTYPE html><html><body>';
                    $html .= '<div style="background-color:lightblue">';
                    $html .= '<h3>User ' . $otherData['User']['full_name'] . ' reported user ' . $textPostData['User']['full_name'] . ' post ' . $textPostData['UserTextPost']['title'] . ' </h3>';
                    $html .= '</div><div><table border="1"><tr><th colspan="2">User Reported</th></tr>';
                    $html .= '<tr><td>Post ID:</td><td>' . $textPostData['UserTextPost']['id'] . '</td></tr>';
                    $html .= '<tr><td>Post Title:</td><td>' . $textPostData['UserTextPost']['title'] . '</td></tr>';
                    $html .= '<tr><td>Post Content:</td><td>' . $textPostData['UserTextPost']['content'] . '</td></tr>';
                    $html .= '<tr><td>Posted UserId:</td><td>' . $textPostData['UserTextPost']['user_id'] . '</td></tr>';
                    $html .= '<tr><td>Posted User:</td><td>' . $textPostData['User']['full_name'] . '</td></tr>';
                    $html .= '<tr><td>Reported UserId:</td><td>' . $otherData['User']['id'] . '</td></tr>';
                    $html .= '<tr><td>Reported User:</td><td>' . $otherData['User']['full_name'] . '</td></tr>';
                    $html .= '</table></div><p>Thank you.</p></body></html>';
                }


                if ($Email->send($html)) {
                    $rsDate['UserPostReport']['id'] = $reportData['UserPostReport']['id'];
                    $rsDate['UserPostReport']['status'] = '1';
                    $this->UserPostReport->save($rsDate);

                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'Unsuccessfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "No data found.";
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found. Parameter not found.";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function user_get_post_report($test_data = null) {
        $offset = "";
        $length_new = 10;

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);
        $responseData = array();

        $this->loadModel('UserPostReport');
        $this->loadModel('UserTextPost');
        $this->loadModel('User');

        if ($this->request->isGet()) {
            if ((isset($this->request->query['offset'])) && (!empty($this->request->query['offset']))) {
                $offset = $this->request->query['offset'];
            }

            if ((isset($this->request->query['limit'])) && (!empty($this->request->query['limit']))) {
                $length_new = $this->request->query['limit'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            $this->User->bindModel(array('hasOne' => array(
                    'UserPostReport' => array(
                        'className' => 'UserPostReport',
                        'foreignKey' => 'other_user_id',
                        'dependent' => true
                    ),
            )));

            $this->UserPostReport->bindModel(array('belongsTo' => array(
                    'UserTextPost' => array(
                        'className' => 'UserTextPost',
                        'foreignKey' => 'post_id'
                    ),
                    'OtherUser' => array(
                        'className' => 'User',
                        'foreignKey' => 'other_user_id',
                        'fields' => array('id', 'username', 'full_name'),
                        'dependent' => true
                    ),
                    'User' => array(
                        'className' => 'User',
                        'foreignKey' => 'user_id',
                        'fields' => array('id', 'username', 'full_name'),
                        'dependent' => true
                    )
                ),));
            $conditions = array();
            if ((isset($this->request->query['other_user_id'])) && (!empty($this->request->query['other_user_id']))) {
                array_push($conditions, array(
                    'UserPostReport.other_user_id =' => $this->request->query['other_user_id'],
                ));
            }
            if ((isset($this->request->query['user_id'])) && (!empty($this->request->query['user_id']))) {
                array_push($conditions, array(
                    'UserPostReport.user_id =' => $this->request->query['user_id'],
                ));
            }
            if ((isset($this->request->query['report_id'])) && (!empty($this->request->query['report_id']))) {
                array_push($conditions, array(
                    'UserPostReport.id =' => $this->request->query['report_id'],
                ));
            }
            if ((isset($this->request->query['post_id'])) && (!empty($this->request->query['post_id']))) {
                array_push($conditions, array(
                    'UserPostReport.post_id =' => $this->request->query['post_id'],
                ));
            }
            if ((isset($this->request->query['status'])) && (!empty($this->request->query['status']))) {
                array_push($conditions, array(
                    'UserPostReport.status =' => $this->request->query['status'],
                ));
            }

            $userPosts = $this->UserPostReport->find('all', array(
                'conditions' => $conditions,
                'recursive' => 1,
                /* 'order' => array('UserPostReport.modified' => 'desc', 'UserPostReport.created' => 'desc'), */
                'order' => array('UserPostReport.id' => 'desc'),
                'page' => $offset,
                'limit' => $length_new,
            ));
            if (!empty($userPosts)) {
                $userRos = array();
                foreach ($userPosts as $key => $userPost) {
                    $userRos[$key]['reported_id'] = $userPost['UserPostReport']['id'];
                    $userRos[$key]['reported_user_id'] = $userPost['OtherUser']['id'];
                    $userRos[$key]['reported_username'] = $userPost['OtherUser']['username'];
                    $userRos[$key]['reported_full_name'] = $userPost['OtherUser']['full_name'];
                    $userRos[$key]['post_id'] = $userPost['UserTextPost']['id'];
                    $userRos[$key]['post_title'] = $userPost['UserTextPost']['title'];
                    $userRos[$key]['post_content'] = $userPost['UserTextPost']['content'];
                    $userRos[$key]['post_user_id'] = $userPost['UserTextPost']['user_id'];
                    $userRos[$key]['post_username'] = $userPost['User']['username'];
                    $userRos[$key]['status'] = $userPost['UserPostReport']['status'];
                    $userRos[$key]['created'] = $userPost['UserPostReport']['created'];
                    $userRos[$key]['modified'] = $userPost['UserPostReport']['modified'];
                }


                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['offset'] = $offset;
                $responseData['response_data'] = $userRos;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 404;
                $responseData['message'] = "No data found.";
                $responseData['offset'] = $offset;
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found. Parameter not found.";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    /* old */

    public function map_feeds_discoverNs($test_data = null) {
        $data_n = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data_n = $testData;
        }

        $offset = "";
        $length_new = 40;

        $decoded = json_decode($data_n, true);
        $responseData = array();

        $current_lat = $decoded['current_lat'];
        $current_lon = $decoded['current_lon'];
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];

        $this->loadModel("User");
        $this->loadModel('UserImage');
        $this->loadModel("UserImageLike");
        $this->loadModel("Message");

        if (!empty($decoded)) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                $length_new = $decoded['limit'] ? $decoded['limit'] : '30';
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            if (isset($decoded['offset'])) {
                $page = $decoded['offset'];
            } else {
                $page = 1;
            };
            $start_from = ($page - 1) * $length_new;

            $user_id = $decoded['user_id'];

            if (!isset($decoded['offset']) || $decoded['offset'] == 1 || $decoded['offset'] == 0 || $decoded['offset'] < 0) {
                $page = 1;
                $start_from = ($page - 1) * $length_new;
                $length_new = 30;
            } else {
                $page = $decoded['offset'];
                /* $length_new = $decoded['limit'] ? $decoded['limit'] : '30'; */
                $length_new = $decoded['limit'] ? $decoded['limit'] : '30';
                if ($decoded['offset'] == 2) {
                    $start_from = 31;
                } else {
                    $start_from = 31 + ($length_new * ($page - 2));
                }
            };

            $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
            $user_data = $this->User->find('all', array('fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*')));

            $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
            $totalRec = 0;

            $userArr = array();
            foreach ($user_data as $userKey => $userVal) {
                $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                if (!empty($userVal['User']['full_name'])) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                }

                $userArr[$userVal['User']['id']]['profile_image'] = "";

                if (!empty($userVal['UserImage']['image'])) {
                    $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                }
            }

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');
            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }


            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                $fr_rs = array();
                foreach ($res as $re) {

                    $fr_rs[] = $re['UserContact']['contact_user_id'];
                }

                if (!empty($fr_rs)) {

                    $fr_where = " AND UserImage.user_id in(" . implode(",", $fr_rs) . ")";
                    $fr_where1 = " AND User.id in(" . implode(",", $fr_rs) . ")";
                } else {
                    $fr_where = " AND UserImage.user_id in(0)";
                    $fr_where1 = " AND User.id in(0)";
                }
                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 " . $fr_where . " ORDER BY id DESC limit $start_from, $length_new");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
				from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
				* COS( RADIANS( user_locations.lat ) )
				* COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
				+ SIN( RADIANS($current_lat) )
				* SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
				where User.distance <= 5 * 1.609 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) " . $fr_where1 . " ORDER BY User.modified DESC limit $start_from, $length_new");
                }
            } else {

                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 ORDER BY id DESC limit $start_from, $length_new");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
				from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
				* COS( RADIANS( user_locations.lat ) )
				* COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
				+ SIN( RADIANS($current_lat) )
				* SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
				where User.distance <= 5 * 1.609 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC limit $start_from, $length_new");
                    /*   where User.distance <= 7 * 1.609 ORDER BY id DESC limit 1, 300"); */
                    /* ORDER BY id DESC */
                }
            }

            $data = array();
            if (!empty($UserImage)) {

                foreach ($UserImage as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['UserImage']['id'];
                    $list['updated'] = $value['UserImage']['updated'];
                    $list['userId'] = $value['UserImage']['userId'];
                    $list['value'] = $value['UserImage']['value'];
                    $list['feed_likes'] = $value['UserImage']['feed_likes'];
                    $list['lat'] = $value['UserImage']['lat'];
                    $list['lon'] = $value['UserImage']['lon'];
                    $list['category_idx'] = $value['UserImage']['category_idx'];
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }
            if (!empty($User)) {

                foreach ($User as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['User']['id'];
                    $list['updated'] = $value['User']['updated'];
                    $list['userId'] = $value['User']['userId'];
                    $list['value'] = $value['User']['value'];
                    $list['feed_likes'] = $value[0]['feed_likes'];
                    $list['lat'] = $value['User']['lat'];
                    $list['lon'] = $value['User']['lon'];
                    $list['category_idx'] = $value[0]['category_idx'];
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }

            $feedArr = array();
            $count = 0;
            $res = [];

            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        $conditions3 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_map' => 1);

                        /* $conditions3 = array('UserLocation.user_id' => $feedArr[$count]['user_id']); */
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions3
                                )
                        );
                        if ($Location[0]['UserLocation']['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions4
                                    )
                            );
                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions1 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.friend_see_on_map' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions1
                                )
                        );
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        $conditions6 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_profile' => 1);
                        $Location = $this->UserLocation->find(
                                'all',
                                array(
                                    'conditions' => $conditions6
                                )
                        );

                        if ($Location[0]['UserLocation']['hide_from_profile'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find(
                                    'all',
                                    array(
                                        'conditions' => $conditions
                                    )
                            );

                            if (empty($res) && $user_id != $feedArr[$count]['user_id'] && $feedArr[$count]['type'] == 'User') {
                                unset($feedArr[$count]);
                            }
                        }

                        if ($Location[0]['UserLocation']['hide_from_profile'] == 1 && $feedArr[$count]['type'] == "User" && $user_id != $feedArr[$count]['user_id']) {
                            unset($feedArr[$count]);
                        }

                        if ((basename($feedArr[$count]['user_profile_pic']) == basename($feedArr[$count]['shared_pic']))) {
                            unset($feedArr[$count]);
                        }
                        if ($feedArr[$count]['user_profile_pic'] == "") {
                            unset($feedArr[$count]);
                        }
                        $count++;
                    }
                }
            }
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array_values($feedArr);
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    /* new */

    public function map_feeds_discoverNew_actual($test_data = null) {
        $data_n = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data_n = $testData;
        }

        $offset = 0;
        $image_length = 30;
        $user_length = 100;

        $decoded = json_decode($data_n, true);
        $responseData = array();

        $current_lat = $decoded['current_lat']; //'53.45392522';
        $current_lon = $decoded['current_lon']; //'-2.17939499';
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];
        $user_id = $decoded['user_id']; //7852;

        $this->loadModel("User");
        $this->loadModel('UserImage');
        $this->loadModel("UserImageLike");
        $this->loadModel("Message");

        if (!empty($decoded)) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            if (isset($decoded['offset'])) {
                $page = $decoded['offset'];
            } else {
                $page = 1;
            };

            if ($page == 1) {
                $start_from = ($page - 1) * $image_length;
                $user_start_from = ($page - 1) * $user_length;
            } else {
                if ($offset == 2) {
                    $start_from = 31;
                    $user_start_from = 101;
                } else {
                    $start_from = 31 + ($image_length * ($page - 2));
                    $user_start_from = 101 + ($user_length * ($page - 2));
                }
            }

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');
            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                $fr_rs = array();
                foreach ($res as $re) {

                    $fr_rs[] = $re['UserContact']['contact_user_id'];
                }

                if (!empty($fr_rs)) {

                    $fr_where = " AND UserImage.user_id in(" . implode(",", $fr_rs) . ")";
                    $fr_where1 = " AND User.id in(" . implode(",", $fr_rs) . ")";
                } else {
                    $fr_where = " AND UserImage.user_id in(0)";
                    $fr_where1 = " AND User.id in(0)";
                }
                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 " . $fr_where . " ORDER BY id DESC limit $start_from, $image_length");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
				from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
				* COS( RADIANS( user_locations.lat ) )
				* COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
				+ SIN( RADIANS($current_lat) )
				* SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
				where User.distance <= 5 * 1.609 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) " . $fr_where1 . " ORDER BY User.modified DESC limit $user_start_from, $user_length");
                }
            } else {

                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 ORDER BY id DESC limit $start_from, $image_length");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
				from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
				* COS( RADIANS( user_locations.lat ) )
				* COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
				+ SIN( RADIANS($current_lat) )
				* SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
				where User.distance <= 5 * 1.609 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC limit $user_start_from, $user_length");
                }
            }

            /* get total count */
            if ($category_idx == -1) {
                $UserCount = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type
				from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
				* COS( RADIANS( user_locations.lat ) )
				* COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
				+ SIN( RADIANS($current_lat) )
				* SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
				where User.distance <= 5 * 1.609 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC");
            }

            $total_records = count($UserCount);
            $showing = count($User);
            if ($total_records < $showing) {
                $showing = $total_records;
            }
            $data = array();
            if (!empty($UserImage)) {

                foreach ($UserImage as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['UserImage']['id'];
                    $list['updated'] = $value['UserImage']['updated'];
                    $list['userId'] = $value['UserImage']['userId'];
                    $list['value'] = $value['UserImage']['value'];
                    $list['feed_likes'] = $value['UserImage']['feed_likes'];
                    $list['lat'] = $value['UserImage']['lat'];
                    $list['lon'] = $value['UserImage']['lon'];
                    $list['category_idx'] = $value['UserImage']['category_idx'];
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }
            if (!empty($User)) {

                foreach ($User as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['User']['id'];
                    $list['updated'] = $value['User']['updated'];
                    $list['userId'] = $value['User']['userId'];
                    $list['value'] = $value['User']['value'];
                    $list['feed_likes'] = $value[0]['feed_likes'];
                    $list['lat'] = $value['User']['lat'];
                    $list['lon'] = $value['User']['lon'];
                    $list['category_idx'] = $value[0]['category_idx'];
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }

            $feedArr = array();
            $count = 0;
            $res = [];

            $filter_user_array = array(0);
            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {
                        $filter_user_array[] = $FeedVal[0]['userId'];
                    }
                }

                $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
                $user_data = $this->User->find('all', array(
                    'fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*'),
                    'conditions' => array('User.id' => array_unique($filter_user_array))
                ));
                $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));

                $userArr = array();
                foreach ($user_data as $userKey => $userVal) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                    if (!empty($userVal['User']['full_name'])) {
                        $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                    }

                    $userArr[$userVal['User']['id']]['profile_image'] = "";

                    if (!empty($userVal['UserImage']['image'])) {
                        $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                    }
                }

                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        $conditions3 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_map' => 1);
                        $Location = $this->UserLocation->find('all', array('conditions' => $conditions3));
                        if ($Location[0]['UserLocation']['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions4));
                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        $conditions1 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.friend_see_on_map' => 1);
                        $Location = $this->UserLocation->find('all', array('conditions' => $conditions1));
                        if ($Location[0]['UserLocation']['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions));

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }
                        $conditions6 = array('UserLocation.user_id' => $FeedVal[0]['userId'], 'UserLocation.hide_from_profile' => 1);
                        $Location = $this->UserLocation->find('all', array('conditions' => $conditions6));

                        if ($Location[0]['UserLocation']['hide_from_profile'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions));

                            if (empty($res) && $user_id != $feedArr[$count]['user_id'] && $feedArr[$count]['type'] == 'User') {
                                unset($feedArr[$count]);
                            }
                        }

                        if ($Location[0]['UserLocation']['hide_from_profile'] == 1 && $feedArr[$count]['type'] == "User" && $user_id != $feedArr[$count]['user_id']) {
                            unset($feedArr[$count]);
                        }

                        if ((basename($feedArr[$count]['user_profile_pic']) == basename($feedArr[$count]['shared_pic']))) {
                            unset($feedArr[$count]);
                        }
                        if ($feedArr[$count]['user_profile_pic'] == "") {
                            unset($feedArr[$count]);
                        }
                        $count++;
                    }
                }
            }

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array('total_records' => $total_records, 'showing' => $showing, 'items' => array_values($feedArr));
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function get_user_profile($user_id) {
        $this->loadModel("User");
        $this->loadModel('UserImage');
        $userImage = $this->UserImage->find('first', array(
            'conditions' => array(
                'UserImage.image_type =' => 1,
                'UserImage.status =' => 1,
                'UserImage.user_id' => $user_id
            ),
            'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')
        ));
        return (!empty($userImage) ? "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userImage['UserImage']['image'] : "");
    }

    public function old_backup_tarun_map_feeds_discoverNew($test_data = null) {
        $data_n = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data_n = $testData;
        }

        $offset = 0;
        $image_length = 30;
        $user_length = 100;

        $decoded = json_decode($data_n, true);
        $responseData = array();
        if(isset($_POST['user_id'])){
            $decoded=$_POST;
        }
        $current_lat = $decoded['current_lat']; // '53.45392522';
        $current_lon = $decoded['current_lon']; // '-2.17939499';
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];
        $user_id = $decoded['user_id']; //7852;

        $this->loadModel("User");
        $this->loadModel('UserImage');
        $this->loadModel("UserImageLike");
        $this->loadModel("Message");

        if (!empty($decoded)) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            if (isset($decoded['offset'])) {
                $page = $decoded['offset'];
            } else {
                $page = 1;
            };

            if ($page == 1) {
                $start_from = ($page - 1) * $image_length;
                $user_start_from = ($page - 1) * $user_length;
            } else {
                if ($offset == 2) {
                    $start_from = 31;
                    $user_start_from = 101;
                } else {
                    $start_from = 31 + ($image_length * ($page - 2));
                    $user_start_from = 101 + ($user_length * ($page - 2));
                }
            }

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');
            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                $fr_rs = array();
                foreach ($res as $re) {

                    $fr_rs[] = $re['UserContact']['contact_user_id'];
                }

                if (!empty($fr_rs)) {

                    $fr_where = " AND UserImage.user_id in(" . implode(",", $fr_rs) . ")";
                    $fr_where1 = " AND User.id in(" . implode(",", $fr_rs) . ")";
                } else {
                    $fr_where = " AND UserImage.user_id in(0)";
                    $fr_where1 = " AND User.id in(0)";
                }
                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 " . $fr_where . " ORDER BY id DESC limit $start_from, $image_length");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type,
            User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
            from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
            * COS( RADIANS( user_locations.lat ) )
            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
            + SIN( RADIANS($current_lat) )
            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
            where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) " . $fr_where1 . " ORDER BY User.modified DESC limit $user_start_from, $user_length");
                }
            } else {

                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 ORDER BY id DESC limit $start_from, $image_length");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type,
            User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
            from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
            * COS( RADIANS( user_locations.lat ) )
            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
            + SIN( RADIANS($current_lat) )
            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
            where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC limit $user_start_from, $user_length");
                }
            }

            $UserCount = [];
            if ($category_idx == -1) {
                $UserCount = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value,
         'NA' as feed_likes, User.lat, User.lon, User.lat, '-1' as category_idx, 'User' as type,
          User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
          from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, 
          ( 6371 * ACOS( COS( RADIANS($current_lat) ) * COS( RADIANS( user_locations.lat ) )
          * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
          + SIN( RADIANS($current_lat) )
          * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
          where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC");
            }

            $total_records = count($UserCount);
            $showing = count($User);
            if ($total_records < $showing) {
                $showing = $total_records;
            }
            if ($showing < $total_records && ($offset > 1 && ($offset * $user_length) > $total_records)) {
                $showing = $total_records;
            }
            $data = array();
            if (!empty($UserImage)) {

                foreach ($UserImage as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['UserImage']['id'];
                    $list['updated'] = $value['UserImage']['updated'];
                    $list['userId'] = $value['UserImage']['userId'];
                    $list['value'] = $value['UserImage']['value'];
                    $list['feed_likes'] = $value['UserImage']['feed_likes'];
                    $list['lat'] = $value['UserImage']['lat'];
                    $list['lon'] = $value['UserImage']['lon'];
                    $list['category_idx'] = $value['UserImage']['category_idx'];
                    $list['hide_from_map'] = 0;
                    $list['friend_see_on_map'] = 0;
                    $list['hide_from_profile'] = 0;
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }
            if (!empty($User)) {

                foreach ($User as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['User']['id'];
                    $list['updated'] = $value['User']['updated'];
                    $list['userId'] = $value['User']['userId'];
                    $list['value'] = $value['User']['value'];
                    $list['feed_likes'] = $value[0]['feed_likes'];
                    $list['lat'] = $value['User']['lat'];
                    $list['lon'] = $value['User']['lon'];
                    $list['category_idx'] = $value[0]['category_idx'];
                    $list['hide_from_map'] = $value[0]['hide_from_map'];
                    $list['friend_see_on_map'] = $value[0]['friend_see_on_map'];
                    $list['hide_from_profile'] = $value[0]['hide_from_profile'];
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }

            $feedArr = array();
            $count = 0;
            $res = [];

            $filter_user_array = array(0);
            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {
                        $filter_user_array[] = $FeedVal[0]['userId'];
                    }
                }

                $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
                $user_data = $this->User->find('all', array(
                    'fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*'),
                    'conditions' => array('User.id' => array_unique($filter_user_array))
                ));
                $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));

                $userArr = array();
                foreach ($user_data as $userKey => $userVal) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                    if (!empty($userVal['User']['full_name'])) {
                        $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                    }

                    $userArr[$userVal['User']['id']]['profile_image'] = "";

                    if (!empty($userVal['UserImage']['image'])) {
                        $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                    }
                }

                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        if ($FeedVal[0]['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions4));
                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        if ($FeedVal[0]['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions));

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        if ($FeedVal[0]['hide_from_profile'] == 1 && $feedArr[$count]['type'] == "User" && $user_id != $feedArr[$count]['user_id']) {
                            unset($feedArr[$count]);
                        }

                        if ((basename($feedArr[$count]['user_profile_pic']) == basename($feedArr[$count]['shared_pic']))) {
                            unset($feedArr[$count]);
                        }
                        if ($feedArr[$count]['user_profile_pic'] == "") {
                            unset($feedArr[$count]);
                        }
                        $count++;
                    }
                }
            }


            /* get vortext setting */
            $this->loadModel("AppSetting");
            $showRandomLocation = $this->AppSetting->find('first', array(
                'conditions' => array('AppSetting.id' => 1)
            ));

            $vortex_setting = $showRandomLocation['AppSetting']['status'];
            $vortex_lat = '';
            $vortex_lng = '';
            if ($vortex_setting) {
                $distance = rand(100, 2000);
                $vortex_location = $User = $this->Message->query("select User.lat, User.lon
              from (select users.*, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
              where User.distance >= $distance * 1.609 and User.lat != '0.0' and User.lat != '' and User.lat != '0' ORDER BY rand() limit 0, 30");
                if (!empty($vortex_location)) {
                    $vortex_location = $this->Message->query("select User.lat, User.lon
              from (select users.*, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
              where User.distance >= 100 * 1.609 and User.lat != '0.0' and User.lat != '' and User.lat != '0' ORDER BY rand() limit 0, 30");
                }
                $random_key = array_rand($vortex_location);
                $vortex_lat = isset($vortex_location[$random_key]['User']['lat']) && !empty($vortex_location[$random_key]['User']['lat']) ? $vortex_location[$random_key]['User']['lat'] : '43.8534184';
                $vortex_lng = isset($vortex_location[$random_key]['User']['lon']) && !empty($vortex_location[$random_key]['User']['lon']) ? $vortex_location[$random_key]['User']['lon'] : '-99.0031829';
            }

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array(
                'total_records' => $total_records,
                'showing' => $showing,
                'showRandomLocation' => $vortex_setting,
                'RandomLat' => $vortex_lat,
                'RandomLong' => $vortex_lng,
               // 'address' => $vortex_address,
                'items' => array_values($feedArr)
            );
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }
    
//    public function map_feeds_discoverNew($test_data = null) {
//        $data_n = file_get_contents("php://input");
//        if (isset($test_data) && (!empty($test_data))) {
//            $testData = base64_decode($test_data);
//            $data_n = $testData;
//        }
//
//        $offset = 0;
//        $image_length = 30;
//        $user_length = 100;
//
//        $decoded = json_decode($data_n, true);
//        $responseData = array();
//        if(isset($_POST['user_id'])){
//            $decoded=$_POST;
//        }
//        $current_lat = $decoded['current_lat']; // '53.45392522';
//        $current_lon = $decoded['current_lon']; // '-2.17939499';
//        $category_idx = $decoded['category_idx'];
//        $show_me = $decoded['show_me'];
//        $user_id = $decoded['user_id']; //7852;
//
//        $this->loadModel("User");
//        $this->loadModel('UserImage');
//        $this->loadModel("UserImageLike");
//        $this->loadModel("Message");
//
//        if (!empty($decoded)) {
//
//            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
//                $offset = $decoded['offset'];
//            }
//
//            if (!isset($offset) || $offset == 0 || $offset < 0) {
//                $offset = 0;
//            }
//
//            if (isset($decoded['offset'])) {
//                $page = $decoded['offset'];
//            } else {
//                $page = 1;
//            };
//
//            if ($page == 1) {
//                $start_from = ($page - 1) * $image_length;
//                $user_start_from = ($page - 1) * $user_length;
//            } else {
//                if ($offset == 2) {
//                    $start_from = 31;
//                    $user_start_from = 101;
//                } else {
//                    $start_from = 31 + ($image_length * ($page - 2));
//                    $user_start_from = 101 + ($user_length * ($page - 2));
//                }
//            }
//
//            $this->loadModel('UserContact');
//            $this->loadModel("UserLocation");
//            $this->UserContact->Behaviors->load('Containable');
//            $category_where = "";
//            if ($category_idx != -1) {
//                $category_where = " AND UserImage.category_idx = $category_idx ";
//            }
//
//            if ($show_me == 1) {
//                $res = $this->UserContact->find(
//                        'all',
//                        array(
//                            'conditions' => array('UserContact.user_id' => $user_id)
//                        )
//                );
//                $fr_rs = array();
//                foreach ($res as $re) {
//
//                    $fr_rs[] = $re['UserContact']['contact_user_id'];
//                }
//
//                if (!empty($fr_rs)) {
//
//                    $fr_where = " AND UserImage.user_id in(" . implode(",", $fr_rs) . ")";
//                    $fr_where1 = " AND User.id in(" . implode(",", $fr_rs) . ")";
//                } else {
//                    $fr_where = " AND UserImage.user_id in(0)";
//                    $fr_where1 = " AND User.id in(0)";
//                }
//                
//                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
//							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
//							  * COS( RADIANS( lat ) )
//							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
//							  + SIN( RADIANS($current_lat) )
//							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 " . $fr_where . " ORDER BY id DESC limit $start_from, $image_length");
//
//                if ($category_idx == -1) {
//                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type,
//            User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
//            from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
//            * COS( RADIANS( user_locations.lat ) )
//            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
//            + SIN( RADIANS($current_lat) )
//            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
//            where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) " . $fr_where1 . " ORDER BY User.modified DESC limit $user_start_from, $user_length");
//                }
//            } else {
//
//                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
//							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
//							  * COS( RADIANS( lat ) )
//							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
//							  + SIN( RADIANS($current_lat) )
//							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 ORDER BY id DESC limit $start_from, $image_length");
//
//                if ($category_idx == -1) {
//                    
//                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type,
//            User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
//            from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
//            * COS( RADIANS( user_locations.lat ) )
//            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
//            + SIN( RADIANS($current_lat) )
//            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
//            where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC limit $user_start_from, $user_length");
//                }
//            }
//
//            $UserCount = [];
//            if ($category_idx == -1) {
//                $UserCount = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value,
//         'NA' as feed_likes, User.lat, User.lon, User.lat, '-1' as category_idx, 'User' as type,
//          User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
//          from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, 
//          ( 6371 * ACOS( COS( RADIANS($current_lat) ) * COS( RADIANS( user_locations.lat ) )
//          * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
//          + SIN( RADIANS($current_lat) )
//          * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
//          where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC");
//            }
//
//            $total_records = count($UserCount);
//            $showing = count($User);
//            if ($total_records < $showing) {
//                $showing = $total_records;
//            }
//            if ($showing < $total_records && ($offset > 1 && ($offset * $user_length) > $total_records)) {
//                $showing = $total_records;
//            }
//            $data = array();
//            if (!empty($UserImage)) {
//
//                foreach ($UserImage as $value) {
//                    $list1 = array();
//                    $list = array();
//                    $list['id'] = $value['UserImage']['id'];
//                    $list['updated'] = $value['UserImage']['updated'];
//                    $list['userId'] = $value['UserImage']['userId'];
//                    $list['value'] = $value['UserImage']['value'];
//                    $list['feed_likes'] = $value['UserImage']['feed_likes'];
//                    $list['lat'] = $value['UserImage']['lat'];
//                    $list['lon'] = $value['UserImage']['lon'];
//                    $list['category_idx'] = $value['UserImage']['category_idx'];
//                    $list['hide_from_map'] = 0;
//                    $list['friend_see_on_map'] = 0;
//                    $list['hide_from_profile'] = 0;
//                    $list['type'] = $value[0]['type'];
//                    array_push($list1, $list);
//                    array_push($data, $list1);
//                }
//            }
//            if (!empty($User)) {
//
//                foreach ($User as $value) {
//                    $list1 = array();
//                    $list = array();
//                    $list['id'] = $value['User']['id'];
//                    $list['updated'] = $value['User']['updated'];
//                    $list['userId'] = $value['User']['userId'];
//                    $list['value'] = $value['User']['value'];
//                    $list['feed_likes'] = $value[0]['feed_likes'];
//                    $list['lat'] = $value['User']['lat'];
//                    $list['lon'] = $value['User']['lon'];
//                    $list['category_idx'] = $value[0]['category_idx'];
//                    //$list['hide_from_map'] = $value[0]['hide_from_map'];
//                   // $list['friend_see_on_map'] = $value[0]['friend_see_on_map'];
//                    //$list['hide_from_profile'] = $value[0]['hide_from_profile'];
//                    $list['type'] = $value[0]['type'];
//                    array_push($list1, $list);
//                    array_push($data, $list1);
//                }
//            }
//
//            $feedArr = array();
//            $count = 0;
//            $res = [];
//
//            $filter_user_array = array(0);
//            if (!empty($data)) {
//                foreach ($data as $FeedKey => $FeedVal) {
//                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {
//                        $filter_user_array[] = $FeedVal[0]['userId'];
//                    }
//                }
//
//                $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
//                $user_data = $this->User->find('all', array(
//                    'fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*'),
//                    'conditions' => array('User.id' => array_unique($filter_user_array))
//                ));
//                $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));
//
//                $userArr = array();
//                foreach ($user_data as $userKey => $userVal) {
//                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
//                    if (!empty($userVal['User']['full_name'])) {
//                        $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
//                    }
//
//                    $userArr[$userVal['User']['id']]['profile_image'] = "";
//
//                    if (!empty($userVal['UserImage']['image'])) {
//                        $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
//                    }
//                }
//
//                foreach ($data as $FeedKey => $FeedVal) {
//                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {
//
//                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
//                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
//                        $feedArr[$count]['liked_by_me'] = 0;
//
//                        if ($FeedVal[0]['type'] == 'Image') {
//                            if (!empty($FeedVal[0]['value'])) {
//                                //$feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
//                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
//                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
//                            }
//                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
//                                $feedArr[$count]['liked_by_me'] = 1;
//                            }
//                        }
//
//                        if ($FeedVal[0]['type'] == 'User') {
//                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
//                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
//                            $feedArr[$count]['shared_pic'] = "";
//                        }
//
//                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
//                       // $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
//                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
//                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
//                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
//                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];
//
////                        if ($FeedVal[0]['hide_from_map'] == 1) {
////                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
////                            $res = $this->UserContact->find('all', array('conditions' => $conditions4));
////                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
////                                unset($feedArr[$count]);
////                            }
////                        }
//
////                        if ($FeedVal[0]['friend_see_on_map'] == 1) {
////                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
////                            $res = $this->UserContact->find('all', array('conditions' => $conditions));
////
////                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
////                                unset($feedArr[$count]);
////                            }
////                        }
//
////                        if ($FeedVal[0]['hide_from_profile'] == 1 && $feedArr[$count]['type'] == "User" && $user_id != $feedArr[$count]['user_id']) {
////                            unset($feedArr[$count]);
////                        }
//
////                        if ((basename($feedArr[$count]['user_profile_pic']) == basename($feedArr[$count]['shared_pic']))) {
////                            unset($feedArr[$count]);
////                        }
////                        if ($feedArr[$count]['user_profile_pic'] == "") {
////                            unset($feedArr[$count]);
////                        }
//                        $count++;
//                    }
//                }
//            }
//
//
//            /* get vortext setting */
//            $this->loadModel("AppSetting");
//            $showRandomLocation = $this->AppSetting->find('first', array(
//                'conditions' => array('AppSetting.id' => 1)
//            ));
//
//            $vortex_setting = $showRandomLocation['AppSetting']['status'];
//            $vortex_lat = '';
//            $vortex_lng = '';
//            if ($vortex_setting) {
//                $distance = rand(100, 2000);
//                $vortex_location = $User = $this->Message->query("select User.lat, User.lon
//              from (select users.*, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
//              * COS( RADIANS( user_locations.lat ) )
//              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
//              + SIN( RADIANS($current_lat) )
//              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
//              where User.distance >= $distance * 1.609 and User.lat != '0.0' and User.lat != '' and User.lat != '0' ORDER BY rand() limit 0, 30");
//                if (!empty($vortex_location)) {
//                    $vortex_location = $this->Message->query("select User.lat, User.lon
//              from (select users.*, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
//              * COS( RADIANS( user_locations.lat ) )
//              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
//              + SIN( RADIANS($current_lat) )
//              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
//              where User.distance >= 100 * 1.609 and User.lat != '0.0' and User.lat != '' and User.lat != '0' ORDER BY rand() limit 0, 30");
//                }
//                $random_key = array_rand($vortex_location);
//                $vortex_lat = isset($vortex_location[$random_key]['User']['lat']) && !empty($vortex_location[$random_key]['User']['lat']) ? $vortex_location[$random_key]['User']['lat'] : '43.8534184';
//                $vortex_lng = isset($vortex_location[$random_key]['User']['lon']) && !empty($vortex_location[$random_key]['User']['lon']) ? $vortex_location[$random_key]['User']['lon'] : '-99.0031829';
//            }
//
//            $responseData = array();
//            $responseData['response_status'] = 1;
//            $responseData['response_code'] = 200;
//            $responseData['response_message_code'] = "";
//            $responseData['message'] = 'successfully';
//            $responseData['response_data'] = array(
//                'total_records' => $total_records,
//                'showing' => $showing,
//                'showRandomLocation' => $vortex_setting,
//                'RandomLat' => $vortex_lat,
//                'RandomLong' => $vortex_lng,
//             //   'address' => $vortex_address,
//                'items' => array_values($feedArr)
//            );
//            $data = array('response' => $responseData);
//            $jsonEncode = json_encode($data);
//            return $jsonEncode;
//        } else {
//            $responseData = array();
//            $responseData['response_status'] = 1;
//            $responseData['response_code'] = 200;
//            $responseData['response_message_code'] = "dd";
//            $responseData['message'] = 'successfully';
//            $responseData['response_data'] = array();
//            $data = array('response' => $responseData);
//            $jsonEncode = json_encode($data);
//            return $jsonEncode;
//        }
//    }
    
    
    public function map_feeds_discoverNew($test_data = null) {
        $data_n = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data_n = $testData;
        }

        $offset = 0;
        $image_length = 30;
        $user_length = 100;

        $decoded = json_decode($data_n, true);
        $responseData = array();
        if(isset($_POST['user_id'])){
            $decoded=$_POST;
        }
        $current_lat = $decoded['current_lat']; // '53.45392522';
        $current_lon = $decoded['current_lon']; // '-2.17939499';
        $category_idx = $decoded['category_idx'];
        $show_me = $decoded['show_me'];
        $user_id = $decoded['user_id']; //7852;

        $this->loadModel("User");
        $this->loadModel('UserImage');
        $this->loadModel("UserImageLike");
        $this->loadModel("Message");

        if (!empty($decoded)) {

            if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                $offset = $decoded['offset'];
            }

            if (!isset($offset) || $offset == 0 || $offset < 0) {
                $offset = 0;
            }

            if (isset($decoded['offset'])) {
                $page = $decoded['offset'];
            } else {
                $page = 1;
            };

            if ($page == 1) {
                $start_from = ($page - 1) * $image_length;
                $user_start_from = ($page - 1) * $user_length;
            } else {
                if ($offset == 2) {
                    $start_from = 31;
                    $user_start_from = 101;
                } else {
                    $start_from = 31 + ($image_length * ($page - 2));
                    $user_start_from = 101 + ($user_length * ($page - 2));
                }
            }

            $this->loadModel('UserContact');
            $this->loadModel("UserLocation");
            $this->UserContact->Behaviors->load('Containable');
            $category_where = "";
            if ($category_idx != -1) {
                $category_where = " AND UserImage.category_idx = $category_idx ";
            }

            if ($show_me == 1) {
                $res = $this->UserContact->find(
                        'all',
                        array(
                            'conditions' => array('UserContact.user_id' => $user_id)
                        )
                );
                $fr_rs = array();
                foreach ($res as $re) {

                    $fr_rs[] = $re['UserContact']['contact_user_id'];
                }

                if (!empty($fr_rs)) {

                    $fr_where = " AND UserImage.user_id in(" . implode(",", $fr_rs) . ")";
                    $fr_where1 = " AND User.id in(" . implode(",", $fr_rs) . ")";
                } else {
                    $fr_where = " AND UserImage.user_id in(0)";
                    $fr_where1 = " AND User.id in(0)";
                }
                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 " . $fr_where . " ORDER BY id DESC limit $start_from, $image_length");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type,
            User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
            from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
            * COS( RADIANS( user_locations.lat ) )
            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
            + SIN( RADIANS($current_lat) )
            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
            where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) " . $fr_where1 . " ORDER BY User.modified DESC limit $user_start_from, $user_length");
                }
            } else {

                $UserImage = $this->Message->query("select UserImage.id, UserImage.modified as updated, UserImage.user_id as userId, UserImage.image as value, UserImage.total_likes as feed_likes, UserImage.lat, UserImage.lon, UserImage.category_idx, 'Image' as type from ((SELECT *,
							  ( 6371 * ACOS( COS( RADIANS($current_lat) )
							  * COS( RADIANS( lat ) )
							  * COS( RADIANS( lon ) - RADIANS($current_lon) )
							  + SIN( RADIANS($current_lat) )
							  * SIN( RADIANS( lat ) ) ) ) AS distance FROM user_images) as UserImage) where UserImage.distance <= 5 * 1.609 AND ( UserImage.image !='' $category_where ) AND UserImage.image_type NOT IN (4,5,6) AND UserImage.status = 1 ORDER BY id DESC limit $start_from, $image_length");

                if ($category_idx == -1) {
                    $User = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value, 'NA' as feed_likes, User.lat, User.lon, '-1' as category_idx, 'User' as type,
            User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
            from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
            * COS( RADIANS( user_locations.lat ) )
            * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
            + SIN( RADIANS($current_lat) )
            * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
            where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC limit $user_start_from, $user_length");
                }
            }

            $UserCount = [];
            if ($category_idx == -1) {
                $UserCount = $this->Message->query("select User.id, User.profile_status_message_updated as updated, User.id as userId, User.profile_status_message as value,
         'NA' as feed_likes, User.lat, User.lon, User.lat, '-1' as category_idx, 'User' as type,
          User.hide_from_map, User.friend_see_on_map, User.hide_from_profile
          from (select users.*, IFNULL(user_locations.hide_from_map, 0) AS hide_from_map, IFNULL(user_locations.friend_see_on_map, 0) AS friend_see_on_map, IFNULL(user_locations.hide_from_profile, 0) AS hide_from_profile, user_locations.lat, user_locations.lon, 
          ( 6371 * ACOS( COS( RADIANS($current_lat) ) * COS( RADIANS( user_locations.lat ) )
          * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
          + SIN( RADIANS($current_lat) )
          * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
          where User.distance <= 5 * 1.609 and User.hide_from_profile != 1 and EXISTS (SELECT * FROM user_images ui WHERE  User.id = ui.user_id) ORDER BY User.modified DESC");
            }

            $total_records = count($UserCount);
            $showing = count($User);
            if ($total_records < $showing) {
                $showing = $total_records;
            }
            if ($showing < $total_records && ($offset > 1 && ($offset * $user_length) > $total_records)) {
                $showing = $total_records;
            }
            $data = array();
            if (!empty($UserImage)) {

                foreach ($UserImage as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['UserImage']['id'];
                    $list['updated'] = $value['UserImage']['updated'];
                    $list['userId'] = $value['UserImage']['userId'];
                    $list['value'] = $value['UserImage']['value'];
                    $list['feed_likes'] = $value['UserImage']['feed_likes'];
                    $list['lat'] = $value['UserImage']['lat'];
                    $list['lon'] = $value['UserImage']['lon'];
                    $list['category_idx'] = $value['UserImage']['category_idx'];
                    $list['hide_from_map'] = 0;
                    $list['friend_see_on_map'] = 0;
                    $list['hide_from_profile'] = 0;
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }
            if (!empty($User)) {

                foreach ($User as $value) {
                    $list1 = array();
                    $list = array();
                    $list['id'] = $value['User']['id'];
                    $list['updated'] = $value['User']['updated'];
                    $list['userId'] = $value['User']['userId'];
                    $list['value'] = $value['User']['value'];
                    $list['feed_likes'] = $value[0]['feed_likes'];
                    $list['lat'] = $value['User']['lat'];
                    $list['lon'] = $value['User']['lon'];
                    $list['category_idx'] = $value[0]['category_idx'];
                    $list['hide_from_map'] = $value['User']['hide_from_map'];
                    $list['friend_see_on_map'] = $value['User']['friend_see_on_map'];
                    $list['hide_from_profile'] = $value['User']['hide_from_profile'];
                    $list['type'] = $value[0]['type'];
                    array_push($list1, $list);
                    array_push($data, $list1);
                }
            }

            $feedArr = array();
            $count = 0;
            $res = [];

            $filter_user_array = array(0);
            if (!empty($data)) {
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {
                        $filter_user_array[] = $FeedVal[0]['userId'];
                    }
                }

                $this->User->bindModel(array('hasOne' => array('UserImage' => array('className' => 'UserImage', 'foreignKey' => 'user_id', 'conditions' => array('UserImage.image_type' => 1, 'UserImage.status' => 1), 'fields' => array('id', 'user_id', 'image', 'status'), 'order' => array('UserImage.id DESC'),))), false);
                $user_data = $this->User->find('all', array(
                    'fields' => array('User.id', 'User.username', 'User.full_name', 'User.phone_number', 'UserImage.*'),
                    'conditions' => array('User.id' => array_unique($filter_user_array))
                ));
                $user_liked_images = $this->UserImageLike->find('list', array('conditions' => array('UserImageLike.user_id' => $user_id, 'UserImageLike.status' => 1), 'fields' => array('id', 'user_image_id')));

                $userArr = array();
                foreach ($user_data as $userKey => $userVal) {
                    $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['phone_number'];
                    if (!empty($userVal['User']['full_name'])) {
                        $userArr[$userVal['User']['id']]['USERNM'] = $userVal['User']['full_name'];
                    }

                    $userArr[$userVal['User']['id']]['profile_image'] = "";

                    if (!empty($userVal['UserImage']['image'])) {
                        $userArr[$userVal['User']['id']]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userVal['UserImage']['image'];
                    }
                }
               
                foreach ($data as $FeedKey => $FeedVal) {
                    if (!empty($FeedVal[0]['value']) || ($FeedVal[0]['value'] == 0)) {

                        $feedArr[$count]['feed_id'] = $FeedVal[0]['id'];
                        $feedArr[$count]['feed_likes'] = $FeedVal[0]['feed_likes'];
                        $feedArr[$count]['liked_by_me'] = 0;

                        if ($FeedVal[0]['type'] == 'Image') {
                            if (!empty($FeedVal[0]['value'])) {
                                
                                if(isset($userArr[$FeedVal[0]['userId']]['USERNM'])){
                                    $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " added a new photo";
                                }
                                
                                $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                                $feedArr[$count]['shared_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $FeedVal[0]['value'];
                            }
                            if (in_array($FeedVal[0]['id'], $user_liked_images)) {
                                $feedArr[$count]['liked_by_me'] = 1;
                            }
                        }

                        if ($FeedVal[0]['type'] == 'User') {
                            $feedArr[$count]['message'] = $userArr[$FeedVal[0]['userId']]['USERNM'] . " '" . $FeedVal[0]['value'] . "'";
                            $feedArr[$count]['added_on'] = $FeedVal[0]['updated'];
                            $feedArr[$count]['shared_pic'] = "";
                        }

                        $feedArr[$count]['type'] = $FeedVal[0]['type'];
                        if(isset($userArr[$FeedVal[0]['userId']]['profile_image'])){
                            $feedArr[$count]['user_profile_pic'] = $userArr[$FeedVal[0]['userId']]['profile_image'];
                        }
                        
                        $feedArr[$count]['user_id'] = $FeedVal[0]['userId'];
                        $feedArr[$count]['lat'] = $FeedVal[0]['lat'];
                        $feedArr[$count]['lon'] = $FeedVal[0]['lon'];
                        $feedArr[$count]['category_idx'] = $FeedVal[0]['category_idx'];

                        if ($FeedVal[0]['hide_from_map'] == 1) {
                            $conditions4 = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions4));
                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        if ($FeedVal[0]['friend_see_on_map'] == 1) {
                            $conditions = array('UserContact.contact_user_id' => $feedArr[$count]['user_id'], 'UserContact.user_id' => $user_id);
                            $res = $this->UserContact->find('all', array('conditions' => $conditions));

                            if (empty($res) && $user_id != $feedArr[$count]['user_id']) {
                                unset($feedArr[$count]);
                            }
                        }

                        if ($FeedVal[0]['hide_from_profile'] == 1 && $feedArr[$count]['type'] == "User" && $user_id != $feedArr[$count]['user_id']) {
                            unset($feedArr[$count]);
                        }
                        if(isset($feedArr[$count]['user_profile_pic'])){
                            if ((basename($feedArr[$count]['user_profile_pic']) == basename($feedArr[$count]['shared_pic']))) {
                            unset($feedArr[$count]);
                        } 
                        }
                       if(isset($feedArr[$count]['user_profile_pic'])){
                        if ($feedArr[$count]['user_profile_pic'] == "") {
                            unset($feedArr[$count]);
                        }
                       }
                        $count++;
                    }
                }
            }


            /* get vortext setting */
            $this->loadModel("AppSetting");
            $showRandomLocation = $this->AppSetting->find('first', array(
                'conditions' => array('AppSetting.id' => 1)
            ));

            $vortex_setting = $showRandomLocation['AppSetting']['status'];
            $vortex_lat = '';
            $vortex_lng = '';
            if ($vortex_setting) {
                $distance = rand(100, 2000);
                $vortex_location = $User = $this->Message->query("select User.lat, User.lon
              from (select users.*, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
              where User.distance >= $distance * 1.609 and User.lat != '0.0' and User.lat != '' and User.lat != '0' ORDER BY rand() limit 0, 30");
                if (!empty($vortex_location)) {
                    $vortex_location = $this->Message->query("select User.lat, User.lon
              from (select users.*, user_locations.lat, user_locations.lon, ( 6371 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users left join user_locations on users.id = user_locations.user_id) as User
              where User.distance >= 100 * 1.609 and User.lat != '0.0' and User.lat != '' and User.lat != '0' ORDER BY rand() limit 0, 30");
                }
                $random_key = array_rand($vortex_location);
                $vortex_lat = isset($vortex_location[$random_key]['User']['lat']) && !empty($vortex_location[$random_key]['User']['lat']) ? $vortex_location[$random_key]['User']['lat'] : '43.8534184';
                $vortex_lng = isset($vortex_location[$random_key]['User']['lon']) && !empty($vortex_location[$random_key]['User']['lon']) ? $vortex_location[$random_key]['User']['lon'] : '-99.0031829';
            }

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array(
                'total_records' => $total_records,
                'showing' => $showing,
                'showRandomLocation' => $vortex_setting,
                'RandomLat' => $vortex_lat,
                'RandomLong' => $vortex_lng,
               // 'address' => $vortex_address,
                'items' => array_values($feedArr)
            );
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = 'successfully';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }
    
    public function get_new_resident($test_data = null) {

        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        $this->loadModel('User');

        if (!empty($decoded['user_id'])) {
            if (!empty($decoded['country'])) {
                $country = $decoded['country'];
            } else {
                $userData = $this->User->find('first', array('fields' => array('country'), 'conditions' => array('User.id' => $decoded['user_id'])));
                $country = $userData['User']['country'];
            }

            if ($country) {

                $this->loadModel('UserImage');
                $this->loadModel('UserBlock');
                $this->loadModel('UserContact');
                $this->loadModel('UserContactRequest');

                $profiledata = $this->User->find('all', array(
                    'fields' => array('User.id', 'User.is_private', 'User.username', 'User.email', 'User.jid', 'User.phone_number', 'User.profile_status_message', 'User.push_notification_status ', 'User.notification_preview_status', 'User.full_name', 'User.profile_image', 'User.profile_cover_image', 'User.hide_username_search', 'User.device_type', 'User.country', 'User.status', 'User.is_verify'), 'conditions' => array('User.country' => $country, 'User.id !=' => $decoded['user_id']),
                    'limit' => 12,
                    'order' => 'User.id DESC'
                ));

                if (!empty($profiledata)) {
                    foreach ($profiledata as $ukey => $uvalue) {


                        $profiledata['User'][$ukey] = $uvalue['User'];

                        $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $uvalue['User']['id'], 'UserImage.image_type' => array(1, 2))));

                        $profiledata['User'][$ukey]['profile_image'] = "";
                        $profiledata['User'][$ukey]['profile_cover_image'] = "";
                        foreach ($userImages as $key => $value) {
                            if ($value['UserImage']['image_type'] == 1) {
                                $profiledata['User'][$ukey]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                            }
                            if ($value['UserImage']['image_type'] == 2) {
                                $profiledata['User'][$ukey]['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $value['UserImage']['image'];
                            }
                        }

                        /* get location and score */
                        $this->loadModel('UserLocation');

                        $user_lat = "0.0";
                        $user_lon = "0.0";
                        $user_score = 0;
                        $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $uvalue['User']['id'])));

                        if (!empty($location_data['UserLocation']['user_id'])) {
                            $user_lat = $location_data['UserLocation']['lat'];
                            $user_lon = $location_data['UserLocation']['lon'];
                            $user_score = $location_data['UserLocation']['score'];
                        }

                        $profiledata['User'][$ukey]['lat'] = $user_lat;
                        $profiledata['User'][$ukey]['lon'] = $user_lon;
                        $profiledata['User'][$ukey]['score'] = $user_score;

                        $profiledata['User'][$ukey]['UserImage'] = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $uvalue['User']['id'], 'UserImage.image_type' => 1), 'order' => array('UserImage.modified' => 'desc', 'UserImage.created' => 'desc')));
                        $profiledata['User'][$ukey]['UserBlock'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.user_id' => $uvalue['User']['id'])));
                        $profiledata['User'][$ukey]['UserBlocked'] = $this->UserBlock->find('all', array('conditions' => array('UserBlock.status' => 1, 'UserBlock.blocked_user_id' => $uvalue['User']['id'])));

                        $profiledata['User'][$ukey]['UserContact'] = $this->UserContact->find('all', array('conditions' => array('UserContact.user_id' => $uvalue['User']['id'], 'UserContact.contact_user_id' => $uvalue['User']['id'])));
                        $profiledata['User'][$ukey]['UserContactRequest'] = $this->UserContactRequest->find('all', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $uvalue['User']['id'])));

                        $user_id = $uvalue['User']['id'];
                        $profiledata['User'][$ukey]['contact_request'] = 0;
                        $profiledata['User'][$ukey]['is_contact'] = 0;
                        $profiledata['User'][$ukey]['is_sender'] = 0;

                        $this->loadModel('UserContactRequest');
                        $contact_request = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id)));

                        if (!empty($contact_request)) {
                            $profiledata['User'][$ukey]['contact_request'] = 1;
                        }

                        $contact_request_is_sender = $this->UserContactRequest->find('first', array('conditions' => array('UserContactRequest.user_id' => $uvalue['User']['id'], 'UserContactRequest.friend_id' => $user_id, 'UserContactRequest.is_sender' => 1)));

                        if (!empty($contact_request_is_sender)) {
                            $profiledata['User'][$ukey]['is_sender'] = 1;
                        }

                        $this->loadModel('UserContact');
                        $contact = $this->UserContact->find('first', array('conditions' => array('UserContact.user_id' => $user_id, 'UserContact.contact_user_id' => $uvalue['User']['id'])));

                        if (!empty($contact)) {
                            $profiledata['User'][$ukey]['is_contact'] = 1;
                        }
                    }
                }

                if (!empty($profiledata)) {
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['message'] = 'success';
                    $responseData['response_data']['data'] = $profiledata['User'];
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                } else {
                    $responseData = array();
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = "";
                    $responseData['message'] = 'successfully';
                    $responseData['response_data'] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                    return $jsonEncode;
                }
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No json data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function update_user_country($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $length = "";
        $offset = "";
        $length_new = 10;

        $decoded = json_decode($data, true);
        $responseData = array();

        if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
            $offset = $decoded['offset'];
        }

        if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
            $length_new = $decoded['limit'];
        }

        if (!isset($offset) || $offset == 0 || $offset < 0) {
            $offset = 0;
        }

        if (!empty($decoded)) {
            $this->loadModel('User');

            $UserData = $this->User->find('all', array(
                'limit' => "$offset, $length_new"
            ));
            $base_path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR;
            $jsonData = file_get_contents($base_path . '/all_country.json');
            $jsonData1 = json_decode($jsonData, true);
            foreach ($UserData as $value) {
                if (empty($value['User']['country'])) {

                    $this->loadModel('UserLocation');
                    $UserData = array();

                    $location_data = $this->UserLocation->find('first', array('conditions' => array('UserLocation.user_id' => $value['User']['id'])));
                    if (!empty($location_data['UserLocation']['lat']) && !empty($location_data['UserLocation']['lon'])) {

                        $lat = array_search($location_data['UserLocation']['lat'], array_column($jsonData1, 'lat'));
                        if (!empty($lat) || $lat === 0) {
                            $country = $jsonData1[$key]['country'];
                            $data['country'] = $country;
                        }
                    }
                }
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = $data;
            $responseData['message'] = 'success';
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function interest_add() {

        $name = isset($_POST['name']) ? $_POST['name'] : '';

        $this->loadModel('Interests');

        $responseData = array();

        if (!empty($name)) {
            $name = ltrim($name, '0');

            $interestata = array(
                'name' => $name,
            );

            if ($this->Interests->save($interestata)) {
                $InterestsId = $this->Interests->getLastInsertId();

                /* -------------------Intrests avatar upload part ----------------------------- */
                App::import('Lib', 'S3');
                if (!empty($_FILES)) {
                    if ($_FILES['image']['size'] < 6291456) {
                        $path_info = pathinfo($_FILES['image']['name']);

                        $_FILES['image']['name'] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                        $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . Interest_Category_DIR . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                        if (!empty($this->Upload->result)) {
                            $interestImage = $this->Upload->result;

                            if ($this->save_image_s3bucket(WWW_ROOT . Interest_Category_DIR . DS . $this->Upload->result, Interest_Category_DIR . DS . $this->Upload->result)) {
                                
                            }

                            $responseData['response_data']['image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $interestImage; //SITE_URL . "/uploads/user_images/large/" . $userImage;

                            $interestData = array();
                            $interestData['image'] = $interestImage;
                            $this->Interests->save($interestData);
                        }
                    }
                }
                //------------------------------------------------------------

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $responseData['response_data']['name'] = $name;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function interest_list() {
        $this->loadModel('Interests');
        $this->loadModel('SubInterests');
        $interest_data = $this->Interests->find('all', array('fields' => array('Interests.id', 'Interests.name', 'Interests.image')));

        $listData = array();
        foreach ($interest_data as $key => $interest) {
            $interest_id = $interest['Interests']['id'];
            $interest_name = $interest['Interests']['name'];
            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $interest['Interests']['image'];

            $sub_interest_data = $this->SubInterests->find('all', array(
                'fields' => array('SubInterests.id', 'SubInterests.name'),
                'conditions' => array('SubInterests.interest' => $interest_id)
            ));

            $sublist_data = array();
            foreach ($sub_interest_data as $key => $subinterest) {
                $sublist_data[] = array('id' => $subinterest['SubInterests']['id'], 'name' => $subinterest['SubInterests']['name']);
            }

            $listData[] = array('id' => $interest_id, 'name' => $interest_name, 'image' => $interest_url, 'sub_category' => $sublist_data);
        }

        $responseData = array();
        $responseData['response_status'] = 0;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = '';
        $responseData['message'] = "success";
        $responseData['response_data'] = array_values($listData);
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function set_interests($test_data = null) {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $this->loadModel('User');
            $Data['User'] = $decoded;

            $profiledata = $this->User->find('first', array('conditions' => array('User.id' => $decoded['userid'])));
            $profiledata['User']['interests'] = $decoded['interests'];

            if ($this->User->save($profiledata)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = array();
                $responseData['message'] = 'success';
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function subinterests_update($test_data = null) {
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $interest = isset($_POST['interest']) ? $_POST['interest'] : '';

        $this->loadModel('SubInterests');

        $responseData = array();

        if (!empty($id)) {

            $SubInterestsDetails = $this->SubInterests->find('first', array(
                'fields' => array('SubInterests.*'),
                'conditions' => array('SubInterests.id' => $id, 'SubInterests.interest' => $interest),
            ));

            if (!empty($SubInterestsDetails)) {

                /* -------------------Sub-Intrests avatar upload part ----------------------------- */
                App::import('Lib', 'S3');
                if (!empty($_FILES)) {
                    if ($_FILES['image']['size'] < 6291456) {
                        $path_info = pathinfo($_FILES['image']['name']);

                        $_FILES['image']['name'] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                        $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . Interest_Category_DIR . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                        if (!empty($this->Upload->result)) {
                            $subinterestImage = $this->Upload->result;

                            if ($this->save_image_s3bucket(WWW_ROOT . Interest_Category_DIR . DS . $this->Upload->result, Interest_Category_DIR . DS . $this->Upload->result)) {
                                
                            }

                            $responseData['response_data']['image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $subinterestImage; //SITE_URL . "/uploads/user_images/large/" . $userImage;

                            $SubInterestsDetails['SubInterests']['image'] = $subinterestImage;
                            $this->SubInterests->save($SubInterestsDetails);
                        }
                    }
                }

                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['message'] = "success";
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 404;
            $responseData['message'] = "No data found in the request";
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_age_gender($test_data = null) {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $this->loadModel('User');
            $Data['User'] = $decoded;

            $profiledata = $this->User->find('first', array('conditions' => array('User.id' => $decoded['userid'])));
            $profiledata['User']['interests'] = $decoded['interests'];

            if ($this->User->save($profiledata)) {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = array();
                $responseData['message'] = 'success';
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    /* old */

    public function friend_match_old() {
        $data_n = file_get_contents("php://input");
        $decoded = json_decode($data_n, true);

        $offset = "";
        $length_new = 50;

        $responseData = array();
        $this->loadModel("User");
        $this->loadModel("UserImage");
        $this->loadModel('Interests');
        $this->loadModel('SubInterests');
        $this->loadModel('SwipeCounts');
        $this->loadModel('SwipeActivitys');

        try {
            if (!empty($decoded)) {
                if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                    $offset = $decoded['offset'];
                }

                if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                    $length_new = $decoded['limit'] ? $decoded['limit'] : '30';
                }

                if (!isset($offset) || $offset == 0 || $offset < 0) {
                    $offset = 0;
                }

                if (isset($decoded['offset'])) {
                    $page = $decoded['offset'];
                } else {
                    $page = 1;
                };
                $start_from = ($page - 1) * $length_new;

                $user_id = $decoded['user_id'];
                $gender = isset($decoded['gender']) ? $decoded['gender'] : 0;
                $location = isset($decoded['location']) ? $decoded['location'] : 1; //2 for every where and 1 for nearby
                $current_lat = isset($decoded['current_lat']) ? $decoded['current_lat'] : "";
                $current_lon = isset($decoded['current_lon']) ? $decoded['current_lon'] : 0;

                $device_type = isset($decoded['device_type']) ? $decoded['device_type'] : '';
                if (!empty($device_type) && $device_type == 2) {
                    $IOS_age = json_decode($decoded['age']);
                    $age_min = isset($IOS_age->min) ? $IOS_age->min : '';
                    $age_max = isset($IOS_age->max) ? $IOS_age->max : '';
                } else {
                    $age_min = isset($decoded['age']['min']) ? $decoded['age']['min'] : '';
                    $age_max = isset($decoded['age']['max']) ? $decoded['age']['max'] : '';
                }

                if (!isset($decoded['offset']) || $decoded['offset'] == 1 || $decoded['offset'] == 0 || $decoded['offset'] < 0) {
                    $page = 1;
                    $start_from = ($page - 1) * $length_new;
                    $length_new = 50;
                } else {
                    $page = $decoded['offset'];
                    $length_new = $decoded['limit'] ? $decoded['limit'] : '30';
                    if ($decoded['offset'] == 2) {
                        $start_from = 51;
                    } else {
                        $start_from = 51 + ($length_new * ($page - 2));
                    }
                };

                $userArr = array();

                /* current user data */
                $profiledata = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
                $user_age = isset($profiledata['User']['age']) ? $profiledata['User']['age'] : 18;
                $device_language = isset($profiledata['User']['device_language']) ? $profiledata['User']['device_language'] : '';
                /* get user swipe count */
                $userSwipeData = $this->SwipeCounts->find('first', array(
                    'conditions' => array('SwipeCounts.user_id' => $user_id)
                ));

                $swipe_count = isset($userSwipeData['SwipeCounts']['swipe_count']) ? $userSwipeData['SwipeCounts']['swipe_count'] : 0;

                $countdown_time = '';
                $countdown_time_new = '';
                if ($swipe_count >= 50) {
                    $time = isset($userSwipeData['SwipeCounts']['date_timestamp']) ? $userSwipeData['SwipeCounts']['date_timestamp'] : "";
                    $countdown_time = $time + 7200; //(24 * 60 * 60);
                    $countdown_time_new = $countdown_time - time();
                    /* refresh countdown time */
                    if ($countdown_time < time()) {
                        $countdown_time = '';
                        $countdown_time_new = '';
                        $swipe_count = 0;

                        if (!empty($userSwipeData)) {
                            $userSwipeData['SwipeCounts']['swipe_count'] = 0;
                            $userSwipeData['SwipeCounts']['date_timestamp'] = time();
                            $userSwipeData['SwipeCounts']['date'] = date("Y-m-d");
                            $this->SwipeCounts->save($userSwipeData);
                        }
                    }
                }

                $userArr['swipe_count'] = $swipe_count;
                $userArr['countdown_time'] = $countdown_time;
                $userArr['countdown_time_new'] = $countdown_time_new;
                $userArr['data'] = [];

                /* already swiped users */
                $swipe_user_array = array(0, $user_id);

                $swiped_users = $this->SwipeActivitys->find('all', array(
                    'fields' => array('SwipeActivitys.swipe_user_id'),
                    'conditions' => array('SwipeActivitys.user_id' => $user_id),
                ));

                if (!empty($swiped_users)) {
                    foreach ($swiped_users as $Key => $Val) {
                        foreach ($Val as $ky => $value) {
                            $swipe_user_array[] = $value['swipe_user_id'];
                        }
                    }
                }

                $country_condition = '';
                if (strtolower($device_language) == 'ar') {
                    $country_condition = " and User.device_language = 'ar'";
                }

                $gender_condition = '';
                if (!empty($gender)) {
                    $gender_condition = " and User.gender = $gender";
                }
                $age_condition = '';
                if (!empty($age_max)) {
                    $age_condition = " and User.age BETWEEN $age_min and $age_max";
                } else {
                    if (!empty($user_age)) {
                        if ($user_age >= 18) {
                            $age_max = $user_age + 5;
                            $age_min = $user_age - 1;
                            if ($age_min < 18) {
                                $age_min = 18;
                            }
                            $age_condition = " and User.age BETWEEN $age_min and $age_max";
                        } else {
                            $age_condition = " and User.age >= 13";
                        }
                    } else {
                        $age_condition = " and User.age >= 18";
                    }
                }
                $uids = join("','", $swipe_user_array);
                /*        $user_data = $this->User->query("SELECT User.id, User.username, User.full_name, User.profile_status_message, User.profile_cover_image, 
                  User.profile_image, User.interests, User.device_language, User.age, UserImage.* from users as User
                  LEFT JOIN user_images as UserImage ON UserImage.user_id = User.id
                  Where UserImage.image_type = 6 and User.full_name <> '' and UserImage.image <> ''
                  $gender_condition $age_condition $country_condition and User.id NOT IN ('$uids') order by rand() limit $length_new offset $offset"); */
                if ($location == 1) {
                    $user_data = $this->User->query("select User.id, User.username, User.full_name, User.profile_status_message, User.profile_cover_image, 
           User.profile_image, User.interests, User.device_language, User.age, UserImage.*
            from (select users.*, ( 3959 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users 
              left join user_locations on users.id = user_locations.user_id) as User
              LEFT JOIN user_images as UserImage ON UserImage.user_id = User.id
              Where UserImage.image_type = 6 and User.full_name <> '' and UserImage.image <> ''
              AND User.distance <= 100 $gender_condition $age_condition $country_condition and User.id NOT IN ('$uids') 
              ORDER BY User.distance ASC limit $length_new offset $offset");
                } else {
                    $user_data = $this->User->query("select User.id, User.username, User.full_name, User.profile_status_message, User.profile_cover_image, 
           User.profile_image, User.interests, User.device_language, User.age, UserImage.*
            from (select users.*, ( 3959 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users 
              left join user_locations on users.id = user_locations.user_id) as User
              LEFT JOIN user_images as UserImage ON UserImage.user_id = User.id
              Where UserImage.image_type = 6 and User.full_name <> '' and UserImage.image <> ''
              AND User.distance <= 100 $gender_condition $age_condition $country_condition and User.id NOT IN ('$uids') 
              ORDER BY User.distance ASC limit $length_new offset $offset");
                }

                /* get the latest 10 user those like the user */
                $likeUsersData = [];
                $howLikeyou = $this->User->query("select id, user_id from swipe_activitys 
                      where swipe_user_id = $user_id and status = 1 and friend_match_status = 1 ORDER BY created limit 0, 10");
                $like_user_count = 0;
                if (!empty($howLikeyou)) {
                    $like_user_array = [];
                    foreach ($howLikeyou as $likeuser) {
                        $like_user_array[] = $likeuser['swipe_activitys']['user_id'];
                    }
                    $likeUsersData = $this->User->find('all', array(
                        'limit' => "0, 10",
                        'fields' => array(
                            'User.id', 'User.username', 'User.full_name', 'User.profile_status_message',
                            'User.profile_cover_image', 'User.profile_image', 'User.interests', 'User.age', 'User.gender', 'User.device_language'
                        ),
                        'conditions' => array(
                            'User.id' => $like_user_array,
                            'NOT' => array('User.full_name' => null),
                        ),
                    ));

                    $like_user_count = count($likeUsersData);
                }

                if ($likeUsersData > 0) {
                    $next_swipe_list = [];
                    foreach ($user_data as $ur_data) {
                        $next_swipe_list[] = $ur_data['User']['id'];
                    }
                    foreach ($likeUsersData as $vue) {
                        $uid = $vue['User']['id'];
                        $u_gender = $vue['User']['gender'];
                        if (!in_array($uid, $next_swipe_list) && (($u_gender == $gender) || $gender == 0)) {
                            $user_data[] = $vue;
                        }
                    }
                    shuffle($user_data);
                }

                $count = 0;
                if (!empty($user_data)) {
                    foreach ($user_data as $userKey => $userVal) {
                        foreach ($userVal as $key => $value) {
                            if ($key == 'UserImage') {
                                continue;
                            }

                            $userArr['data'][$count]['id'] = $value['id'];
                            $userArr['data'][$count]['profile_status_message'] = isset($value['profile_status_message']) && ($value['profile_status_message'] != NULL || $value['profile_status_message']) ? $value['profile_status_message'] : '';
                            $userArr['data'][$count]['username'] = isset($value['username']) && $value['username'] != null ? $value['username'] : '';
                            $userArr['data'][$count]['full_name'] = isset($value['full_name']) && $value['full_name'] != null ? $value['full_name'] : '';
                            $userArr['data'][$count]['age'] = isset($value['age']) && $value['age'] != null ? $value['age'] : '';
                            $userArr['data'][$count]['profile_image'] = "";
                            $userArr['data'][$count]['profile_thumb_image'] = "";
                            $userArr['data'][$count]['profile_cover_image'] = "";
                            $userArr['data'][$count]['dating_pic'] = "";
                            $userArr['data'][$count]['dating_pic_id'] = "";
                            $userArr['data'][$count]['device_language'] = isset($value['device_language']) && $value['device_language'] != null ? $value['device_language'] : '';

                            /* user interests */
                            $user_interests = explode(',', $value['interests']);
                            $interests_data = array();
                            if (!empty($user_interests)) {
                                foreach ($user_interests as $k => $vl) {
                                    $sub_interests = $this->SubInterests->find('first', array('fields' => array('SubInterests.name', 'SubInterests.interest', 'SubInterests.image'), 'conditions' => array('SubInterests.id' => $vl)));
                                    if (!empty($sub_interests)) {
                                        $interest_name = $sub_interests['SubInterests']['name'];
                                        $subinterest_image = $sub_interests['SubInterests']['image'];
                                        if (!empty($subinterest_image)) {
                                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $subinterest_image;
                                        } else {
                                            $interest_id = $sub_interests['SubInterests']['interest'];
                                            $interestDetails = $this->Interests->find('first', array('fields' => array('Interests.image'), 'conditions' => array('Interests.id' => $interest_id)));
                                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $interestDetails['Interests']['image'];
                                        }

                                        $interests_data[] = array('id' => $vl, 'name' => $interest_name, 'image' => $interest_url);
                                    }
                                }
                            }
                            $userArr['data'][$count]['interests'] = $interests_data;

                            /* check 2nd user already like you */
                            $swiped_like = $this->SwipeActivitys->find('first', array(
                                'conditions' => array('SwipeActivitys.swipe_user_id' => $user_id, 'SwipeActivitys.user_id' => $value['id']),
                            ));

                            $userArr['data'][$count]['like'] = "0";
                            if (!empty($swiped_like)) {
                                $userArr['data'][$count]['like'] = "1";
                            }

                            $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $value['id'], 'UserImage.image_type' => array(1, 2, 6))));
                            foreach ($userImages as $val) {
                                if ($val['UserImage']['image_type'] == 1) {
                                    $userArr['data'][$count]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                                    $userArr['data'][$count]['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                                }
                                if ($val['UserImage']['image_type'] == 2) {
                                    $userArr['data'][$count]['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                                }
                                if ($val['UserImage']['image_type'] == 6) {
                                    $userArr['data'][$count]['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $val['UserImage']['image'];
                                    $userArr['data'][$count]['dating_pic_id'] = $val['UserImage']['id'];
                                }
                            }
                            $count++;
                        }
                    }
                }

                if (!empty($userArr)) {
                    shuffle($userArr['data']);
                }
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userArr;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            die(" herer");
        }
    }

    /* new */

    public function friend_match() {
        $data_n = file_get_contents("php://input");
        $decoded = json_decode($data_n, true);

        $offset = "";
        $length_new = 50;

        $responseData = array();
        $this->loadModel("User");
        $this->loadModel("UserImage");
        $this->loadModel('Interests');
        $this->loadModel('SubInterests');
        $this->loadModel('SwipeCounts');
        $this->loadModel('SwipeActivitys');

        try {
            if (!empty($decoded)) {
                if ((isset($decoded['offset'])) && (!empty($decoded['offset']))) {
                    $offset = $decoded['offset'];
                }

                if ((isset($decoded['limit'])) && (!empty($decoded['limit']))) {
                    $length_new = $decoded['limit'] ? $decoded['limit'] : '30';
                }

                if (!isset($offset) || $offset == 0 || $offset < 0) {
                    $offset = 0;
                }

                if (isset($decoded['offset'])) {
                    $page = $decoded['offset'];
                } else {
                    $page = 1;
                };
                $start_from = ($page - 1) * $length_new;

                $user_id = $decoded['user_id'];
                $gender = isset($decoded['gender']) ? $decoded['gender'] : 0;
                $location = isset($decoded['location']) ? $decoded['location'] : 2; //2 for every where and 1 for nearby
                $current_lat = isset($decoded['current_lat']) ? $decoded['current_lat'] : "53.45394607";
                $current_lon = isset($decoded['current_lon']) ? $decoded['current_lon'] : "-2.17931214";

                $device_type = isset($decoded['device_type']) ? $decoded['device_type'] : '';
                if (!empty($device_type) && $device_type == 2) {
                    $IOS_age = json_decode($decoded['age']);
                    $age_min = isset($IOS_age->min) ? $IOS_age->min : '';
                    $age_max = isset($IOS_age->max) ? $IOS_age->max : '';
                } else {
                    $age_min = isset($decoded['age']['min']) ? $decoded['age']['min'] : '';
                    $age_max = isset($decoded['age']['max']) ? $decoded['age']['max'] : '';
                }

                if (!isset($decoded['offset']) || $decoded['offset'] == 1 || $decoded['offset'] == 0 || $decoded['offset'] < 0) {
                    $page = 1;
                    $start_from = ($page - 1) * $length_new;
                    $length_new = 50;
                } else {
                    $page = $decoded['offset'];
                    $length_new = $decoded['limit'] ? $decoded['limit'] : '30';
                    if ($decoded['offset'] == 2) {
                        $start_from = 51;
                    } else {
                        $start_from = 51 + ($length_new * ($page - 2));
                    }
                };

                $userArr = array();

                /* current user data */
                $profiledata = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
                $user_age = isset($profiledata['User']['age']) ? $profiledata['User']['age'] : 18;
                $device_language = isset($profiledata['User']['device_language']) ? $profiledata['User']['device_language'] : '';
                /* get user swipe count */
                $userSwipeData = $this->SwipeCounts->find('first', array(
                    'conditions' => array('SwipeCounts.user_id' => $user_id)
                ));

                $swipe_count = isset($userSwipeData['SwipeCounts']['swipe_count']) ? $userSwipeData['SwipeCounts']['swipe_count'] : 0;

                $countdown_time = '';
                $countdown_time_new = '';
                if ($swipe_count >= 50) {
                    $time = isset($userSwipeData['SwipeCounts']['date_timestamp']) ? $userSwipeData['SwipeCounts']['date_timestamp'] : "";
                    $countdown_time = $time + 86400; //(24 * 60 * 60);
                    $countdown_time_new = $countdown_time - time();

                    /* refresh countdown time */
                    if ($countdown_time < time()) {
                        $countdown_time = '';
                        $countdown_time_new = '';
                        $swipe_count = 0;

                        if (!empty($userSwipeData)) {
                            $userSwipeData['SwipeCounts']['swipe_count'] = 0;
                            $userSwipeData['SwipeCounts']['date_timestamp'] = time();
                            $userSwipeData['SwipeCounts']['date'] = date("Y-m-d");
                            $userSwipeData['SwipeCounts']['reset_timestamp'] = (time() + 86400);
                            $this->SwipeCounts->save($userSwipeData);
                        }
                    }
                }

                $reset_time = isset($userSwipeData['SwipeCounts']['reset_timestamp']) ? $userSwipeData['SwipeCounts']['reset_timestamp'] : "";
                if ($reset_time <= time()) {
                    $new_reset_time = (time() + 86400); //(24 * 60 * 60);
                    $countdown_time = '';
                    $countdown_time_new = '';
                    $swipe_count = 0;

                    if (!empty($userSwipeData)) {
                        $userSwipeData['SwipeCounts']['swipe_count'] = 0;
                        $userSwipeData['SwipeCounts']['date_timestamp'] = time();
                        $userSwipeData['SwipeCounts']['date'] = date("Y-m-d");
                        $userSwipeData['SwipeCounts']['reset_timestamp'] = $new_reset_time;
                        $this->SwipeCounts->save($userSwipeData);
                    }
                }

                $userArr['swipe_count'] = $swipe_count;
                $userArr['countdown_time'] = $countdown_time;
                $userArr['countdown_time_new'] = $countdown_time_new;
                $userArr['data'] = [];

                /* already swiped users */
                $swipe_user_array = array(0, $user_id);

                $swiped_users = $this->SwipeActivitys->find('all', array(
                    'fields' => array('SwipeActivitys.swipe_user_id'),
                    'conditions' => array('SwipeActivitys.user_id' => $user_id),
                ));

                if (!empty($swiped_users)) {
                    foreach ($swiped_users as $Key => $Val) {
                        foreach ($Val as $ky => $value) {
                            $swipe_user_array[] = $value['swipe_user_id'];
                        }
                    }
                }

                $country_condition = '';
                if (strtolower($device_language) == 'ar') {
                    $country_condition = " and User.device_language = 'ar'";
                }

                $gender_condition = '';
                if (!empty($gender)) {
                    $gender_condition = " and User.gender = $gender";
                }
                $age_condition = '';
                if (!empty($age_max)) {
                    $age_condition = " and User.age BETWEEN $age_min and $age_max";
                } else {
                    if (!empty($user_age)) {
                        if ($user_age >= 18) {
                            $age_max = $user_age + 5;
                            $age_min = $user_age - 5;
                            if ($age_min < 18) {
                                $age_min = 18;
                            }
                            $age_condition = " and User.age BETWEEN $age_min and $age_max";
                        } else {
                            $age_condition = " and User.age >= 13";
                        }
                    } else {
                        $age_condition = " and User.age >= 18";
                    }
                }
                $uids = join("','", $swipe_user_array);
                /*        $user_data = $this->User->query("SELECT User.id, User.username, User.full_name, User.profile_status_message, User.profile_cover_image, 
                  User.profile_image, User.interests, User.device_language, User.age, UserImage.* from users as User
                  LEFT JOIN user_images as UserImage ON UserImage.user_id = User.id
                  Where UserImage.image_type = 6 and User.full_name <> '' and UserImage.image <> ''
                  $gender_condition $age_condition $country_condition and User.id NOT IN ('$uids') order by rand() limit $length_new offset $offset"); */
                if ($location == 1) {
                    $user_data = $this->User->query("select User.id, User.username, User.full_name, User.profile_status_message, User.profile_cover_image, 
           User.profile_image, User.interests, User.device_language, User.age, UserImage.*
            from (select users.*, ( 3959 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users 
              left join user_locations on users.id = user_locations.user_id) as User
              LEFT JOIN user_images as UserImage ON UserImage.user_id = User.id
              Where UserImage.image_type = 6 and User.full_name <> '' and UserImage.image <> ''
              AND User.distance <= 30 $gender_condition $age_condition $country_condition and User.id NOT IN ('$uids') 
              ORDER BY User.distance ASC limit $length_new offset $offset");
                } else {
                    $user_data = $this->User->query("select User.id, User.username, User.full_name, User.profile_status_message, User.profile_cover_image, 
           User.profile_image, User.interests, User.device_language, User.age, UserImage.*
            from (select users.*, ( 3959 * ACOS( COS( RADIANS($current_lat) )
              * COS( RADIANS( user_locations.lat ) )
              * COS( RADIANS( user_locations.lon ) - RADIANS($current_lon) )
              + SIN( RADIANS($current_lat) )
              * SIN( RADIANS( user_locations.lat ) ) ) ) AS distance from users 
              left join user_locations on users.id = user_locations.user_id) as User
              LEFT JOIN user_images as UserImage ON UserImage.user_id = User.id
              Where UserImage.image_type = 6 and User.full_name <> '' and UserImage.image <> ''
              AND User.distance <= 12714 $gender_condition $age_condition $country_condition and User.id NOT IN ('$uids') 
              ORDER BY User.distance ASC limit $length_new offset $offset");
                }

                /* get the latest 10 user those like the user */
                $likeUsersData = [];
                $howLikeyou = $this->User->query("select id, user_id from swipe_activitys 
                      where swipe_user_id = $user_id and status = 1 and friend_match_status = 1 ORDER BY created limit 0, 10");
                $like_user_count = 0;
                if (!empty($howLikeyou)) {
                    $like_user_array = [];
                    foreach ($howLikeyou as $likeuser) {
                        $like_user_array[] = $likeuser['swipe_activitys']['user_id'];
                    }
                    $likeUsersData = $this->User->find('all', array(
                        'limit' => "0, 10",
                        'fields' => array(
                            'User.id', 'User.username', 'User.full_name', 'User.profile_status_message',
                            'User.profile_cover_image', 'User.profile_image', 'User.interests', 'User.age', 'User.gender', 'User.device_language'
                        ),
                        'conditions' => array(
                            'User.id' => $like_user_array,
                            'NOT' => array('User.full_name' => null),
                        ),
                    ));

                    $like_user_count = count($likeUsersData);
                }

                if ($likeUsersData > 0) {
                    $next_swipe_list = [];
                    foreach ($user_data as $ur_data) {
                        $next_swipe_list[] = $ur_data['User']['id'];
                    }
                    foreach ($likeUsersData as $vue) {
                        $uid = $vue['User']['id'];
                        $u_gender = $vue['User']['gender'];
                        if (!in_array($uid, $next_swipe_list) && (($u_gender == $gender) || $gender == 0)) {
                            $user_data[] = $vue;
                        }
                    }
                    shuffle($user_data);
                }

                $count = 0;
                if (!empty($user_data)) {
                    foreach ($user_data as $userKey => $userVal) {
                        foreach ($userVal as $key => $value) {
                            if ($key == 'UserImage') {
                                continue;
                            }

                            $userArr['data'][$count]['id'] = $value['id'];
                            $userArr['data'][$count]['profile_status_message'] = isset($value['profile_status_message']) && ($value['profile_status_message'] != NULL || $value['profile_status_message']) ? $value['profile_status_message'] : '';
                            $userArr['data'][$count]['username'] = isset($value['username']) && $value['username'] != null ? $value['username'] : '';
                            $userArr['data'][$count]['full_name'] = isset($value['full_name']) && $value['full_name'] != null ? $value['full_name'] : '';
                            $userArr['data'][$count]['age'] = isset($value['age']) && $value['age'] != null ? $value['age'] : '';
                            $userArr['data'][$count]['profile_image'] = "";
                            $userArr['data'][$count]['profile_thumb_image'] = "";
                            $userArr['data'][$count]['profile_cover_image'] = "";
                            $userArr['data'][$count]['dating_pic'] = "";
                            $userArr['data'][$count]['dating_pic_id'] = "";
                            $userArr['data'][$count]['device_language'] = isset($value['device_language']) && $value['device_language'] != null ? $value['device_language'] : '';
                            $userArr['data'][$count]['distance'] = isset($value['distance']) ? $value['distance'] : '';

                            /* user interests */
                            $user_interests = explode(',', $value['interests']);
                            $interests_data = array();
                            if (!empty($user_interests)) {
                                foreach ($user_interests as $k => $vl) {
                                    $sub_interests = $this->SubInterests->find('first', array('fields' => array('SubInterests.name', 'SubInterests.interest', 'SubInterests.image'), 'conditions' => array('SubInterests.id' => $vl)));
                                    if (!empty($sub_interests)) {
                                        $interest_name = $sub_interests['SubInterests']['name'];
                                        $subinterest_image = $sub_interests['SubInterests']['image'];
                                        if (!empty($subinterest_image)) {
                                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $subinterest_image;
                                        } else {
                                            $interest_id = $sub_interests['SubInterests']['interest'];
                                            $interestDetails = $this->Interests->find('first', array('fields' => array('Interests.image'), 'conditions' => array('Interests.id' => $interest_id)));
                                            $interest_url = "https://d35ugz1sdahij6.cloudfront.net/uploads/interest/" . $interestDetails['Interests']['image'];
                                        }

                                        $interests_data[] = array('id' => $vl, 'name' => $interest_name, 'image' => $interest_url);
                                    }
                                }
                            }
                            $userArr['data'][$count]['interests'] = $interests_data;

                            /* check 2nd user already like you */
                            $swiped_like = $this->SwipeActivitys->find('first', array(
                                'conditions' => array('SwipeActivitys.swipe_user_id' => $user_id, 'SwipeActivitys.user_id' => $value['id']),
                            ));

                            $userArr['data'][$count]['like'] = "0";
                            if (!empty($swiped_like)) {
                                $userArr['data'][$count]['like'] = "1";
                            }

                            $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $value['id'], 'UserImage.image_type' => array(1, 2, 6))));
                            foreach ($userImages as $val) {
                                if ($val['UserImage']['image_type'] == 1) {
                                    $userArr['data'][$count]['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                                    $userArr['data'][$count]['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                                }
                                if ($val['UserImage']['image_type'] == 2) {
                                    $userArr['data'][$count]['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                                }
                                if ($val['UserImage']['image_type'] == 6) {
                                    $userArr['data'][$count]['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $val['UserImage']['image'];
                                    $userArr['data'][$count]['dating_pic_id'] = $val['UserImage']['id'];
                                }
                            }
                            $count++;
                        }
                    }
                }

                if (!empty($userArr)) {
                    shuffle($userArr['data']);
                }
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userArr;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            die(" herer");
        }
    }

    public function like_dislike() {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded)) {
            $this->loadModel("User");
            $this->loadModel("UserImage");
            $this->loadModel('SwipeCounts');
            $this->loadModel('SwipeActivitys');

            $userSwipeData = $this->SwipeCounts->find('first', array(
                'conditions' => array('SwipeCounts.user_id' => $decoded['user_id'])
            ));

            $swipe_count = 1;
            if (!empty($userSwipeData)) {
                $scount = $userSwipeData['SwipeCounts']['swipe_count'];
                if ($scount == 0) {
                    $userSwipeData['SwipeCounts']['swipe_count'] = $scount + 1;
                    $userSwipeData['SwipeCounts']['date_timestamp'] = time();
                    $userSwipeData['SwipeCounts']['date'] = date("Y-m-d");
                    $userSwipeData['SwipeCounts']['reset_timestamp'] = (time() + 86400);
                    $this->SwipeCounts->save($userSwipeData);
                } else {
                    $userSwipeData['SwipeCounts']['swipe_count'] = $scount + 1;
                    $userSwipeData['SwipeCounts']['date_timestamp'] = time();
                    $userSwipeData['SwipeCounts']['date'] = date("Y-m-d");
                    $this->SwipeCounts->save($userSwipeData);
                }
            } else {
                $swipedata = array(
                    'user_id' => $decoded['user_id'],
                    'swipe_count' => $swipe_count,
                    'date_timestamp' => time(),
                    'reset_timestamp' => (time() + 86400),
                    'date' => date("Y-m-d")
                );
                $this->SwipeCounts->save($swipedata);
            }

            /* if swiped user already like then make friend_match_status cloumn to 0 */
            /* and friend_match_status cloumn to 0 for current swipe activity as well so current user */
            /* will not show again to swiped user */
            $userSwipeAlready = $this->SwipeActivitys->find('all', array(
                'conditions' => array(
                    'SwipeActivitys.user_id' => $decoded['swipe_user_id'],
                    'SwipeActivitys.swipe_user_id' => $decoded['user_id']
                )
            ));

            if (!empty($userSwipeAlready)) {
                foreach ($userSwipeAlready as $key => $value) {
                    /*          $likedataUpdate = array();
                      $likedataUpdate['SwipeActivitys']['id'] = $value['SwipeActivitys']['id'];
                      $likedataUpdate['SwipeActivitys']['friend_match_status'] = 0;
                      $this->SwipeActivitys->save($likedataUpdate); */
                    $udate_id = $value['SwipeActivitys']['id'];

                    $updatedLikeData = $this->SwipeActivitys->query("update swipe_activitys set friend_match_status = 0 where id=$udate_id");
                }
            }

            /* save user like/dislike activity */
            $friend_match_status = $decoded['like'];
            if ($decoded['like'] == '1' && $decoded['found'] == '1') {
                $friend_match_status = 0;
            }
            if (!empty($userSwipeAlready)) {
                $friend_match_status = 0;
            }

            $likedata = array(
                'user_id' => $decoded['user_id'],
                'swipe_user_id' => $decoded['swipe_user_id'],
                'status' => $decoded['like'],
                'friend_match_status' => $friend_match_status,
                'date' => time()
            );

            if ($this->SwipeActivitys->save($likedata)) {
                if ($decoded['like'] == '1' && $decoded['found'] == '1' && !empty($decoded['user_id']) && !empty($decoded['swipe_user_id'])) {
                    /* set reciver user notification flag */
                    $userData = $this->User->find('first', array('conditions' => array('User.id' => $decoded['swipe_user_id'])));
                    $userData['User']['notification_flag'] = 1;
                    $this->User->save($userData);

                    $this->set_notification($decoded['user_id'], $decoded['swipe_user_id']);
                }
            }

            /* get user swipe count */
            $userSwipeData = $this->SwipeCounts->find('first', array(
                'conditions' => array('SwipeCounts.user_id' => $decoded['user_id'])
            ));

            $data = array();
            $swipe_count = isset($userSwipeData['SwipeCounts']['swipe_count']) ? $userSwipeData['SwipeCounts']['swipe_count'] : 0;

            $countdown_time = '';
            $countdown_time_new = '';
            if ($swipe_count >= 50) {
                $time = isset($userSwipeData['SwipeCounts']['date_timestamp']) ? $userSwipeData['SwipeCounts']['date_timestamp'] : "";
                $countdown_time = $time + 86400; //(24 * 60 * 60);
                $countdown_time_new = $countdown_time - time();
            }
            $data['swipe_count'] = $swipe_count;
            $data['countdown_time'] = $countdown_time;
            $data['countdown_time_new'] = $countdown_time_new;
            $data['like'] = $decoded['like'];

            $user_data = $this->User->find('first', array(
                'fields' => array('User.id', 'User.username', 'User.full_name', 'User.profile_status_message', 'User.profile_cover_image', 'User.profile_image', 'User.device_id', 'User.device_type', 'User.interests'),
                'conditions' => array('User.id' => $decoded['swipe_user_id']),
            ));

            if (!empty($user_data)) {
                foreach ($user_data as $key => $value) {
                    $data['user']['id'] = $value['id'];
                    $data['user']['profile_status_message'] = isset($value['profile_status_message']) && ($value['profile_status_message'] != NULL || $value['profile_status_message']) ? $value['profile_status_message'] : '';
                    $data['user']['username'] = isset($value['username']) && $value['username'] != null ? $value['username'] : '';
                    $data['user']['full_name'] = isset($value['full_name']) && $value['full_name'] != null ? $value['full_name'] : '';
                    $data['user']['profile_image'] = "";
                    $data['user']['profile_thumb_image'] = "";
                    $data['user']['profile_cover_image'] = "";
                    $data['user']['dating_pic'] = "";

                    /* check 2nd user already like you */
                    $swiped_like = $this->SwipeActivitys->find('first', array(
                        'conditions' => array('SwipeActivitys.swipe_user_id' => $decoded['user_id'], 'SwipeActivitys.user_id' => $decoded['swipe_user_id']),
                    ));

                    $data['user']['like'] = "0";
                    if (!empty($swiped_like)) {
                        $data['user']['like'] = "1";
                    }

                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $value['id'], 'UserImage.image_type' => array(1, 2, 6))));
                    foreach ($userImages as $val) {
                        if ($val['UserImage']['image_type'] == 1) {
                            $data['user']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                            $data['user']['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                        }
                        if ($val['UserImage']['image_type'] == 2) {
                            $data['user']['profile_cover_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                        }
                        if ($value['UserImage']['image_type'] == 6) {
                            $data['user']['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $val['UserImage']['image'];
                        }
                    }

                    if (!empty($decoded['like']) && $decoded['found'] != '1') {
                        /* send like notificatio */
                        if ($value['device_type'] == 1) {
                            $this->send_notification_for_android($value['device_id'], "Somebody just swiped right on you! Swipe to find your Friend Match!");
                        } else if ($value['device_type'] == 2) {
                            $this->send_notification_for_iphone_test($value['device_id'], "Somebody just swiped right on you! Swipe to find your Friend Match!");
                        } else {
                            $this->send_notification_for_iphone_test($value['device_id'], "Somebody just swiped right on you! Swipe to find your Friend Match!");
                        }
                    }

                    if ($decoded['like'] == '1' && $decoded['found'] == '1' && !empty($decoded['user_id'])) {
                        /* send match notification */
                        $CurrentUser = $this->User->find('first', array('conditions' => array('User.id' => $decoded['user_id'])));
                        $current_username = isset($value['User']['username']) && $value['User']['username'] != null ? $value['User']['username'] : '';
                        $current_full_name = isset($value['User']['full_name']) && $value['User']['full_name'] != null ? $value['User']['full_name'] : '';
                        if (empty($current_full_name)) {
                            $current_full_name = $current_username;
                        }
                        if (empty($current_full_name)) {
                            $current_full_name = "Someone";
                        }
                        if ($value['device_type'] == 1) {
                            $this->send_notification_for_android($value['device_id'], "Yay! You have a new friend match");
                        } else if ($value['device_type'] == 2) {
                            $this->send_notification_for_iphone_test($value['device_id'], "Yay! You have a new friend match");
                        } else {
                            $this->send_notification_for_iphone_test($value['device_id'], "Yay! You have a new friend match");
                        }
                    }
                }
            }

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['response_data'] = $data;
            $responseData['message'] = 'success';
            $data1 = array('response' => $responseData);

            $jsonEncode = json_encode($data1);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data1 = array('response' => $responseData);
            $jsonEncode = json_encode($data1);
            return $jsonEncode;
        }
    }

    public function upload_dating_pic() {
        $responseData = array();
        $user_id = $_POST['user_id'];
        $this->loadModel('SwipeActivitys');

        App::import('Lib', 'S3');
        if (!empty($_FILES) && !empty($user_id)) {
            if ($_FILES['image']['size'] < 6291456) {
                $path_info = pathinfo($_FILES['image']['name']);

                $_FILES['image']['name'] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                $res3 = $this->Upload->upload($_FILES['image'], WWW_ROOT . USER_DATING_PIC_DIR . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                $this->loadModel('UserImage');
                $result = array();
                if (!empty($this->Upload->result)) {
                    $userImage = $this->Upload->result;

                    if ($this->save_image_s3bucket(WWW_ROOT . USER_DATING_PIC_DIR . DS . $this->Upload->result, USER_DATING_PIC_DIR . DS . $this->Upload->result)) {
                        @unlink(WWW_ROOT . USER_DATING_PIC_DIR . DS . $this->Upload->result);
                    }

                    $result['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $userImage; //SITE_URL . "/uploads/dating_pic/" . $userImage;

                    $userImageData = $this->UserImage->find('first', array(
                        'conditions' => array('UserImage.user_id' => $user_id, 'UserImage.image_type' => 6)
                    ));

                    if (!empty($userImageData)) {
                        $userImageData['UserImage']['image'] = $userImage;
                        $this->UserImage->save($userImageData);
                    } else {
                        $profileData = array();
                        $profileData['UserImage']['user_id'] = $user_id;
                        $profileData['UserImage']['image_type'] = 6;
                        $profileData['UserImage']['image'] = $userImage;

                        if ($this->UserImage->save($profileData)) {
                            
                        }
                    }

                    /* remove the swipe activity so user will see in list swipe list again */
                    $userSwipeData = $this->SwipeActivitys->find('all', array(
                        'conditions' => array('SwipeActivitys.swipe_user_id' => $user_id, 'SwipeActivitys.status' => 0)
                    ));

                    if (!empty($userSwipeData)) {
                        foreach ($profileData as $value) {
                            $this->SwipeActivitys->delete($value['SwipeActivitys']['id']);
                        }
                    }
                }
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = '';
                $responseData['response_data'] = $result;
                $responseData['message'] = 'success';
                $data = array('response' => $responseData);

                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData['response_status'] = 0;
                $responseData['response_code'] = 400;
                $responseData['response_message_code'] = 401;
                $responseData['message'] = 'Image size is big try low size image';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function stagging_multi_upload_dating_pic() {
        $responseData = array();
        $user_id = $_POST['user_id'];
        $image_type = $_POST['image_type'];
        $this->loadModel('SwipeActivitys');

        App::import('Lib', 'S3');
        if (!empty($_FILES) && !empty($user_id)) {
            foreach ($_FILES['image']['name'] as $id => $val) {
                if ($_FILES['image']['size'][$id] < 6291456) {
                    $path_info = pathinfo($_FILES['image']['name'][$id]);

                    $_FILES['image']['name'][$id] = "a" . rand(0, 99) . "_" . time() . "a." . $path_info['extension'];

                    $res3 = $this->Upload->multi_upload($_FILES['image'], $id, WWW_ROOT . USER_DATING_PIC_DIR . DS, '', '', array('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'));

                    $this->loadModel('UserImage');
                    $result = array();
                    if (!empty($this->Upload->result)) {
                        $userImage = $this->Upload->result;

                        if ($this->save_image_s3bucket(WWW_ROOT . USER_DATING_PIC_DIR . DS . $this->Upload->result, USER_DATING_PIC_DIR . DS . $this->Upload->result)) {
                            @unlink(WWW_ROOT . USER_DATING_PIC_DIR . DS . $this->Upload->result);
                        }

                        $result['dating_pic'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/" . $userImage; //SITE_URL . "/uploads/dating_pic/" . $userImage;
                        //$userImageData = $this->UserImage->find('first', array(
                        //    'conditions' => array('UserImage.user_id' => $user_id, 'UserImage.image_type' => 6)
                        // ));
                        //  if (!empty($userImageData)) {
                        //       $userImageData['UserImage']['image'] = $userImage;
                        //       $this->UserImage->save($userImageData);
                        //  } else {
                        $profileData = array();
                        $profileData['UserImage']['user_id'] = $user_id;
                        $profileData['UserImage']['image_type'] = $image_type;
                        $profileData['UserImage']['image'] = $userImage;

                        if ($this->UserImage->save($profileData)) {
                            
                        }
                        //  }

                        /* remove the swipe activity so user will see in list swipe list again */
                        //$userSwipeData = $this->SwipeActivitys->find('all', array(
                        //    'conditions' => array('SwipeActivitys.swipe_user_id' => $user_id, 'SwipeActivitys.status' => 0)
                        //  ));
//                        if (!empty($userSwipeData)) {
//                            foreach ($profileData as $value) {
//                                $this->SwipeActivitys->delete($value['SwipeActivitys']['id']);
//                            }
//                        }
                    }
                    $responseData['response_status'] = 1;
                    $responseData['response_code'] = 200;
                    $responseData['response_message_code'] = '';
                    $responseData['response_data'][$id] = $result;
                    $responseData['message'] = 'success';
                    $data = array('response' => $responseData);

                    $jsonEncode = json_encode($data);
                } else {
                    $responseData['response_status'] = 0;
                    $responseData['response_code'] = 400;
                    $responseData['response_message_code'] = 401;
                    $responseData['message'] = 'Image size is big try low size image';
                    $responseData['response_data'][$id] = array();
                    $data = array('response' => $responseData);
                    $jsonEncode = json_encode($data);
                }
            }
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function set_notification($user_id, $target_id) {
        $responseData = array();

        if (!empty($user_id) && !empty($target_id)) {
            $this->loadModel('User');
            $this->loadModel('UserNotification');

            $userArr = array();
            $userArr['UserNotification']['fromid'] = $user_id;
            $userArr['UserNotification']['toid'] = $target_id;
            $userArr['UserNotification']['image_url'] = "";
            $userArr['UserNotification']['message'] = "You have a new friend match!";
            $userArr['UserNotification']['type'] = 99;
            $this->UserNotification->save($userArr);
            unset($userArr);

            /* send push notification */
            /* $user_profile = $this->User->find('first', array(
              'fields' => array('id', 'username', 'email', 'jid', 'phone_number', 'profile_status_message', 'online_status', 'full_name', 'device_id', 'device_type', 'profile_cover_image', 'status', 'is_private', 'device_id'),
              'conditions' => array('User.id' => $target_id)
              ));

              if ($user_profile['User']['device_type'] == 1) {
              $this->send_notification_for_android($user_profile['User']['device_id'], "You have a new friend match!");
              } else if ($user_profile['User']['device_type'] == 2) {
              $this->send_notification_for_iphone_test($user_profile['User']['device_id'], "You have a new friend match!");
              } else {
              $this->send_notification_for_iphone_test($user_profile['User']['device_id'], "You have a new friend match!");
              } */
            return '1';
        }
        return '2';
    }

    public function random_chatold() {
        $data_n = file_get_contents("php://input");
        $decoded = json_decode($data_n, true);

        $responseData = array();
        $this->loadModel("User");
        $this->loadModel("UserImage");
        $this->loadModel("RandomChatSwipes");

        try {
            if (!empty($decoded)) {
                $user_id = $decoded['user_id'];
                $chat_users = isset($decoded['chat_users']) ? $decoded['chat_users'] : '';
                $gender = isset($decoded['gender']) ? $decoded['gender'] : '0';
                $age = isset($decoded['age']) ? $decoded['age'] : '18';

                if (!empty($chat_users)) {
                    $chat_users_array = explode(',', $chat_users);
                } else {
                    $chat_users_array = array(0);
                }
                if ($gender == '2') {
                    $gender = 1;
                } else {
                    if ($gender == '1') {
                        $gender = 2;
                    }
                }
                /* get user swipe count */
                $userSwipeData = $this->RandomChatSwipes->find('first', array(
                    'conditions' => array('RandomChatSwipes.user_id' => $user_id)
                ));
                $swipe_count = isset($userSwipeData['RandomChatSwipes']['swipe_count']) ? $userSwipeData['RandomChatSwipes']['swipe_count'] : '0';
                if (!empty($userSwipeData)) {
                    if ($swipe_count < 10) {
                        $scount = $userSwipeData['RandomChatSwipes']['swipe_count'];
                        $userSwipeData['RandomChatSwipes']['swipe_count'] = $scount + 1;
                        $userSwipeData['RandomChatSwipes']['date_timestamp'] = time();
                        $userSwipeData = $this->RandomChatSwipes->save($userSwipeData);
                    } else {
                        $scount = $userSwipeData['RandomChatSwipes']['swipe_count'];
                        $userSwipeData['RandomChatSwipes']['swipe_count'] = $scount + 1;
                        $userSwipeData = $this->RandomChatSwipes->save($userSwipeData);
                    }
                } else {
                    $swipedata = array(
                        'user_id' => $user_id,
                        'swipe_count' => 1,
                        'date_timestamp' => time(),
                    );
                    $userSwipeData = $this->RandomChatSwipes->save($swipedata);
                }

                $swipe_count = $userSwipeData['RandomChatSwipes']['swipe_count'];

                $countdown_time = '';
                $countdown_time_new = '';
                if ($swipe_count >= 10) {
                    $time = isset($userSwipeData['RandomChatSwipes']['date_timestamp']) ? $userSwipeData['RandomChatSwipes']['date_timestamp'] : "";
                    $countdown_time = $time + 14400; //(24 * 60 * 60);
                    $countdown_time_new = $countdown_time - time();
                    /* refresh countdown time */
                    if ($countdown_time < time()) {
                        $countdown_time = '';
                        $countdown_time_new = '';
                        $swipe_count = 1;

                        if (!empty($userSwipeData)) {
                            $userSwipeData['RandomChatSwipes']['swipe_count'] = 1;
                            $userSwipeData['RandomChatSwipes']['date_timestamp'] = time();
                            $this->RandomChatSwipes->save($userSwipeData);
                        }
                    }
                    if ($swipe_count >= 11) {
                        $userArr = array();
                        $userArr['swipe_count'] = "10";
                        $userArr['countdown_time'] = $countdown_time;
                        $userArr['countdown_time_new'] = $countdown_time_new;

                        $userArr['data'] = new \stdClass();

                        $responseData = array();
                        $responseData['response_status'] = 1;
                        $responseData['response_code'] = 200;
                        $responseData['response_message_code'] = "";
                        $responseData['message'] = 'successfully';
                        $responseData['response_data'] = $userArr;
                        $data = array('response' => $responseData);
                        $jsonEncode = json_encode($data);
                        return $jsonEncode;
                    }
                }

                $userArr = array();
                $userArr['swipe_count'] = (string) $swipe_count;
                $userArr['countdown_time'] = $countdown_time;
                $userArr['countdown_time_new'] = $countdown_time_new;

                if ($age >= 18) {
                    $filter_min_age = $age - 5;
                    if ($filter_min_age < 18) {
                        $filter_min_age = 18;
                    }
                    $user_data = $this->User->find('all', array(
                        'limit' => "0, 30",
                        'fields' => array('User.id', 'User.username', 'User.full_name', 'User.gender', 'User.profile_image'),
                        'conditions' => array(
                            'NOT' => array('User.id' => $chat_users_array),
                            'NOT' => array('User.hide_random_chat' => 1),
                            'NOT' => array('User.username' => null),
                            'User.gender' => $gender,
                            'User.age BETWEEN ' . ($filter_min_age) . ' AND ' . ($age + 5) . ''
                        ),
                    ));
                } else {
                    $user_data = $this->User->find('all', array(
                        'limit' => "0, 30",
                        'fields' => array('User.id', 'User.username', 'User.full_name', 'User.gender', 'User.profile_image'),
                        'conditions' => array(
                            'NOT' => array('User.id' => $chat_users_array),
                            'NOT' => array('User.hide_random_chat' => 1),
                            'NOT' => array('User.username' => null),
                            'User.gender' => $gender,
                            'User.age BETWEEN ' . ($age - 5) . ' AND ' . ($age + 5) . ''
                        ),
                    ));
                }

                if (empty($user_data)) {
                    $user_data = $this->User->find('all', array(
                        'limit' => "0, 30",
                        'fields' => array('User.id', 'User.username', 'User.full_name', 'User.gender', 'User.profile_image'),
                        'conditions' => array(
                            'NOT' => array('User.id' => $chat_users_array),
                            'NOT' => array('User.username' => null),
                            'NOT' => array('User.hide_random_chat' => 1)
                        ),
                        'order' => 'User.id DESC',
                    ));
                }

                $random_key = array_rand($user_data);
                $chat_user = $user_data[$random_key];

                if (!empty($chat_user)) {
                    $userArr['data']['id'] = $chat_user['User']['id'];
                    $userArr['data']['username'] = isset($chat_user['User']['username']) && $chat_user['User']['username'] != null ? $chat_user['User']['username'] : '';
                    $userArr['data']['full_name'] = isset($chat_user['User']['full_name']) && $chat_user['User']['full_name'] != null ? $chat_user['User']['full_name'] : '';
                    $userArr['data']['gender'] = isset($chat_user['User']['gender']) ? $chat_user['User']['gender'] : '';
                    $userArr['data']['profile_image'] = "";
                    $userArr['data']['profile_thumb_image'] = "";

                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $chat_user['User']['id'], 'UserImage.image_type' => array(1))));
                    foreach ($userImages as $val) {
                        if ($val['UserImage']['image_type'] == 1) {
                            $userArr['data']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                            $userArr['data']['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                        }
                    }
                }

                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userArr;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            die(" herer");
        }
    }

    public function random_chat() {
        $data_n = file_get_contents("php://input");
        $decoded = json_decode($data_n, true);

        $responseData = array();
        $this->loadModel("User");
        $this->loadModel("UserImage");
        $this->loadModel("RandomChatSwipes");

        try {
            if (!empty($decoded)) {
                $user_id = $decoded['user_id'];
                $chat_users = isset($decoded['chat_users']) ? $decoded['chat_users'] : '0';
                $gender = isset($decoded['gender']) ? $decoded['gender'] : '0';
                $age = isset($decoded['age']) ? $decoded['age'] : '18';

                if ($gender == '2') {
                    $gender = 1;
                } else {
                    if ($gender == '1') {
                        $gender = 2;
                    }
                }
                /* get user swipe count */
                $userSwipeData = $this->RandomChatSwipes->find('first', array(
                    'conditions' => array('RandomChatSwipes.user_id' => $user_id)
                ));
                $swipe_count = isset($userSwipeData['RandomChatSwipes']['swipe_count']) ? $userSwipeData['RandomChatSwipes']['swipe_count'] : '0';
                if (!empty($userSwipeData)) {
                    if ($swipe_count < 10) {
                        $scount = $userSwipeData['RandomChatSwipes']['swipe_count'];
                        $userSwipeData['RandomChatSwipes']['swipe_count'] = $scount + 1;
                        $userSwipeData['RandomChatSwipes']['date_timestamp'] = time();
                        $userSwipeData = $this->RandomChatSwipes->save($userSwipeData);
                    } else {
                        $scount = $userSwipeData['RandomChatSwipes']['swipe_count'];
                        $userSwipeData['RandomChatSwipes']['swipe_count'] = $scount + 1;
                        $userSwipeData = $this->RandomChatSwipes->save($userSwipeData);
                    }
                } else {
                    $swipedata = array(
                        'user_id' => $user_id,
                        'swipe_count' => 1,
                        'date_timestamp' => time(),
                    );
                    $userSwipeData = $this->RandomChatSwipes->save($swipedata);
                }

                $swipe_count = $userSwipeData['RandomChatSwipes']['swipe_count'];

                $countdown_time = '';
                $countdown_time_new = '';
                /* if ($swipe_count >= 10) {
                  $time = isset($userSwipeData['RandomChatSwipes']['date_timestamp']) ? $userSwipeData['RandomChatSwipes']['date_timestamp'] : "";
                  $countdown_time = $time + 14400; //(24 * 60 * 60);
                  $countdown_time_new = $countdown_time - time();
                  //refresh countdown time
                  if ($countdown_time < time()) {
                  $countdown_time = '';
                  $countdown_time_new = '';
                  $swipe_count = 1;

                  if (!empty($userSwipeData)) {
                  $userSwipeData['RandomChatSwipes']['swipe_count'] = 1;
                  $userSwipeData['RandomChatSwipes']['date_timestamp'] = time();
                  $this->RandomChatSwipes->save($userSwipeData);
                  }
                  }
                  if ($swipe_count >= 11) {
                  $userArr = array();
                  $userArr['swipe_count'] = "10";
                  $userArr['countdown_time'] = $countdown_time;
                  $userArr['countdown_time_new'] = $countdown_time_new;

                  $userArr['data'] = new \stdClass();

                  $responseData = array();
                  $responseData['response_status'] = 1;
                  $responseData['response_code'] = 200;
                  $responseData['response_message_code'] = "";
                  $responseData['message'] = 'successfully';
                  $responseData['response_data'] = $userArr;
                  $data = array('response' => $responseData);
                  $jsonEncode = json_encode($data);
                  return $jsonEncode;
                  }
                  } */

                $userArr = array();
                $userArr['swipe_count'] = (string) $swipe_count;
                $userArr['countdown_time'] = $countdown_time;
                $userArr['countdown_time_new'] = $countdown_time_new;

                $courses = array('Eng', 'Deu', 'Bio', 'Chemi');
                $placeholders = rtrim(str_repeat('?, ', count($courses)), ', ');
                if ($age >= 18) {
                    $filter_min_age = $age - 5;
                    if ($filter_min_age < 18) {
                        $filter_min_age = 18;
                    }
                    $filter_max_age = $filter_min_age + 5;

                    $user_data = $this->User->query("select id, username, full_name, gender, profile_image from users where hide_random_chat != 1 and 
            username IS NOT NULL and username != '' and gender = $gender and age BETWEEN $filter_min_age AND $filter_max_age and id NOT IN ($chat_users) ORDER BY rand() limit 0, 30");
                } else {
                    $filter_min_age = $age - 5;
                    $filter_max_age = $age + 5;

                    $user_data = $this->User->query("select id, username, full_name, gender, profile_image from users where hide_random_chat != 1 and 
            username IS NOT NULL and username != '' and gender = $gender and age BETWEEN $filter_min_age AND $filter_max_age and id NOT IN ($chat_users) ORDER BY rand() limit 0, 30");
                }

                if (empty($user_data)) {
                    $user_data = $this->User->query("select id, username, full_name, gender, profile_image from users where hide_random_chat != 1 and 
            username IS NOT NULL and username != '' and gender = $gender and id NOT IN ($chat_users) ORDER BY rand() limit 0, 30");
                }


                $random_key = array_rand($user_data);
                $chat_user = $user_data[$random_key];

                if (!empty($chat_user)) {
                    $userArr['data']['id'] = $chat_user['users']['id'];
                    $userArr['data']['username'] = isset($chat_user['users']['username']) && $chat_user['users']['username'] != null ? $chat_user['users']['username'] : '';
                    $userArr['data']['full_name'] = isset($chat_user['users']['full_name']) && $chat_user['users']['full_name'] != null ? $chat_user['users']['full_name'] : '';
                    $userArr['data']['gender'] = isset($chat_user['users']['gender']) ? $chat_user['users']['gender'] : '';
                    $userArr['data']['profile_image'] = "";
                    $userArr['data']['profile_thumb_image'] = "";

                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $chat_user['users']['id'], 'UserImage.image_type' => array(1))));
                    foreach ($userImages as $val) {
                        if ($val['UserImage']['image_type'] == 1) {
                            $userArr['data']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                            $userArr['data']['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                        }
                    }
                }

                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userArr;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            die(" herer");
        }
    }

    public function app_logs() {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $responseData = array();

        if (!empty($decoded) && !empty($decoded['user_id']) && !empty($decoded['log_message'])) {
            $this->loadModel("AppLogs");
            $this->loadModel("User");

            $logdata = array(
                'user_id' => $decoded['user_id'],
                'log_message' => $decoded['log_message'],
                'created_at' => date("Y-m-d H:i:s", strtotime('now'))
            );
            $this->AppLogs->save($logdata);

            /* load user */
            $userData = $this->User->find('first', array(
                'conditions' => array('User.id' => $decoded['user_id'])
            ));
            $login_ban = isset($userData['User']['login_ban']) ? $userData['User']['login_ban'] : '0';
            $language = isset($decoded['device_language']) ? $decoded['device_language'] : '';

            $userData['User']['app_version'] = $decoded['app_version'];
            $userData['User']['device_language'] = $language;
            $this->User->save($userData);

            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = '';
            $responseData['message'] = 'success';
            $responseData['response_data'] = array('login_ban' => $login_ban);
            $data1 = array('response' => $responseData);

            $jsonEncode = json_encode($data1);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data1 = array('response' => $responseData);

            $jsonEncode = json_encode($data1);
            return $jsonEncode;
        }
    }

    public function user_exist_check() {
        $data = file_get_contents("php://input");
        $decoded = json_decode($data, true);
        $responseData = array();
        $this->loadModel("User");

        if (!empty($decoded) && !empty($decoded['username']) && !empty($decoded['email'])) {
            $username = $decoded['username'];
            $email = $decoded['email'];

            $msg = '';
            /* check user exist or not */
            $checkusername = $this->User->query("select id from users where username = '$username'");
            if (!empty($checkusername)) {
                $msg = "Username is already taken by someone";
            }
            $checkemail = $this->User->query("select id from users where email = '$email'");
            if (!empty($checkemail)) {
                $msg = "Email is already taken by someone";
            }
            if (!empty($checkusername) && !empty($checkemail)) {
                $msg = "Username and Email is already taken by someone";
            }

            $responseData = array();
            $responseData['response_status'] = 1;
            $responseData['response_code'] = 200;
            $responseData['response_message_code'] = "";
            $responseData['message'] = $msg;
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        } else {
            $responseData['response_status'] = 0;
            $responseData['response_code'] = 400;
            $responseData['response_message_code'] = 401;
            $responseData['message'] = 'Unauthorized Data or action';
            $responseData['response_data'] = array();
            $data = array('response' => $responseData);
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function test_pushnotification() {
        $city = 'shahabad';
        $sender_id = '7852';

        $image_category_id = '2';
        $msg = base64_encode($city . '_' . $sender_id . '_' . $image_category_id);
        $proc_command = "wget https://admin.mapbuzz.com/app2_notification.php?t=$msg -q -O - -b";
        $proc = popen($proc_command, "r");
        pclose($proc);

        $responseData = array();
        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = "";
        $responseData['message'] = "in app notification sent";
        $responseData['response_data'] = array();
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function delete_test_user() {

        $this->loadModel('User');
        $this->loadModel('UserTextPost');
        $this->loadModel('UserPostComment');
        $this->loadModel('UserPostLike');
        $this->User->bindModel(array(
            'hasMany' => array(
                'UserImageLike' => array(
                    'className' => 'UserImageLike',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserLike' => array(
                    'className' => 'UserLike',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserLiked' => array(
                    'className' => 'UserLike',
                    'foreignKey' => 'liked_user_id',
                    'dependent' => true
                ),
                'UserChatSetting' => array(
                    'className' => 'UserChatSetting',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'FromUserNotification' => array(
                    'className' => 'UserNotification',
                    'foreignKey' => 'fromid',
                    'dependent' => true
                ),
                'ToUserNotification' => array(
                    'className' => 'UserNotification',
                    'foreignKey' => 'toid',
                    'dependent' => true
                ),
                'UserBlock' => array(
                    'className' => 'UserBlock',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserBlocked' => array(
                    'className' => 'UserBlock',
                    'foreignKey' => 'blocked_user_id',
                    'dependent' => true
                ),
                'UserImage' => array(
                    'className' => 'UserImage',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserLocation' => array(
                    'className' => 'UserLocation',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserContact' => array(
                    'className' => 'UserContact',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserContacted' => array(
                    'className' => 'UserContact',
                    'foreignKey' => 'contact_user_id',
                    'dependent' => true
                ),
                'UserContactRequest' => array(
                    'className' => 'UserContactRequest',
                    'foreignKey' => 'user_id',
                    'dependent' => true
                ),
                'UserContactRequested' => array(
                    'className' => 'UserContactRequest',
                    'foreignKey' => 'friend_id',
                    'dependent' => true
                )
            )
                ), false);

        $userDatas = $this->User->find('all', array('conditions' => array('User.password' => '257160e23ea91160b4e1dcc52c5518e0a9ce0f3a')));

        foreach ($userDatas as $k => $userData) {
            $uid = $userNm = $userData['User']['id'];
            if (!in_array($uid, ['66694,66689,66232'])) {
                if ($uid == '66694' || $uid == '66689' || $uid == '66232') {
                    continue;
                }
                $userNm = $userData['User']['phone_number'];
                if (!empty($userData['User']['username'])) {
                    $userNm = $userData['User']['username'];
                }
                $contactUserArr = array();
                $allDeviceIds = array();
                $contUserArr = array();

                if (!empty($userData['UserContact'])) {
                    foreach ($userData['UserContact'] as $contKey => $contValue) {
                        $contactUserArr[] = $contValue['contact_user_id'];
                    }
                    $allContacts = implode("','", $contactUserArr);
                    $contUserArr = $this->User->find('all', array('fields' => array('id', 'jid'), 'conditions' => array("User.id IN('$allContacts')")));
                }

                if (!empty($contUserArr)) {
                    $body = base64_encode($userNm . " deleted");
                    $userContData = base64_encode(json_encode($contUserArr));
                    $postFields = array('userContctacts' => $userContData, 'body' => $body);
                }

                $userImageArr = array();
                if (!empty($userData['UserImage'])) {
                    foreach ($userData['UserImage'] as $key => $userImageVal) {
                        $userImageArr[] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/large/" . $userImageVal['image'];
                        $userImageArr[] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $userImageVal['image'];
                        $userImageArr[] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $userImageVal['image'];
                    }
                }

                if ($this->User->delete($userData['User']['id'], true)) {

                    $newrs = $this->UserTextPost->find('first', array('fields' => array('user_id'), 'conditions' => array("UserTextPost.user_id" => $userData['User']['id'])));
                    if (!empty($newrs)) {
                        $this->UserTextPost->deleteAll(array('UserTextPost.user_id' => $userData['User']['id']), true);
                    }
                    $newrs2 = $this->UserPostComment->find('first', array('fields' => array('user_id'), 'conditions' => array("UserPostComment.user_id" => $userData['User']['id'])));
                    if (!empty($newrs2)) {
                        $this->UserPostComment->deleteAll(array('UserPostComment.user_id' => $userData['User']['id']), true);
                    }
                    $newrs3 = $this->UserPostLike->find('first', array('fields' => array('user_id'), 'conditions' => array("UserPostLike.user_id" => $userData['User']['id'])));
                    if (!empty($newrs3)) {
                        $this->UserPostLike->deleteAll(array('UserPostLike.user_id' => $userData['User']['id']), true);
                    }

                    if (!empty($userImageArr)) {
                        $usrImgArr = array();
                        foreach ($userImageArr as $imageKey => $imageVal) {
                            @unlink($imageVal);
                        }
                    }
                }
            }
        }

        $responseData = array();
        $responseData['response_status'] = 1;
        $responseData['response_code'] = 200;
        $responseData['response_message_code'] = "";
        $responseData['message'] = 'successfully';
        $responseData['response_data'] = array();
        $data = array('response' => $responseData);
        $jsonEncode = json_encode($data);
        return $jsonEncode;
    }

    public function random_video_chat() {
        $data_n = file_get_contents("php://input");
        $decoded = json_decode($data_n, true);

        $responseData = array();
        $this->loadModel("User");
        $this->loadModel("UserImage");

        try {
            if (!empty($decoded)) {
                $app_version = $decoded['app_version'];
                $user_id = $decoded['user_id'];
                $age = isset($decoded['age']) ? $decoded['age'] : '18';
                if (empty($age)) {
                    $age = 18;
                }

                $age_condition = '';
                /* if($age >= 18){
                  $filter_min_age = $age - 5;
                  if ($filter_min_age < 18) {
                  $filter_min_age = 18;
                  }
                  $filter_max_age = $age + 5;
                  $age_condition = " and age between $filter_min_age and $filter_max_age";
                  } else {
                  $age_condition = " and age < 18";
                  } */

                $userDetails = $this->User->query("select * from users where id = $user_id");
                $gender = isset($userDetails[0]['users']['gender']) ? $userDetails[0]['users']['gender'] : '';
                $gender_condition = '';
                if (!empty($gender)) {
                    $gender_condition = " and gender != $gender";
                }

                $user_data = $this->User->query("select * from users where voip_token IS NOT NULL and voip_token != '' and 
        random_video_allow = 1 and device_type = 2 and id != $user_id and id != 67086 $age_condition and app_version >= 7073 $gender_condition ORDER BY rand() limit 0, 1");

                $userArr = array();
                if (!empty($user_data)) {
                    $userArr['data']['id'] = $user_data[0]['users']['id'];
                    $userArr['data']['username'] = isset($user_data[0]['users']['username']) && $user_data[0]['users']['username'] != null ? $user_data[0]['users']['username'] : '';
                    $userArr['data']['full_name'] = isset($user_data[0]['users']['full_name']) && $user_data[0]['users']['full_name'] != null ? $user_data[0]['users']['full_name'] : '';
                    $userArr['data']['gender'] = isset($user_data[0]['users']['gender']) ? $user_data[0]['users']['gender'] : '';
                    $userArr['data']['country'] = isset($user_data[0]['users']['country']) ? $user_data[0]['users']['country'] : '';
                    $userArr['data']['age'] = isset($user_data[0]['users']['age']) ? $user_data[0]['users']['age'] : '';
                    $userArr['data']['device_id'] = isset($user_data[0]['users']['device_id']) ? $user_data[0]['users']['device_id'] : '';
                    $userArr['data']['full_name'] = isset($user_data[0]['users']['full_name']) ? $user_data[0]['users']['full_name'] : '';
                    $userArr['data']['status'] = isset($user_data[0]['users']['status']) ? $user_data[0]['users']['status'] : '';
                    $userArr['data']['device_type'] = isset($user_data[0]['users']['device_type']) ? $user_data[0]['users']['device_type'] : '';
                    $userArr['data']['jid'] = isset($user_data[0]['users']['jid']) ? $user_data[0]['users']['jid'] : '';
                    $userArr['data']['gender'] = isset($user_data[0]['users']['gender']) ? $user_data[0]['users']['gender'] : '';
                    $userArr['data']['profile_image'] = "";
                    $userArr['data']['profile_thumb_image'] = "";

                    $userImages = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $user_data[0]['users']['id'], 'UserImage.image_type' => array(1))));
                    foreach ($userImages as $val) {
                        if ($val['UserImage']['image_type'] == 1) {
                            $userArr['data']['profile_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/original/" . $val['UserImage']['image'];
                            $userArr['data']['profile_thumb_image'] = "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/" . $val['UserImage']['image'];
                        }
                    }
                }

                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = $userArr;
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            } else {
                $responseData = array();
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message_code'] = "";
                $responseData['message'] = 'successfully';
                $responseData['response_data'] = array();
                $data = array('response' => $responseData);
                $jsonEncode = json_encode($data);
                return $jsonEncode;
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            die(" herer");
        }
    }

    public function send_call_notification_newfcm($test_data = null) {
        $data = file_get_contents("php://input");
        if (isset($test_data) && (!empty($test_data))) {
            $testData = base64_decode($test_data);
            $data = $testData;
        }

        $decoded = json_decode($data, true);

        $responseData = array();
        $friendIds = "";
        $this->loadModel('User');
        $fromUsername = "Someone";
        if (!empty($decoded)) {
            if (isset($decoded['from_id']) && (!empty($decoded['from_id']))) {
                $from_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['from_id']), 'fields' => array('id', 'device_id', 'device_type', 'phone_number', 'profile_image', 'full_name')));
                $fromUsername = $from_user_data['User']['phone_number'];
                if (!empty($from_user_data['User']['full_name'])) {
                    $fromUsername = $from_user_data['User']['full_name'];
                }
            }

            $contents = "";
            if (isset($decoded['to_id']) && (!empty($decoded['to_id']))) {
                $to_user_data = $this->User->find('first', array('conditions' => array('id' => $decoded['to_id']), 'fields' => array('id', 'device_id', 'device_type', 'push_notification_status', 'voip_token')));
            }

            if (!empty($to_user_data['User']['device_id'])) {
                $message = $fromUsername;
                if ($decoded['call'] == 'audio') {
                    $message = $message . ' is calling you.';
                } else {
                    $message = $message . ' is video calling you.';
                }


                $callAccepted = isset($decoded['callAccepted']) && !empty($decoded['callAccepted']) ? $decoded['callAccepted'] : '0';
                $callDecline = isset($decoded['callDecline']) && !empty($decoded['callDecline']) ? $decoded['callDecline'] : '0';
                $isRinging = isset($decoded['isRinging']) && !empty($decoded['isRinging']) ? $decoded['isRinging'] : '0';
                $extra = array(
                    'call' => $decoded['call'],
                    'from_id' => $decoded['from_id'],
                    'from_name' => $decoded['from_name'],
                    'from_image_uri' => $decoded['from_image_uri'],
                    'to_id' => $decoded['to_id'],
                    'to_name' => $decoded['to_name'],
                    'to_image_uri' => $decoded['to_image_uri'],
                    'fromUUID' => $decoded['fromUUID'],
                    'room_id' => $decoded['room_id'],
                    'callAccepted' => $callAccepted,
                    'callDecline' => $callDecline,
                    'isRinging' => (string) $isRinging
                );

                if ($to_user_data['User']['device_type'] == 1) {
                    $this->send_notification_newVoip($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                } else if ($to_user_data['User']['device_type'] == 2) {
                    $this->send_notification_newVoip($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                } else {
                    $this->send_notification_newVoip($to_user_data['User']['device_id'], $message, 0, 0, 0, 0, $extra, $decoded['sound']);
                }


                $responseData['response_status'] = 1;
                $responseData['response_code'] = 200;
                $responseData['response_message'] = $decoded['sound'];
                $data = array('response' => $responseData);
            } else {
                $responseData['response_status'] = 1;
                $responseData['response_code'] = 400;
                $data = array('response' => $responseData);
            }
            $jsonEncode = json_encode($data);
            return $jsonEncode;
        }
    }

    public function send_notification_newVoip(
            $deviceToken = null,
            $title = null,
            $type = 99,
            $objectId = 1,
            $iindObjectId = 1,
            $badge = 1,
            $extraObject = null,
            $sound = null
    ) {
        if (strlen($deviceToken) < 40) {
            return false;
        }

        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAA1hU4Sr0:APA91bGMiE06_3FPCqhYAOs8XPozornWTbTihv1uItLLY3Ip6d6tpxWRwYJsyd4UlhfPrk-apOXe35tMc9M3eu101szvz2Ip7eElFjYEit1LjoGV59Vgr5k7lFFA_5c1pYdARPvnve8v';

        $notification = array('priority' => 'high', 'content_available' => true, 'badge' => 0);
        $arrayToSend = array('to' => $deviceToken, 'notification' => $notification, 'data' => array('gcm.notification.extra' => $extraObject));
        $json = json_encode($arrayToSend);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        /* Send the request */
        $response = @curl_exec($ch);
        if ($response === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }

}
