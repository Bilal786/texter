<?php

/**
 * Category
 *
 * PHP version 5
 *
 * @category Model
 *
 */
class Category extends AppModel {

    /**
     * Model name
     *
     * @public  string
     * @access public
     */
    public $name = 'Category';

    /**
     * Behaviors used by the Model
     *
     * @public  array
     * @access public
     */
    public $actsAs = array(
        'Multivalidatable', 'Tree'
    );
    public $belongsTo = array(
        'Parent' => array(
            'className' => 'Category',
            'foreignKey' => 'parent_id',
            'fields' => array('name')
        )
    );

    /**
     * Custom validation rulesets
     *
     * @public  array
     * @access public
     */
    public $validationSets = array(
        'admin' => array(
            'name' => array(
                'checkWhiteSpaces' => array(
                    'rule' => array('checkWhiteSpace', 'name'),
                    'message' => 'Name should not contain white spaces on left and right side of string.'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Name is required'
                ),
                'isUnique' => array(
                    'rule' => array('checkAlreadyExist'),
                    'message' => 'Category is already exist.'
                )
            )
        )
    );

    /*
     * get all category list
     */

    function parentsList() {
        $data = $this->find('list', array(
            'conditions' => array('Category.status' => Configure::read('App.Status.active')),
            'conditions' => array('parent_id' => 0)
        ));
        return $data;
    }

    function groupList() {
        $data = $this->find('list', array(
            "fields" => array("Category.id", "Category.name", "Parent.name"),
            "joins" => array(
                array(
                    "table" => "categories",
                    "alias" => "Parent",
                    "type" => "INNER",
                    "conditions" => array("Parent.id = Category.parent_id")
                )
            ),
            "order" => array('Category.id ASC') // whatever ordering you want
        ));

        return $data;
    }

    function get_category_list() {
        $categories = $this->find('list', array('conditions' => array('Category.status' => Configure::read('App.Status.active'))));
        return $categories;
    }

    public function get_categories($type, $fields = '*', $cond = array(), $order = 'Category.id desc', $limit = 999, $offset = 0) {
        $categories = $this->find($type, array('conditions' => array('Category.status' => Configure::read('App.Status.active'), $cond), 'fields' => array($fields), 'order' => array($order), 'offset' => $offset, 'limit' => $limit));

        return $categories;
    }

    public function getCategory($id, $type) {

        $data = $this->find($type, array(
            'conditions' => array('Category.status' => Configure::read('App.Status.active'), 'id' => $id),
            'order' => array('Category.name ASC'),
            'fields' => array('parent_id')
        ));
        return $data;
    }

    public function getSubCategory($id, $type) {

        $data = $this->find($type, array(
            'conditions' => array('Category.status' => Configure::read('App.Status.active'), 'Category.parent_id' => $id),
            'order' => array('Category.name ASC'),
            'fields' => array('parent_id')
        ));
        return $data;
    }

    public function displayCategoryTree($pid, $level) {
        global $res;
        $blank = "";
        $parents = $this->find('list', array('conditions' => array('Category.parent_id' => $pid, 'Category.status' => Configure::read('App.Status.active'))));
        return $parents;
        
    }

    public function displayOnlyCategoryFront() {
        $mainCat = $this->find('list', array('conditions' => array('Category.parent_id' => 0, 'Category.status' => Configure::read('App.Status.active'))));
        $mainReturnArray = array();
        foreach ($mainCat as $k => $v) {
            $subCat = array();
            $subCat = $this->find('list', array('conditions' => array('Category.parent_id' => $k)));


            $mainReturnArray[$k][$v] = $subCat;
        }
        return $mainReturnArray;
    }

    public function displayCategoryTreeFront($pid, $level, $conditions) {
        global $res;

        $blank = "";
        $class = '';

        for ($i = 0; $i < $level; $i++)
            $blank .= "__";

        $parents = $this->find('all', array('conditions' => array('Category.parent_id' => $pid, 'Category.status' => Configure::read('App.Status.active'), $conditions)));
        if ($pid == 0) {
            $class = "parent_cat";
        } else {
            $class = "child_cat";
        }

        if (!empty($parents))
            $level++;
        $id = '';
        foreach ($parents as $value) {
            if ($value['Category']['parent_id'] == 0) {
                $id = $value['Category']['id'];
            } else {
                $id = $value['Category']['parent_id'];
            }
            
            $res[$value['Category']['id']] = $blank . $value['Category']['name'];
            $this->displayCategoryTreeFront($value['Category']['id'], $level, $conditions);
        }

        return $res;
    }

    public function checkAlreadyExist() {
		
        $data = array();
		if(isset($this->data['Category']['id'])&& empty($this->data['Category']['id'])){
			if(isset($this->data['Category']['parent_id'])&& empty($this->data['Category']['parent_id'])){
			  $data = $this->find('count', array('conditions' => array('Category.name' => $this->data['Category']['name'],'Category.parent_id'=>0)));
				if(empty($data))
				{
					return true;
				}else{
				
					return false;
				}
			
			}else{
			
				$data = $this->find('count', array('conditions' => array('Category.name' => $this->data['Category']['name'],'Category.parent_id'=>$this->data['Category']['parent_id'])));
				
				if(empty($data))
				{
					return true;
				}else{
				
					return false;
				}
			}	
		
		}else{
			
			if(isset($this->data['Category']['parent_id'])&& empty($this->data['Category']['parent_id'])){
			  $data = $this->find('count', array('conditions' => array('Category.name' => $this->data['Category']['name'],'Category.parent_id'=>0,'Category.id'=>$this->data['Category']['id'])));
				if(empty($data))
				{
					return true;
				}else{				
					$data = $this->find('count', array('conditions' => array('Category.name' => $this->data['Category']['name'],'Category.parent_id'=>0)));
					if(empty($data))
					{
						return true;
					}else{
					
						return false;
					}
				}
			
			}else{
			
			 $data = $this->find('count', array('conditions' => array('Category.name' => $this->data['Category']['name'],'Category.parent_id'=>$this->data['Category']['parent_id'],'Category.id'=>$this->data['Category']['id'])));
			
				if(!empty($data))
				{
					return true;
				}else{
					$data = $this->find('count', array('conditions' => array('Category.name' => $this->data['Category']['name'],'Category.parent_id'=>$this->data['Category']['parent_id'])));
					if(empty($data))
					{
						return true;
					}else{
					
						return false;
					}
				
				
				}
			}
		}
   
    }
	
    function get_categories_front($type_for = 0) {
        $this->unbindModel(array('belongsTo' => array('Parent')), false);
        $this->bindModel(array(
            'hasMany' => array(
                'Child' => array(
                    'className' => 'Category',
                    'foreignKey' => 'parent_id',
                    'conditions' => array('parent_id != ' => 0),
                    'fields' => array('Child.name', 'Child.id')
                )
            )
                ), false);
        $data = $this->find('all', array('conditions' => array('Category.status' => Configure::read('App.Status.active'), 'Category.parent_id' => 0), 'fields' => array('Category.name', 'Category.id')));
        return $data;
    }

    function get_parent_categories_front($type_for = 0) {
        $this->unbindModel(array('belongsTo' => array('Parent')), false);

        $data = $this->find('all', array('conditions' => array('Category.status' => Configure::read('App.Status.active'), 'Category.parent_id' => 0), 'fields' => array('Category.name', 'Category.id')));
        return $data;
    }

	



	
}