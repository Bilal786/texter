<?php

Class Message extends AppModel {

    Public $name = 'Message';
    public $actsAs = array(
        'Multivalidatable'
    );
    var $validationSets = array(
        'admin' => array(
            'group_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Group is required'
                )
            ),
            'member_id' => array(
                'notEmpty' => array(
                    'rule' => array('multiple', array('min' => 1)),
                    'message' => 'Choose at least one Member'
                )
            ),
            'message' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Message is required.'
                )
            )
        )
    );
}

?>