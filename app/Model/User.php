<?php

/**
 * User
 *
 * PHP version 5

 *
 */
class User extends AppModel {

    /**
     * Model name
     *
     * @var string
     * @access public
     */
    var $name = 'User';

    /**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
    var $actsAs = array(
        'Multivalidatable'
    );

    /**
     * Custom validation rulesets
     *
     * @var array
     * @access public
     */
    var $validationSets = array(
        'admin' => array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Username is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Username already exists.'
                ) ,
                'minlength' => array(
                    'rule' => array('minLength', 5),
                    'message' => 'Username must be atleast 5 characters long.'
                )/* ,
				'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                )
                ,
                'maxlength' => array(
                    'rule' => array('maxlength', 18),
                    'message' => 'Username Maxlenght 18 character.'
                ) */,
                'check_string' => array(
                    'rule' => array('check_string'),
                    'message' => 'Please start from string.'
                ), 'checkWhiteSpaces' => array(
                    'rule' => array('checkWhiteSpace', 'username'),
                    'message' => 'No white spaces on left and right side of string.'
                )
            ),
            'password2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Password is required'
                ),
                'minlength' => array(
                    'rule' => array('minLength', 8),
                    'message' => 'Password must be atleast 8 characters long.'
                )/* ,
                  'maxlength' => array(
                  'rule'	=> 	array('maxlength', 12),
                  'message'	=>	'Password no long from 12 charcter.'
                  ) */,
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'password2'),
                    'message' => 'Password should not have white space at both ends'
                )
            ),
            'confirm_password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Confirm Password is required'
                ),
                'identicalFieldValues' => array(
                    'rule' => array('identicalFieldValues', 'password2'),
                    'message' => 'Password does not match.'
                )
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Email already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'email'),
                    'message' => 'Email should not have white space at both ends'
                )
            ),
			/*
            'first_name' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'allowEmpty' => true,
					'message' => 'First name is required.'
				),
				'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                ),
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'first_name'),
					'message' => 'No white spaces on left and right side of string.'
				)
            ),
			'last_name' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'allowEmpty' => true,
					'message' => 'Last name is required.'
				),
				'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                ),
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'last_name'),
					'message' => 'No white spaces on left and right side of string.'
				)
            ),
			*/
            'display_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Display Name is required'
                ),
				'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                ),
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'display_name'),
					'message' => 'No white spaces on left and right side of string.'
				)
            ),
            'phone_number' => array(                
                    'rule' => '/^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}(\s*(ext|x)\s*\.?:?\s*([0-9]+))?$/',
                    'message' => 'Invalid Phone Number.',
                    'allowEmpty' => true
              
            ),
            'gender' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Gender is required.'
                )
            ),
        ),
		'reset_password'	=>	array(
			'password'=>array(
			'R1'=>array(
			'rule'=>'notEmpty',
			'message' => 'Password is required.'
			)

			),
			'password2'=>array(
			'identicalFieldValues' => array(
			'rule' => array('identicalFieldValues', 'password' ),
			'message' => 'Passwords does not match.'
			),
			'R2'=>array(
			'rule'=>'notEmpty',
			'message' => 'Confirm password is required.'
			)
			)
		),
			'mobile_change_password' => array(
				'oldpassword' => array(
					'PasswordMatch' => array(
						'rule' => 'PasswordMatch',
						'required' => true,
						'message' => 'Wrong current password. Please try again.'
					),
					'notEmpty' => array(
						'rule' => 'notEmpty',
						'required' => true,
						'message' => 'Please enter current password.'
					)
				),
				'newpassword2' => array(
					'minLength'=>array(
							'rule'=>array('minLength', 8),
							'message'=>'Invalid new password. Please choose a new password with minimum 8 characters.'
					),
					'notEmpty' => array(
						'rule' => 'notEmpty',
						'required' => true,
						'message' => 'Please enter new password.'
					)
				)
			),
        'admin_change_password' => array(
            'new_password' => array(                
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'New password is required.'
                ),
				'minLength' => array(
                    'rule' => array('minLength', 8),
                    'message' => 'Passwords must be at least 8 characters long.'
                ),
            ),
            'confirm_password' => array(                
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'confirm password is required.'
                ),
				'minLength' => array(
                    'rule' => array('minLength', 8),
                    'message' => 'confirm password must be at least 8 characters long.'
                ),
				'match_password' => array(
                    'rule' => 'adminConfirmPassword',
                    'message' => 'confirm password should be same as new password.'
                )
            )
        ),
        'change_password' => array(
            'password2' => array(                
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'New password is required.'
                ),
				'minLength' => array(
                    'rule' => array('minLength', 8),
                    'message' => 'Passwords must be at least 8 characters long.'
                ),
            )
        ),
		'forgot_password' => array(
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'email'),
                    'message' => 'Email should not have white space at both ends'
                )
            )
        ),
		'register' => array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Username is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Username already exists.'
                ),
				/* 'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                ), */
                'check_string' => array(
                    'rule' => array('check_string'),
                    'message' => 'Please start from string.'
                ), 
				'checkWhiteSpaces' => array(
                    'rule' => array('checkWhiteSpace', 'username'),
                    'message' => 'No white spaces on left and right side of string.'
                )
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Email already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'email'),
                    'message' => 'Email should not have white space at both ends'
                )
            ),
            'display_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Display Name is required'
                ),
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'display_name'),
					'message' => 'No white spaces on left and right side of string.'
				)
            ),
            'gender' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Gender is required.'
                )
            ),
        ),
		'register_mobile_app' => array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Username is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'username already exists.'
                )
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Email Id already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.'
                )
            ),
            'display_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Display Name is required'
                ),
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'display_name'),
					'message' => 'No white spaces on left and right side of string.'
				),
				'minLength' => array(
                    'rule' => array('minLength', 6),
                    'message' => 'name is less than 6 characters long'
                )
            ),
            'gender' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Gender is required.'
                )
            ),
            'password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Gender is required.'
                ),
				'minLength' => array(
                    'rule' => array('minLength', 6),
                    'message' => 'password is less than 6 characters long'
                )
            ),
        ),
		'update_profile' => array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Username is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Username already exists.'
                ),
				/* 'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                ), */
                'check_string' => array(
                    'rule' => array('check_string'),
                    'message' => 'Please start from string.'
                ), 
				'checkWhiteSpaces' => array(
                    'rule' => array('checkWhiteSpace', 'username'),
                    'message' => 'No white spaces on left and right side of string.'
                )
            ),
            'first_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'First name is required'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'first_name'),
                    'message' => 'Email should not have white space at both ends'
                )
            ),
            'gender' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'gender is required'
                )
            ),
			'dob' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please enter your birthday.'
                )
            ),			
			/* 'profile_image' => array(
				'rule' => 'savePicture',
				'message' => 'Please supply a valid image.',
				'allowEmpty' => true
			), */
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Email already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'email'),
                    'message' => 'Email should not have white space at both ends'
                )
            ),
			'password2' => array(
				'minLength'=>array(
						'rule'=>array('minLength', 8),
						'message'=>'Please choose a new password with minimum 8 characters.',
						'allowEmpty' => true
				)
			),
            'alternate_email' => array(                
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.',
                    'allowEmpty' => true
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'alternate_email'),
                    'message' => 'Email should not have white space at both ends',
                    'allowEmpty' => true
                )
            ),
            'phone_number' => array(                
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'phone_number'),
					'message' => 'No white spaces on left and right side of string.',
					'allowEmpty' => true
				),
                'minlength' => array(
                    'rule' => array('minLength', 8),
                    'message' => 'phone number must be atleast 8 digit long.',
					'allowEmpty' => true
                )
            ),
            'alternate_phone_number' => array(
				'checkWhiteSpaces' => array(
					'rule' => array('checkWhiteSpace', 'alternate_phone_number'),
					'message' => 'No white spaces on left and right side of string.',
                    'allowEmpty' => true
				),
                'minlength' => array(
                    'rule' => array('minLength', 8),
                    'message' => 'phone number must be atleast 8 digit long.',
                    'allowEmpty' => true
                )
            )
        ),
		'check_required_fields' => array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Username is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Username already exists.'
                ),
				/* 'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'message' => 'Alpha-Numeric characters only.'
                ), */
                'check_string' => array(
                    'rule' => array('check_string'),
                    'message' => 'Please start from string.'
                ), 
				'checkWhiteSpaces' => array(
                    'rule' => array('checkWhiteSpace', 'username'),
                    'message' => 'No white spaces on left and right side of string.'
                )
            ),
            'first_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'First name is required'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'first_name'),
                    'message' => 'Email should not have white space at both ends'
                )
            ),
            'gender' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'gender is required'
                )
            ),
			'dob' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please enter your birthday.'
                )
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Email already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Invalid Email.'
                ),
                'checkWhiteSpace' => array(
                    'rule' => array('checkWhiteSpace', 'email'),
                    'message' => 'Email should not have white space at both ends'
                )
            ),
			'password2' => array(
				'minLength'=>array(
						'rule'=>array('minLength', 8),
						'message'=>'Please choose a new password with minimum 8 characters.',
						'allowEmpty' => true
				)
			)
        )
		
    );
	
	public function savePicture($data)
	{
	//pr($data);die('test');
		if(!empty($data['profile_image']['name']))
		{
			$file = $data['profile_image'];
			if($file['type'] == 'image/jpeg' || $file['type'] == 'image/png' || $file['type'] == 'image/gif' || $file['type'] == 'image/jpg' )
			{
				if($file['size'] <= 2097152)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	public function save_cover_image($data)
	{
	//pr($data);die('test');
		if(!empty($data['profile_cover_image']['name']))
		{
			$file = $data['profile_cover_image'];
			if($file['type'] == 'image/jpeg' || $file['type'] == 'image/png' || $file['type'] == 'image/gif' || $file['type'] == 'image/jpg' )
			{
				if($file['size'] <= 2097152)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	public function checkbirthday($data)
	{
		//pr($data);die('octal');
		
		if(!empty($data['dob']))
		{
			return true;
		}
		else
		{
			return true;
		}
	}
    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $parameters = compact('conditions');
        $this->recursive = $recursive;
        $count = $this->find('count', array_merge($parameters, $extra));

        if (isset($extra['group'])) {

            $count = $this->getAffectedRows();
        }
        return $count;
    }

    public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        if (empty($order)) {
            $order = array($extra['passit']['sort'] => $extra['passit']['direction']);
        }
        if (isset($extra['group'])) {
            $group = $extra['group'];
        }
        if (isset($extra['joins'])) {
            $joins = $extra['joins'];
        }
        return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group', 'joins'));
    }

    /* check for identical values in field */

    function identicalFieldValues($field = array(), $compare_field = null) {
        foreach ($field as $key => $value) {
            $v1 = $value;
            $v2 = $this->data[$this->name][$compare_field];

            if ($v1 !== $v2) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }
	
	## Function Password Match ##
	function PasswordMatch($data){
		//pr($this->data);die;
		$op =  Security::hash($this->data['User']['oldpassword'], null, true);
		
		//echo $this->data['User']['hidden_password'];
		//echo "<br>".$this->data['User']['hidden_password'];die;
			//echo $this->data['User']['hidden_password']."<br>".$op;die;
		
		if ($this->data['User']['hidden_password'] != $op)
		{
			//$this->data['User']['hidden_password'];
			//die("RAM");
			return false;   
		}
		return true;   
	}
	
	
    /* check confirm password */
    function adminConfirmPassword() {
        if (!empty($this->data['User']['new_password'])) {
            if ($this->data['User']['new_password'] != $this->data['User']['confirm_password']) 			{
                return false;
            } else {
                return true;
            }
        }
    }
	
    /* check confirm password */
    function confirmPassword() {
        if (!empty($this->data['User']['user_password'])) {
            if ($this->data['User']['user_password'] != $this->data['User']['confirm_password']) 			{
                return false;
            } else {
                return true;
            }
        }
    }

    /* check existing email */

    function checkEmail($data = null, $field = null) {
        if (!empty($field)) {
            if (!empty($this->data[$this->name][$field])) {
                if ($this->hasAny(array('User.email' => $this->data[$this->name][$field], 'User.status' => Configure::read('App.Status.active')))) {
                    return true;
                } elseif ($this->hasAny(array('User.username' => $this->data[$this->name][$field], 'User.status' => Configure::read('App.Status.active')))) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /* check old password */

   /*  function checkOldPassword($field = array(), $password = null) {
        App::uses('CakeSession', 'Model/Datasource');
        $userId = CakeSession::read('Auth.User.id');
        $count = $this->find('count', array('conditions' => array(
                'User.password' => Security::hash($field[$password], null, true),
                'User.id' => $userId
        )));
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    } */

    function beforeValidate($options = array()) {
        foreach ($this->hasAndBelongsToMany as $k => $v) {
            if (isset($this->data[$k][$k])) {
                $this->data[$this->alias][$k] = $this->data[$k][$k];
            }
        }
    }

    function check_string($field = array()) {
        $user = $field['username'];
        $value = substr($user, 0, 1);

        if (preg_match('/[A-Za-z]$/', $value) == true) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    public function get_users($type, $fields = '*', $cond = array(), $order = 'User.id desc', $limit = 999, $offset = 0) {
        $users = $this->find($type, array('conditions' => array($cond), 'fields' => array($fields), 'order' => array($order), 'offset' => $offset, 'limit' => $limit));

        return $users;
    }

    		
	function apple_push_notification($device_token = null, $msg = null, $requestType = null, $sound_status = null){
			//$device_token = "5b5b552588706d7a8ac96c34241293ecff2099228753a963fdb59d1e4159a3fe";		
			App::import('Vendor', 'push', array('file' => 'push/urbanairship.php'));
			$APP_MASTER_SECRET  = "Gu-TyT06Qn2q7twy9pCa-g";
			$APP_KEY 			= "t_uXfVmtTdKg3uM4zk1sRA";	
			$airship = new Airship($APP_KEY, $APP_MASTER_SECRET);
			$token = $device_token;
			$message = array('aps'=>array('alert'=>$msg));
			$arr=$airship->push($message, $token, array(), array('testTag'), $requestType, $sound_status);
	}
		
	function android_push_notification($device_tokens = array(), $msg = null){
/* 			$registatoin_ids = "APA91bGJTN8G9B8aCs-5rY7tb5qFUP5yQhHFB5Ov9qkbwQCDWAO4ITmda2QwG2lmueWZs9_zb-x8E25qusVFx56E4pgJ1K47ezCpeg4jJC7hXpsRTLjh7w1e8hfkesBSIItQqVRiCYhyU47wj9IfRw-0EcsYHWi5bw";	 */		
			
			App::import('Vendor', 'send_notification', array('file' => 'gcm_server_php/GCM.php'));
			$gcm = new GCM();
			$result = $gcm->send_notification($device_tokens, $msg);
	}


}

