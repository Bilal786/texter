<?php
App::uses('AppModel', 'Model');

class UserTextPost extends AppModel
{

    public $name  = 'UserTextPost';

    public $actsAs = array(
        'Multivalidatable'
    );

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'fields' => array('id', 'username', 'full_name')
        )
    );

    // public $hasOne = array(
    //     'UserImage' => array(
    //         'className' => 'UserImage',
    //         'foreignKey' => 'user_id',
    //         'fields' => array('id', 'user_id', 'image', 'image_type', 'lat', 'lon', 'category_idx', 'status'),
    //         'conditions' => array('UserImage.image_type' => '1'),
    //         'order' => 'UserImage.created DESC'            
    //     )
    // );

    public $hasMany = array(
        'UserPostLike' => array(
            'className' => 'UserPostLike',
            'foreignKey' => 'post_id',
        ),
        'UserPostComment' => array(
            'className' => 'UserPostComment',
            'foreignKey' => 'post_id',
        ),
        'UserPostReport' => array(
            'className' => 'UserPostReport',
            'foreignKey' => 'post_id',
        )
    );
}
