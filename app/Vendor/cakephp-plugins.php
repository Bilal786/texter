<?php
$baseDir = dirname(dirname(__FILE__));

return [
    'plugins' => [
        'Bake' => $baseDir . '/Vendor/cakephp/bake/',
        'Cake/TwigView' => $baseDir . '/Vendor/cakephp/twig-view/',
        'DebugKit' => $baseDir . '/Vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/Vendor/cakephp/migrations/',
    ],
];
