<ul class="shortcut-buttons-set">
	<li><a class="shortcut-button" href="<?php echo (Router::url(array('controller'=>'admins','action'=>'index')));?>" style="min-height:126px;"><span style="border:none;">
		<?php echo $this->Html->image('admin/icon-48-admin.png', array('alt'=>'icon','width'=>48,'height'=>48)); ?><br>
		Administrator
	</span></a></li>
<li><a class="shortcut-button" href="<?php echo (Router::url(array('controller'=>'users','action'=>'index')));?>" style="min-height:126px;"><span style="border:none;">
		<?php echo $this->Html->image('admin/icon-48-user.png', array('alt'=>'icon')); ?><br>
		Manage User
	</span></a></li>
	<li ><a class="shortcut-button"  href="<?php echo (Router::url(array('controller'=>'templates','action'=>'index')));?>" style="min-height:126px;"><span style="border:none;">
		<?php echo $this->Html->image('admin/email_templates.png', array('alt'=>'icon')); ?><br>
		Manage Email Templates
	</span></a></li>
	<li><a class="shortcut-button" href="<?php echo (Router::url(array('controller'=>'settings','action'=>'index')));?>" style="min-height:126px;"><span style="border:none;">
		<?php echo $this->Html->image('admin/cog-icon-2-48x48.png', array('alt'=>'icon')); ?><br>
		Manage Settings
	</span></a></li>
    <li><a class="shortcut-button" href="<?php echo (Router::url(array('controller'=>'notifications','action'=>'index')));?>" style="min-height:126px;"><span style="border:none;">
		<?php echo $this->Html->image('admin/notification.png', array('alt'=>'icon')); ?><br>
		Send Notification
	</span></a></li>
	<li><a class="shortcut-button" href="<?php echo (Router::url(array('controller'=>'admins','action'=>'logout')));?>" style="min-height:126px;"><span style="border:none;">
		<?php echo $this->Html->image('admin/1379602599_on-off.png', array('alt'=>'icon')); ?><br>
		Logout
	</span></a></li>
	<li>
      <a class="shortcut-button" href="javascript:void(0)" style="min-height:126px;">
        <span style="border:none;">
          <?php
          //echo "<pre>"; print_r($users['user']['vortex']['AppSetting']['status']); die;
          $checked = '';
          if(!empty($users['user']['vortex']) && $users['user']['vortex']['AppSetting']['status'] == 1) {
            $checked = "checked='checked'";
          }
          ?>
          <label class="switch vortex_option">
            <input type="checkbox" value="1" <?= $checked ?>>
            <span class="slider round"></span>
          </label>
            <br>
            Vortex
        </span>
      </a>
    </li>
	
</ul>
<div class="clear"></div>
<div class="content-box column-left">
				
	<div class="content-box-header">
		
		<h3 style="cursor: s-resize;">Users</h3>
		
	</div> <!-- End .content-box-header -->
	
	<div class="content-box-content">
		
		<div class="tab-content default-tab" style="display: block;">
			<ul class="shortcut-buttons-set">
							
				<li><a href="<?php echo (Router::url(array('controller'=>'users', 'action'=>'index',  'All')));?>" class="shortcut-button"><span>
					<div><?php echo($users['user']['tot']);?></div><br>
					Total Users
				</span></a></li>
				
				<li><a href="<?php echo (Router::url(array('controller'=>'users', 'action'=>'index',  'Active')));?>" class="shortcut-button"><span>
					<div><?php echo($users['user']['active']);?></div><br>
					Active Users
				</span></a></li>
	
				<li><a href="<?php echo (Router::url(array('controller'=>'users', 'action'=>'index',  'Inactive')));?>" class="shortcut-button"><span>
					<div><?php echo($users['user']['inactive']);?></div><br>
					Inactive Users
				</span></a></li>
	
			</ul>
			
		</div> <!-- End #tab3 --> 
		
	</div> <!-- End .content-box-content -->
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery('body').on('change', '.vortex_option input', function(){
        var checked = 0;
        if(jQuery(this).is(':checked') == true) {
          checked = 1;
        }
        jQuery.ajax({
            url: 'https://appservices.txtter.com/admin/admins/vortex',
            type:'POST',
            data: {
              checked: checked
            },
            beforeSend: function(){
            },
            success: function(response) {
              console.log("worked");
            }
        });
      });
    });
</script>