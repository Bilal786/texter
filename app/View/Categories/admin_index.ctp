<?php echo $this->Html->css('catlist', null, array('inline' => false));?>
<?php echo $this->Html->script('custom_cat.js', false); ?>
	<div class="content-box-content">
		<ul class="catList edit-cat-list-ul">
			<?php 
			//pr($data);die;
			foreach($data as $key=>$catVal){?>
				<li class="" id="<?php echo $catVal['Category']['id'];?>Cat">
				<span class="cat-list-prt-ttl"><?php echo $catVal['Category']['name'];?></span>
				<a href="javascript:void(0);<?php echo $catVal['Category']['id'];?>Cat" class="edit-cat-list" lang="<?php echo $catVal['Category']['id'];?>"></a>
				<a href="javascript:void(0);" class="delete-cat" lang="<?php echo $catVal['Category']['id'];?>"></a>
				<a href="javascript:void(0);" class="add_subcat" lang="<?php echo $catVal['Category']['id'];?>"></a>
				<span class="subcat_box" lang="<?php echo $catVal['Category']['id'];?>"></span>
					<?php if(!empty($catVal['Child'])){?>
					<ul style="display: block;" class="catList edit-cat-child-list-ul" id="<?php echo $catVal['Category']['id'];?>">
						<?php foreach($catVal['Child'] as $Childkey=>$ChildCatVal){?>
							<li id="<?php echo $ChildCatVal['id'];?>Cat">
								<span class="cat-list-child-ttl"><?php echo $ChildCatVal['name'];?></span>
								<a href="#<?php echo $ChildCatVal['id'];?>Cat" class="edit-cat-child-list" lang="<?php echo $ChildCatVal['id'];?>"></a>
								<a href="#" class="delete-cat" lang="<?php echo $ChildCatVal['id'];?>"></a>
							</li>
						<?php }?>						
					</ul>
					<?php }?>
				</li>
			<?php }?>
			
		</ul>
	</div>
<script type="text/javascript">
/* var controller = 'categories';
jQuery(document).ready(function(){
	init('#target<?php //echo($defaultTab);?>');
}); */
</script>