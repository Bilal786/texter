<?php
if (!empty($data)) {
    $this->ExPaginator->options = array('url' => $this->passedArgs);
    ?>
    <table class="wordwrap">

        <thead>
            <tr>
				<td width="10px"><input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="check-all"/></td>
				<td width="100px"><?php echo ($this->ExPaginator->sort('User.first_name', 'First name')) ?></td>
				<td width="100px"><?php echo ($this->ExPaginator->sort('User.last_name', 'Last name')) ?></td>
				<td width="100px"><?php echo ($this->ExPaginator->sort('User.role_id', 'Type')) ?></td>
                <td><?php echo ($this->ExPaginator->sort('User.username', 'Username')) ?></td>
                <td width="200px"><?php echo ($this->ExPaginator->sort('User.email', 'Email')) ?></td>
				
                <td width="100px"><?php echo ($this->ExPaginator->sort('User.created', 'Created on')) ?></td>
               
				<td><?php echo ($this->ExPaginator->sort('User.status', 'Status')) ?></td>
                <td width="100px">Action</td>
            </tr>

        </thead>
		
		<tfoot>
            <tr>
                <td colspan="6" <?php //echo ($this->params['pass'][0]=='artist' ? '9' : '8');               ?>">
					<?php
					if($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id')== Configure::read('App.Admin.role'))
					{
						if(count($data)>1)
						{
					?>
                    <div class="bulk-actions align-left">
						
                        <select name="data['User']['action']" id="UserAction<?php echo ($defaultTab); ?>">
                            <option selected="selected" value="">Choose an action...</option>
                            <option value="activate">Activate</option>
                            <option value="deactivate">Deactivate</option>
                            <option value="delete">Delete</option>
                        </select>
                        <?php echo ($this->Form->submit('Apply to selected', array('name' => 'activate', 'class' => 'button', 'div' => false, 'type' => 'button', "onclick" => "javascript:return validateChk('User','UserAction{$defaultTab}');"))); ?>
	
                    </div>
					<?php
						}
					}
					?>
                    <?php
                    $this->Paginator->options(array(
                        'url' => $this->passedArgs,
                    ));
                    echo $this->element('Admin/admin_pagination', array("paging_model_name" => "User", "total_title" => "Admins/SubAdmins"));
                    ?>

                </td>
            </tr>
        </tfoot>


        <tbody>

            <?php
            $alt = 0;
            foreach ($data as $value) {
                ?>
                <tr <?php
                echo ($alt == 0) ? 'class="alt-row"' : '';
                $alt = !$alt;
                ?>>
				<td><?php 
				if($value['User']['role_id']!= Configure::read('App.Admin.role'))
				{
				echo ($this->Form->checkbox('User.id.', array('value' => $value['User']['id'], 'hiddenField' => false))); 
				}
				?></td>
					<td><?php echo (ucfirst($value['User']['first_name'])); ?></td>
					<td><?php echo (ucfirst($value['User']['last_name'])); ?></td>
					<td><?php
					$type = "Sub Admin";					
					if($value['User']['role_id']==Configure::read('App.Admin.role'))
					{
						$type = "Admin";	
					}
					echo $type;
					
					?></td>
					
                    <td><b><?php echo ($this->Html->link($value['User']['username'], array('action' => 'view', $value['User']['id']), array('title' => 'View Details'))); ?></b></td>
                    <td><?php echo ($value['User']['email']); ?></td>
					
                    <td><?php echo ($this->Time->niceShort(strtotime($value['User']['created']))); ?></td>
                    
					<td><?php 
					if($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id')== Configure::read('App.Admin.role'))
					{
						if($value['User']['role_id']==Configure::read('App.Admin.role'))
						{
							echo ($this->Html->link($this->Layout->userStatus($value['User']['status']), 'javascript:;', array('title' => $value['User']['status'] == 0 ? 'activate' : $value['User']['status'] == 1 ? 'deactivate' : 'activate')));
						}
						else
						{
							echo ($this->Html->link($this->Layout->userStatus($value['User']['status']), array('action' => 'status', $value['User']['id'], 'token' => $this->params['_Token']['key']), array('title' => $value['User']['status'] == 0 ? 'activate' : $value['User']['status'] == 1 ? 'deactivate' : 'activate')));
						}
						
							
					}
					else
					{
						echo ($this->Html->link($this->Layout->userStatus($value['User']['status']), 'javascript:;', array('title' => $value['User']['status'] == 0 ? 'activate' : $value['User']['status'] == 1 ? 'deactivate' : 'activate')));
					}
					?>
                    </td>
                    <td>
                        <!-- Icons -->
                        <?php
						if($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id')== Configure::read('App.Admin.role'))
						{
                        echo ($this->Html->link($this->Html->image('admin/pencil.png', array('title' => 'Edit', 'alt' => 'Edit')), array('controller' => 'admins', 'action' => 'edit', $value['User']['id']), array('escape' => false)));
						
                        ?>
                        <?php
						if($value['User']['role_id'] ==  Configure::read('App.SubAdmin.role'))
						{
                        echo ($this->Html->link($this->Html->image('admin/cross.png', array('title'=>'Delete','alt'=>'Delete')), array('controller'=>'admins', 'action'=>'delete', $value['User']['id'],'token'=>$this->params['_Token']['key']), array('escape'=>false, 'onclick'=>'javascript:return confirm_delete(this)')));
						}
                        ?>
                        <?php
                        echo ($this->Html->link($this->Html->image('admin/hammer_screwdriver.png', array('title' => 'Change Password', 'alt' => 'Change Password')), array('controller' => 'admins', 'action' => 'change_password', $value['User']['id']), array('escape' => false)));
						}
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

        </tbody>

    </table>
    <?php
} else {
    echo ($this->element('admin_flash_info', array('message' => 'NO RESULTS FOUND')));
}
?>