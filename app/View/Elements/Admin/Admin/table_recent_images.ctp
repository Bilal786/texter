<?php
  if (!empty($data)) {
    //echo "<pre>"; print_r($data); die(" here");
    $this->ExPaginator->options = array('url' => $this->passedArgs);
  ?>
    <table class="wordwrap">
      <thead>
        <tr>
          <th><?= 'User Image' ?></th>
          <th><?= 'Image Type' ?></th>
          <th><?= 'Username' ?></th>
          <th><?= 'Uploaded Date' ?></th>
          <th width="120px">Action</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td colspan="6">
            <?php
            $this->Paginator->options(array(
                'url' => $this->passedArgs,
            ));
            echo $this->element('Admin/admin_pagination', array("paging_model_name" => "UserImage", "total_title" => "Recent Images"));
            ?>
          </td>
        </tr>
      </tfoot>
      <tbody>
        <?php
          $alt = 0;
          $n = 1;
          foreach ($data as $value) {
          ?>
            <tr <?php
              echo ($alt == 0) ? 'class="alt-row"' : '';
              $alt = !$alt;
              ?>>
              <td align="center">
                <?php
                  if(!empty($value['UserImage']['image']) && $value['UserImage']['image_type'] == '1'){
                ?>
                  <div class="recentimage-wrapper">
<!--                    <img src="<?php echo SITE_URL."/uploads/user_images/thumb/".$value['UserImage']['image'];?>" width="60" /><br/>-->
                    <img src="<?php echo "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/".$value['UserImage']['image'];?>" width="60" /><br/>
                  </div>
                <?php
                  }
                  else if(!empty($value['UserImage']['image']) && $value['UserImage']['image_type'] == '6'){
                ?>
                  <div class="recentimage-wrapper">
<!--                    <img src="<?php echo SITE_URL."/uploads/dating_pic/".$value['UserImage']['image'];?>" width="60" /><br/>-->
                    <img src="<?php echo "https://d35ugz1sdahij6.cloudfront.net/uploads/dating_pic/".$value['UserImage']['image'];?>" width="60" /><br/>
                  </div>
                <?php
                  }
                  else if(!empty($value['UserImage']['image']) && $value['UserImage']['image_type'] == '2'){
                ?>
                  <div class="recentimage-wrapper">
                    <!--<img src="<?php echo SITE_URL."/uploads/user_cover/thumb/".$value['UserImage']['image'];?>" width="60" /><br/>-->
                    <img src="<?php echo "https://d35ugz1sdahij6.cloudfront.net/uploads/user_cover/thumb/".$value['UserImage']['image'];?>" width="60" /><br/>
                  </div>
                <?php
                  }
                  else if(!empty($value['UserImage']['image'])) {
                ?>
                  <div class="recentimage-wrapper">
<!--                    <img src="<?php echo SITE_URL."/uploads/user_images/thumb/".$value['UserImage']['image'];?>" width="60" /><br/>-->
                    <img src="<?php echo "https://d35ugz1sdahij6.cloudfront.net/uploads/user_images/thumb/".$value['UserImage']['image'];?>" width="60" /><br/>
                  </div>
                <?php
                  }
                  else
                  {
                    echo $this->Html->image('no-picture.gif', array('alt'=>'img'  ,'id'=>"main_profile_photo", 'width'=>'60', 'height'=>'60'));
                  }
                ?>
              </td>
              <td align="center">
                <b>
                <?php 
                if($value['UserImage']['image_type'] == '1') {
                  echo "Profile Pic";
                }
                else if($value['UserImage']['image_type'] == '2') {
                   echo "profile Cover Pic";
                }
                else if($value['UserImage']['image_type'] == '6') {
                   echo "Dating Pic";
                } else {
                   echo "Shared Pic";
                }
                ?>
                </b>
              </td>
     
              <td align="center"><b><?php echo $value['UserDetails']['username']; // $this->Html->link($value['UserDetails']['username'], array('action' => 'view', $value['UserDetails']['id']), array('title' => 'View Details')); ?></b></td>
              <td align="center"><?= $this->Time->niceShort(strtotime($value['UserImage']['created'])); ?></td>
              <td align="center">
                <?php
                echo ($this->Html->link($this->Html->image('admin/cross.png', array('title' => 'Delete', 'alt' => 'Delete')), array('controller' => 'recent_images', 'action' => 'delete', $value['UserImage']['id']), array('escape' => false, 'onclick' => 'javascript:return confirm_delete(this)')));
                ?>
              </td>
            </tr>
        <?php
          $n++;
         }
        ?>
      </tbody>
    </table>
<?php
} else {
  echo ($this->element('admin_flash_info', array('message' => 'NO RESULTS FOUND')));
}
?>