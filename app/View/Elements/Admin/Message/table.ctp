<?php
if (!empty($data)) {
   
    $this->ExPaginator->options = array('url' => $this->passedArgs);
    ?>
    <table class="wordwrap">

        <thead>
            <tr>
               <!-- <th width="10px"><input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="check-all"/></th>-->
                <th><?php echo ($this->ExPaginator->sort('Message.sender_id', 'Sender')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('Message.receiver_id', 'Receiver')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('Message.message', 'Message')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('Message.created', 'Created')); ?></th>
                <th width="120px">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $alt = 0;
            //pr($data);die;
            foreach ($data as $value) {
//pr($value);
                ?>
                <tr <?php
                echo ($alt == 0) ? 'class="alt-row"' : '';
                $alt = !$alt;
                ?>>
					<?php /* ?>
                    <td><?php 
					if($value['Message']['email'] != 'info@flymigo.com')
					{
					echo ($this->Form->checkbox('Message.id.', array('value' => $value['Message']['id'], 'hiddenField' => false))); 
					}
					?></td>
					<?php  */?>
                    <td align="center">
					<?php echo ($value['Sender']['username']); ?><br>
					(<?php echo ($value['Sender']['phone_number']); ?>)
					</td>          
                    <td align="center">
					<?php echo ($value['Receiver']['username']); ?><br>
					(<?php echo ($value['Receiver']['phone_number']); ?>)
					</td> 
                    <td align="center"><?php echo ($value['Message']['message']); ?></td>
                    <td align="center"><?php echo date("Y-m-d h:i A" ,strtotime($value['Message']['created'])); ?></td>
                    <td align="center">
                        <!-- Icons -->
                        <?php
						
                       // echo ($this->Html->link($this->Html->image('admin/pencil.png', array('title' => 'Edit', 'alt' => 'Edit')), array('controller' => 'messages', 'action' => 'edit', $value['Message']['id']), array('escape' => false)));
                        ?>
                        <?php						
						echo ($this->Html->link($this->Html->image('admin/cross.png', array('title' => 'Delete', 'alt' => 'Delete')), array('controller' => 'messages', 'action' => 'delete_conversation', $value['Message']['conversation_id']), array('escape' => false), 'This operation delete all messages of this conversation. Are you sure?'));						
                        ?>
                        <?php
                       // echo ($this->Html->link($this->Html->image('admin/hammer_screwdriver.png', array('title' => 'Change Password', 'alt' => 'Change Password')), array('controller' => 'messages', 'action' => 'change_password', $value['Message']['id']), array('escape' => false)));
                        ?>
                        <?php					
                        echo ($this->Html->link($this->Html->image('admin/view.jpg', array('title' => 'View', 'alt' => 'View')), array('controller' => 'messages', 'action' => 'conversations', $value['Message']['conversation_id']), array('escape' => false)));
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

        </tbody>

    </table><?php
                    $this->Paginator->options(array('url' => $this->passedArgs));
                    echo $this->element('Admin/admin_pagination', array("paging_model_name" => "Message", "total_title" => "Messages"));
                    ?>
    <?php
} else {
    echo ($this->element('admin_flash_info', array('message' => 'NO RESULTS FOUND')));
}
?>