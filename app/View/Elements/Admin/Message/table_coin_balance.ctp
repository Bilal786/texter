<?php
if (!empty($data)) {
   
    $this->ExPaginator->options = array('url' => $this->passedArgs);
    ?>
    <table class="wordwrap">

        <thead>
            <tr>
                <th width="10px"><input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="check-all"/></th>
                <th><?php echo ($this->ExPaginator->sort('UserActivity.coin', 'User Coin')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('UserActivity.cost', 'Cost')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('UserActivity.type', 'Type')); ?></th>				
                <th><?php echo ($this->ExPaginator->sort('UserActivity.created', 'Created')) ?></th>
                <th><?php echo ($this->ExPaginator->sort('UserActivity.modified', 'Updated')) ?></th>
                <th width="97px">Action</th>
            </tr>

        </thead>
		
        <!--<tfoot>
            <tr>
                <td colspan="6">
					<?php
					/* if($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id')== Configure::read('App.Admin.role'))
					{  */
					?>
                    <div class="bulk-actions align-left">
						
                        <select name="data['UserActivity']['action']" id="UserAction<?php //echo ($defaultTab); ?>">
                            <option selected="selected" value="">Choose an action...</option>
                            <option value="activate">Activate</option>
                            <option value="deactivate">Deactivate</option>
                            <option value="delete">Delete</option>
                        </select>
                        <?php 
						
						/* echo ($this->Form->submit('Apply to selected', array('name' => 'activate', 'class' => 'button', 'div' => false, 'type' => 'button', "onclick" => "javascript:return validateChk('UserActivity','UserAction{$defaultTab}');"))); */ ?>
	
                    </div>
					<?php
					//}
					?>
                    <?php
                   /*  $this->Paginator->options(array(
                        'url' => $this->passedArgs,
                    ));
                    echo $this->element('Admin/admin_pagination', array("paging_model_name" => "UserActivity", "total_title" => "UserActivities")); */
                    ?>

                </td>
            </tr>
        </tfoot>-->
		
        <tbody>

            <?php
            $alt = 0;
            //pr($data);
            foreach ($data as $value) {
                ?>
                <tr <?php
                echo ($alt == 0) ? 'class="alt-row"' : '';
                $alt = !$alt;
                ?>>
                    <td>
					<?php 					
					echo ($this->Form->checkbox('UserActivity.id.', array('value' => $value['UserActivity']['id'], 'hiddenField' => false))); 
					?>
					</td>
                              
                    <td><b><?php echo ($value['UserActivity']['coins']); ?></b></td>

                    <td><?php 
							if(($value['UserActivity']['cost']=='0.00')||(empty($value['UserActivity']['cost']))){
							echo "Free!";
							}else{
							echo ($value['UserActivity']['cost']);
							}

					?></td>
					
                    <td><?php if($value['UserActivity']['type']==0){
									echo "Debit";
								}else{
									echo "Credit";
								}
						?>
					</td>
					
                    <td><?php 
					if(strtotime($value['UserActivity']['created']))
					{
						echo ($this->Time->niceShort(strtotime($value['UserActivity']['created']))); 
					}
					else
					{
						//echo date("M d, Y",strtotime(DEFAULT_DATE));
						echo ($this->Time->niceShort(strtotime(DEFAULT_DATE)));
					}
					?></td>
					

                    <td>
					<?php 					
						echo ($this->Html->link($this->Layout->userStatus($value['UserActivity']['status']), 'javascript:;', array('title' => $value['UserActivity']['status'] == 0 ? 'activate' : $value['UserActivity']['status'] == 1 ? 'deactivate' : 'activate')));					
					?>
                    </td>
					
                    <td>
                        <!-- Icons -->                        
                        <?php					
                        echo ($this->Html->link($this->Html->image('admin/view.jpg', array('title' => 'View', 'alt' => 'View')), array('controller' => 'users', 'action' => 'view_coin', $value['UserActivity']['id']), array('escape' => false)));
                        ?>						
                    </td>
                </tr>
                <?php
            }
            ?>
			<tr><td colspan="7"><strong>Total Balance: <?php echo $total_balace;?></strong></td></tr>
        </tbody>

    </table>
    <?php
} else {
    echo ($this->element('admin_flash_info', array('message' => 'NO RESULTS FOUND')));
}
?>

