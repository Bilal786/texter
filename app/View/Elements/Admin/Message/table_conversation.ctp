<?php
if (!empty($data)) {
   
    $this->ExPaginator->options = array('url' => $this->passedArgs);
    ?>
    <table class="wordwrap">

        <thead>
            <tr>
               <!-- <th width="10px"><input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="check-all"/></th>-->
                <th><?php echo ($this->ExPaginator->sort('Message.sender_id', 'Sender')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('Message.receiver_id', 'Receiver')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('Message.message', 'Message')); ?></th>
                <th><?php echo ($this->ExPaginator->sort('Message.created', 'Created')); ?></th>
                <!--<th width="120px">Action</th>-->
            </tr>
        </thead>
        <tbody>
            <?php
            $alt = 0;
            //pr($data);die;
            foreach ($data as $value) {
//pr($value);
                ?>
                <tr <?php
                echo ($alt == 0) ? 'class="alt-row"' : '';
                $alt = !$alt;
                ?>>
					<?php /* ?>
                    <td><?php 
					if($value['Message']['email'] != 'info@flymigo.com')
					{
					echo ($this->Form->checkbox('Message.id.', array('value' => $value['Message']['id'], 'hiddenField' => false))); 
					}
					?></td>
					<?php  */?>
                    <td align="center">
					<?php echo ($value['Sender']['username']); ?>
					</td>          
                    <td align="center">
					<?php echo ($value['Receiver']['username']); ?>
					</td> 
                    <td align="center"><?php echo ($value['Message']['message']); ?></td>
                    <td align="center"><?php echo date("Y-m-d h:i A" ,strtotime($value['Message']['created'])); ?></td>
                    <td align="center">
					<?php						
						echo ($this->Html->link($this->Html->image('admin/cross.png', array('title' => 'Delete', 'alt' => 'Delete')), array('controller' => 'messages', 'action' => 'delete', $value['Message']['id']), array('escape' => false), 'Are you sure to delete?'));						
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

        </tbody>

    </table><?php
                    $this->Paginator->options(array('url' => $this->passedArgs));
                    echo $this->element('Admin/admin_pagination', array("paging_model_name" => "Message", "total_title" => "Messages"));
                    ?>
    <?php
} else {
    echo ($this->element('admin_flash_info', array('message' => 'NO RESULTS FOUND')));
}
?>