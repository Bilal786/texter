<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
	<p>
		<?php  echo ($this->Form->input('message', array('type'=>'textarea', 'div'=>false, 'label'=>'Message*', "class" => "text-input small-input", 'rows'=>'5', 'cols'=>'10')));?> 
        <br><small><span id="chars">140</span> Characters.</small>
		
	</p>
<!--	<p>
		<?php // ($this->Form->input('user_type', array('type'=>'select', 'div'=>false, 'options' => [['0' => '--select--','1' => 'Old', '2' => 'New']], 'label'=>'User Type*', "class" => "text-input small-input")));?> 		
	</p>-->
    <p>
		<?php  echo ($this->Form->submit('Submit', array('class' => 'button', "div"=>false)));?>
		
		<?php echo $this->Html->link("Cancel", array('admin'=>true, 'controller'=>'admins', 'action'=>'dashboard'), array("class"=>"button", "escape"=>false)); ?>
		
	</p>
</fieldset>