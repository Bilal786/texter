<style type="text/css">
	.content-box-content input[type="text"] {
		background-color: #FFFFFF;
		background-image: none;
		border-radius: 0;
		height: 25px;
		width: 293px;
	}
</style>
<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
    <span class="input-notification error png_bg" id="invalid_price" style="position: relative; top: 115px; right: 430px;display: none;">Please enter valid integer value.</span>
    <?php 
    echo ($this->Form->input('id')); ?>	

   <p>
        <?php echo ($this->Form->input('plan_name', array('div' => false, 'label' => 'Plan name*', "class" => "text-input "))); ?> 

    </p>
<br/>
 <p>
        <?php echo ($this->Form->input('plan_price', array('div' => false, 'label' => 'Plan price (In $)*', 'type' => 'text', "class" => "text-input "))); ?> 

    </p>
 <p>
        <?php echo ($this->Form->input('credits', array('div' => false, 'label' => 'Credits', 'type' => 'text', "class" => "text-input "))); ?> 

    </p>
 <p>
        <?php echo ($this->Form->input('free_credits', array('div' => false, 'label' => 'Free credits', 'type' => 'text', "class" => "text-input "))); ?> 

    </p>

    <p>
        <?php echo ($this->Form->input('status', array('label' => 'Status', 'options' => array('1' => 'Active', '0' => 'Inactive'), 'div' => false, "class" => "small-input"))); ?> 

    </p>

    <p>
        <?php echo ($this->Form->submit('Submit', array('class' => 'button', "div" => false,'id'=>'btnSubmit'))); ?>

        <?php echo $this->Html->link("Cancel", array('admin' => true, 'controller' => 'subscription_plans', 'action' => 'index'), array("class" => "button", "escape" => false)); ?>

    </p>

</fieldset>
<script type="text/javascript">
   
   /* $(document).ready(function() {
        $('#btnSubmit').click(function() {
            var plan_price = $('#SubscriptionPlanPlanPrice').val();
           
             if (!isPositiveInteger(plan_price)) {
                $('#invalid_price').show();
                $(window).scrollTop('500');
                return false;
            }else{$('#invalid_price').hide();}
               
        });
    });
    function isPositiveInteger(val) {
        return val == "0" || ((val | 0) > 0 && val % 1 == 0);
    }*/
</script>