<?php
if (!empty($data)){
    $this->ExPaginator->options = array('url' => $this->passedArgs);
    ?>
    <table>
        <thead>
            <tr>
                <th><?php echo ($this->ExPaginator->sort('SubscriptionPlan.plan_name', 'Plan Name')) ?></th>
                <th><?php echo ($this->ExPaginator->sort('SubscriptionPlan.plan_price', 'Plan Price')) ?></th>
                <th><?php echo ($this->ExPaginator->sort('SubscriptionPlan.credits', 'Credit')) ?></th>
                <th><?php echo ($this->ExPaginator->sort('SubscriptionPlan.free_credits', 'Free credits')) ?></th>
                <th><?php echo ($this->ExPaginator->sort('SubscriptionPlan.total_credits', 'Total credits')) ?></th>
                <th><?php echo ($this->ExPaginator->sort('SubscriptionPlan.status', 'Status')) ?></th>
                <th width="16px">Action</th>
            </tr>

        </thead>

        <tfoot>
            <tr>
                <td colspan="4">
                    <div class="bulk-actions align-left">
                    </div>
                    <?php
                    $this->Paginator->options(array(
                        'url' => $this->passedArgs,
                    ));
                    echo $this->element('Admin/admin_pagination', array("paging_model_name" => "SubscriptionPlan", "total_title" => "SubscriptionPlan"));
                    ?>

                </td>
            </tr>
        </tfoot>

        <tbody>

            <?php
            $alt = 0;

            foreach ($data as $value) {
                ?>
                <tr <?php echo ($alt == 0) ? 'class="alt-row"' : '';
                $alt = !$alt;
                ?>>
                     <td><?php echo $value['SubscriptionPlan']['plan_name']; ?></td>	
                     <td><?php echo '$ '.number_format($value['SubscriptionPlan']['plan_price'],2); ?></td>
                     <td><?php echo $value['SubscriptionPlan']['credits']; ?></td>
                     <td><?php echo $value['SubscriptionPlan']['free_credits']; ?></td>
                     <td><?php echo $value['SubscriptionPlan']['total_credits']; ?></td>
                    <td>
					<?php 
                    echo ($this->Html->link($this->Layout->Status($value['SubscriptionPlan']['status']), array('action' => 'status', $value['SubscriptionPlan']['id'], 'token' => $this->params['_Token']['key']), array('title' => $value['SubscriptionPlan']['status'] == 1 ? 'deactivate' : 'activate'))); 
                    ?>
					</td>

                    <td>
                        <!-- Icons -->
                        <?php
                            echo ($this->Html->link($this->Html->image('admin/pencil.png', array('title' => 'Edit', 'alt' => 'Edit')), array('action' => 'edit', $value['SubscriptionPlan']['id']), array('escape' => false)));
							
                            echo ($this->Html->link($this->Html->image('admin/cross.png', array('title' => 'Delete', 'alt' => 'Delete')), array('action' => 'delete', $value['SubscriptionPlan']['id']), array('escape' => false, 'onclick'=>'javascript:return confirm_delete(this)')));
                        ?>
                    </td>
                </tr>
        <?php
    }
    ?>
        </tbody>
    </table>
    <?php
} else {
    echo ($this->element('admin_flash_info', array('message' => 'NO RESULTS FOUND')));
}
?>