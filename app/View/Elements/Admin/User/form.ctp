<script>
    $(document).ready(function() {

        //$(".Timing :input").attr('disabled', true);
        $(".day_check").live("click", function() {
            var id = $(this).attr("show_time_id");

            if ($(this).is(":checked") == true) {
                $("#" + id + " :input").removeAttr('disabled');
            } else {
                $("#" + id + " :input").attr('disabled', true);
                $("#UserAvailability" + id + "StartTime").get(0).selectedIndex = 0;
                $("#UserAvailability" + id + "EndTime").get(0).selectedIndex = 0;
            }
        });

        $("#location_anywhere").live("click", function() {
            if ($(this).is(":checked") == true) {
                $("#location_div :input").attr('disabled', true);
                $("#UserCountryId").get(0).selectedIndex = 0;
                $("#UserStateId").get(0).selectedIndex = 0;
                $("#UserCityZip").val("");
            } else {
                $("#location_div :input").removeAttr('disabled');
            }
        });

    });
</script>
<?php
echo $this->Html->script(array('jquery/jquery-ui.min'));
echo $this->Html->script(array('multiselect/jquery.multiselect'));
echo $this->Html->css(array('multiselect/jquery.multiselect'));
?>
<style>
    .ui-multiselect {
        width: 225px !important;
    }
</style>
<script type="text/javascript">
    jQuery(function() {

        if ($('#UserRoleId2').attr("checked") != "undefined" && $('#UserRoleId2').attr("jchecked") == "checked")
            $('.contractor-field').toggle();
        else
            $('.buyer-field').toggle();

        if ($('#UserAccountType2').attr("checked") != "undefined" && $('#UserAccountType2').attr("jchecked") == "checked") {
            $('.Account-Name').show();
            $('.Company-Name').hide();
        }
        else {
            $('.Company-Name').show();
            $('.Account-Name').hide();
        }
<?php
if (isset($this->request->data['User']['role_id']) && $this->request->data['User']['role_id'] == 3) { /* ?>
  $('.buyer-field').show();
  $('.contractor-field').hide();
  <?php
 */
}
?>
        var characters = 1000;
<?php $TextBox = array('0' => 'Overview', '1' => 'ServiceDescription', '2' => 'PaymentTerm'); ?>
<?php foreach ($TextBox as $index => $value) { ?>
            $("#<?php echo $value; ?>").keyup(function() {

                if ($(this).val().length > characters) {
                    $(this).val($(this).val().substr(0, characters));
                }
                var remaining = characters - $(this).val().length;
                if (remaining == 0)
                {
                    $("#count<?php echo $index; ?>").html("maximum words enter");
                }
                else
                {
                    $("#count<?php echo $index; ?>").html(remaining + " characters remaining");
                }
            });
<?php } ?>

        var warning = $(".message");

        $(".select_skill").multiselect({
            header: "Select Category First!",
            click: function(e) {
                warning.addClass("success").removeClass("error").html("Check a few boxes.");
            }
        });
    });

    function onRoleChange() {
        $('.buyer-field').toggle('fast');
        $('.contractor-field').toggle('fast');
    }


    function AccountType() {
        $('.Company-Name').toggle('fast');
        $('.Account-Name').toggle('fast');
    }

    function getstate_list(cid) {
        //alert(cid);
    }

</script>
<script>
    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            maxDate: "0",
            dateFormat: 'yy-mm-dd',
			yearRange : '1880:2014'
        });
    });
</script>

<fieldset class="column-left" style="width:75%;"> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
    <?php 
		echo ($this->Form->input('id')); ?>
    <p>
        <?php echo ($this->Form->input('first_name', array('div' => false, 'label' => 'First Name', "class" => "text-input medium-input","maxlength"=>30))); ?>

    </p>

    <p>
        <?php echo ($this->Form->input('last_name', array('div' => false, 'label' => 'Last Name', "class" => "text-input medium-input","maxlength"=>30))); ?>

    </p>
       
    <p>
        <?php echo ($this->Form->input('username', array('div' => false, 'label' => 'Username*', "class" => "text-input medium-input"))); ?>
        <br><small>Minimum length: 5 characters</small>
    </p>

    <?php if ($this->params['action'] == 'admin_add') { ?>
        <p>
            <?php echo ($this->Form->input('password2', array('autocomplete' => 'off', "type" => "password", 'div' => false, 'label' => 'Password*', "class" => "text-input medium-input"))); ?>
            <br><small>Minimum length: 6 characters</small>
        </p>

        <p>
            <?php echo ($this->Form->input('confirm_password', array('autocomplete' => 'off', "type" => "password", 'div' => false, 'label' => 'Confirm Password*', "class" => "text-input medium-input"))); ?>
            <br><small>Re-Type Password here</small>
        </p>
    <?php } ?>


    <p>
        <?php echo ($this->Form->input('email', array('div' => false, 'label' => 'Email*', "class" => "text-input medium-input"))); ?>

    </p>
	
	<p>
		<label>Alternate Email</label>
		<?php  echo ($this->Form->input('alternate_email', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
        <?php echo ($this->Form->input('phone_number', array('div' => false, 'label' => 'Phone Number', "class" => "text-input medium-input"))); ?>

    </p> 
	<p>
		<label>Alternate Phone Number</label>
		<?php  echo ($this->Form->input('alternate_phone_number', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
	 	<?php echo $this->Autosearch->autocomplete('UserFullAddress','/admin/users/location_search'); 
		echo ($this->Form->input('full_address', array('div' => false, 'label' => 'Location', "class" => "text-input medium-input"))); ?>

    </p>
	<p>
		<label>Skype Name</label>
		<?php  echo ($this->Form->input('skype', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
		<label>Website Link</label>
		<?php  echo ($this->Form->input('website', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
		<label>Facebook Profile Link</label>
		<?php  echo ($this->Form->input('facebook_link', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
		<label>Twitter Profile Link</label>
		<?php  echo ($this->Form->input('twitter_link', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
		<label>Google+ Profile Link</label>
		<?php  echo ($this->Form->input('google_plus_link', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
	<p>
		<label>Linkedin Profile Link</label>
		<?php  echo ($this->Form->input('linkedin_link', array('div'=>false, 'label'=>false, "class" => "text-input medium-input")));?> 
		
	</p>
    <p>
        <?php		
		if(!empty($imageInfo['User']['profile_image']) &&  file_exists(WWW_ROOT . USER_THUMB_DIR
	. DS .$imageInfo['User']['profile_image']) )
		{
			echo $this->General->user_show_pic($imageInfo['User']['profile_image'], 'SMALL',$imageInfo['User']['username'], $imageInfo['User']['id']);
		}
		elseif(!empty($imageInfo['User']['profile_image']))
		{
			echo "<img src='".$imageInfo['User']['profile_image']."'  width='56' alt='".$imageInfo['User']['username']."' />";
		}
		else
		{
			echo "<img src='".SITE_URL.DEFAULT_USER_IMAGE."'  width='56' alt='".$imageInfo['User']['username']."' />";
		}
		
		echo ($this->Form->input('profile_image', array('div' => false, 'label' => 'Profile Image', "class" => "text-input ", 'type' => 'file')));
        ?>
    </p>
	<p>
        <?php echo ($this->Form->input('dob', array('div' => false,'type'=>'text','label' => 'Birthday', "class" => "text-input medium-input datepicker"))); ?>

    </p>   

    <p>
     
        <?php
        $Gender = Configure::read('App.Sex');
        echo ($this->Form->input('gender', array('div' => false, 'label' => 'Gender', "class" => "text-input small-input", 'type' => 'select', 'options' => $Gender)));
        ?>

    </p>
	<p>
		<label>Education</label>
		<?php if($this->params['action'] == 'admin_edit') {
				$total = 0;
				 /* if(isset($this->request->data['User']['education'])){ */
					 $User_data_edu	= explode(";" ,$this->request->data['User']['education']);
					
					 $total = count($User_data_edu);
					?>
					<ol class="education-list" style="list-style-type:lower-numeric">
					<?php
						$i=1;
					 foreach ($User_data_edu as $key => $value) {
						if(!empty($value)){
					 ?>
						<li id=<?php echo 'edu_'.$i;?> class="listContent"><?php echo $value;?>&nbsp;&nbsp;&nbsp;
						<a href="javascript:void(0);" class="delete_education" id="<?php echo $i.'_'.$value;?>"><?php echo $this->Html->image('admin/cross.png',array('alt'=>'Delete','title'=>'Delete')); ?></a>
						</li>
						
					<?php
							}
						$i++;
						}
					?>
					</ol>
				 <?php
				/* } */
		 }?>
		
		<?php  echo $this->Form->input('education', array("class" => "text-input medium-input eductaion_text_hidden",'type'=>'hidden'));?> 
		<?php  echo ($this->Form->input('eductaion_text', array('div'=>false, 'label'=>false, "class" => "text-input medium-input eductaion_text")));?> 
		<a class="add-education addMreBtn" style="cursor:pointer;">Add</a>
		<p id="erroreducation"></p>
	</p>
	<p>
		<label>Interest</label>
		<?php if($this->params['action'] == 'admin_edit') {
				$total_int = 0;
				/* if(isset($this->request->data['User']['interests'])){ */
					 $User_data_int	= explode(";" ,$this->request->data['User']['interests']);
					 $total_int = count($User_data_int);
					
					?>
					<ol class="interest-list" style="list-style-type:lower-numeric">
					<?php
						$i=1;
					 foreach ($User_data_int as $key => $value) {
						if(!empty($value)){
					 ?>
						<li id=<?php echo 'int_'.$i;?> class="listContent"><?php echo $value;?>&nbsp;&nbsp;&nbsp;
						<a href="javascript:void(0);" class="delete_interest" id="<?php echo $i.'_'.$value;?>"><?php echo $this->Html->image('admin/cross.png',array('alt'=>'Delete','title'=>'Delete')); ?></a>
						</li>
						
					<?php
						}
						$i++;
						}
					?>
					</ol>
				 <?php
				/* } */
		 }?>
		
		<?php  echo $this->Form->input('interests', array("class" => "text-input medium-input interest_text_hidden",'type'=>'hidden'));?> 
		<?php  echo ($this->Form->input('interest_text', array('div'=>false, 'label'=>false, "class" => "text-input medium-input interest_text")));?> 
		<a class="add-interest addMreBtn" style="cursor:pointer;">Add</a>
		<p id="errorinterest"></p>
	</p>
	<p>
		<label>Work</label>
		<?php if($this->params['action'] == 'admin_edit') {
				$total = 0;
				 /* if(isset($this->request->data['User']['education'])){ */
					 $User_data_exp	= explode(";" ,$this->request->data['User']['experience']);
					
					 $total_exp = count($User_data_exp);
					?>
					<ol class="experience-list" style="list-style-type:lower-numeric">
					<?php
						$i=1;
					 foreach ($User_data_exp as $key => $value) {
						if(!empty($value)){
					 ?>
						<li id=<?php echo 'exp_'.$i;?> class="listContent"><?php echo $value;?>&nbsp;&nbsp;&nbsp;
						<a href="javascript:void(0);" class="delete_experience" id="<?php echo $i.'_'.$value;?>"><?php echo $this->Html->image('admin/cross.png',array('alt'=>'Delete','title'=>'Delete')); ?></a>
						</li>
						
					<?php
							}
						$i++;
						}
					?>
					</ol>
				 <?php
				/* } */
		 }?>
		
		<?php  echo $this->Form->input('experience', array("class" => "text-input medium-input experience_text_hidden",'type'=>'hidden'));?> 
		<?php  echo ($this->Form->input('experience_text', array('div'=>false, 'label'=>false, "class" => "text-input medium-input experience_text")));?> 
		<a class="add-experience addMreBtn" style="cursor:pointer;">Add</a>
		<p id="errorexperience"></p>
	</p>
	
	<p>
		<?php echo ($this->Form->input('is_admin', array('type'=>'checkbox','div' => false, "style" => "float: left;margin-right: 10px;margin-top:3px;"))); ?>
    </p>
	
    <p>
        <?php echo ($this->Form->input('status', array('type' => 'select', 'options' => array('0' => 'Inactive', '1' => 'Active'), 'default' => 2, "class" => "text-input medium-input", 'div' => false, 'legend' => 'Status*'))); ?>
    </p>
    <p>
        <?php echo ($this->Form->submit('Submit', array('class' => 'button', "div" => false, 'onclick' => "return customvalidation()"))); ?>
        <?php if (isset($this->request->data['User']['role_id'])) { ?>
            <?php
            echo $this->Html->link("Cancel", array('admin' => true, 'controller' => 'users', 'action' => 'index', ucfirst(Configure::read('App.Roles.' . $this->request->data['User']['role_id']))), array("class" => "button", "escape" => false));
        } else {

            echo $this->Html->link("Cancel", array('admin' => true, 'controller' => 'users', 'action' => 'index'), array("class" => "button", "escape" => false));
        }
        ?>

    </p>
	
</fieldset>
<?php
if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) {
    echo $this->Js->writeBuffer();
}
?>
<script type="text/javascript">
   unique_number = "<?php echo $total; ?>";
   jQuery(".add-education").click(function(e) {
		e.preventDefault();
		var val_hidden = jQuery("#UserEducation").val();
		var val2 = jQuery(".eductaion_text").val();
		if (val2 != "") {
			valuer = jQuery("#UserEducation").val();
			if(valuer != ""){
				valuer_arr = valuer.split(";");
				if(valuer_arr != ""){
					for(i=0;i <= valuer_arr.length;i++){
						if(valuer_arr[i] == val2){
							jQuery('#erroreducation').html("Education must be unique.");
							return false;
						}
					}
				}		
			}			
			jQuery(".education-list").append("<li>" + val2 + "</li>");
			valuern = valuer+";"+val2;
			jQuery("#UserEducation").val(valuern);
			jQuery('#erroreducation').html("");
		} else {
			jQuery('#erroreducation').html("Please add education title.");
			return false;
		}
		jQuery(".eductaion_text").attr("value", "");
		jQuery('#erroreducation').html("");
		unique_number++;
	});
	
	jQuery('.delete_education').live('click', function() {
		var id =jQuery(this).attr('id');		
		var idArr = id.split("_");		
		jQuery('#edu_'+idArr[0]).remove();		
		valsearch = jQuery('#UserEducation').val().search(";"+idArr[1]);
		if(valsearch > -1){		
			var replit = $('#UserEducation').val().replace(";"+idArr[1],"");			
		}
		else{		
			var replit = $('#UserEducation').val().replace(idArr[1]+";","");
		}
		jQuery('#UserEducation').val(replit); 
		
	});
	
   unique_number_int = "<?php echo $total_int; ?>";
   jQuery(".add-interest").click(function(e) {	
		e.preventDefault();
		var val_hidden = jQuery("#UserInterests").val();
		var val2 = jQuery(".interest_text").val();
		
		if (val2 != "") {		
			valuer = jQuery("#UserInterests").val();			
			if(valuer != ""){
				valuer_arr = valuer.split(";");
				if(valuer_arr != ""){
					for(i=0;i <= valuer_arr.length;i++){
						if(valuer_arr[i] == val2){
							jQuery('#errorinterest').html("Interest must be unique.");
							return false;
						}
					}
				}		
			}
			
			jQuery(".interest-list").append("<li>" + val2 + "</li>");
			valuern = valuer+";"+val2;
			jQuery("#UserInterests").val(valuern);
			jQuery('#errorinterest').html("");
			
		} else {		
			jQuery('#errorinterest').html("Please add interest.");
			return false;
		}
		jQuery(".interest_text").attr("value", "");
		jQuery('#errorinterest').html("");
		unique_number_int++;
	});
	
	jQuery('.delete_interest').live('click', function() {
		var id =jQuery(this).attr('id');		
		var idArr = id.split("_");		
		jQuery('#int_'+idArr[0]).remove();		
		valsearch = jQuery('#UserInterests').val().search(";"+idArr[1]);
		if(valsearch > -1){		
			var replit = $('#UserInterests').val().replace(";"+idArr[1],"");			
		}
		else{		
			var replit = $('#UserInterests').val().replace(idArr[1]+";","");
		}
		jQuery('#UserInterests').val(replit); 
		
	});
	
	unique_number_int = "<?php echo $total_exp; ?>";
   jQuery(".add-experience").click(function(e) {	
		e.preventDefault();
		var val_hidden = jQuery("#UserExperience").val();
		var val2 = jQuery(".experience_text").val();
		
		if (val2 != "") {
			valuer = jQuery("#UserExperience").val();			
			if(valuer != ""){
				valuer_arr = valuer.split(";");
				if(valuer_arr != ""){
					for(i=0;i <= valuer_arr.length;i++){
						if(valuer_arr[i] == val2){
							jQuery('#errorexperience').html("Experience must be unique.");
							return false;
						}
					}
				}		
			}
			
			jQuery(".experience-list").append("<li>" + val2 + "</li>");
			valuern = valuer+";"+val2;
			jQuery("#UserExperience").val(valuern);
			jQuery('#errorexperience').html("");
			
		} else {
			jQuery('#errorexperience').html("Please add experience.");
			return false;
		}
		jQuery(".experience_text").attr("value", "");
		jQuery('#errorexperience').html("");
		unique_number_int++;
	});
	
	jQuery('.delete_experience').live('click', function() {
		var id =jQuery(this).attr('id');		
		var idArr = id.split("_");		
		jQuery('#exp_'+idArr[0]).remove();		
		valsearch = jQuery('#UserExperience').val().search(";"+idArr[1]);
		if(valsearch > -1){		
			var replit = $('#UserExperience').val().replace(";"+idArr[1],"");			
		}
		else{		
			var replit = $('#UserExperience').val().replace(idArr[1]+";","");
		}
		jQuery('#UserExperience').val(replit); 
		
	});
</script>