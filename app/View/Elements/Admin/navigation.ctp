<ul id="main-nav">  <!-- Accordion Menu -->
    <li>
        <?php
        $class = ($this->params['controller'] == 'admins' && $this->params['action'] != 'admin_dashboard') ? 'current' : null;
        ?>
        <?php echo ($this->Html->link('Admin Management', array(), array('class' => 'nav-top-item ' . $class))); ?>
        <ul>
            <li>
                <?php
                $class = $this->params['controller'] == "admins" && ($this->params['action'] == 'admin_edit' || $this->params['action'] == 'admin_change_password') ? array('class' => 'current') : null;
                echo ($this->Html->link(__('List Admins', true), array('plugin' => null, 'controller' => 'admins', 'action' => 'index'), $class));
				if($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id')== Configure::read('App.Admin.role'))
				{
					echo ($this->Html->link(__('Add Sub Admins', true), array('plugin' => null, 'controller' => 'admins', 'action' => 'add'), $class));
				}
                ?>
            </li>
        </ul>
    </li>
	<li>
        <?php
        $class = $this->params['controller'] == 'users' ? 'current' : null;
        ?>
        <?php echo ($this->Html->link('User Management', array(), array('class' => 'nav-top-item ' . $class))); ?>
        <ul>
            <li>
				<?php
                $class = $this->params['controller'] == 'users' && $this->params['action'] == 'admin_index' && (!empty($this->params['pass']) && $this->params['pass'][0] == 'Contractor') ? array('class' => 'current') : null;
                echo ($this->Html->link(__('User List', true), array('plugin' => null, 'controller' => 'users', 'action' => 'index'), $class));
				if($this->Session->check('Auth.User.id') && $this->Session->read('Auth.User.role_id')== Configure::read('App.Admin.role'))
				{
				 //echo ($this->Html->link(__('User Add', true), array('plugin' => null, 'controller' => 'users', 'action' => 'add'), $class));
				} 
                ?>
            </li>
        </ul>
    </li>
    <li>
        <?php echo ($this->Html->link('Recent pictures', array('plugin' => null, 'controller' => 'recent_images', 'action' => 'index'), array('class' => 'nav-top-item no-submenu '))); ?>
    </li>
	<?php /* ?>
	<li> 
		<?php
			$class= $this->params['controller']=='categories'?'current':null;
		?>
		<?php echo ($this->Html->link('Category', array(), array('class'=>'nav-top-item '.$class)));?>
		<ul>
		<li><?php 
		$class= $this->params['controller']=='categories' && ($this->params['action']=='admin_index' || $this->params['action']=='admin_edit')?array('class'=>'current'):null;
		echo ($this->Html->link(__('Category', true), array('plugin' => null, 'controller'=>'categories', 'action'=>'index'),$class));?></li>
		<li><?php 
		$class= $this->params['controller']=='categories' && $this->params['action']=='admin_add'?array('class'=>'current'):null;
		echo ($this->Html->link(__('Add a new Category', true), array('plugin' => null, 'controller'=>'categories', 'action'=>'add'),$class));?></li>    
		</ul>
	</li>
	<li> 
		<?php
			$class= $this->params['controller']=='subscription_plans'?'current':null;
		?>
		<?php echo ($this->Html->link('Credit Plans', array(), array('class'=>'nav-top-item '.$class)));?>
		<ul>
		<li><?php 
		$class= $this->params['controller']=='subscription_plans' && ($this->params['action']=='admin_index' || $this->params['action']=='admin_edit')?array('class'=>'current'):null;
		echo ($this->Html->link(__('Credit Plans', true), array('plugin' => null, 'controller'=>'subscription_plans', 'action'=>'index'),$class));?></li>
		<li><?php 
		$class= $this->params['controller']=='subscription_plans' && $this->params['action']=='admin_add'?array('class'=>'current'):null;
		echo ($this->Html->link(__('Add a new Credit Plan', true), array('plugin' => null, 'controller'=>'subscription_plans', 'action'=>'add'),$class));?></li>    
		</ul>
	</li>
	<?php  */?>
	<li> 
		<?php
		
			$class = $this->params['controller']=='pages' ? 'current' : null;
		?>
		<?php echo ($this->Html->link('Static Page Management', array(), array('class'=>'nav-top-item '.$class)));?>
		<ul>
			<li><?php 
				$class= $this->params['controller']=='pages' && ($this->params['action']=='admin_index' || $this->params['action']=='admin_edit')?array('class'=>'current'):null;
				echo ($this->Html->link(__('Pages', true), array('plugin' => null, 'controller'=>'pages', 'action'=>'index')));?></li>
				<?php
			/* <li><?php 
				echo ($this->Html->link(__('Add Page', true), array('plugin' => null, 'controller'=>'pages', 'action'=>'add')));?></li> */
				?>
		</ul>
	</li>
	<li> 
		<?php
		
			$class = $this->params['controller']=='messages' ? 'current' : null;
		?>
		<?php echo ($this->Html->link('Message Management', array(), array('class'=>'nav-top-item '.$class)));?>
		<ul>
			<li><?php 
				$class= $this->params['controller']=='messages' && ($this->params['action']=='admin_index' || $this->params['action']=='admin_edit')?array('class'=>'current'):null;
				echo ($this->Html->link(__('Messages', true), array('plugin' => null, 'controller'=>'messages', 'action'=>'index')));?></li>
		</ul>
	</li>
	<?php /* ?>
	<li>
        <?php
        $class = ($this->params['controller'] == 'templates' || $this->params['controller'] == 'footers') ? 'current' : null;
        ?>
        <?php echo ($this->Html->link('Email Templates Management', array(), array('class' => 'nav-top-item ' . $class))); ?>
        <ul>
            <li>
                <?php
                $class = $this->params['controller'] == "templates" && ($this->params['action'] == 'admin_index' || $this->params['action'] == 'admin_edit') ? array('class' => 'current') : null;
					echo ($this->Html->link(__('Email Templates', true), array('plugin' => null, 'controller' => 'templates', 'action' => 'index'), $class));
                ?>
            </li>
        </ul>
    </li>
	<?php  */?>
	 <li>
        <?php echo ($this->Html->link('Settings', array('plugin' => null, 'controller' => 'settings', 'action' => 'index'), array('class' => 'nav-top-item no-submenu '))); ?>
    </li> 
    <li>
        <?php echo ($this->Html->link('Send Notification', array('plugin' => null, 'controller' => 'notifications', 'action' => 'index'), array('class' => 'nav-top-item no-submenu '))); ?>
    </li>
    <li>
        <?php echo ($this->Html->link('Logout', array('plugin' => null, 'controller' => 'admins', 'action' => 'logout'), array('class' => 'nav-top-item no-submenu '))); ?>
    </li>
</ul>
