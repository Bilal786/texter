<footer>
    <div class="container">
        <ul class="footer-nav">
            <!--li><a href="mailto:admin@girlforhire.com?subject=I have questions&body=Replied from https://GirlForHire.com">Contact </a></li-->
            <li>
			<?php echo($this->Html->link(__('Contact'), array('controller'=>'pages', 'action'=>'contact_us'), array('escape'=>false,'class'=>'')));?>
			</li>                  
            <li>
			<?php echo($this->Html->link(__('Terms of Use'), array('controller'=>'pages', 'action'=>'view','slug'=>TermofUseSlug), array('escape'=>false,'class'=>'')));?>
			</li>
            <li>
			<?php echo($this->Html->link(__('Privacy Policy'), array('controller'=>'pages', 'action'=>'view','slug'=>PrivacySlug), array('escape'=>false,'class'=>'')));?>
			</li>
        </ul>
        <p class="copyright">Copyright &copy; 2013 - 2014 Girl For Hire. All Rights Reserved.  <a id="top" class="button small" href="#">Back to top<i class="top"></i></a></p>
    </div>
</footer>