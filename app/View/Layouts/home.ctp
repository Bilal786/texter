<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]--><head>
		<?php echo $this->Html->charset(); ?>
         <meta charset="utf-8">
         <!--[if IE]>
         <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
         <![endif]-->
         <title><?php echo $title_for_layout;?> | <?php echo Configure::read('Site.title');?></title>
<?php	
	if(isset($description_for_layout)){
		echo $this->Html->meta('description', $description_for_layout);
	} 
	if(isset($keywords_for_layout)){
		echo $this->Html->meta('keywords', $keywords_for_layout);	
	}	 	 	
	echo $this->Html->meta('icon');
	  ?>	 
	 <meta name="description" content="Girl for Hire. Stunning App Showcase.">
	 <meta name="keywords" content="neue, app, ios, android, showcase, landing page, stunning">
	 <meta name="author" content="Aether Themes">
	 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	 <link rel="shortcut icon" href="images/favicon.png">
	 <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	 <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>/img/apple-touch-icon-72x72.png">
	 <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>/img/apple-touch-icon-114x114.png">
	 
	  <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	
	<?php	
	  echo $this->Html->script(array(
				'jquery-1',
				'jquery',
				'jquery-ui',
				'custom'
		  ));
	  echo $scripts_for_layout;
	 ?>

	 <!--[if lt IE 9]>
	 <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	 <![endif]-->
		 
	<?php
	echo $this->Html->css(array(
			'css',
			'reset',
			'grid',
			'style',
			'newStyle',
			'form',
			'jquery-ui',
			'my-style',
		));
	?>
         <!--[if lt IE 9]>
         <link rel="stylesheet" type="text/css" href="https://www.girlforhire.com/frontend/stylesheets/ie.css" />
         <![endif]-->
      </head>
      <body>

	<!-- Begin Mobile Navigation -->
	<div id="mobile-nav">
	<div class="container clearfix">
	<div>
		<!-- Mobile Nav Button -->
		<div class="navigationButton sixteen columns clearfix">
			<?php echo $this->Html->image('mobile-nav.png', array('alt'=>'Navigation', 'height'=>'17', 'width'=>'29'));?>
		</div>
		<!-- Mobile Nav Links -->
		<div style="display:none;" class="navigationContent sixteen columns clearfix">
			<ul>
				<li><a href="#section1">Hello</a></li>
				<li><a href="#section2">Overview</a></li>
				<li><a href="#section3">Animations</a></li>
				<li><a href="#section4">Compatibility</a></li>
				<li><a href="#section5">Detail</a></li>
				<li><a href="#section6">Gallery</a></li>
			</ul>
		</div>
	</div>
	</div>
	</div>
	<!-- End Mobile Navigation -->   
	<?php echo $this->element("header");?>
<!-- Begin Hero -->
	<section id="section1" class="home-banner">
	<div class="container">
	<div class="image">
	<?php echo $this->Html->image('banner-phone.png', array('alt'=>'banner-phone'));?>
	</div>
	<div class="banner-text">
	<h2>Seriously Talented Females In YOUR Local Area!</h2>
	<h3>
		Introducing the Girl For Hire App. A Ladies-Only place to promote 
	their business, service, or talent. Everyone can use it to discover 
	amazing people in their local area, but only females can promote. It's 
	unique I tell ya!
	</h3>
<br>

<p class="social-links">
	<a href="javascript:void(0);">
	<?php echo $this->Html->image('coming-appstore.png', array('alt'=>'coming-appstore'));?>
	</a>
	<a href="javascript:void(0);">
	<?php echo $this->Html->image('coming-gplay.png', array('alt'=>'coming-gplay'));?>	</a>
	<br>
</p>

<p><strong></strong></p>
<center><strong><a href="https://www.girlforhire.com/blog/">p.s It's FREE! Read Our Blog</a></strong><br>
<h2>LAUNCHING IN MAY 2014</h2></center><p></p>
	</div>
	</div>	
	</section>
	<!-- End Hero -->
 	<!-- Begin Overview -->
	<section id="section2">
	<div class="overview container">
		<h2 class="sixteen columns">A Visually Stunning App</h2>
		<p class="sub-heading">This robust and beautiful app makes it simple 
to promote female-owned businesses, services, and talents to thousands 
of people in your local area. Looking for females for a specific job? 
This is the App for you and the best part - it's FREE! <br>Here is how it works for females with services to offer:
</p>
		<br class="clear">

		<!-- Content Boxes -->
		<div class="content-box one-third column">	
	<?php echo $this->Html->image('icons/ad-icon.png', array('alt'=>'ad'));?>
			<h3>Post Ads</h3>
			<p>Ladies, post your business, service or talent. You are now attracting thousands of customers!  </p>
		</div>
		<div class="content-box one-third column">
	<?php echo $this->Html->image('icons/chat-icon.png', array('alt'=>'chat'));?>
			<h3>Get Chatting</h3>
			<p>Instantly chat with customers in your local area (or world wide) looking to buy what your offering!</p>
		</div>
		<div class="content-box one-third column">
	<?php echo $this->Html->image('icons/rating-icon.png', array('alt'=>'rating'));?>
			<h3>Your Hired</h3>
			<p>Perform your service, charge your own rates, and keep 100% of the cash! No commission or fees!</p>
		</div>
		<p></p>
		<h3><a href="#">Lots More About The App On Our Blog, Read Here</a></h3>
		<p></p>
		</div>
	</section>
	<!-- End Overview -->
	<!-- Begin Static -->
	<section id="section3" class="static container">
	<?php echo $this->Html->image('sidephone-small.png', array('class'=>'sidephone-small'));?>
	<?php echo $this->Html->image('sidephone-big.png', array('class'=>'sidephone-big'));?>
		<!-- Content -->
		<div class="sixteen columns offset-by-eight">
			<h2>Why Only Females?</h2>
			<p>The Girl For Hire App is a place for females only to post thier 
business, services, or talents, but don't panic! Everyone can reply to 
ads, use the wanted section, or chat to the females offering their 
services for hire. If your're a male wanting to post services, please 
download the "Guy For Hire", which is also in development.  </p>
			<p class="social-links">
				<a href="javascript:void(0);"><?php echo $this->Html->image('coming-appstore.png', array('alt'=>'coming-appstore'));?></a>
				<a href="javascript:void(0);"><?php echo $this->Html->image('coming-gplay.png', array('alt'=>'coming-gplay'));?></a>
			</p>
		</div>
	</section>
	<!-- End Static -->

	<!-- Begin Static -->
	<section id="section4">
		<div class="static container">
	<?php echo $this->Html->image('static-tablet.png', array('class'=>'static-tablet'));?>
	<?php echo $this->Html->image('static-phone.png', array('class'=>'static-phone'));?>
			<!-- Content -->
			<div class="eight columns">
				<h2>Available For iPad, iPhone &amp; Android! </h2>
				<p>The all-female staff at Girl For Hire has been developing this app
	 for a VERY long time. We are now proud to say it will be launched in <b>May 2014</b> and available across all major platforms. </p>
			<p class="social-links">
			<a href="javascript:void(0);">
	<?php echo $this->Html->image('coming-appstore.png', array('alt'=>'coming-appstore'));?>
			</a>
			<a href="javascript:void(0);">
	<?php echo $this->Html->image('coming-gplay.png', array('alt'=>'coming-gplay'));?>
			</a>
			</p>
			</div>
		</div>
	</section>
	<!-- End Static -->

	<!-- Begin Gallery -->
	<section id="section6" class="gallery container">
		<h2 class="sixteen columns">The screenshot gallery.</h2>
		<p class="sub-heading twelve columns offset-by-four">This easy shortcode is amazing. If you want to show your app just pop in the screenshots and the magic happens.</p>
		<br class="clear">
		<!-- Slider -->
		<ul class="gallery-bxslider clearfix">
			<li>
				<div class="one-third column screenshot">
	<?php echo $this->Html->image('gallery/screen1.png', array('alt'=>'screen1'));?>
				</div>
				<div class="one-third column screenshot">
	<?php echo $this->Html->image('gallery/screen2.jpg', array('alt'=>'screen2'));?>
				</div>
				<div class="one-third column screenshot">
	<?php echo $this->Html->image('gallery/screen3.jpg', array('alt'=>'screen3'));?>
				</div>
			</li>
			<li>
				<div class="one-third column screenshot">
	<?php echo $this->Html->image('gallery/screen4.png', array('alt'=>'screen4'));?>
				</div>
				<div class="one-third column screenshot">
	<?php echo $this->Html->image('gallery/screen5.png', array('alt'=>'screen5'));?>
				</div>
				<div class="one-third column screenshot">
	<?php echo $this->Html->image('gallery/screen6.png', array('alt'=>'screen6'));?>
				</div>
			</li>
		</ul>
		<!-- Pager -->
		<div id="gallery-pager"></div>
		<div class="small-border"></div>
	</section>
	<!-- End Gallery -->

	<!-- Begin Subscribe -->
	<section class="download-now">
		<div class="container">
			<div class="sixteen columns">
				<br><p>Delicately Built By Females For Females (Guys will love it too!)</p>
				<center>
					<p class="social-links">
						<a href="javascript:void(0);">
			<?php echo $this->Html->image('coming-appstore.png', array('alt'=>'coming-appstore'));?></a>
			<a href="javascript:void(0);">
			<?php echo $this->Html->image('coming-gplay.png', array('alt'=>'coming-gplay'));?>
			</a>
					</p>
				</center>
			</div>
		</div>
	</section>
	<!-- End Subscribe -->
	<!--<center>
		<p class="social-links">
			<a href="javascript:void(0);">
	<?php echo $this->Html->image('coming-appstore.png', array('alt'=>'coming-appstore'));?>
	</a>
			<a href="javascript:void(0);">
	<?php echo $this->Html->image('coming-gplay.png', array('alt'=>'coming-gplay'));?>
	</a>
		</p>
	</center>-->		
	<!-- End Subscribe -->
<!-- Begin Footer -->
<?php echo $this->element("footer");?>
<?php /* ?>
<footer>
    <div class="container">
        <ul class="footer-nav">
            <!--li><a href="mailto:admin@girlforhire.com?subject=I have questions&body=Replied from https://GirlForHire.com">Contact </a></li-->
            <li><a href="https://www.girlforhire.com/contact">Contact</a></li>                
            <li><a href="https://www.girlforhire.com/terms">Terms of Use</a></li>
            <li><a href="https://www.girlforhire.com/privacy">Privacy Policy</a></li>
        </ul>
        <p class="copyright">Copyright &copy; 2013 - 2014 Girl For Hire. All Rights Reserved.  <a id="top" class="button small" href="#">Back to top<i class="top"></i></a></p>
    </div>
</footer>
<?php  */?>
<!-- End Footer -->

<!-- Javascript -->
<script src="<?php echo SITE_URL;?>/js/prototype.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/scriptaculous.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/sizzle.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/smoothscroll.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/jquery_005.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/jquery_004.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/jquery_003.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/waypoints.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/loupe.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/notifications.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>/js/init.js" type="text/javascript" charset="utf-8"></script>
<!--container end-->                                             
</body>
</html>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-48961393-1', 'girlforhire.com');
ga('send', 'pageview');

</script>