<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]--><head>
		<?php echo $this->Html->charset(); ?>
         <meta charset="utf-8">
         <!--[if IE]>
         <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
         <![endif]-->
<title><?php echo $title_for_layout;?> | <?php echo Configure::read('Site.title');?></title>
<?php	
	if(isset($description_for_layout)){
		echo $this->Html->meta('description', $description_for_layout);
	} 
	if(isset($keywords_for_layout)){
		echo $this->Html->meta('keywords', $keywords_for_layout);	
	}	 	 	
	echo $this->Html->meta('icon');
	  ?>
	  <?php	
	  echo $this->Html->script(array(
				'jquery-1',
				'jquery',
				'jquery-ui',
				'custom',
				'custom_cat'
		  ));
	  echo $scripts_for_layout;
	 ?>
	 <meta name="description" content="Girls For hire available and wanted for work in your local area">
	 <meta name="keywords" content="girl, girls, female, available, work, hire, local, wanted, freelance ">
	 <meta name="author" content="gfhire Limited">	 
      </head>
<body>
	<?php echo $content_for_layout;?>
<br />

<!-- FOOTER SECTION -->
<table width="100%" border="0" cellspacing="3" cellpadding="3" bgcolor="#CCCCCC">
<tr>
<td width="50%" align="center">
<a href="mailto:admin@girlforhire.com?subject=I have questions&body=Replied from https://GirlForHire.com"><font face="Verdana, Geneva, sans-serif" size="2" color="#666666">Contact </font></a> |&nbsp; 
<?php echo($this->Html->link(__("<font face='Verdana, Geneva, sans-serif' size='2' color='#666666'>Terms of Use</font>"), array('controller'=>'pages', 'action'=>'view','slug'=>TermofUseSlug), array('escape'=>false,'class'=>'')));?>

 |&nbsp;
<?php echo($this->Html->link(__("<font face='Verdana, Geneva, sans-serif' size='2' color='#666666'>Privacy Policy</font>"), array('controller'=>'pages', 'action'=>'view','slug'=>PrivacySlug), array('escape'=>false,'class'=>'')));?>
 </td>

<td width="50%" align="center"><font face="Verdana, Geneva, sans-serif" size="2" color="#666666">Copyright &copy; 2013 - 2014 Girl For Hire. All Rights Reserved.</font></td></tr>
</table>
<!-- FOOTER SECTION -->
</body>
</html>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-48961393-1', 'girlforhire.com');
ga('send', 'pageview');

</script>