<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">

        <h3 style="cursor: s-resize;">Manage</h3>
        <div class="total">
            <?php echo $this->element('Admin/admin_total', array("paging_model_name" => "UserImage", "total_title" => "Recent Images")); ?>
        </div>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <div id="page-loader">
            <?php
            echo ($this->Html->image('admin/loading.gif'));
            ?>
        </div>

        <?php
        echo ($this->Form->create('User', array('name' => 'User', 'url' => array('controller' => 'admins', 'action' => 'process'))));
        echo ($this->Form->hidden('pageAction', array('id' => 'pageAction')));
		
        foreach ($tabs as $tab => $count) {
            ?>

            <div class="tab-content<?php echo ($defaultTab == $tab ? ' default-tab' : ''); ?>" id="<?php echo ($tab); ?>"> <!-- This is the target div. id must match the href of this div's tab -->

                <div id="target<?php echo ($tab); ?>"><?php
                    echo ($defaultTab == $tab ? $this->element('Admin/Admin/table_recent_images') : '');
                    ?></div>

            </div>

            <?php
        }
        echo ($this->Form->end());
        ?>

    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->
<script type="text/javascript">
    var controller = 'admins';
    jQuery(document).ready(function() {
        //init('#target<?php echo($defaultTab); ?>');
    });
</script>