


<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">

        <h3 style="cursor: s-resize;">Manage Credit Plan</h3>
         <div class="total" style="width:19%">
                 <?php //echo $this->Element('Admin/admin_paging', array("paging_model_name" => "Emoticon", "total_title" => "")); ?>
                </div>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <div id="page-loader">
            <?php
            echo ($this->Html->image('admin/loading.gif'));
            ?>
        </div>

        <?php
        echo $this->Form->create('SubscriptionPlan', array('name' => 'SubscriptionPlan', 'url' => array('action' => 'process')));
        echo $this->Form->hidden('pageAction', array('id' => 'pageAction'));

        foreach ($tabs as $tab => $count) {
            ?>

            <div class="tab-content<?php echo ($defaultTab == $tab ? ' default-tab' : ''); ?>" id="<?php echo ($tab); ?>"> <!-- This is the target div. id must match the href of this div's tab -->




                <div id="target<?php echo ($tab); ?>"><?php
                    echo ($defaultTab == $tab ? $this->element('Admin/SubscriptionPlan/table') : '');
                    ?></div>

            </div> 

            <?php
        }
        echo ($this->Form->end());
        ?>

    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->
<script type="text/javascript">
var controller = 'subscription_plans';
jQuery(document).ready(function(){
	init('#target<?php echo($defaultTab);?>');
});
</script>
