<?php $siteUrl = Configure::read('App.SiteUrl');?>
<?php 
if(!preg_match("/^[0-9+-]+$/", '5524124314')){
	
}
?>
<style>
#content{
	font-family:monospace;
}
#content td{
	border-right:1px solid #CCC;
	border-bottom:1px solid #CCC;
	word-wrap: break-word;
	padding:10px;
}
#content tr{
}

.latest{
	background-color: #FFFFD1;
}
.Desc{
	border:1px dotted #333333;
	background-color: #E6F3FF;
	padding:10px;
	margin:5px;
}
.btn a{
	border:2px solid red;
	color:#ffffff;
	text-decoration:none;
	background-color: green;
	padding:3px;
	margin:2px;
}
.indexDiv{
	margin-left:150px;
	padding:5px;
	border:1px dotted #CCC;
	width:600px;
	margin-bottom:10px;
	float:left;
}

.indexDiv a{
	font-size:14px;
	color:green;
	text-decoration:none;
}

.LasModiDate{
	font-size:14px;
	color:#333333;
	background-color:#99FF33;
	padding:3px;
	border:1px dotted blue;
}
</style>
<table border="0" cellpadding="5" cellspacing="0" width="80%" align="center">
	<tr>
		<td colspan="4"><h3>WEBSERVICE API READ DOC</h3></td>
	</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" width="80%" align="center" style="border:1px solid #CCC;" id="content">
	<tr style="background-color:green;color:#FFF;">
		<td width="5%">Title</td>
		<td width="25%">API URL</td>
		<td width="35%">INPUTS</td>
		<td width="35%">OUTPUT</td>
	</tr>
	<!-- REGISTER WEB SERVICE -->
	<?php if(!empty($data)){?>
		<tr style="background-color:#FFFFD1" id="wb_<?php echo $data['TestWebService']['id'];?>">
			<td valign="top"><strong><?php echo $data['TestWebService']['title'];?></strong>
			<br>
			<br>
			<br>
			<br>
			<strong>Last Changes:</strong>
			<div class="LasModiDate"><?php echo $data['TestWebService']['modified'];?></div><?php ?>
			</td>
			<td valign="top">
			<?php //echo $siteUrl."/web_services/".$data['TestWebService']['title'];?>
			<?php echo $data['TestWebService']['url'];?>
			<br /><br />
			<strong>REQUEST TYPE : <?php echo $data['TestWebService']['type'];?></strong>
			<br/>
			<br/>
			<strong>HEADER VALUES</strong><br/>
			<?php echo nl2br($data['TestWebService']['header']);?><br/>
			</td>
			<td valign="top">
			<div style="height: 200px;overflow-x: scroll;padding: 10px;>
			<?php echo nl2br($data['TestWebService']['request']);?>
			</div>
			<?php if(!empty($data['TestWebService']['description'])){?>
				<h3>Description:</h3>
				<div class="Desc"><?php echo nl2br($data['TestWebService']['description']);?></div>
			<?php }?>
			</td>
			<td valign="top">
			<div style="height: 200px;overflow-x: scroll;padding: 10px;><?php echo nl2br($data['TestWebService']['response']);?></div>
			<br/>
			<div class="btn">
			<?php echo $this->Html->link("View Detail", array('controller' => 'test_web_services', 'action' => 'detail', $data['TestWebService']['id']), array('title' => $data['TestWebService']['title']));
			?>
			</div>
			</td>
		</tr>
	<?php }else{?>
		<tr>
			<td colspan="4" align="center">NO DATA AVAILABLE</td>
		</tr>
	<?php }?>
	<tr>
		<td></td>
		<td colspan="3">
		<?php	echo $this->Form->create('TestWebService', 
				array('url' => array('controller' => 'test_web_services', 'action' => 'post_message'),
		));
?>
<p>
	Enter Request [In Json Formate]<br />
	<?php  echo ($this->Form->input('message', array('div'=>false, 'label'=>false, "class" => "ckeditor text-input  medium-input text-area", 'cols'=>'80', 'rows'=>'10')));?>
</p>
<?php  echo ($this->Form->submit('Submit', array('class' => 'button', "div"=>false)));?>
<?php
	echo ($this->Form->end());
?>
		</td>
	</tr>
</table>