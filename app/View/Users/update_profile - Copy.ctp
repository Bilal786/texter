<?php /* ?><div id="containerDiv">
		<!--<span class="noac">No account yet</span>-->
		<h3>Edit Profile</h3>
		<div class="inner clearfix">
			<div style="display: block;" class="" id="tab">
						<?php echo ($this->Form->create('User', array('id' => 'SearchForm', 'url' => array('admin' => true, 'controller' => 'users', 'action' => 'index'), 'onsubmit' => 'javascript: return true;'))); ?>
						<fieldset>               
							<p>
								<label>Username</label>
								<?php echo ($this->Form->input('username', array('div' => false, 'label' => false, "class" => "text-input small-input"))); ?>
							</p>
							<p>
								<label>Email</label>
								<?php echo ($this->Form->input('email', array('div' => false, 'label' => false, "class" => "text-input small-input"))); ?>
							</p>
							<!--<p>
								<label>According to login</label>
								<?php //echo ($this->Form->input('last_login_status', array('options' => Configure::read('User.Login.Search'), 'empty' => '--Select--', 'div' => false, 'label' => false, "class" => "small-input"))); ?>
							</p>-->
							<p>
								<label>Status</label>
								<?php echo ($this->Form->input('status', array('options' => Configure::read('Status'), 'empty' => 'All', 'div' => false, 'label' => false, "class" => "small-input"))); ?>
							</p>
							<p>
								<?php echo ($this->Form->submit('Search', array('class' => 'button', "div" => false))); ?>
								<?php echo $this->Html->link("Show All", array('action' => 'index'), array("class" => "button", "escape" => false)); ?>
							</p>
						</fieldset>
						<div class="clear"></div><!-- End .clear -->
						<?php
						echo ($this->Form->end());
						?>
					</div> <!-- End #tab2 -->
			</div>
    </div>
<!-- end login -->
</div>
<?php  */?>


<!-- Begin login -->
<section class="mt106">    
    <div class="AccMid">
	<?php echo $this->element("account_sidebar");?>    
    
    <div class="AccMidRight">
    <h2>Account Setting</h2>
    
    <div class="ProfPhotoAccSet">
	<?php echo $this->Html->image('profile_img1.png', array('alt'=>'img'));?>
    </div>
    
    
    <ul class="AccSetFrm">
    <li><label>Name</label><input name="" type="text" class="AccSetFrmTxtFild" value="itout"></li>
    <li style="width:300px;"><label>Gender</label><select name="" class="AccSetFrmSeFild"><option value="Female">Female</option></select></li>
    <li><label>Date Of Birth</label><input name="" type="text" class="AccSetFrmTxtFild" value="22/03/1990"></li>    
    </ul>
    
    
    
    <div class="AccSetConDeBox">
    <h3>Contact Detail</h3>
    <ul class="AccSetConDeFrm">
		<li>
			<div class="ConDeLeTxtFild">
				<input name="" type="text" class="AccSetFrmTxtFild" value="admin@girlforhire.com">
			</div>
			<div class="ConDeRiTxtFild">
				<input name="" type="text" class="AccSetFrmTxtFild" value="admin@girlforhire.com">
			</div>
		</li>
		<li>
			<div class="ConDeLeTxtFild">
				<input name="" type="text" class="AccSetFrmTxtFild" value="Phone Number">
			</div>
			<div class="ConDeRiTxtFild">
				<input name="" type="text" class="AccSetFrmTxtFild" value="Phone Number">
			</div>
		</li>
		<li>
			<div class="ConDeLeTxtFild skypeIcon">
				<input name="" type="text" class="AccSetFrmTxtFild" value="Skype Name">
				<a href="#" class="SkypIconFild">
					<?php echo $this->Html->image('skype.png', array('alt'=>'skype'));?>
				</a>
			</div>
			<div class="ConDeRiTxtFild">
				<input name="" type="text" class="AccSetFrmTxtFild" value="Website Link">
			</div>
		</li>
		<li>
			<input name="" type="text" class="SocialFild" value="Facebook Profile Link">
			<a href="#" class="SocialIconFrm">
				<?php echo $this->Html->image('facebook.png', array('alt'=>'facebook'));?>
			</a>
		</li>
		<li>
			<input name="" type="text" class="SocialFild" value="Twitter Profile Link">
			<a href="#" class="SocialIconFrm">
				<?php echo $this->Html->image('twitter.png', array('alt'=>'twitter'));?>
			</a>
		</li>
		<li>
			<input name="" type="text" class="SocialFild" value="Google + Profile Link">
			<a href="#" class="SocialIconFrm">
				<?php echo $this->Html->image('google_plash.png', array('alt'=>'google plash'));?>
			</a>
		</li>
		<li>
			<input name="" type="text" class="SocialFild" value="Linked in Profile Link">
			<a href="#" class="SocialIconFrm">
				<?php echo $this->Html->image('linked_in.png', array('alt'=>'linked_in'));?>
			</a>
		</li>
    </ul>
    </div>
    
    <div class="AccSetConDeBox">
    <h3>Education</h3>    
    <ul class="AccSetConDeFrm">
    <li><input name="" type="text" class="AccSetFrmTxtFild" value="Where you have studied?"><input name="" type="button" class="AddBtnOrange" value="ADD"></li>
    </ul>
    </div>
    
    
    
    
    <div class="AccSetConDeBox">
    <h3>Education</h3>    
		<ul class="AccSetConDeFrm">
			<li>
				<input name="" type="text" class="AccSetFrmTxtFild" value="Where you have worked?">
				<input name="" type="button" class="AddBtnOrange" value="ADD">
			</li>
		</ul>    
    </div>   
    
    <div class="AccSetConDeBox">
    <h3>Education</h3>
		<a href="#" class="DeleteIconFrm">
			<?php echo $this->Html->image('delete_icon.png', array('alt'=>'delete_icon'));?>
		</a>
		<ul class="AccSetConDeFrm">
			<li>
				<input name="" type="text" class="AccSetFrmTxtFild" value="What are your interests?">
				<input name="" type="button" class="AddBtnOrange" value="ADD">
			</li>
		</ul>
    </div>
	
    <div class="AccSetBtnRow">
		<input name="" type="button" class="OrangeBtn" value="Save">
		<input name="" type="button" class="GrayBtn" value="Cancel">
	</div>
     </div>    
    <div class="clear"></div>
    </div>
</section>