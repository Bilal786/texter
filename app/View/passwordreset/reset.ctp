<!DOCTYPE html>
<html>
<head>
  <title>Mapchat - Password reset</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!--<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">-->
  <script src='//code.jquery.com/jquery-1.11.2.min.js'></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
  <div class="container">
    <div class="content">
      <br>
      <img src="https://www.txtter.com/images/mapchat_logo.png" class="img-responsive" style="margin: 0 auto" alt="Responsive image">
      <br>
      <?php if(isset($_GET['token'])){?>
        <?php if(isset($_GET['error'])){ ?>
          <div class="alert alert-danger">
            <strong>Error!</strong> <?php
            if($_GET['error'] == 1){
              echo "Passwords don’t match!";
            }elseif($_GET['error'] == 2){
              echo "Sorry, password must be more than 5 characters!";
            }else{
              echo "There was an error!";
            }
             ?>
          </div>
        <?php } ?>
        <form method="post" action="https://<?php echo $_SERVER['HTTP_HOST']; ?>/WebServicesV2/change_password">
          <input type="hidden" name="token" value="<?php echo $_GET['token']; ?>">
          <div class="form-group">
            <input type="password" class="form-control" name="password"  placeholder="Password">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password_confirm" placeholder="Password Confirmation">
          </div>
          <button type="submit" class="btn btn-default" style="margin: 0 auto">Submit</button>
        </form>
      <?php } ?>

      <?php if(isset($_GET['success'])){ ?>
        <div class="alert alert-success">
          <strong>Well done!</strong> Password changed successfully!.
        </div>
      <?php } ?>
    </div>
  </div>

</body>
</html>
