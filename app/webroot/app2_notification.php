<?php

if (isset($_GET['t']) && $_GET['t'] != "") {
  $data = base64_decode($_GET['t']);
  $data_arr = explode('|', $data);
  $city = $data_arr[0];
  $sender_id = $data_arr[1];
  $cid = $data_arr[2];
  $image_path = $data_arr[3];

  $con1 = mysqli_connect("mysqltxtterserver.c8ezemdemcoy.us-west-2.rds.amazonaws.com", "db-txtter-octal", "7cKv[JfJvcKH#as1", "db_txtter_20170103");

  $sql = "SELECT id, device_id, full_name, username, near_map_photo_notification_status from users where city = '$city' and username != '' and id != '$sender_id'";
  $result = mysqli_query($con1, $sql);
  $row_cnt = mysqli_num_rows($result);

  date_default_timezone_set('Asia/Kolkata');
  $date = date('Y-m-d H:i:s');

  $category_array = array(
    0 => "Ask People Nearby",
    1 => "Safety",
    2 => "Lost &amp; Found",
    3 => "Recommend Food",
    4 => "Recommend Drink",
    5 => "Nightlife",
    6 => "Sports Activity",
    7 => "Gaming Event",
    8 => "Teach",
    9 => "Neighbours",
    10 => "Gigs",
    11 => "Bored",
    12 => "Carpool",
    13 => "Sports Fans",
    14 => "Roommate",
    15 => "Offer Services",
    16 => "Landscape",
    17 => "Travel Buddy",
    18 => "Charity",
    19 => "Environment",
    20 => "Free Parking Spots",
    21 => "Other",
    23 => "Lockdown",
  );

  $catName = $category_array[$cid];

  $device_token_array = array();
  if ($row_cnt > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $username = isset($row['username']) ? $row['username'] : '';
      if (empty($username)) {
        continue;
      }

      $id = isset($row['id']) ? $row['id'] : '';

      $sql3 = "INSERT INTO `user_notifications`(`type`, `fromid`, `toid`, `image_url`, `message`, `created`, `image_path`)
          VALUES ('52','$sender_id','$id','new_photo','New photo posted near you!','$date', '$image_path')";
      $result3 = mysqli_query($con1, $sql3);

      $sql4 = "update `users` set `notification_flag`='1' where `id`=$id";
      $result4 = mysqli_query($con1, $sql4);

      //send push notification
      if ($row['near_map_photo_notification_status'] == '1') {
        $device_token = isset($row['device_id']) ? $row['device_id'] : '';
        $token = trim($device_token);
        if (empty($token)) {
          continue;
        }

        if (strlen($token) >= 150 && !in_array($token, $device_token_array)) {
          send_FCM_notification($token, $catName);
        }
        $device_token_array[] = $token;
      }
    }
  }
}

function send_FCM_notification($deviceToken = null, $catName = "", $title = null, $type = 52, $objectId = 1, $iindObjectId = 1, $badge = 1)
{
  $sound = 'default';

  $url = "https://fcm.googleapis.com/fcm/send";
  $serverKey = 'AAAA1hU4Sr0:APA91bGMiE06_3FPCqhYAOs8XPozornWTbTihv1uItLLY3Ip6d6tpxWRwYJsyd4UlhfPrk-apOXe35tMc9M3eu101szvz2Ip7eElFjYEit1LjoGV59Vgr5k7lFFA_5c1pYdARPvnve8v';

  $notification = array(
    'title' => 'New photo posted near you!',
    'text' => 'New photo posted near you!',
    //    'body' => "Someone posted to $catName category. Go on notification page to see the photo!",
    'body' => "Someone posted to $catName category.",
    'badge' => $badge,
    'type' => $type,
    'objId' => $objectId,
    'iindObjId' => $iindObjectId,
    'sound' => $sound
  );
  $arrayToSend = array('to' => $deviceToken, 'notification' => $notification, 'priority' => 'high');
  $json = json_encode($arrayToSend);

  $headers = array();
  $headers[] = 'Content-Type: application/json';
  $headers[] = 'Authorization: key=' . $serverKey;

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  //Send the request
  $response = @curl_exec($ch);
  curl_close($ch);
  return $response;
}
